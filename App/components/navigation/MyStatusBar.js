import React from 'react';
import { Platform, StatusBar, View } from 'react-native';

function MyStatusBar({ backgroundColor, ...otherProps }) {
    return (
        <View style={{
            backgroundColor,
            height: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight,
        }}>
            <StatusBar
                translucent
                backgroundColor={backgroundColor}
                {...otherProps}
                // barStyle='light-content'
            />
        </View>
    );
}

export default MyStatusBar;
