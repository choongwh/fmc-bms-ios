import React from 'react';
import { Platform, SafeAreaView, StatusBar, StyleSheet, Text, View } from 'react-native';
import { Button, Icon } from 'native-base';
import PropTypes from 'prop-types';

function NavbarWithHamburger({ title, openDrawerAction, leftItem, rightItem }) {
	return (
		<View style={styles.container}>
			<StatusBar translucent barStyle='light-content' />
			<SafeAreaView>
				<View style={styles.subcontainer}>

					<View style={styles.NBsubcontainer}>
						{leftItem ? leftItem :
							<Button transparent onPress={openDrawerAction} style={styles.NBdrawerButton}>
								<Icon style={{ color: '#FFFFFF' }} name='navicon' type='FontAwesome' />
							</Button>
						}
					</View>

					<View style={styles.NBtitleContainer}>
						<Text style={styles.NBtitleText} children={title} />
					</View>

					{rightItem ? rightItem : <View style={styles.NBemptyRightItem} />}

				</View>
			</SafeAreaView>
		</View>
	);
}

NavbarWithHamburger.propTypes = {
	/**
	 * @description Title of the screen
	 */
	title: PropTypes.string.isRequired,
	/**
	 * @description The function to trigger the side drawer. Usually accessible through `this.props.navigation.openDrawer`. This is not required if the `leftItem` is provided.
	 */
	openDrawerAction: PropTypes.func,
	/**
	 * @description Override the hamburger button with another component
	 */
	leftItem: PropTypes.node,
	/**
	 * @description Additional component to display on the right side of the navigation bar.
	 */
	rightItem: PropTypes.node
};

export default NavbarWithHamburger;

const styles = StyleSheet.create({

	// Navigation Bar
	container: {
		backgroundColor: '#529ff3',
		elevation: 1,
		overflow: 'hidden',
		paddingTop: Platform.OS === 'android' ? 20 : 0,
		shadowColor: '#494949',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.75,
	},
	subcontainer: {
		flexDirection: 'row',
		height: 58,
		justifyContent: 'space-between',
		paddingLeft: 10,
		paddingRight: 10,
	},
	NBsubcontainer: {
		alignItems: 'center',
		justifyContent: 'center',
		width: 50,
	},
	NBtitleContainer: {
		alignItems: 'center',
		flex: 1,
		justifyContent: 'center',
	},
	NBtitleText: {
		color: '#FFFFFF',
		fontSize: 20,
		fontWeight: 'bold',
		textAlign: 'center',
		width: '100%'
	},
	NBdrawerButton: {
		alignItems: 'center',
		justifyContent: 'center',
		marginLeft: -5,
		paddingLeft: 0,
		paddingRight: 0,
		width: 60,
	},
	NBemptyRightItem: {
		alignItems: 'center',
		justifyContent: 'center',
		width: 50,
	},

});
