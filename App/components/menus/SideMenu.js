import PropTypes from 'prop-types';
import React, { Component } from 'react';
import styles from './SideMenu.style';
import { NavigationActions } from 'react-navigation';
import { ScrollView, Text, Image, View, AsyncStorage, TouchableOpacity, Animated, Platform } from 'react-native';
import { Button, Container, List, ListItem, Content, Icon, Footer, Left, CardItem } from "native-base";

import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import { LoginButton, AccessToken, LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';

import Ripple from 'react-native-material-ripple';
import Axios from 'axios';
import { connect } from 'react-redux';
import { updateQrCode, resetAuth } from '../../redux/actions/auth';
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';
// import Toast from 'react-native-toast-native';
import Toast from 'react-native-root-toast';
import bridge from '../../modules/bridge';

const routes = [
  { link: 'Locations', namelink: 'Locations' },
  // { link: 'Calendars', namelink: 'Calendars' },
  // { link: 'Mybooking', namelink: 'My booking' },
  { link: 'Profile', namelink: 'Profile' },
];

const stylesToast = {
  backgroundColor: "#2f4145",
  height: Platform.OS === ("ios") ? 70 : 125,
  color: "#ffffff",
  fontSize: 12,
  borderRadius: 15,
}

class SideMenu extends Component {

  constructor(props) {
    super(props);
    bridge.set({ destroySession: this.destroySession.bind(this) })
  }

  componentWillMount() {
    this._configureGoogleSignIn();
  }

  //bms:1095739769941-a84erfv04d3kvsgrvf267sg68mi2p751.apps.googleusercontent.com
  //bmsfamiie:787160340563-hmos18rocot8c6pmddvsdgsqkj4umm18.apps.googleusercontent.com

  _configureGoogleSignIn() {
    GoogleSignin.configure({
      webClientId: Url.googewebclientid,
      offlineAccess: false,
    });
  }

  async linkbtnArray(link, index) {
    this.props.navigation.navigate(link);
  };

  async destroySession() {
    AccessToken.getCurrentAccessToken().then(
      (data) => {
        if (data) {
          console.log(data.accessToken);
          var accessToken = data.accessToken;
          let logout =
            new GraphRequest(
              "me/permissions/",
              {
                accessToken: accessToken,
                httpMethod: 'DELETE'
              },
              (error, result) => {
                if (error) {
                  console.log('Error fetching data: ' + error.toString());
                } else {
                  LoginManager.logOut();
                }
              });
          new GraphRequestManager().addRequest(logout).start();
        }
      }
    );
    const isSignedIn = await GoogleSignin.isSignedIn();
    if (isSignedIn) {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
    }
    const workspace = this.props.reduxState.Auth.workspaceqr;
    await AsyncStorage.clear();
    this.props.resetAuth();
    this.props.updateQrCode(workspace.qrcode, workspace.opencamera, workspace.workspacename);
    Toast.show('Logout successful!', Toast.SHORT, Toast.CENTER, stylesToast);
    this.props.navigation.navigate('Auth');
  };

  render() {
    let self = this;
    console.log('SideMenu render');
    return (
      <Container>
        <Content>
          <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 40 }}>
            <CardItem style={{ height: 200, width: '90%' }}>
              <Image source={require('../../image/logo-bms.png')} style={{ height: '100%', width: '100%', resizeMode: 'contain' }} />
            </CardItem>
          </View>

          <List
            dataArray={routes}
            contentContainerStyle={{}}
            renderRow={(data, sectionId, rowId, highlightRow) => {
              return (
                <View>
                  <ListItem
                    button
                    style={{ padding: 0, height: 45, width: '100%' }}
                  >
                    <Ripple rippleDuration={1000} style={{ flex: 1, height: 45, justifyContent: 'center', width: '100%' }} onPress={() => this.linkbtnArray(data.link)}>
                      <Text style={{ fontSize: 20, color: '#000' }}>{data.namelink}</Text>
                    </Ripple>

                  </ListItem>

                </View>
              );
            }}
          />
        </Content>
        <Footer style={{ backgroundColor: '#fff', height: 45 }}>
          <View style={{ flex: 1 }}>
            <Button full transparent dark block style={{ paddingTop: 0, paddingBottom: 0 }}>
              <Ripple iconLeft onPress={() => this.destroySession()}
                style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', height: '100%' }}>
                <Icon name='ios-log-out' type='Ionicons' />
                <Text style={{ marginLeft: 15, fontSize: 16, color: '#000' }}>Log out</Text>
              </Ripple>
            </Button>
          </View>
        </Footer>
      </Container>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

const mapStateToProps = (state) => ({
  reduxState: state
});
export default connect(mapStateToProps, {
  updateQrCode,
  resetAuth,
})(SideMenu);