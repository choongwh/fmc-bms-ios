const _ = require('lodash');
const CONSTANTS = {
    ID: 'id', NAME: 'name', PARENT_ID: 'parentId',
    // ID: 'location_id',
    // NAME: 'location_name',
    // PARENT_ID: 'location_parent_id',
};

/**
 * @description Forcefully convert the array of locations provided by server into a JSON object.
 * @param {Array} list The array of locations
 * @returns {object} The converted data format.
 */
function convertLocationArrayToObject(list) {
    let obj = {};
    for (let i = 0; i < list.length; i++) {
        const { location_id: id, location_name: name } = list[i];
        obj[id] = { name, parentId: 'Root' };
    }
    return { 'Root': {}, ...obj };
}

/**
 * @description Search for a node in a tree.
 * @param {object} props
 * @param {object} props.tree The original object tree
 * @param {any} props.target The target key to find
 * @param {Array} props.path [Used internally for recursion] [Leave this blank] Pre-existing keys that build up the path.
 * @returns {Array} The path to the node. Inclusive of `target` as the last item.
 */
function getPathFromTree({ tree, target, path = [] }) {
    if (typeof target !== 'string') {
        throw Error(`Expected target to be a string but got ${typeof target}`);
    }
    let tIndex = Object.keys(tree);
    if (tIndex.includes(target)) {
        path.push(target);
        return path;
    } else {
        for (let i = 0; i < tIndex.length; i++) {
            if (typeof tree[tIndex[i]] === 'object') {
                const searchRef = getPathFromTree({
                    tree: tree[tIndex[i]],
                    target: target,
                    path: [...path, tIndex[i]]
                });
                if (searchRef) {
                    return searchRef;
                }
            }
        }
    }
}

/**
 * @description Arranges a collection of data into a tree.
 * (*) = Parts of code meant for optimization by removing data that may be unnecassary.
 * @param {object} collection The original data obtained via API.
 * @returns {object} The sorted tree.
 */
function getTreeFrom(collection) {
    // If old structure and new structure are the same
    // This means the nodes cannot be further sorted
    let oldStructure = {};
    // Somehow, the `Object.is()` method is undefined when not connected to a debugger
    // while (!Object.is(collection, oldStructure)) {
    while (JSON.stringify(collection) !== JSON.stringify(oldStructure)) {
        oldStructure = collection;
        const cIndex = Object.keys(collection);
        for (let i = 0; i < cIndex.length; i++) {

            const CURRENT_ID = cIndex[i];
            delete collection[CURRENT_ID][CONSTANTS.ID]; // (*) Extraneous since it's key is the ID

            // Check if current item belongs to a parent
            const PARENT_ID = collection[CURRENT_ID][CONSTANTS.PARENT_ID];
            if (PARENT_ID) {

                // (*) Extraneous, since it will be nested already
                // Deletion occurs first so that it will not be included
                // when being appended to it's parent node
                delete collection[CURRENT_ID][CONSTANTS.PARENT_ID];

                let pathToInsert = [];
                if (typeof collection[PARENT_ID] !== 'undefined') {
                    // Parent is in first level
                    pathToInsert = [PARENT_ID, 'children', CURRENT_ID];
                } else {
                    // Parent has already been nested, a search is necessary
                    const PARENT_PATH = getPathFromTree({ tree: collection, target: PARENT_ID });
                    pathToInsert = [...PARENT_PATH, 'children', CURRENT_ID];
                }

                _.set(collection, pathToInsert, collection[CURRENT_ID]);

                // Since it has already been copied to its `parentId`-based node
                delete collection[CURRENT_ID];
            }

        }
    }
    return collection;
}

/**
 * @description Flattens a location tree into an array.
 * @param {object} props
 * @param {object} props.tree The sorted tree from the raw collection.
 * @param {Array} props.arr [Used internally for recursion] [Leave this blank] The cumulative list of all end nodes.
 * @param {Array} props.path [Used internally for recursion] [Leave this blank] Path of each end node.
 * @param {string} props.section Section that the item in list belong to.
 * @returns {Array} The flattened array.
 */
function flatten({ tree, arr = [], path = [], section }) {
    const tIndex = Object.keys(tree);
    for (let i = 0; i < tIndex.length; i++) {
        // Parameter provided by server change from `name` to `location_name`???
        // const { children, location_name: name } = tree[tIndex[i]];
        const { children, name } = tree[tIndex[i]];
        path.push(name) // Upon entering new node, the name is pushed
        const hasChildren = typeof children === 'object';
        if (hasChildren) {
            flatten({ tree: children, arr, path, section });
        } else {
            arr.push({
                section,
                id: tIndex[i],
                name: Array.from(path).pop(),
                // deep copy is required because of the pop below
                path: (() => {
                    let copiedArray = Array.from(path);
                    copiedArray.pop();
                    return copiedArray;
                })()
            });
        }
        path.pop();
    }
    return arr;
}

/**
 * @description Converts a location tree into an array that can be understood by React Native's SectionList for rendering.
 * @param {object} sortedTree The sorted tree derived from the raw collection.
 * @returns {Array} The converted array.
 * @example <SectionList sections={compileSectionsFromTree(sortedTree)}/>
 */
function compileSectionsFromTree(sortedTree) {
    let sections = [], nodesWithNoChildren = [];
    const tIndex = Object.keys(sortedTree);
    for (let i = 0; i < tIndex.length; i++) {
        const { name, children = {} } = sortedTree[tIndex[i]];
        const flattenedChildren = flatten({ tree: children, section: name });
        if (flattenedChildren.length > 0) {
            sections.push({ title: name, data: flattenedChildren });
        } else {
            nodesWithNoChildren.push({ id: tIndex[i], name });
        }
    }
    if (nodesWithNoChildren.length > 0) {
        sections.unshift({ title: '', data: nodesWithNoChildren });
    }
    return sections;
}

/**
 * @description Converts a collection of locations into an array that can be understood by React Native's SectionList for rendering.
 * @param {object} collection The original data obtained via API.
 * @returns {Array} The converted array.
 * @example <SectionList sections={compileSectionsFromTree(collection)}/>
 */
function compileSectionsFromCollection(collection) {
    return compileSectionsFromTree(getTreeFrom(collection));
}

module.exports = {
    convertLocationArrayToObject,
    getPathFromTree,
    getTreeFrom,
    flatten,
    compileSectionsFromTree,
    compileSectionsFromCollection,
};
