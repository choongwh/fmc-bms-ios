import React from 'react';
import PropTypes from 'prop-types';
import { SectionList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Svg, Path } from 'react-native-svg';

/**
 * @returns {Text} A <Text/> that has been 'highlighted'
 */
function TextInHighlight({ children }) {
    return (<Text children={children} style={styles.matchedText} />);
}

/**
 * @description Highlights text in children that matches the searchFilter.
 * @param {object} props
 * @param {string} props.children The name of path of location
 * @param {string} props.searchFilter The search text
 * @returns {Text} Partially highlighted text, only matching words are highlighted.
 */
function Matcher({ children, searchFilter }) {
    searchFilter = searchFilter.toLowerCase()
    if (children.toLowerCase() === searchFilter) {
        return (<TextInHighlight children={children} />);
    } else {
        const splitted = children.split(' ');
        return (
            <Text>
                {splitted.map((value, index) => {
                    const EXACT_MATCH = searchFilter.includes(value.toLowerCase());
                    return (
                        <Text key={index}>
                            {EXACT_MATCH ? <TextInHighlight children={value} /> : value}
                            {index < (splitted.length - 1) ? ' ' : null}
                        </Text>
                    );
                })}
            </Text>
        )
    }
}

/**
 * @description Concatenates path array into one single <Text/>.
 * @returns {Text}
 */
function BreadCrumb({ path = [], searchFilter = '' }) {
    let renderArray = [];
    if (path.length > 0) {
        for (let i = 0; i < path.length; i++) {
            renderArray.push(
                <Matcher key={i} children={path[i]} searchFilter={searchFilter} />
            );
            if (i < path.length - 1) {
                renderArray.push(' > ');
            }
        }
    } else {
        renderArray = "(No description available)";
    }
    return (
        <Text children={renderArray}
            style={{
                color: path.length > 0 ? '#666666' : '#AAAAAA'
            }}
        />
    );
}

function SectionHeader({ section: { title } }) {
    if (title.length > 0) {
        return (
            <View style={styles.sectionHeaderContainer}>
                <Text children={title} style={styles.sectionHeaderText} />
            </View>
        );
    } else {
        return null;
    }
}

function RenderItem({ item, onPress, searchFilter }) {
    const { name, path } = item;
    const ARROW_SIZE = 24;
    return (
        <TouchableOpacity style={styles.renderItemContainer} onPress={() => { onPress(item); }}>
            <View style={{ flex: 1 }}>
                <Text style={styles.renderItemMain}>
                    <Matcher children={name} searchFilter={searchFilter} />
                </Text>
                <BreadCrumb path={path} searchFilter={searchFilter} />
            </View>
            <Svg width={ARROW_SIZE} height={ARROW_SIZE} viewBox='0 0 24 24'>
                <Path fill="none" d="M0 0h24v24H0V0z" />
                <Path fill="#888888" d="M9.29 6.71c-.39.39-.39 1.02 0 1.41L13.17 12l-3.88 3.88c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0l4.59-4.59c.39-.39.39-1.02 0-1.41L10.7 6.7c-.38-.38-1.02-.38-1.41.01z" />
            </Svg>
        </TouchableOpacity>
    );
}

function ItemSeparator() {
    return (
        <View style={styles.itemSeparatorContainer}>
            <View style={styles.itemSeparatorStart} />
            <View style={styles.itemSeparatorEnd} />
        </View>
    );
}

function LocationList({ sections, onPress, searchFilter, refreshAction, hasError }) {
    const passDownSearchFilter = searchFilter.replace(/\s+$/, '');
    if (sections.length > 0 && !hasError) {
        return (
            <SectionList
                onRefresh={refreshAction}
                refreshing={sections.length <= 0 && !hasError}
                sections={sections}
                keyExtractor={({ id }) => id}
                renderSectionHeader={SectionHeader}
                stickySectionHeadersEnabled
                ItemSeparatorComponent={ItemSeparator}
                renderItem={props => (
                    <RenderItem {...props}
                        searchFilter={passDownSearchFilter}
                        onPress={onPress}
                    />
                )}
            />
        );
    } else if (searchFilter.length > 0) {
        return (
            <View style={{
                ...styles.fallbackContainer,
                justifyContent: 'flex-start',
                paddingTop: 30,
            }}>
                <Text
                    children='No results'
                    style={[styles.fallbackHint, styles.fallbackHintTitle]}
                />
            </View >
        );
    } else {
        return (
            <TouchableOpacity onPress={refreshAction} style={styles.fallbackContainer}>
                <Text style={styles.fallbackHint}>
                    <Text
                        children={hasError ? "There was a problem while loading the list" : "List is empty"}
                        style={styles.fallbackHintTitle}
                    />
                    {'\n\n'}
                    <Text children="Tap to retry" style={styles.fallbackHintDesc} />
                </Text>
            </TouchableOpacity>
        );
    }
}

LocationList.propTypes = {
    /**
     * @description The sections compiled from the collection of locations
     */
    sections: PropTypes.array.isRequired,
    /**
     * @description Function to be invoked when the list items are pressed.
     */
    onPress: PropTypes.func.isRequired,
    /**
     * @description This allows the text in list items to be bolded if it matches the search value.
     */
    searchFilter: PropTypes.string,
    /**
     * @description Function to be invoked when a refresh is requested (through fallback UI)
     */
    refreshAction: PropTypes.func,
    /**
     * @description Is there error while fetching the locations? If `true`, this will direct the component to render a fallback UI instead of an empty list.
     */
    hasError: PropTypes.bool,
}

export default LocationList;

const styles = StyleSheet.create({

    // Fallback UI
    fallbackContainer: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
    },
    fallbackHint: {
        color: '#AAAAAA',
        textAlign: 'center',
    },
    fallbackHintTitle: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    fallbackHintDesc: {
        fontSize: 18,
    },

    // Header
    sectionHeaderContainer: {
        backgroundColor: '#EEEEEE',
        paddingVertical: 5,
        paddingHorizontal: 10,
    },
    sectionHeaderText: {
        color: '#4A4A4A',
        fontSize: 18,
        fontWeight: 'bold',
    },

    // Separator
    itemSeparatorContainer: {
        flexDirection: 'row',
        height: 1,
    },
    itemSeparatorStart: {
        width: 10
    },
    itemSeparatorEnd: {
        backgroundColor: '#CCCCCC',
        flex: 1,
    },

    // Render Item
    renderItemContainer: {
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        flexDirection: 'row',
        padding: 10,
    },
    renderItemMain: {
        color: '#000000',
        fontSize: 16,
    },
    matchedText: {
        backgroundColor: '#dae5f2',
        fontWeight: 'bold',
        color: '#000000',
    },

});
