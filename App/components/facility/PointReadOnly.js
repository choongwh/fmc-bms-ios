import React from 'react';
import { Text, View } from 'react-native';
import TitleIndicatorWrapper from './TitleIndicatorWrapper';
// import areAlike from '../../modules/are-alike';
import pointStyles from './styles';

export default class PointReadOnly extends React.Component {

  // /**
  //  * @description Suffix will be appended to end of pointName if it matches any of the keywords below
  //  */
  // suffixToAddByMatch = [
  //   { keyword: 'temp', suffix: '(ºC)' },
  //   { keyword: 'humid', suffix: '(%)' },
  // ]

  render() {
    const { value, pointName, lookupType } = this.props
    let valueToDisplay = value, nameToDisplay = pointName;

    // // Only add bracketed unit if pointName does not already have one
    // if (!pointName.match(/.+\(.+\)$/g)) {
    //   for (let i = 0; i < this.suffixToAddByMatch.length; i++) {
    //     const { keyword, suffix } = this.suffixToAddByMatch[i];
    //     if (areAlike([keyword, pointName])) {
    //       nameToDisplay = `${pointName} ${suffix}`;
    //     }
    //   }
    // }

    if (lookupType === 'LK.PointBinaryStatus') {
      valueToDisplay = value > 0 ? 'On' : 'Off';
    }

    return (
      <View style={pointStyles.container}>

        <View style={pointStyles.titleView}>
          <TitleIndicatorWrapper>
            <Text style={pointStyles.titleText} children={nameToDisplay} />
          </TitleIndicatorWrapper>
        </View>

        <View style={pointStyles.controlContainer}>
          <Text style={{ color: '#000000', fontSize: 14, textAlign: 'center' }}>
            <Text style={{ fontWeight: 'bold', }} children='Value: ' />
            <Text children={valueToDisplay} />
          </Text>
        </View>

      </View>
    );
  }

}
