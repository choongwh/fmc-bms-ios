import React from 'react';
import { ActivityIndicator, View } from 'react-native';

const Gap = () => <View style={{ width: 10 }} />;

function TitleIndicatorWrapper({ children, loading }) {
    return (
        <>
            <ActivityIndicator size='small' color={loading ? '#000000' : 'transparent'} />
            <Gap />
            {children}
            <Gap />
            <ActivityIndicator color='transparent' />
        </>
    );
}

export default TitleIndicatorWrapper;
