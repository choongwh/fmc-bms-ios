import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import TitleIndicatorWrapper from './TitleIndicatorWrapper';
import Slider from 'react-native-slider';
import Memoizer from '../../modules/memoizer';
import { debounce } from 'lodash';

export default class PointAnalogRange extends React.Component {

  constructor(props) {
    super(props);
    const { minValue, maxValue, step, value } = getStatsFromProps(props)
    this.state = {
      loading: false,
      nextValue: this.getSafeValue(value),
      previewing: false,
    }
    this.step = step;
    this.minValue = minValue;
    this.maxValue = maxValue;
  }

  static getDerivedStateFromProps = (newProps, currentState) => {
    const { loading } = currentState;
    if (loading) {
      const { value } = getStatsFromProps(newProps)
      return {
        loading: (
          // Loading is considered complete only when new value from prop is same as `nextValue`
          value !== currentState.nextValue &&
          // Loading cannot be true while still sliding(previewing)
          currentState.previewing === false
        )
      }
    } else {
      return null;
    }
  }

  getSafeValue = (value) => Math.max(this.minValue, Math.min(value, this.maxValue))

  onSliding = debounce((value) => {
    this.setState({
      nextValue: this.getSafeValue(value),
      previewing: true,
    })
  }, 50)

  onSlidingComplete = (value) => {
    this.setState({
      loading: true,
      nextValue: this.getSafeValue(value),
      previewing: false
    });
    const { fnName, deviceId, objectId, onValueChanged } = this.props;
    if (fnName) {
      const data = { device_id: deviceId, objectId: objectId, fnName, fnValue: value };
      onValueChanged(data);
    } else {
      this.setState({ loading: false });
    }
  }

  render() {
    const { value } = this.props
    console.log(`this.props.value = ${value}`)
    const { loading, nextValue, previewing } = this.state
    const valueToUse = loading || previewing ? nextValue : value;
    const safeValueToUse = this.getSafeValue(valueToUse);
    return (
      <View style={styles.container}>

        <View style={styles.titleView}>
          <TitleIndicatorWrapper loading={loading}>
            <Text style={styles.titleText}>
              {this.props.pointName}: {valueToUse}
            </Text>
          </TitleIndicatorWrapper>
        </View>

        <View style={styles.controlContainer}>
          <Slider
            value={safeValueToUse} disabled={loading} step={this.step}
            minimumValue={this.minValue} maximumValue={this.maxValue}
            style={{ opacity: loading ? 0.5 : 1, width: '100%' }}
            onValueChange={this.onSliding}
            onSlidingComplete={this.onSlidingComplete}
          />
        </View>

      </View>
    );
  }
}

const statMemoizer = new Memoizer();
function getStatsFromProps(props) {
  return statMemoizer.do(() => {
    let maxValue = 0, minValue = 0, step = 0, decimal = 0;

    for (let item of props.data) {
      if (item.display == 'SETTING MAX LIMIT') {
        maxValue = parseFloat(item.value);
        if (isNaN(maxValue)) { maxValue = 0; }
      } else if (item.display == 'SETTING MIN LIMIT') {
        minValue = parseFloat(item.value);
        if (isNaN(minValue)) { minValue = 0; }
      } else if (item.display == 'INCREMENT') {
        step = parseFloat(item.value);
        if (isNaN(step)) { step = 0; }
      } else if (item.display == 'DECIMAL POINT') {
        decimal = parseFloat(item.value)
        if (isNaN(decimal)) { decimal = 0; }
      }
    }

    if (maxValue < minValue) { maxValue = minValue; }

    let value = parseFloat(parseFloat(props.value).toFixed(decimal));
    if (value === null || isNaN(value) || value < minValue) {
      value = minValue;
    }

    if (value > maxValue) { value = maxValue; }

    return { maxValue, minValue, step, value };
  }, [props])
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#E6F1FD',
    borderColor: '#96C5F8',
    borderRadius: 5,
    borderWidth: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginBottom: 4,
    width: '90%',
  },
  titleView: {
    alignItems: 'center',
    backgroundColor: '#96C5F8',
    flexDirection: 'row',
    height: 28,
    justifyContent: 'center',
    width: '100%',
  },
  titleText: {
    color: 'black',
    fontWeight: 'bold',
  },
  controlContainer: {
    alignItems: 'center',
    height: 40,
    justifyContent: 'center',
    marginTop: 4,
    marginBottom: 2,
    width: '70%',
  },
})
