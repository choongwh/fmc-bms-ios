import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Collapse, CollapseHeader, CollapseBody } from 'accordion-collapse-react-native';
import { Svg, Path } from 'react-native-svg';
import PropTypes from 'prop-types';

class CollapseComponent extends React.Component {

	static propTypes = {
		objectsName: PropTypes.string.isRequired,
		arr: PropTypes.array.isRequired,
	}

	state = { expanded: false }

	render() {
		const { objectsName, arr } = this.props;
		const ICON_SIZE = 36, ICON_FILL = '#999999';
		const { expanded } = this.state;
		return (
			<View style={styles.container}>
				<Collapse onToggle={() => { this.setState({ expanded: !expanded }) }}>
					<CollapseHeader>
						<View style={styles.collapseHeaderView}>
							<Svg width={ICON_SIZE} height={ICON_SIZE} viewBox="0 0 24 24">
								<Path fill="none" d="M0 0h24v24H0V0z" />
								<Path
									fill={ICON_FILL}
									d={expanded ?
										'M8.71 11.71l2.59 2.59c.39.39 1.02.39 1.41 0l2.59-2.59c.63-.63.18-1.71-.71-1.71H9.41c-.89 0-1.33 1.08-.7 1.71z'
										:
										'M11.71 15.29l2.59-2.59c.39-.39.39-1.02 0-1.41L11.71 8.7c-.63-.62-1.71-.18-1.71.71v5.17c0 .9 1.08 1.34 1.71.71z'}
								/>
							</Svg>
							<Text style={styles.collapseHeaderText}>{objectsName}</Text>
						</View>
					</CollapseHeader>
					<CollapseBody children={arr} />
				</Collapse>
			</View>
		);
	}

}

export default CollapseComponent;

const styles = StyleSheet.create({
	container: {
		flexDirection: 'column',
	},
	collapseHeaderView: {
		alignItems: 'center',
		backgroundColor: '#f0f0f0',
		flexDirection: 'row'
	},
	collapseHeaderText: {
		color: 'black',
		flex: 1,
		fontSize: 18,
		padding: 10
	},
});
