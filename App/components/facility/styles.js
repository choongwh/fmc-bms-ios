import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: '#E6F1FD',
        borderColor: '#96C5F8',
        borderRadius: 5,
        borderWidth: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        marginBottom: 4,
        width: '90%',
    },
    titleView: {
        alignItems: 'center',
        backgroundColor: '#96C5F8',
        flexDirection: 'row',
        height: 28,
        justifyContent: 'center',
        width: '100%',
    },
    titleText: {
        color: 'black',
        fontWeight: 'bold',
    },
    controlContainer: {
        alignItems: 'center',
        height: 40,
        justifyContent: 'center',
        marginTop: 4,
        marginBottom: 2,
        width: '70%',
    },
});
