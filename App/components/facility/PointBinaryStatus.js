import React from 'react';
import { Alert, Text, View } from 'react-native';
import { RadioGroup } from 'react-native-btr';
import TitleIndicatorWrapper from './TitleIndicatorWrapper';
import pointStyles from './styles';

export default class PointBinaryStatus extends React.Component {

  constructor(props) {
    super(props);

    var radioState = this.props.data.map(item => {
      return { label: item.display, value: item.value, flexDirection: 'row', checked: false }
    });

    this.state = {
      data: radioState,
      value: this.props.value,
      loading: true,
    };

    this.props.onRef(this);
  }

  componentDidMount() {
    this.setState({ loading: false });
  }

  static getDerivedStateFromProps = (newProps, currentState) => {
    if (newProps.value !== currentState.value) {
      return {
        loading: false
      };
    } else {
      return null;
    }
  }

  onPress = data => {
    // If multiple setStates are called, React will group the updates together
    // Set timeout is used to overcome that delay
    // So that the loader is shown immediately,
    // then only wait for changes in the control's value
    setTimeout(() => { this.setState({ loading: true }) });

    const selectedButton = data.find(e => e.checked == true);
    if (selectedButton != null) {
      this.setState({
        value: selectedButton.value,
      });
    }

    // this.setState(data);
    if (this.props.fnName) {
      const fnName = this.props.fnName;
      const fnData = { device_id: this.props.deviceId, objectId: this.props.objectId, fnName: fnName, fnValue: selectedButton.value };
      this.props.onValueChanged(fnData);
    } else {
      this.setState({ loading: false });
    }
  };

  setOn = () => {
    // this.setState({ value: 1 });
    if (this.props.fnName) {
      const fnName = this.props.fnName;
      const fnData = { device_id: this.props.deviceId, objectId: this.props.objectId, fnName: fnName, fnValue: 1 };
      this.props.onValueChanged(fnData);
    } else {
      this.setState({ loading: false });
      Alert.alert('Unable to turn on the device');
    }
  }

  setOff = () => {
    // this.setState({ value: 0 });
    if (this.props.fnName) {
      const fnName = this.props.fnName;
      const fnData = { device_id: this.props.deviceId, objectId: this.props.objectId, fnName: fnName, fnValue: 0 };
      this.props.onValueChanged(fnData);
    } else {
      this.setState({ loading: false });
      Alert.alert('Unable to turn off the device');
    }
  }

  render() {
    const radioButtons = this.state.data.map(button => {
      button.checked = (this.state.value == button.value);
      button.disabled = this.state.loading
      return button;
    });

    const value = this.props.value;
    if (value != this.state.value) {
      this.setState({ value: value });
    }

    return (
      <View style={pointStyles.container}>

        <View style={pointStyles.titleView}>
          <TitleIndicatorWrapper loading={this.state.loading}>
            <Text style={pointStyles.titleText} children={this.props.pointName} />
          </TitleIndicatorWrapper>
        </View>

        <View style={pointStyles.controlContainer}>
          <RadioGroup style={{ flexDirection: 'row' }}
            labelStyle={{ color: 'black' }}
            radioButtons={radioButtons}
            onPress={data => { this.onPress(data) }}
          />
        </View>

      </View>
    );
  }
}
