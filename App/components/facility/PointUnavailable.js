import React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import TitleIndicatorWrapper from './TitleIndicatorWrapper';
import pointStyles from './styles';

function PointUnavailable({ pointName = 'Device' }) {
	return (
		<View style={pointStyles.container}>

			<View style={pointStyles.titleView}>
				<TitleIndicatorWrapper>
					<Text children={`${pointName} (Unavailable)`} style={pointStyles.titleText} />
				</TitleIndicatorWrapper>
			</View>

			<View style={[pointStyles.controlContainer, { width: '80%' }]}>
				<Text
				children="The device might be offline or experiencing some problems now"
				style={{ color: '#4A4A4A', fontSize: 14, fontweight: 'bold', textAlign: 'center' }}
			/>
		</View>

		</View >
	);
}

PointUnavailable.propTypes = {
	pointName: PropTypes.string
};

export default PointUnavailable;
