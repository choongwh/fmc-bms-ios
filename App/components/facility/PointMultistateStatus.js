import React from 'react';
import { Text, View } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import TitleIndicatorWrapper from './TitleIndicatorWrapper';
import pointStyles from './styles';

export default class PointMultistateStatus extends React.Component {

  constructor(props) {
    super(props);

    var selectedIndex = this.props.data.map(item => item.value.toString()).indexOf(this.props.value.toString())

    this.state = {
      data: this.props.data,
      selectedIndex: selectedIndex,
      loading: true,
      value: 0,
    };
  }

  static getDerivedStateFromProps = (newProps, currentState) => {
    if (newProps.value === parseInt(currentState.value, 10)) {
      return {
        loading: false
      };
    } else {
      return null;
    }
  }

  componentDidMount() {
    this.setState({ loading: false })
  }

  onValueChanged = (displayValue, index) => {
    let value = this.state.data[index].value;

    setTimeout(() => {
      this.setState({
        loading: true,
        value: value
      })
    });

    this.setState({ selectedIndex: index });

    if (this.props.fnName) {
      const fnName = this.props.fnName;
      const data = { device_id: this.props.deviceId, objectId: this.props.objectId, fnName: fnName, fnValue: value };
      this.props.onValueChanged(data);
    } else {
      this.setState({ loading: false });
    }
  };

  render() {
    const newSelectedIndex = this.props.data.map(item => item.value.toString()).indexOf(this.props.value.toString())

    if (newSelectedIndex != this.state.selectedIndex) {
      this.setState({ selectedIndex: newSelectedIndex });
    }

    let value = '';
    if (this.state.selectedIndex >= 0) {
      value = this.state.data[this.state.selectedIndex].display;
    }

    return (
      <View style={pointStyles.container}>

        <View style={pointStyles.titleView}>
          <TitleIndicatorWrapper loading={this.state.loading}>
            <Text style={pointStyles.titleText}>
              {this.props.pointName}
            </Text>
          </TitleIndicatorWrapper>
        </View>

        <View style={{ ...pointStyles.controlContainer, alignItems: 'stretch' }}>
          <Dropdown
            disabled={this.state.loading}
            label=' '
            value={value}
            style={{
              opacity: this.state.loading ? 0.5 : 1,
              textAlign: 'center'
            }}
            data={this.state.data}
            dropdownOffset={{ top: 0, left: 0 }}
            rippleOpacity={0.0}
            valueExtractor={(item, index) => item.display}
            onChangeText={(value, index, data) => this.onValueChanged(value, index)} />
        </View>

      </View>
    );
  }
}
