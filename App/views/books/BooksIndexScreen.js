import React from 'react';
import { StyleSheet, ScrollView, TouchableOpacity, FlatList, Alert, View, Text, Platform, StatusBar, NetInfo } from 'react-native';
import { Button, Fab, Icon, Content, List, ListItem, Container, Header, Left,
  Right, Body, Title, CardItem } from 'native-base';
import { connect } from 'react-redux';
import Axios from 'axios';
//const timer = require('react-native-timer');
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';
import { updateAccessToken, updateDetailEvent} from '../../redux/actions/auth';
import AppointmentCard from '../../components/appointments/Card';
import {Agenda, Calendar} from 'react-native-calendars';
import { Divider } from 'react-native-elements';
import Ripple from 'react-native-material-ripple';
import { MenuContext, Menu, MenuOptions, MenuOption, MenuTrigger, MenuProvider, renderers } from 'react-native-popup-menu';
import moment from 'moment';
import _ from 'lodash';
import Toast from 'react-native-root-toast';

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[statusbar.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

const statusbar = {
  container: {
    height: 20,
    width: '100%'
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#000',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#000'
  },
}

class BooksIndexScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: {},
      ListEventArray: [],
      EventListAll: {},
      eventListByDay: [],
      eventListByMonth: [],
      markedDates: {[moment(new Date()).format('YYYY-MM-DD')]: {disabled: true}},
      renderT: 1,
      chooseDate: moment(new Date()).format('YYYY-MM-DD'),
      day: moment(new Date()).format('DD'),
      month: moment(new Date()).format('MM'),
      year: moment(new Date()).format('YYYY'),
      connection_Status : "Online",
      counter : 0,
    };
    this.changeState = this.changeState.bind(this);
    this.loadEventByDay = this.loadEventByDay.bind(this);
    this.loadEventByMonth = this.loadEventByMonth.bind(this);
    this._interval = null;
  }

  changeState(number){
    this.setState({
        renderT: number
    });
  }

  componentWillMount = () => {
   var self = this;
  
  this._interval = setInterval(() => {
    if(self.state.renderT == 1){
      self.loadPublicEvent();
    }else{
       self.loadPublicEvent();
       self.loadCreatedEvent();
    }
  }, 5000);
  }
  componentDidMount() {

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"});
        this.loadPublicEvent();
        
      }
      else
      {
       
        this.setState({connection_Status : "Offline"})
          //Toast.show('Connection to host lost.');
      }

    });
  }
  componentWillUnmount() {
     clearInterval(this._interval);
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
  }
  /***
  Start Status connection
  ***/

    _handleConnectivityChange = (isConnected) => {



    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"});
        Toast.show('CONNECTION TO HOST RESUME.');
        this.loadPublicEvent();
      }
      else
      {
        Toast.show('Connection to host lost.');
        this.setState({connection_Status : "Offline"})
      }
    };

    connectionLost(self){
        self.setState({ isLoading: false })
        self.setState({counter: self.state.counter+1 });
       if(self.state.counter>=3){
            Toast.show('CONNECTION LOST');
       }
        
     }

     noInternet(self){
       if(self.state.connection_Status=="Online"){
         return true
       }else{
         Toast.show('Connection to host lost.');
         return false;
       }
     }


  /***
 End Status connection
  ***/


  refreshComponent() {
    this.loadPublicEvent();
  }

  async loadPublicEvent(){
    var self = this;
    var credentials = {'mtd': 4, 'token': this.props.reduxState.Auth.token["access-token"]};
    if(!self.noInternet(self)){
      self.setState({
        isLoading: false
      });
        self.setState({
        disableSubmit: false,
        isLoading: false,
          eventListByDay:[],
         EventListAll:[],
         markedDates: {} 
      });
        self.loadEventByDay({dateString : self.state.chooseDate});
      return;
    }
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      self.setState({counter: 0 });
      console.log("loadPublicEvent",response.data);
      if(response.data.status == 3){
        Toast.show('Token expired.');
       self.props.navigation.navigate('Auth');
       return;   
      }
      if(response.data.status == 1){
        self.setState({
          ListEventArray: response.data.data
        });
        self.loadPrivateEvent();
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
       self.setState({
        disableSubmit: false,
        isLoading: false,
          eventListByDay:[],
         EventListAll:[],
         markedDates: {} 
      });
      self.loadEventByDay({dateString : self.state.chooseDate});
      self.connectionLost(self);
      console.log(error);
    });
  }

  async loadPrivateEvent(){
    var self = this;
    if(!self.noInternet(self)){
      self.setState({
        disableSubmit: false,
        isLoading: false,
          eventListByDay:[],
         EventListAll:[]
      });

     self.loadEventByDay({dateString : self.state.chooseDate});
      return;
    }
    var credentials = {'mtd': 5, 'token': this.props.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      self.setState({counter: 0 });
       if(response.data.status == 3){
        Toast.show('Token expired.');
       self.props.navigation.navigate('Auth');
       return;   
      }
      
      if(response.data.status == 1){
        var dataUnion = _.union(self.state.ListEventArray, response.data.data);
        self.setState({
          ListEventArray: dataUnion
        });
        self.loadCreatedEvent();
      }else{
        console.log('Get Private Event Fail!');
      }
    })
    .catch(function (error) {
        self.setState({
        disableSubmit: false,
        isLoading: false,
          eventListByDay:[],
         EventListAll:[],
         markedDates: {} 
      });

      self.loadEventByDay({dateString : self.state.chooseDate});
      self.connectionLost(self);
      console.log(error);
    });
  }

  async loadCreatedEvent(){
    var self = this;
    var credentials = {'mtd': 6, 'token': this.props.reduxState.Auth.token["access-token"]};
    if(!self.noInternet(self)){

      self.setState({
        disableSubmit: false,
        isLoading: false,
         eventListByDay:[],
         EventListAll:[]
      });
     self.loadEventByDay({dateString : self.state.chooseDate});
      return;
    }
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      self.setState({counter: 0 });
      if(response.data.status == 3){
        Toast.show('Token expired.');
       self.props.navigation.navigate('Auth');
       return;   
      }
      if(response.data.status == 1){
        var dataUnion = _.union(self.state.ListEventArray, response.data.data);
        let arrayData = [];
        for (let eventObject of dataUnion) {
            eventObject.start = moment(eventObject.event_start).format('h:mm a');
            eventObject.end = moment(eventObject.event_end).format('h:mm a');
            eventObject.event_date = moment(eventObject.event_start).format('YYYY-MM-DD');
            arrayData.push(eventObject);
        }
        self.setState({
          EventListAll: arrayData
        });
        if(self.state.renderT == 1){
          self.loadEventByMonth({
            day: parseInt(self.state.day),
            month: parseInt(self.state.month),
            year: parseInt(self.state.year),
            dateString : self.state.chooseDate,
          });
        }
      }else{
        console.log('Get Created Event Fail!');
      }
    })
    .catch(function (error) {
     self.setState({
        disableSubmit: false,
        isLoading: false,
        eventListByDay:[],
         EventListAll:[],
         markedDates: {} 
      });

     self.loadEventByDay({dateString : self.state.chooseDate});
     self.connectionLost(self);
    });
  }


  getMonthDateRange(year, month) {
    var startDate = moment([year, month - 1]);
    var endDate = moment(startDate).endOf('month');
    return { startDate: startDate.format('YYYY-MM-DD'), endDate: endDate.format('YYYY-MM-DD') };
  }

  loadEventByMonth(CalendarData){
    this.loadEventByDay(CalendarData);
  }

  loadEventByDay(CalendarData){
    this.setState({
      chooseDate: CalendarData.dateString,
    });


    this.state.eventListByDay = _.filter(this.state.EventListAll, { 'event_date': CalendarData.dateString});

    var markedDates = {};
    markedDates[CalendarData.dateString] = {selected: true, marked: true, dots: []};
    if(this.state.EventListAll.length > 0){
      this.state.EventListAll.forEach((val) => {
        if(val.event_date == CalendarData.dateString){
          var selected = true;
        }else{
          var selected = false;
        }
        if(markedDates[val.event_date] && markedDates[val.event_date].dots.length > 0){
          if(markedDates[val.event_date].dots.length < 3){
            markedDates[val.event_date].dots.push({key: 'vacation'+ markedDates[val.event_date].dots.length, color: 'blue', selectedDotColor: 'red'});
          }
        }else{
          markedDates[val.event_date] = {selected: selected, dots: [{key: 'massage', color: 'red', selectedDotColor: 'blue'}]};
        } 
      }); 
    }

    this.setState({ markedDates: markedDates });
  }

  renderEventByDate(){
    if(this.state.eventListByDay.length > 0){
      var eventList = _.sortBy(this.state.eventListByDay, [function(o) { return moment(o.event_start); }]);

      return eventList.map((EventInfo) => {
        return (
          <TouchableOpacity key={EventInfo.event_id} onPress={() => {this.detailEvent(EventInfo)} }>
            <View style={{
              backgroundColor: '#fff', borderRadius: 0, padding: 10,
               borderWidth: 1, borderColor: '#eee', flex: 1, minHeight: 80, marginBottom: 10}}>
              <Text style={{fontSize: 20, color: '#000'}}>
                {EventInfo.event_name}
              </Text>
              <Text style={{color: '#666'}}>
                {EventInfo.start} - {EventInfo.end}
              </Text>
            </View>
          </TouchableOpacity>
        );
      });
    }else{
      return (
        <View style={{
          backgroundColor: '#fff', borderRadius: 0, padding: 10,
           borderWidth: 1, borderColor: '#eee', flex: 1, minHeight: 80, marginBottom: 10}}>
          <Text style={{fontSize: 20, color: '#000'}}>
            No event
          </Text>
        </View>
      );
    }
  }

  static navigationOptions = {
    header: null
  };

  detailEvent(detail_event){
    this.props.updateDetailEvent(detail_event);
    this.props.navigation.navigate("Show",{detail_event: detail_event, refreshComponent: () => this.refreshComponent()});
  }
  render() {
    if(this.state.renderT == 1){
      return(
        <Container>
          <MenuProvider>
            <View style={statusbar.container}>
              <MyStatusBar backgroundColor="#000" barStyle="light-content" />
              <View style={statusbar.appBar} />
              <View style={statusbar.content} />
            </View>
            <View style={{flexDirection: "row", justifyContent: "space-between", backgroundColor: '#529ff3', height: 58,
              shadowColor: '#000', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 3, paddingLeft: 10, paddingRight: 10}}>
              <View style={{width: 50, justifyContent:'center', alignItems:'center'}}>
                <Button transparent onPress={() => this.props.navigation.openDrawer()}
                 style={{justifyContent:'center', alignItems:'center', width:60,paddingLeft: 0, paddingRight: 0, marginLeft: -5}}>
                  <Icon style={{color:'#fff', }} name='navicon' type='FontAwesome' />
                </Button>
              </View>

              <View style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
                <Text  style={{color: '#fff', fontSize: 20, fontWeight: 'bold', textAlign: 'center', width: '100%'}}>Home</Text>
              </View>
              <Menu style={{width: 50, justifyContent:'center', alignItems:'center'}}>
                <MenuTrigger style={{justifyContent:'center', alignItems:'center', height: 40, width: 40, borderRadius: 100/2}}>
                  <Icon
                    name="dots-vertical" type="MaterialCommunityIcons"
                    color="white"
                    size={30}
                    style={{color: '#fff'}}
                  />
                </MenuTrigger>

                <MenuOptions optionsContainerStyle={{ marginTop: 49.5,borderRadius: 0, paddingTop: 10, paddingBottom: 10}}>
                  <MenuOption style={{paddingTop: 0, paddingRight: 10, paddingLeft:10, paddingBottom: 0, }}>
                    <Ripple style={{padding: 12, backgroundColor: '#529ff3', width: '100%'}}  onPress={() => this.changeState(1)}>
                      <Text style={{ color: '#fff'}}>Calendar month</Text>
                    </Ripple>
                  </MenuOption>
                  <MenuOption style={{paddingTop: 0, paddingRight: 10, paddingLeft:10, paddingBottom: 0,}}>
                    <Ripple style={{padding: 12, width: '100%'}}  onPress={() => this.changeState(2)}>
                      <Text style={{ color: '#000'}}>Calendar day</Text>
                    </Ripple>
                  </MenuOption>
                </MenuOptions>
              </Menu>
            </View>
            <Content style={{backgroundColor: '#eee', flex: 1}}>
              <ScrollView>
                <Calendar
                  style={styles.calendar}
                  current={this.state.chooseDate}
                  onDayPress={(day) => {this.loadEventByDay(day)}}
                  onMonthChange={(month) => {this.loadEventByMonth(month)}}
                  disableMonthChange={false}
                  markingType={'multi-dot'}
                  markedDates={this.state.markedDates}
                  hideArrows={false}
                  onPressArrowLeft={substractMonth => substractMonth()}
                  onPressArrowRight={addMonth => addMonth()}
                />
                <View style={{padding: 10, backgroundColor: '#eee'}}>
                  <View style={{justifyContent: "flex-start",flex: 1}}>
                    <CardItem style={{backgroundColor: '#eee'}}>
                      <Text style={{fontSize: 20, textAlign: 'center', color: '#666', marginRight: 10}}>
                        {moment(this.state.chooseDate).format('ddd')}
                      </Text>
                      <Text style={{fontSize: 30, textAlign: 'center', color: '#666'}}>
                        {moment(this.state.chooseDate).format('DD')}
                      </Text> 
                    </CardItem>
                    <View style={{flex:1}}>
                      {this.renderEventByDate()}
                    </View>
                  </View>
                </View>
              </ScrollView>
            </Content>

            <Fab 
              style={{ backgroundColor:'rgb(100,121,133)' }}
              position="bottomRight" >
                <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} rippleDuration = {400}
                  style={{height: 56, width: 56, borderRadius: 100/2, justifyContent: 'center', alignItems: 'center'}}
                  onPress={() => {this.props.navigation.navigate("New", { refreshComponent: () => this.refreshComponent(), chooseDate : this.state.chooseDate })} }>
                  <Icon style={{color: '#fff'}} name="add" />
                </Ripple>
            </Fab>
          </MenuProvider>
        </Container>
      );
    }else{
      return (
        <Container>
          <MenuProvider>
            <View style={statusbar.container}>
              <MyStatusBar backgroundColor="#000" barStyle="light-content" />
              <View style={statusbar.appBar} />
              <View style={statusbar.content} />
            </View>
            <View style={{flexDirection: "row", justifyContent: "space-between", backgroundColor: '#529ff3', height: 58,
              shadowColor: '#000', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 3, paddingLeft: 10, paddingRight: 10}}>
              <View style={{width: 50, justifyContent:'center', alignItems:'center'}}>
                <Button transparent onPress={() => this.props.navigation.openDrawer()}
                 style={{justifyContent:'center', alignItems:'center', width:60,paddingLeft: 0, paddingRight: 0, marginLeft: -5}}>
                  <Icon style={{color:'#fff', }} name='navicon' type='FontAwesome' />
                </Button>
              </View>

              <View style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
                <Text  style={{color: '#fff', fontSize: 20, fontWeight: 'bold', textAlign: 'center', width: '100%'}}>Home</Text>
              </View>
              <Menu style={{width: 50, justifyContent:'center', alignItems:'center', }}>
                <MenuTrigger style={{justifyContent:'center', alignItems:'center', height: 40, width: 40, }}>
                  <Icon
                    name="dots-vertical" type="MaterialCommunityIcons"
                    color="white"
                    size={30}
                    style={{color: '#fff'}}
                  />
                </MenuTrigger>
                <MenuOptions optionsContainerStyle={{ marginTop: 49.5, borderRadius: 0, paddingTop: 10, paddingBottom: 10 }}>
                  <MenuOption style={{paddingTop: 0, paddingRight: 10, paddingLeft:10, paddingBottom: 0,}}>
                    <Ripple style={{padding: 10, width: '100%'}}  onPress={() => this.changeState(1)}>
                      <Text style={{ color: '#000'}}>Calendar month</Text>
                    </Ripple>
                  </MenuOption>
                  <MenuOption style={{paddingTop: 0, paddingRight: 10, paddingLeft:10, paddingBottom: 0}}>
                    <Ripple style={{padding: 10, backgroundColor: '#529ff3', width: '100%'}}  onPress={() => this.changeState(2)}>
                      <Text style={{ color: '#fff'}}>Calendar day</Text>
                    </Ripple>
                  </MenuOption>
                </MenuOptions>
              </Menu>
            </View>
            <Agenda
              items={this.state.items}
              loadItemsForMonth={this.loadItems.bind(this)}
              selected={this.state.chooseDate}
              renderItem={this.renderItem.bind(this)}
              renderEmptyDate={this.renderEmptyDate.bind(this)}
              rowHasChanged={this.rowHasChanged.bind(this)}
              onDayPress={(day) =>{this.setState({chooseDate : day.dateString})}}
              onDayChange={(day) =>{this.setState({chooseDate : day.dateString})}}
            />
            
            <Fab 
              style={{ backgroundColor:'rgb(100,121,133)' }}
              position="bottomRight" >
                <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} rippleDuration = {400}
                  style={{height: 56, width: 56, borderRadius: 100/2, justifyContent: 'center', alignItems: 'center'}}
                  onPress={() => {this.props.navigation.navigate("New", { refreshComponent: () => this.refreshComponent(), chooseDate : this.state.chooseDate })} }>
                  <Icon style={{color: '#fff'}} name="add" />
                </Ripple>
            </Fab>
          </MenuProvider>
        </Container>

      );
    }
  }

  loadItems(day) {
    setTimeout(() => {
      for (let i = -15; i < 85; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = this.timeToString(time);
        if (!this.state.items[strTime]) {
          this.state.items[strTime] = [];
          var eventList = _.sortBy(this.state.EventListAll, [function(o) { return moment(o.event_start); }]);
          for (var k = 0; k < eventList.length; k++) {
            const event = eventList[k];
            if(event.event_date){
              if(this.timeToString(event.event_date) == strTime){
                this.state.items[strTime].push(event);
              }
            }   
          }
        }
      }
      const newItems = {};
      Object.keys(this.state.items).forEach(key => {newItems[key] = this.state.items[key];});
      this.setState({
        items: newItems
      });
    }, 2000);
  }

  renderItem(item) {
    return (
      <TouchableOpacity key={item.event_id} onPress={() => {this.detailEvent(item)} }>
        <View style={{flex:1}}>
          <View style={{
            backgroundColor: '#fff', borderRadius: 0, padding: 10,
             borderWidth: 1, borderColor: '#eee', flex: 1, minHeight: 80, marginBottom: 10}}>
            <Text style={{fontSize: 20,  color: '#000'}}>
              {item.event_name}
            </Text>
            <Text style={{color: '#666'}}>
              {item.start} - {item.end}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}><Text>This is empty date!</Text></View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  timeToString(time) {
    if(time){
      var date = new Date(time);
      return date.toISOString().split('T')[0];
    }else{
      return new Date().toISOString().split('T')[0];
    }
  }
}

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex:1,
    paddingTop: 30
  },
  //css calender
  calendar: {
    borderTopWidth: 1,
    paddingTop: 5,
    borderBottomWidth: 1,
    borderColor: '#eee',
    height: 350
  },
  text: {
    textAlign: 'center',
    borderColor: '#bbb',
    padding: 10,
    backgroundColor: '#eee'
  },
  container: {
    flex: 1,
    backgroundColor: 'gray'
  }
});

const mapStateToProps = (state) => ({
  reduxState:state
});
export default connect(mapStateToProps, {
  Auth,
  updateAccessToken,
  updateDetailEvent,
})(BooksIndexScreen);
