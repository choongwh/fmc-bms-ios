import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image, TouchableOpacity, ListView, Alert, Platform, StatusBar, Dimensions, NetInfo  } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Card, CardItem, Thumbnail, Left, Body, Right, Form,
  Item, Input, Label, Button, Icon ,DatePicker, List, ListItem, Fab, Picker } from 'native-base';
import { connect } from 'react-redux';
import { updateDetailEvent, grantAccess} from '../../redux/actions/auth';
import { NavigationActions, StackActions, DrawerActions } from 'react-navigation';
import moment from 'moment';
import { Url } from '../../../config/api/credentials';
import Axios from 'axios';
import Toast from 'react-native-root-toast';
import Ripple from 'react-native-material-ripple';
import QRCodeScanner from 'react-native-qrcode-scanner';
import Ionicons from "react-native-vector-icons/Ionicons";
import * as Animatable from "react-native-animatable";

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[statusbar.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

const statusbar = {
  container: {
    height: 20,
    width: '100%'
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#000',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#000'
  },
}

const styles = {
  wrapper: {
    flex: 1,
    marginTop: 150,
  },
  submitButton: {
    paddingHorizontal: 10,
    paddingTop: 20,
  },
};

const stylesToast={
  backgroundColor: "#2f4145",
  height: Platform.OS === ("ios") ? 70 : 125,
  color: "#ffffff",
  fontSize: 12,
  borderRadius: 15,
}

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

console.disableYellowBox = true;

const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency
const rectDimensions = SCREEN_WIDTH * 0.85; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.0025; // this is equivalent to 2 from a 393 device width
const scanBarWidth = SCREEN_WIDTH * 0.6; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const rectBorderColor = "#fff";
const scanBarColor = "red";
const iconScanColor = "#fff";

class BookShow extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      attendee:[],
      openCamera : false,
      // pastevent : moment(this.props.reduxState.Auth.event_detail.event_start).unix()>moment().set('hour', 0).set('minute', 0).unix()?false:true,
      pastevent: moment(this.props.reduxState.Auth.event_detail.event_end).unix()>moment().unix()?false:true,
      hideEditDelete : this.props.reduxState.Auth.event_detail.created_user != this.props.reduxState.Auth.user.user_id ? true : false,
      showAcess : this.props.reduxState.Auth.event_detail.created_user == this.props.reduxState.Auth.user.user_id || this.props.reduxState.Auth.event_detail.delegeted_user_id == this.props.reduxState.Auth.user.user_id ? true : false,
      connection_Status : "Online",
      counter : 0,
    };
  }
  componentWillMount = () => {
    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );

    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {

        this.setState({connection_Status : "Online"})

      }
      else
      {

        this.setState({connection_Status : "Offline"})
      }

    });
  }

   componentDidMount() {

    this.getUserListByEventId(this.props.reduxState.Auth.event_detail.event_id);
    const headerConfig = {
      header: null,
    };
    this.props.navigation.setParams({headerConfig});
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );

  }
 /***
  Start Status connection
  ***/

    _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"});
        Toast.show('CONNECTION TO HOST RESUME.');
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
    };

    connectionLost(self){
        self.setState({ isLoading: false })
        self.setState({counter: self.state.counter+1 });
       if(self.state.counter>=3){
            Toast.show('CONNECTION LOST');
       }

     }

     noInternet(self){
       if(self.state.connection_Status=="Online"){

         return true
       }else{
         Toast.show('Connection to host lost.');
         return false;
       }
     }


  /***
 End Status connection
  ***/

  async getUserListByEventId(event_id){
    var self = this;
    var credentials = {'mtd': 9,'id': event_id , 'token':this.props.reduxState.Auth.token["access-token"]};
    if(!self.noInternet(self)){
       self.setState({
       disableSubmit: false,
       isLoading: false
       });
      return;
    }
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      self.setState({counter: 0 });

      if(response.data.status == 3){
        Toast.show('Token expired.');
        self.props.navigation.navigate('Auth');
       return;
      }
      if(response.data.status == 1){
        self.props.reduxState.Auth.event_detail.attendee = response.data.data;
        self.props.updateDetailEvent(self.props.reduxState.Auth.event_detail);
        self.setState({attendee: response.data.data});
      }else{
        console.log('Get User List In Event Fail!');
      }
    })
    .catch(function (error) {
      self.connectionLost(self);
      console.log(error);
    });
  }
  deleteEvent = async () =>{
    var self = this;
     if(!self.noInternet(self)){
      self.setState({
        isLoading: false
      });
      return;
    }
    var credentials = {'mtd': 3,'id': this.props.reduxState.Auth.event_detail.event_id , 'token':this.props.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) {
       self.setState({counter: 0 });
      if(response.data.status == 3){
        Toast.show('Token expired.');
        self.props.navigation.navigate('Auth');
       return;
      }
      if(response.data.status == 1){
        Toast.show('Delete event successful!',Toast.SHORT, Toast.CENTER, stylesToast);
        self.props.updateDetailEvent({});
        self.props.navigation.state.params.refreshComponent();
        self.props.navigation.navigate("Index");
      }else{
        Toast.show('Delete event fail!',Toast.SHORT, Toast.CENTER, stylesToast);
      }
    })
    .catch(function (error) {
      self.connectionLost(self);
      console.log(error);
    });
  }

  confirmDeleteEvent = () =>{
    Alert.alert(
      'Delete Event',
      'Are you sure delete this event?',
      [
        {text: 'NO', onPress: () => console.log('NO Pressed'), style: 'cancel'},
        {text: 'YES', onPress: async () => this.deleteEvent()},
      ]
    );
  }

  onSuccess(e) {
    this.setState({
      openCamera: false
    });

    let that = this;
    setTimeout(function() {
      that.verifyAccess(e.data);
    }, 500);
  }

  verifyAccess(qr_code){
    var self = this;
    const { detail_event } = this.props.navigation.state.params;
    if(!self.noInternet(self)){
      self.setState({
        isLoading: false
      });
      return;
    }
    var credentials = {mtd:14,key:qr_code,token: this.props.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers:  Url.headers})
    .then(function (response) {
      self.setState({counter: 0 });
      switch(parseInt(response.data.status)){
        case 1:
          Toast.show('Success!',Toast.SHORT);
          // self.props.navigation.navigate('Access', {'detail_event': detail_event});
          self.props.grantAccess(detail_event.event_id);
          self.props.navigation.navigate('Facility', {detail_event: detail_event, refreshComponent: () => self.refreshComponent()});
        break;
        case 2:
          Toast.show('Success!',Toast.SHORT);
          self.props.grantAccess(detail_event.event_id);
          self.props.navigation.navigate('Facility', {detail_event: detail_event, refreshComponent: () => self.refreshComponent()});
        break;
        case 3:
          Toast.show('Token expired!',Toast.SHORT);
          self.props.navigation.navigate('Auth');
        break;
      }
    })
    .catch(function (error) {
      self.connectionLost(self);
      console.log(error);
    });
  }

  accessEvent(){
    let self = this;

    const { detail_event } = this.props.navigation.state.params;
    if (this.props.reduxState.Auth.access_granted_event_ids.includes(detail_event.event_id)) {
    // if (true) {
      // const { detail_event } = this.props.navigation.state.params;
      // this.props.navigation.navigate('Access', {'detail_event': detail_event});
      self.props.navigation.navigate('Facility', {detail_event: detail_event, refreshComponent: () => this.refreshComponent()});
    } else {
      if(this.scanner){
        this.scanner.reactivate();
      }
      this.setState({openCamera : true});
    }
  }

  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18
      },
      to: {
        [translationType]: fromValue
      }
    };
  }

  async gobackScreen() {
    this.setState({openCamera : false});
  }

  static navigationOptions = ({ navigation }) => {
    return navigation.state.params.headerConfig;
  };

  refreshComponent = () => {
    this.render();
  };

  renderAccessButton(){
    if(this.state.showAcess){
      return(
        <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
          <Button full info style={{paddingTop: 0, paddingBottom: 0}}>
            <Ripple style={{height: "100%", width: '100%', paddingTop: 10, paddingBottom: 10, justifyContent: 'center', alignItems: 'center'}}
             onPress={this.accessEvent.bind(this)}>
              <Text style={{color:'#fff'}}>{'Access'.toUpperCase()}</Text>
            </Ripple>
          </Button>
        </View>
      );
    }else{
      return(
        <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
        </View>
      );
    }
  }

  renderBottomButton(detail_event){
    const { params } = this.props.navigation.state;
    if(this.state.hideEditDelete){
      return(
        <View style={{paddingTop:10, paddingBottom:10, paddingLeft:5, paddingRight: 5}}>
          <View style={{flex: 1, flexDirection: "row", justifyContent: 'space-between'}}>
            <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
            </View>
            <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
            </View>
            {this.renderAccessButton()}
          </View>
        </View>
      );
    }else{
      return(
        <View style={{paddingTop:10, paddingBottom:10, paddingLeft:5, paddingRight: 5}}>
          <View style={{flex: 1, flexDirection: "row", justifyContent: 'space-between'}}>
            <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
              <Button full info style={{paddingTop: 0, paddingBottom: 0}}>
                <Ripple style={{height: "100%", width: '100%', paddingTop: 10, paddingBottom: 10, justifyContent: 'center', alignItems: 'center'}}
                 onPress={() => this.props.navigation.navigate("Edit",params) }>
                  <Text style={{color:'#fff'}}>{'Edit'.toUpperCase()}</Text>
                </Ripple>
              </Button>
            </View>
            <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
              <Button full info style={{paddingTop: 0, paddingBottom: 0}}>
                <Ripple style={{height: "100%", width: '100%', paddingTop: 10, paddingBottom: 10, justifyContent: 'center', alignItems: 'center'}}
                 onPress={this.confirmDeleteEvent}>
                  <Text  style={{color:'#fff'}}>{'Delete'.toUpperCase()}</Text>
                </Ripple>
              </Button>
            </View>
            {this.renderAccessButton()}
          </View>
        </View>
      );
    }
  }

  _renderBottomButtonA(detail_event,navigate){
    if(this.state.hideEditDelete && !this.state.showAcess || this.state.pastevent){
      return(<View></View>);
    }else{
      return(
        <View>
          <Card>
            {this.renderBottomButton(detail_event,navigate)}
          </Card>
        </View>
      );
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    // const { detail_event } = this.props.navigation.state.params;
    const { detail_event } = this.props.reduxState.Auth.event_detail;
    if(this.state.openCamera){
      return (
        <Container>
          <View style={statusbar.container}>
            <MyStatusBar backgroundColor="#000" barStyle="light-content" />
            <View style={statusbar.appBar} />
            <View style={statusbar.content} />
          </View>
          <View style={{backgroundColor:'#529ff3', flexDirection: 'row', justifyContent: "space-between",
            height: 58, shadowColor: '#494949', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 1, paddingLeft: 10, paddingRight: 10 }}>
            <View style={{width: 40, justifyContent: 'center', alignItems: 'center'}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}
              onPress={() => this.gobackScreen()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                </Button>
              </Ripple>
            </View>

            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontSize: 20, color:'#fff', fontWeight: 'bold', textAlign: 'center'}}>
                Scan qr code
              </Text>
            </View>

            <View style={{width: 40, justifyContent: 'center', alignItems: 'center'}}>

            </View>
          </View>
          <Content>
            <QRCodeScanner ref={(node) => { this.scanner = node }} onRead={this.onSuccess.bind(this)}
              showMarker
              cameraStyle={{ height: SCREEN_HEIGHT }}
              customMarker={
                <View style={stylesCamera.rectangleContainer}>
                  <View style={stylesCamera.topOverlay}>
                    <Text style={{ fontSize: 30, color: "white" }}>
                      SCAN TO ACCESS
                    </Text>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <View style={stylesCamera.leftAndRightOverlay} />

                    <View style={stylesCamera.rectangle}>
                      <Ionicons
                        name="ios-qr-scanner"
                        size={SCREEN_WIDTH * 0.95}
                        color={iconScanColor}
                      />
                      <Animatable.View
                        style={stylesCamera.scanBar}
                        direction="alternate-reverse"
                        iterationCount="infinite"
                        duration={1700}
                        easing="linear"
                        animation={this.makeSlideOutTranslation(
                          "translateY",
                          SCREEN_WIDTH * -0.7
                        )}
                      />
                    </View>

                    <View style={stylesCamera.leftAndRightOverlay} />
                  </View>

                  <View style={stylesCamera.bottomOverlay} />
                </View>
              }
            />
          </Content>
        </Container>
      );
    }else{
      return (
        <Container>
          <View style={statusbar.container}>
            <MyStatusBar backgroundColor="#000" barStyle="light-content" />
            <View style={statusbar.appBar} />
            <View style={statusbar.content} />
          </View>

          <View style={{flexDirection: "row", justifyContent: "space-between", backgroundColor: '#529ff3', height: 58,
            shadowColor: '#494949', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 1, paddingLeft: 10, paddingRight: 10}}>
            <View style={{width: 40, justifyContent:'center', alignItems:'center'}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}
              onPress={() => this.props.navigation.goBack()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                </Button>
              </Ripple>
            </View>

            <View style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
              <Text style={{fontSize: 20, color:'#fff', fontWeight: 'bold', textAlign: 'center'}}>
                {this.props.reduxState.Auth.event_detail.event_name}
              </Text>
            </View>

            <View style={{width: 40, justifyContent:'center', alignItems:'center'}}>

            </View>
          </View>
          <Content>
            <View>
              <View  style={{padding: 10}}>
                <View>
                  <Card style={{paddingRight:20}}>
                    <CardItem>
                      <Body>
                        <View >
                          <View style={{borderBottomColor:'gray', borderBottomWidth:0.5, paddingTop:10, paddingBottom: 10}}>
                            <View style={{paddingBottom:5, paddingTop:5}}>
                              <TouchableOpacity onPress={() => this.props.navigation.navigate("Attendee")}>
                                <View style={{ flexDirection: "row",justifyContent:'space-between'}}>
                                  <View>
                                    <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                      ATTENDEE:
                                    </Text>
                                  </View>
                                  <Icon name='chevron-small-right' type='Entypo' style={{color:'gray'}}/>
                                </View>

                                <View>
                                  <Text style={{ fontSize: 20, color: '#000'}}>
                                    {this.state.attendee.length}
                                  </Text>
                                </View>
                              </TouchableOpacity>
                            </View>

                            <View style={{paddingBottom:5, paddingTop:5}}>
                              <View style={{width:130}}>
                                <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                  DELEGATE:
                                </Text>
                              </View>
                              <View>
                                <Text style={{ fontSize: 20, color: '#000'}}>
                                  {this.props.reduxState.Auth.event_detail.delegeted_user_name}
                                </Text>
                              </View>
                            </View>

                            <View style={{paddingBottom:5, paddingTop:5}}>
                              <View style={{width:130}}>
                                <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                  LOCATION:
                                </Text>
                              </View>
                              <View>
                                <Text style={{ fontSize: 20, color: '#000'}}>
                                  {this.props.reduxState.Auth.event_detail.location_name}
                                </Text>
                              </View>
                            </View>
                            <View style={{flexDirection: 'row', justifyContent:'space-between'}}>
                              <View style={{paddingBottom:5, paddingTop:5, width: '50%'}}>
                                <View style={{width:130}}>
                                  <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                    START DATE:
                                  </Text>
                                </View>
                                <View>
                                  <Text style={{ fontSize: 20, color: '#000'}}>
                                    {moment(this.props.reduxState.Auth.event_detail.event_start).format('MM/DD/YYYY')}
                                  </Text>
                                </View>
                              </View>
                              <View style={{paddingBottom:5, paddingTop:5, width: '50%'}}>
                                <View style={{width:130}}>
                                  <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                    START TIME:
                                  </Text>
                                </View>
                                <View>
                                  <Text style={{ fontSize: 20, color: '#000'}}>
                                    {this.props.reduxState.Auth.event_detail.start}
                                  </Text>
                                </View>
                              </View>
                            </View>

                            <View style={{flexDirection: 'row', justifyContent:'space-between'}}>
                              <View style={{paddingBottom:5, paddingTop:5, width: '50%'}}>
                                <View style={{width:130}}>
                                  <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                    END DATE:
                                  </Text>
                                </View>
                                <View>
                                  <Text style={{ fontSize: 20, color: '#000'}}>
                                    {moment(this.props.reduxState.Auth.event_detail.event_start).format('MM/DD/YYYY')}
                                  </Text>
                                </View>
                              </View>

                              <View style={{paddingBottom:5, paddingTop:5, width: '50%'}}>
                                <View style={{width:130}}>
                                  <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                    END TIME:
                                  </Text>
                                </View>
                                <View>
                                  <Text style={{ fontSize: 20, color: '#000'}}>
                                    {this.props.reduxState.Auth.event_detail.end}
                                  </Text>
                                </View>

                              </View>
                            </View>

                          </View>
                        </View>
                      </Body>
                    </CardItem>

                    <CardItem>
                      <Body>
                        <View >
                          <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center',}}>
                            <View>
                              <View>
                                <Icon style={{fontSize: 12}} name='asterisk' type='MaterialCommunityIcons'/>
                              </View>
                            </View>
                            <Text style={{fontSize: 14, color: '#000'}}>
                              NOTE:
                            </Text>

                          </View>
                          <View>
                              <Text style={{fontStyle: 'italic', color:'gray', paddingLeft:10, textAlign:'justify', fontSize: 16}}>
                                {this.props.reduxState.Auth.event_detail.event_note}
                              </Text>
                            </View>
                        </View>
                      </Body>
                    </CardItem>

                  </Card>
                </View>

                {this._renderBottomButtonA(detail_event,navigate)}
              </View>
            </View>

          </Content>
        </Container>
      );
    }
  }
}

const stylesCamera = {
  rectangleContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    borderWidth: rectBorderWidth,
    borderColor: rectBorderColor,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: "center",
    alignItems: "center"
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor
  },

  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: scanBarColor
  }
};

const mapStateToProps = (state) => ({
  reduxState:state
});

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

export default connect(mapStateToProps, {updateDetailEvent, grantAccess})(BookShow);