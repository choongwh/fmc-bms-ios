import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image, TouchableOpacity, ListView, Platform, StatusBar } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Card, CardItem, Thumbnail, Left, Body, Right, Form, 
  Item, Input, Label, Button, Icon ,DatePicker, List, ListItem, Fab, Picker} from 'native-base';
import GenerateForm from 'react-native-form-builder';
import Axios from 'axios';
import { connect } from 'react-redux';
import Auth from '../../redux/reducers/auth';
import { updateAccessToken } from '../../redux/actions/auth';
import { Url } from '../../../config/api/credentials';
import CheckBox from 'react-native-check-box';
import Ripple from 'react-native-material-ripple';

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[statusbar.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

const statusbar = {
  container: {
    height: 20,
    width: '100%'
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#000',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#000'
  },
}

const styles = {
  wrapper: {
    flex: 1,
    marginTop: 150,
  },
  submitButton: {
    paddingHorizontal: 10,
    paddingTop: 20,
  },
};

class BookAttendee extends React.Component {
  static navigationOptions = {
    header: null,
    // title: 'List Attendee',
    // headerStyle: {
    //   backgroundColor: '#529ff3',
    // },    
    // headerTintColor: 'white',
    // headerTitleStyle: { fontWeight: 'bold', alignSelf: 'center', textAlign: 'center', justifyContent: 'center', flex: 1, marginLeft: -40 },
  };


  constructor(props){
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(this.props.reduxState.Auth.event_detail.attendee),
    };
  }

  taoHang(data,navigate){
    return (
      <View style={{paddingLeft:10, paddingRight:10, paddingTop:10}} >
        <Card>
          <CardItem  >
            <Body>
              <View>
                <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                  <Text style={{fontSize: 24, color: '#d9534f'}}>{data.user_profile_name}</Text>
                </View>
                <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                  <Text style={{fontSize:14, color:'#5bc0de', width: 60}}>
                    Email:
                  </Text>
                  <Text style={{fontSize:20, color: '#000'}}>
                    {data.user_email}
                  </Text>
                </View>
                <View style={{width: '100%', flexDirection: "row", justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                    {/*<Checkbox
                      disabled={true}
                      checked={data.event_attendee_status == '1' ? true : false}
                      style={{borderColor: 'green', color:'green', borderRadius: 5}}
                    />
                    <Text style={{ marginLeft:10 ,color: 'green'}}>
                      Yes
                    </Text>*/}
                    <CheckBox
                      style={{flex: 1, borderRadius: 5}}
                      onClick={() => console.log()}
                      isChecked={data.event_attendee_status == '1' ? true : false}
                      rightText={"Yes"}
                      rightTextStyle={{color:'green'}}
                      checkBoxColor={'green'}
                      disabled={true}
                    />
                    <CheckBox
                      style={{flex: 1, borderRadius: 5}}
                      onClick={() => console.log()}
                      isChecked={data.event_attendee_status == '2' ? true : false}
                      rightText={"No"}
                      rightTextStyle={{color:'red'}}
                      checkBoxColor={'red'}
                      disabled={true}
                    />
                    <CheckBox
                      style={{flex: 1, borderRadius: 5}}
                      onClick={() => console.log()}
                      isChecked={data.event_attendee_status == '0' ? true : false}
                      rightText={"Maybe"}
                      rightTextStyle={{color:'#0066ff'}}
                      checkBoxColor={'#0066ff'}
                      disabled={true}
                    />
                  </View>
                </View>
              </View>
            </Body>
          </CardItem>
        </Card>
      </View>);
  }
  render(){
    const { navigate } = this.props.navigation;
    return(
      <Container>
        <View style={statusbar.container}>
          <MyStatusBar backgroundColor="#000" barStyle="light-content" />
          <View style={statusbar.appBar} />
          <View style={statusbar.content} />
        </View>
        <View style={{flexDirection: "row", justifyContent: "space-between", backgroundColor: '#529ff3', height: 58,
          shadowColor: '#494949', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 1, paddingLeft: 10, paddingRight: 10}}>
          <View style={{width: 40, justifyContent:'center', alignItems:'center'}}>
            <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
            onPress={() => this.props.navigation.goBack()} >
              <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                <Icon style={{color:'#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
              </Button>
            </Ripple>
          </View>
         
          <View style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
            <Text style={{fontSize: 20, color:'#fff', fontWeight: 'bold', textAlign: 'center'}}>
              List Attendee
            </Text>
          </View>

          <View style={{width: 40, justifyContent:'center', alignItems:'center'}}>
          
          </View>
        </View>
        <Content>
          <ListView  dataSource={this.state.dataSource}
            enableEmptySections
            renderRow={data => {
              return this.taoHang(data,navigate)
            }}
          />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  reduxState:state
});

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

export default connect(mapStateToProps, {
  Auth,
  updateAccessToken,
})(BookAttendee);