import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image, TouchableOpacity, TextInput, Dimensions, Platform, StatusBar, NetInfo  } from 'react-native';
import { Container, Header, Content, Footer, Form, Item, Input, Label, CardItem, Button, Icon, DatePicker, Picker, Left, Right, Body, Textarea, Switch } from 'native-base';
import Axios from 'axios';
import { connect } from 'react-redux';
import DateTimePicker from 'react-native-modal-datetime-picker';
import CheckBox from 'react-native-check-box';
import Auth from '../../redux/reducers/auth';
import { updateAccessToken, updateQrCodeRoom } from '../../redux/actions/auth';
import { Url } from '../../../config/api/credentials';
import QRCodeScanner from 'react-native-qrcode-scanner';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import Toast from 'react-native-root-toast';
import moment from 'moment';
import _ from 'lodash';
import Ionicons from "react-native-vector-icons/Ionicons";
import * as Animatable from "react-native-animatable";
import Ripple from 'react-native-material-ripple';
import Loader from '../../../App/views/unconnected/Loader';
const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[statusbar.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

const statusbar = {
  container: {
    height: 20,
    width: '100%'
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#000',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#000'
  },
}

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

console.disableYellowBox = true;

const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency
const rectDimensions = SCREEN_WIDTH * 0.85; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.0025; // this is equivalent to 2 from a 393 device width
const scanBarWidth = SCREEN_WIDTH * 0.6; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const rectBorderColor = "#fff";
const scanBarColor = "red";  
const iconScanColor = "#fff";

const styles = {
  
  wrapper: {
    flex: 1,
    marginTop: 150,
  },
  submitButton: {
    paddingHorizontal: 10,
    paddingTop: 20,
  },
  cardImage_h:{
    height: 80,flex: 1
  },

  footerbtn:{backgroundColor:'#fff', height: 45},
  outbtnfooter:{flex: 1, justifyContent: 'flex-start', alignItems: 'center'},
  btnfooter:{flex: 1, height: 45, justifyContent: 'center'},
  btnfooterText:{color:"#fff", fontSize: 16, textAlign:'center'},
  textForm:{
    textAlign: 'center', fontSize: 22,  fontWeight: 'bold', padding: 15,
  },

  color_blur:{color: '#2089dc'},

  layout_r_f1:{flexDirection: "row", flex: 1, justifyContent: 'space-between', },
  layout_c_f1:{flexDirection: "column", flex: 1, justifyContent: 'space-between', alignItems:'center'},

  w40:{width: 40}, w50:{width: 50}, w100:{width: 100}, w150:{width: 150}, w200:{width: 200}, w250:{width: 250}, w300:{width: 300},  w320:{width: 320},

  pdL5_100: {paddingLeft: '5%'}, 
  pdR5_100: {paddingRight: '5%'}, 

  mgB0:{marginBottom: 0}, mgB5:{marginBottom: 5}, mgB10:{marginBottom: 10}, mgB15:{marginBottom: 15}, mgB20:{marginBottom: 20}, 
  mgT50:{marginTop: 50},

  pdT0:{paddingTop: 0}, pdT5:{paddingTop: 5}, pdT10:{paddingTop: 10}, pdT15:{paddingTop: 15}, pdT20:{paddingTop: 20},
  pdB0:{paddingBottom: 0}, pdB5:{paddingBottom: 5}, pdB10:{paddingBottom: 10}, pdB15:{paddingBottom: 15}, pdB20:{paddingBottom: 20},
  pdL0:{paddingLeft: 0}, pdL5:{paddingLeft: 5}, pdL10:{paddingLeft: 10}, pdL15:{paddingLeft: 15}, pdL20:{paddingLeft: 20},
  pdR0:{paddingRight: 0}, pdR5:{paddingRight: 5}, pdR10:{paddingRight: 10}, pdR15:{paddingRight: 15}, pdR20:{paddingRight: 20},

  fSize16:{fontSize: 16}, fSize18:{fontSize: 18}, fSize20:{fontSize: 20}, fSize24:{fontSize: 24},

  textCenter:{textAlign:'center'}, textLeft:{textAlign:'left'}, textRight:{textAlign:'right'},

  flex1:{flex: 1}, jusbetween:{justifyContent:'space-between'}, 
  flex_column:{flexDirection:'column'}, flex_row:{flexDirection: 'row'}, aligI_center:{alignItems:'center'},
};

const stylesCamera = {
  rectangleContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    borderWidth: rectBorderWidth,
    borderColor: rectBorderColor,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: "center",
    alignItems: "center"
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor
  },

  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: scanBarColor
  }
};

const stylesToast={
  backgroundColor: "#2f4145",
  height: Platform.OS === ("ios") ? 70 : 125,
  color: "#ffffff",
  fontSize: 12,
  borderRadius: 15,
}

class BookEdit extends React.Component { 

  constructor(props) {
    super(props);

    this.state={
      opencamera : false,
      isVisible : false,
      isVisibleEnd : false,
      event_name: this.props.reduxState.Auth.event_detail.event_name,
      event_start: this.props.reduxState.Auth.event_detail.event_start,
      event_end: this.props.reduxState.Auth.event_detail.event_end,
      start_time: moment(this.props.reduxState.Auth.event_detail.event_start).format('MM/DD/YYYY HH:mm'),
      end_time: moment(this.props.reduxState.Auth.event_detail.event_end).format('MM/DD/YYYY HH:mm'),
      event_note: this.props.reduxState.Auth.event_detail.event_note,
      selectedItems : [],
      selectedItemsSimple : [],
      items:[],
      itemsUsers:[],
      SwitchOnValueHolder : this.props.reduxState.Auth.event_detail.event_type == '1' ? true : false,
      check: this.props.reduxState.Auth.event_detail.delegeted_user_id == '0'? false : true,
      selectedDelegate: this.props.reduxState.Auth.event_detail.delegeted_user_id,
      listUserDelegate: [],
      isLoading: false,
      connection_Status : "Online",
      counter : 0,
    };
    this.onScanRoomScreenCamera = this.onScanRoomScreenCamera.bind(this);
  }

  showLoader = () => {
    this.setState({ isLoading: true });
  };

  componentWillMount = () => {

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true){
        this.setState({connection_Status : "Online"});
        this.getListUser();
        this.getLocation();
      }
      else
      {
       
        this.setState({connection_Status : "Offline"})
      }

    });
    
  }

   componentDidMount() {
   
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );

  }

  /***
  Start Status connection
  ***/

    _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"});
        Toast.show('CONNECTION TO HOST RESUME.');
        this.getListUser();
        this.getLocation();
      }
      else
      {
        Toast.show('Connection to host lost.');
        this.setState({connection_Status : "Offline"});
      }
    };

    connectionLost(self){
        self.setState({ isLoading: false })
        self.setState({counter: self.state.counter+1 });
       if(self.state.counter>=3){
            Toast.show('CONNECTION LOST');
       }
        
     }

     noInternet(self){
       if(self.state.connection_Status=="Online"){
         return true
       }else{
         Toast.show('Connection to host lost.');
         return false;
       }
     }


  /***
 End Status connection
  ***/

  async getUserListByEventId(event_id){
    var self = this;
    /*if(!self.noInternet(self)){
       self.setState({
       isLoading: false
       });
      return;
    }*/
    var credentials = {'mtd': 9,'id': event_id , 'token':this.props.reduxState.Auth.token["access-token"]};
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      self.setState({counter: 0 });
      if(response.data.status == 3){
        Toast.show('Token expired.');
       self.props.navigation.navigate('Auth');
       return;   
      }
      if(response.data.status == 1){
        var selectedItemsSimple = _.map(response.data.data, 'user_id');
        var delegateuser =  _.filter(self.state.itemsUsers, function(o) { 
            return selectedItemsSimple.indexOf(o.user_id) > -1; 
          });
        self.setState({
          selectedItemsSimple: selectedItemsSimple,
          listUserDelegate: delegateuser
        });
      }else{
        console.log('Get User List In Event Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
    self.connectionLost(self);
      console.log(error);
    });
  }

  getLocation(){
    var self = this;
  /*   if(!self.noInternet(self)){
       self.setState({
     
       isLoading: false
       });
      return;
    }*/
    var credentials = {'mtd': 11, 'token':this.props.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      self.setState({counter: 0 });
      if(response.data.status == 3){
        Toast.show('Token expired.');
       self.props.navigation.navigate('Auth');
       return;   
      }
      if(response.data.status == 1){
        self.setState({
          items:response.data.data,
        });
        self.setState({
          selectedItems:[self.props.reduxState.Auth.event_detail.location_id],
        });
        self.props.updateQrCodeRoom(response.data.data, self.props.reduxState.Auth.event_detail.location_id);
      }else{
        console.log('Get Location Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      self.connectionLost(self);
      console.log(error);
    });
  }
  getListUser(){
    var self = this;
    if(!self.noInternet(self)){
       self.setState({
     
       isLoading: false
       });
      return;
    }
    var credentials = {'mtd': 6, 'token':this.props.reduxState.Auth.token["access-token"]};
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      self.setState({counter: 0 });
      if(response.data.status == 3){
        Toast.show('Token expired.');
        self.props.navigation.navigate('Auth');
        return;   
      }
      if(response.data.status == 1){
        var userList = response.data.data.filter(item => {return item.user_email});
        self.setState({
          itemsUsers: userList,
        })
        self.getUserListByEventId( self.props.reduxState.Auth.event_detail.event_id);
      }else{
        console.log('Get Users Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      self.connectionLost(self);
      console.log(error);
    });
  }

  onScanRoomScreenCamera(){
    if(this.scanner){
      this.scanner.reactivate();
    }
    this.setState({
      opencamera:true,
    });
  }

  onSuccess(e) {
    this.setState({
      opencamera:false,
    });
    var self = this;
    var credentials = {'mtd': 15, 'id': e.data,'token': this.props.reduxState.Auth.token["access-token"]};
   /* if(!self.noInternet(self)){
       self.setState({
     
       isLoading: false
       });
      return;
    }*/
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      self.setState({counter: 0 });
      if(response.data.status == 3){
        Toast.show('Token expired.');
        self.props.navigation.navigate('Auth');
        return;   
      }
      if(response.data.status == 1){
        var obj = _.find(self.props.reduxState.Auth.qrcode_room.location, function(o) { return o.location_id == response.data.data.location_id; });
        if(obj){
          self.setState({
            selectedItems:[obj.location_id],
          });
          self.props.updateQrCodeRoom(self.props.reduxState.Auth.qrcode_room.location,response.data.data.location_id);
        }
      }else{
        Toast.show('Get Location Detail Fail!',Toast.SHORT, Toast.CENTER,);
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
     self.connectionLost(self);
      console.log(error);
    });
  }

  onSelectedItemsChangeSimple = selectedItemsSimple => {
    this.setState({ 
      selectedItemsSimple,
      listUserDelegate : _.filter(this.state.itemsUsers, function(o) { 
        return selectedItemsSimple.indexOf(o.user_id) > -1; 
      })
    });
  };

  onSelectedItemsChange = selectedItems => {
    this.setState({ selectedItems });
  };

  static navigationOptions = {
    header: null,
  };

  handlePicker = (time) => {
    this.setState({
      isVisible:false,
      event_start: moment(time).format('YYYY-MM-DD HH:mm:ss'),
      start_time: moment(time).format('MM/DD/YYYY HH:mm'),
      event_end: moment(time).add(30, 'm').format('YYYY-MM-DD HH:mm:ss'),
      end_time: moment(time).add(30, 'm').format('MM/DD/YYYY HH:mm'),
    })
  }

  handlePickerEnd = (time) => {
    if(!moment(this.state.event_start).isBefore(moment(time))){
      this.setState({
        isVisibleEnd:false,
      })
      Toast.show('Endtime must be greater than starttime!',Toast.SHORT, Toast.CENTER, stylesToast);
      return;
    }
    this.setState({
      isVisibleEnd:false,
      event_end: moment(time).format('YYYY-MM-DD HH:mm:ss'),
      end_time: moment(time).format('MM/DD/YYYY HH:mm'),
    })
  }

  showPicker = () => {
    this.setState({
      isVisible:true
    })
  }

  showPickerEnd = () => {
    this.setState({
      isVisibleEnd:true
    })
  }

  hidePicker = () => {
    this.setState({
      isVisible:false
    })
  }

  hidePickerEnd = () => {
    this.setState({
      isVisibleEnd:false
    })
  }

  checkBoxDelegate(){
    if(this.state.check){
      this.setState({
        selectedDelegate: ''
      });
    }
    this.setState({
      check: !this.state.check
    })
  }

  onValueChange2(value: string) {
    this.setState({
      selectedDelegate: value
    });
  }

  validate(){
    if(this.state.event_name == '' || this.state.event_start == '' || this.state.event_end == '' || this.state.selectedItems.length == 0 || this.state.selectedItemsSimple.length == 0){
      Toast.show('Please fill required fields!',Toast.SHORT, Toast.CENTER, stylesToast);
      return;
    }else{
      this.updateEvent();
    }
  }

  updateEvent() {
    var self = this;
    var credentials = {
      "mtd": 2, 
      "token": this.props.reduxState.Auth.token["access-token"],
      "id": this.props.reduxState.Auth.event_detail.event_id,
      "nm": this.state.event_name,
      "lid": this.state.selectedItems[0],
      "srt": this.state.event_start,
      "end": this.state.event_end,
      "not": this.state.event_note != '' ? this.state.event_note : 0,
      "typ": this.state.SwitchOnValueHolder ? 1 : 2,
      "uid": this.state.selectedDelegate != '' ? this.state.selectedDelegate : 0
    };
    /* if(!self.noInternet(self)){
       self.setState({
      
       isLoading: false
       });
      return;
    }*/
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      self.setState({counter: 0 });
      if(response.data.status == 3){
        Toast.show('Token expired.');
        self.props.navigation.navigate('Auth');
        return;   
      }
      if(response.data.status == 1){
        self.addAttendee(self.props.reduxState.Auth.event_detail.event_id);
        self.showLoader();
      }else{
        Toast.show('Fail to update event!',Toast.SHORT, Toast.CENTER, stylesToast);
      }
    })
    .catch(function (error) {
      Toast.show('Fail to update event!',Toast.SHORT, Toast.CENTER, stylesToast);
        self.connectionLost(self);
      
      console.log(error);
    });
  }

  addAttendee(event_id){
    var self = this;
    var credentials = {
      "mtd": 7, 
      "token": this.props.reduxState.Auth.token["access-token"],
      "id": event_id,
      "uid": this.state.selectedItemsSimple
    };
  /*  if(!self.noInternet(self)){
       self.setState({
       disableSubmit: false,
       isLoading: false
       });
      return;
    }*/
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) {
      self.setState({counter: 0 });

      switch(response.data.status){
        case 1:
          self.setState({
            disableSubmit: false
          });
          Toast.show('Event updated successfully!',Toast.SHORT, Toast.CENTER, stylesToast);
          self.props.navigation.state.params.refreshComponent();
          self.props.navigation.navigate("Index");
        break;
        case 2:
          Toast.show('Update attendee fail!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
        case 3:
          Toast.show('Token expired!',Toast.SHORT, Toast.CENTER, stylesToast);
          self.props.navigation.navigate('Auth');
        break;
        case 4:
          self.props.navigation.state.params.refreshComponent();
          self.props.navigation.navigate("Index");
        break;
        default :
          Toast.show('Error!');
        break;
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
       self.connectionLost(self);
      console.log(error);
    });
  }

  changeStateEvent = (value) =>{
    this.setState({
      SwitchOnValueHolder: value
    })
  }

  componentDidMount = () => {
    this.setState({
      _isMounted : true
    })
  }

  componentWillUnmount = () => {
    this.setState({
      _isMounted : false
    })
  }

  async gobackscreen(){
    this.setState({
      opencamera:false,
    })
  }

  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18
      },
      to: {
        [translationType]: fromValue
      }
    };
  }

  render() {
    const { navigate } = this.props.navigation;
    const { detail_event } = this.props.navigation.state.params;
    const { selectedItems, selectedItemsSimple, items, itemsUsers, opencamera } = this.state;
    const listUserDelegate = this.state.listUserDelegate.map( (s, i) => {
        return <Picker.Item key={i} value={s.user_id} label={s.user_email} />
    });
    if(this.state.opencamera){
      return (
        <Container>
          <View style={statusbar.container}>
            <MyStatusBar backgroundColor="#000" barStyle="light-content" />
            <View style={statusbar.appBar} />
            <View style={statusbar.content} />
          </View>
          <View style={{backgroundColor:'#529ff3', flexDirection: 'row', justifyContent: "space-between",
            height: 58, shadowColor: '#494949', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 1, paddingLeft: 10, paddingRight: 10 }}>
            <View style={{width: 40, justifyContent: 'center', alignItems: 'center'}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
              onPress={() => this.gobackscreen()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                </Button>
              </Ripple>
            </View>
           
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontSize: 20, color:'#fff', fontWeight: 'bold', textAlign: 'center'}}>
                Scan qr code
              </Text>
            </View>

            <View style={{width: 40, justifyContent: 'center', alignItems: 'center'}}>
              
            </View>
          </View>
          <Content>
            <QRCodeScanner ref={(node) => { this.scanner = node }} onRead={this.onSuccess.bind(this)}
              showMarker
              cameraStyle={{ height: SCREEN_HEIGHT }}
              customMarker={
                <View style={stylesCamera.rectangleContainer}>
                  <View style={stylesCamera.topOverlay}>
                    <Text style={{ fontSize: 30, color: "white" }}>
                      SCAN ROOM
                    </Text>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <View style={stylesCamera.leftAndRightOverlay} />

                    <View style={stylesCamera.rectangle}>
                      <Ionicons
                        name="ios-qr-scanner"
                        size={SCREEN_WIDTH * 0.95}
                        color={iconScanColor}
                      />
                      <Animatable.View
                        style={stylesCamera.scanBar}
                        direction="alternate-reverse"
                        iterationCount="infinite"
                        duration={1700}
                        easing="linear"
                        animation={this.makeSlideOutTranslation(
                          "translateY",
                          SCREEN_WIDTH * -0.7
                        )}
                      />
                    </View>

                    <View style={stylesCamera.leftAndRightOverlay} />
                  </View>

                  <View style={stylesCamera.bottomOverlay} />
                </View>
              }
            />
          </Content>
        </Container>
      );
    }else{
      return (
        <Container>
          <View style={statusbar.container}>
            <MyStatusBar backgroundColor="#000" barStyle="light-content" />
            <View style={statusbar.appBar} />
            <View style={statusbar.content} />
          </View>
          <View style={{flexDirection: "row", justifyContent: "space-between", backgroundColor: '#529ff3', height: 58,
            shadowColor: '#494949', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 1, paddingLeft: 10, paddingRight: 10}}>
            <View style={{width: 40, justifyContent:'center', alignItems:'center'}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
              onPress={() => this.props.navigation.goBack()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                </Button>
              </Ripple>
            </View>
           
            <View style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
              <Text style={{fontSize: 20, color:'#fff', fontWeight: 'bold', textAlign: 'center'}}>
                Edit event
              </Text>
            </View>

            <View style={{width: 40, justifyContent:'center', alignItems:'center'}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
              onPress={() => this.validate()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,justifyContent: 'center', alignItems: 'center'}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", top: 7, }} name="check" type="Octicons" />
                </Button>
              </Ripple>
            </View>
          </View>
          <Content>
            <ScrollView>
              <View style={{paddingBottom: 20}}>
                <View style={{paddingBottom:20}}>
                  <Form style={{paddingRight:15}}>
                    <Item iconLeft style={{paddingTop: 20, paddingBottom: 15}}>
                      <Input placeholder="Event name" placeholderTextColor="#494949" style={{fontSize: 16}} onChangeText={(event_name) => this.setState({event_name})} value={this.state.event_name}/>
                    </Item>

                    <Item iconLeft style={{paddingTop: 15}}>
                      <View style={{flex: 1, justifyContent: 'space-between', flexDirection: 'row', width: '100%'}}>
                        <View style={{width: '50%'}}>
                          <SectionedMultiSelect
                            single
                            items={this.state.items}
                            ref={SectionedMultiSelect => this.SectionedMultiSelectRoom = SectionedMultiSelect}
                            uniqueKey="location_id"
                            displayKey="location_name"
                            headerComponent={
                              <View style={{ padding: 15, position: 'absolute', top: 0, right: 0, zIndex: 99 }}>
                                <TouchableOpacity onPress={this.SectionedMultiSelectRoom && this.SectionedMultiSelectRoom._cancelSelection}>
                                  <Icon>Cancel</Icon>
                                </TouchableOpacity>
                              </View>
                            }
                            selectText='Select facility'
                            searchPlaceholderText='Search Room...'
                            showDropDowns={true}
                            animateDropDowns={false}
                            onSelectedItemsChange={this.onSelectedItemsChange}
                            selectedItems={this.state.selectedItems}
                            colors={{ primary: this.state.selectedItems.length ? 'forestgreen' : 'crimson',}}
                            styles={{
                              selectedItemText: {
                                color: 'blue',
                              },
                              fontSize: 20,
                            }}
                            cancelIconComponent={
                              <Icon
                                size={20}
                                name="close"
                                style={{ color: 'white' }}
                              />
                            }
                          />
                        </View>
                        <View style={{width: '50%'}}>
                          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} >
                            <Button iconLeft transparent dark style={{paddingRight: 20}} onPress={this.onScanRoomScreenCamera}>
                              <Icon style={{fontSize: 30, marginRight: 10}} name='qrcode' type='MaterialCommunityIcons'/>
                              <Text style={{fontSize: 20, color:'#d9534f'}}>{'Scan QR'.toUpperCase()}</Text>
                            </Button>
                          </View>
                        </View>
                      </View>
                    </Item>

                    <Item iconLeft style={{paddingTop: 15}}>
                      <View style={{height:'100%',width:'100%'}}>
                        <View>
                          <SectionedMultiSelect
                            items={this.state.itemsUsers}
                            ref={SectionedMultiSelect => this.SectionedMultiSelect = SectionedMultiSelect}
                            uniqueKey="user_id"
                            displayKey="user_email"
                            headerComponent={
                              <View style={{ padding: 15, position: 'absolute', top: 0, right: 0, zIndex: 99 }}>
                                <TouchableOpacity onPress={this.SectionedMultiSelect && this.SectionedMultiSelect._cancelSelection}>
                                  <Icon>Cancel</Icon>
                                </TouchableOpacity>
                              </View>
                            }
                            selectText='Invite attendee'
                            searchPlaceholderText='Search Attendee...'
                            chipRemoveIconComponent={<Icon name='cancel' type='MaterialCommunityIcons' style={{
                              fontSize: 18,
                              marginHorizontal: 6,
                            }}></Icon>}
                            showDropDowns={true}
                            animateDropDowns={false}
                            showRemoveAll
                            onSelectedItemsChange={this.onSelectedItemsChangeSimple}
                            onCancel={this.onCancel}
                            onConfirm={this.onConfirm}
                            selectedItems={this.state.selectedItemsSimple}
                            colors={{ primary: this.state.selectedItems.length ? 'forestgreen' : 'crimson',}}
                            styles={{
                              selectedItemText: {
                                color: 'blue',
                              },
                            }}
                          />
                        </View> 
                      </View>
                    </Item>

                    <View style={{paddingLeft: 15, paddingTop: 15}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center', 
                      borderBottomColor:'#d0cbd4', borderBottomWidth: 1,  paddingBottom: 15}}>
                        <View style={{flex: 1}}>
                          <Picker
                            mode="dropdown"
                            style={{ width: undefined, color: 'black' }}
                            placeholder="Select user to delegate"
                            placeholderStyle={{ color: "#a7a7a7", fontSize: 17 }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selectedDelegate}
                            onValueChange={this.onValueChange2.bind(this)}
                            enabled={this.state.check}
                          >
                            <Picker.Item label='Select user to delegate' value=""/> 
                            {listUserDelegate}
                          </Picker>
                        </View>
                         
                        <View style={{width: 30}}>
                          <CheckBox 
                            style={{borderRadius: 5}}
                            onClick={() =>this.checkBoxDelegate()}
                            disabled={this.state.selectedItemsSimple.length == 0 ? true : false}
                            isChecked={this.state.check}
                          />
                        </View>
                      </View>
                    </View>

                    <View style={{paddingLeft: 15,}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'space-between', alignItems: 'center', 
                      borderBottomColor:'#d0cbd4', borderBottomWidth: 1,  paddingTop: 30, paddingBottom: 30}}>
                        <View style={{flex: 1}}>
                          <Text style={{fontSize: 16,  color: '#494949', paddingLeft: 10}}>Public event</Text>
                        </View>
                         
                         <View style={{width: 60, paddingRight:15}}>
                          <Switch
                            onValueChange={(value) => this.changeStateEvent(value)}
                            style={{ transform: [{ scaleX: 1 }, { scaleY: 1 }] }}
                            value={this.state.SwitchOnValueHolder} />
                         </View>
                      </View>
                    </View>

                    <Item iconLeft style={{paddingTop: 30, paddingBottom: 30}}>
                      <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
                      <View style={{ flex: 1 }}>
                       <TouchableOpacity onPress={this.showPicker}>
                          <Text style={{color: "#494949", fontSize: 16,textAlign:'left',  paddingLeft:10}}>
                            {this.state.start_time}
                          </Text>
                        </TouchableOpacity>

                        <DateTimePicker
                          isVisible={this.state.isVisible}
                          onConfirm={this.handlePicker}
                          onCancel={this.hidePicker}
                          mode={'datetime'}
                          is24Hour={false}
                          date={new Date(moment(this.state.event_start))}
                        />
                      </View>
                    </Item>

                    <Item iconLeft style={{paddingTop: 30, paddingBottom: 30}}>
                      <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
                      <View style={{ flex: 1 }}>
                        <TouchableOpacity onPress={this.showPickerEnd}>
                          <Text style={{color: "#494949", fontSize: 16,textAlign:'left',  paddingLeft:10}}>
                            {this.state.end_time}
                          </Text>
                        </TouchableOpacity>

                        <DateTimePicker
                          isVisible={this.state.isVisibleEnd}
                          onConfirm={this.handlePickerEnd}
                          onCancel={this.hidePickerEnd}
                          mode={'datetime'}
                          is24Hour={false}
                          date={new Date(moment(this.state.event_end))}
                        />
                      </View>
                    </Item>

                    <View style={{paddingLeft: 15, paddingRight: 15, marginTop: 10}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center'}}>
                        <View style={{width: 40}}>
                          <Icon style={{width: 40}} name='clipboard-outline' type='MaterialCommunityIcons'/>
                        </View>
                         
                         <View style={{flex: 1}}>
                          <Textarea style={{flex: 1, fontSize: 16}} rowSpan={5} bordered placeholder="Note(Optional)" onChangeText={(event_note) => this.setState({event_note})} value={this.state.event_note}/>
                         </View>
                      </View>
                    </View>

                  </Form>
                </View>
              </View>
              </ScrollView>
          </Content>
          <Loader opacity={1} loading={this.state.isLoading}/>
        </Container>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  reduxState:state
});

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;


export default connect(mapStateToProps, {
  Auth,
  updateAccessToken,
  updateQrCodeRoom,
})(BookEdit);