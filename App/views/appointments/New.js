import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image, TouchableOpacity, ListView, Alert, Dimensions, Platform } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Card, CardItem, Thumbnail, Left, Body, Right, Form, 
  Item, Input, Label, Button, Icon ,DatePicker, List, ListItem, Fab, Picker, Textarea, Footer, Switch } from 'native-base';

import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import Axios from 'axios';
import { connect } from 'react-redux';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import _ from 'lodash';
import Auth from '../../redux/reducers/auth';
import { updateAccessToken, updateDetailEvent, updateQrCode } from '../../redux/actions/auth';
import { Url } from '../../../config/api/credentials';
import QRCodeScanner from 'react-native-qrcode-scanner';
import MultiSelect from 'react-native-multiple-select';

import Toast from 'react-native-root-toast';
import { TabNavigator, StackNavigator,createMaterialTopTabNavigator,createStackNavigator } from 'react-navigation';
import CheckBox from 'react-native-check-box';
import Ionicons from "react-native-vector-icons/Ionicons";
import * as Animatable from "react-native-animatable";
import Ripple from 'react-native-material-ripple';
import AppointmentCard from '../../components/appointments/Card';

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

console.disableYellowBox = true;

const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency
const rectDimensions = SCREEN_WIDTH * 0.85; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.0025; // this is equivalent to 2 from a 393 device width
const scanBarWidth = SCREEN_WIDTH * 0.6; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const rectBorderColor = "#fff";
const scanBarColor = "red";  
const iconScanColor = "#fff";
import { MenuContext, Menu, MenuOptions, MenuOption, MenuTrigger, MenuProvider, renderers } from 'react-native-popup-menu';
// import styles from '../../stylecss/style.css';
const styles = {
  
  wrapper: {
    flex: 1,
    marginTop: 150,
  },
  submitButton: {
    paddingHorizontal: 10,
    paddingTop: 20,
  },
  cardImage_h:{
    height: 80,flex: 1
  },

  footerbtn:{backgroundColor:'#fff', height: 45},
  outbtnfooter:{flex: 1, justifyContent: 'flex-start', alignItems: 'center'},
  btnfooter:{flex: 1, height: 45, justifyContent: 'center'},
  btnfooterText:{color:"#fff", fontSize: 16, textAlign:'center'},
  textForm:{
    textAlign: 'center', fontSize: 22,  fontWeight: 'bold', padding: 15,
  },

  color_blur:{color: '#2089dc'},

  layout_r_f1:{flexDirection: "row", flex: 1, justifyContent: 'space-between', },
  layout_c_f1:{flexDirection: "column", flex: 1, justifyContent: 'space-between', alignItems:'center'},

  w40:{width: 40}, w50:{width: 50}, w100:{width: 100}, w150:{width: 150}, w200:{width: 200}, w250:{width: 250}, w300:{width: 300},  w320:{width: 320},

  pdL5_100: {paddingLeft: '5%'}, 
  pdR5_100: {paddingRight: '5%'}, 

  mgB0:{marginBottom: 0}, mgB5:{marginBottom: 5}, mgB10:{marginBottom: 10}, mgB15:{marginBottom: 15}, mgB20:{marginBottom: 20}, 
  mgT50:{marginTop: 50},

  pdT0:{paddingTop: 0}, pdT5:{paddingTop: 5}, pdT10:{paddingTop: 10}, pdT15:{paddingTop: 15}, pdT20:{paddingTop: 20},
  pdB0:{paddingBottom: 0}, pdB5:{paddingBottom: 5}, pdB10:{paddingBottom: 10}, pdB15:{paddingBottom: 15}, pdB20:{paddingBottom: 20},
  pdL0:{paddingLeft: 0}, pdL5:{paddingLeft: 5}, pdL10:{paddingLeft: 10}, pdL15:{paddingLeft: 15}, pdL20:{paddingLeft: 20},
  pdR0:{paddingRight: 0}, pdR5:{paddingRight: 5}, pdR10:{paddingRight: 10}, pdR15:{paddingRight: 15}, pdR20:{paddingRight: 20},

  fSize16:{fontSize: 16}, fSize18:{fontSize: 18}, fSize20:{fontSize: 20}, fSize24:{fontSize: 24},

  textCenter:{textAlign:'center'}, textLeft:{textAlign:'left'}, textRight:{textAlign:'right'},

  flex1:{flex: 1}, jusbetween:{justifyContent:'space-between'}, 
  flex_column:{flexDirection:'column'}, flex_row:{flexDirection: 'row'}, aligI_center:{alignItems:'center'},
};

const stylesCamera = {
  rectangleContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    borderWidth: rectBorderWidth,
    borderColor: rectBorderColor,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: "center",
    alignItems: "center"
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor
  },

  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: scanBarColor
  }
};

const stylesToast={
  backgroundColor: "#2f4145",
  height: Platform.OS === ("ios") ? 70 : 125,
  color: "#ffffff",
  fontSize: 12,
  borderRadius: 15,
}

class AppointmentNew extends React.Component { 
  constructor(props) {
    super(props);

    this.state={
      opencamera : false,
      disableSubmit : false,
      isVisible : false,
      isVisibleEnd : false,
      event_name:'',
      event_start:'',
      event_end:'',
      start_time:'Select start time',
      end_time:'Select end time',
      note:'',
      selectedItems : [],
      selectedItemsSimple : [],
      items:[],
      itemsUsers:[],
      SwitchOnValueHolder :  false,
      check: false,
      selectedDelegate: "",
      listUserDelegate: [],
    };
    this.onScanRoomScreenCamera = this.onScanRoomScreenCamera.bind(this);
  }

  componentWillMount = () => {
    this.getLocation();
    this.getListUser();
  }

  getLocation(){
    var self = this;
    var credentials = {'mtd': 11, 'token': screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.setState({
          items:response.data.data,
        })
        self.props.updateQrCodeRoom(response.data.data,0);
      }else{
        console.log('Get Location Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }
  getListUser(){
    var self = this;
    var credentials = {'mtd': 6, 'token': screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.setState({
          itemsUsers:response.data.data,
        })
      }else{
        console.log('Get Users Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  onScanRoomScreenCamera(){
    if(this.scanner){
      this.scanner.reactivate();
    }
    this.setState({
      opencamera:true,
    })
  }

  onSuccess(e) {
    this.setState({
      opencamera:false,
    })
    this.props.updateQrCodeRoom(screenProps.reduxState.Auth.qrcode_room.location,e.data);
    var obj = _.find(screenProps.reduxState.Auth.qrcode_room.location, function(o) { return o.location_id == e.data; });
    if(obj){
      this.setState({
        selectedItems:[obj.location_id],
      })
    }
  }

  onSelectedItemsChangeSimple = selectedItemsSimple => {
    this.setState({ 
      selectedItemsSimple,
      listUserDelegate : _.filter(this.state.itemsUsers, function(o) { 
        return selectedItemsSimple.indexOf(o.user_id) > -1; 
      }) 
    });
  };

  onSelectedItemsChange = selectedItems => {
    this.setState({ selectedItems });
  };


  static navigationOptions = {
    header: null,
  };

  handlePicker = (time) => {
    this.setState({
      isVisible:false,
      event_start: moment(time).format('YYYY-MM-DD HH:mm:ss'),
      start_time: moment(time).format('MM/DD/YYYY HH:mm:ss'),
    })
  }

  handlePickerEnd = (time) => {
    if(!moment(this.state.event_start).isBefore(moment(time))){
      this.setState({
        isVisibleEnd:false,
      })
      Toast.show('Endtime must be greater than starttime!',Toast.SHORT, Toast.CENTER, stylesToast);
      return;
    }
    this.setState({
      isVisibleEnd:false,
      event_end: moment(time).format('YYYY-MM-DD HH:mm:ss'),
      end_time: moment(time).format('MM/DD/YYYY HH:mm:ss'),
    })
  }

  showPicker = () => {
    this.setState({
      isVisible:true
    })
  }

  showPickerEnd = () => {
    this.setState({
      isVisibleEnd:true
    })
  }

  hidePicker = () => {
    this.setState({
      isVisible:false
    })
  }

  hidePickerEnd = () => {
    this.setState({
      isVisibleEnd:false
    })
  }


  validate(){
    if(this.state.event_name == '' || this.state.event_start == '' || this.state.event_end == '' || this.state.note == '' || this.state.selectedItems.length == 0){
      Toast.show('Please fill input!',Toast.SHORT, Toast.CENTER, stylesToast);
      return;
    }else{
      this.createEvent();
    }
  }

  createEvent() {
    this.setState({
      disableSubmit: true
    });
    var self = this;
    var credentials = {
      "mtd": 1, 
      "token": screenProps.reduxState.Auth.token["access-token"],
      "nm": this.state.event_name,
      "lid": parseInt(this.state.selectedItems[0]),
      "srt": this.state.event_start,
      "end": this.state.event_end,
      "not": this.state.note,
      "typ": this.state.SwitchOnValueHolder ? 1 : 2,
    };
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.addAttendee(response.data.data.event_id);
      }else{
        Toast.show('Add Event Fail!',Toast.SHORT, Toast.CENTER, stylesToast);
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  addAttendee(event_id){
    var self = this;
    var credentials = {
      "mtd": 7, 
      "token": screenProps.reduxState.Auth.token["access-token"],
      "id": event_id,
      "uid": this.state.selectedItemsSimple
    };
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      switch(response.data.status){
        case 1:
          self.setState({
            disableSubmit: false
          });
          Toast.show('Create event successfull!',Toast.SHORT, Toast.CENTER, stylesToast);
          self.props.navigation.state.params.refreshComponent();
          self.props.navigation.navigate("ActiveBooking");
        break;
        case 2:
          Toast.show('Add Attendee Fail!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
        case 3:
          Toast.show('Token expired!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
        default :
          Toast.show('Error!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  changeStateEvent = (value) =>{
    this.setState({
      SwitchOnValueHolder: value
    })
  }

  checkBoxDelegate(){
    if(this.state.check){
      this.setState({
        selectedDelegate: ''
      });
    }
    this.setState({
      check: !this.state.check
    })
  }

  onValueChange2(value: string) {
    this.setState({
      selectedDelegate: value
    });
  }
  
  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18
      },
      to: {
        [translationType]: fromValue
      }
    };
  }

  render() {
    const { navigate } = this.props.navigation;
    const { selectedItems, selectedItemsSimple, items, itemsUsers, opencamera } = this.state;
    const listUserDelegate = this.state.listUserDelegate.map( (s, i) => {
      return <Picker.Item key={i} value={s.user_id} label={s.user_email} />
    });
    if(this.state.opencamera){
      return (
        <QRCodeScanner ref={(node) => { this.scanner = node }} onRead={this.onSuccess.bind(this)}
          showMarker
          cameraStyle={{ height: SCREEN_HEIGHT }}
          customMarker={
            <View style={stylesCamera.rectangleContainer}>
              <View style={stylesCamera.topOverlay}>
                <Text style={{ fontSize: 30, color: "white" }}>
                  SCAN ROOM
                </Text>
              </View>

              <View style={{ flexDirection: "row" }}>
                <View style={stylesCamera.leftAndRightOverlay} />

                <View style={stylesCamera.rectangle}>
                  <Ionicons
                    name="ios-qr-scanner"
                    size={SCREEN_WIDTH * 0.95}
                    color={iconScanColor}
                  />
                  <Animatable.View
                    style={stylesCamera.scanBar}
                    direction="alternate-reverse"
                    iterationCount="infinite"
                    duration={1700}
                    easing="linear"
                    animation={this.makeSlideOutTranslation(
                      "translateY",
                      SCREEN_WIDTH * -0.7
                    )}
                  />
                </View>

                <View style={stylesCamera.leftAndRightOverlay} />
              </View>

              <View style={stylesCamera.bottomOverlay} />
            </View>
          }
        />
      );
    }else{
      return (
        <Container>
          <Header style={{backgroundColor:'#529ff3', flexDirection: 'row', justifyContent: "space-between"}}>
            <View style={{width: 40, paddingTop: 8}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
              onPress={() => this.props.navigation.goBack()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                </Button>
              </Ripple>
            </View>
           
            <View style={{flex: 1, paddingTop: 15, paddingBottom: 12,}}>
              <Text style={{fontSize: 20, color:'#fff', fontWeight: 'bold', textAlign: 'center'}}>
                New event
              </Text>
            </View>

            <View style={{width: 40, paddingTop: 8,}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
              onPress={() => this.validate()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", top: 7, }} name="check" type="Octicons" />
                </Button>
              </Ripple>
            </View>
          </Header>
          <Content>
            <ScrollView>
              <View style={{paddingBottom: 20}}>
                <View style={{marginTop:30}}>
                  <View>
                    <Left style={{flex: 1}}></Left>
                    <Body style={{flex: 1}}>
                      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} >
                        <Button iconLeft transparent dark style={{paddingRight: 20}} onPress={this.onScanRoomScreenCamera}>
                          <Icon style={{fontSize: 30, marginRight: 10}} name='qrcode' type='MaterialCommunityIcons'/>
                          <Text style={{fontSize: 26, color:'#d9534f'}}>{'Scan QR Code'.toUpperCase()}</Text>
                        </Button>
                      </View>
                    </Body>
                    <Right style={{flex: 1}}>
                      
                    </Right>
                  </View>
                </View>
                <View style={{paddingBottom:20}}>
                  <Form style={{paddingRight:15}}>
                    <Item iconLeft style={{paddingTop: 5, paddingBottom: 5}}>
                      <Icon style={{width:40}} name='event' type='SimpleLineIcons'/>
                      <Input placeholder="Title event" placeholderTextColor="#a7a7a7" style={{fontSize: 20}} onChangeText={(event_name) => this.setState({event_name})} value={this.state.event_name}/>
                    </Item>

                    <Item iconLeft style={{}}>
                      <Icon style={{width:40}} name='location-city' type='MaterialIcons'/>
                      <View style={{height:'100%',width:'100%'}}>
                          <View>
                            <MultiSelect
                              single
                              items={items}
                              uniqueKey="location_id"
                              onSelectedItemsChange={this.onSelectedItemsChange}
                              selectedItems={selectedItems}
                              selectText="Select facility"
                              searchInputPlaceholderText="Search facility..."
                              onChangeInput={ (text)=> console.log(text)}
                              tagRemoveIconColor="#494949"
                              tagBorderColor="#494949"
                              tagTextColor="red"
                              selectedItemTextColor="#CCC"
                              selectedItemIconColor="#494949"
                              itemTextColor="#000"
                              displayKey="location_name"
                              searchInputStyle={{ color: '#494949' }}
                              submitButtonColor="#494949"
                            />
                          </View>
                          
                      </View>
                    </Item>

                    <View style={styles.layout_r_f1}>
                      <View style={styles.flex1}>
                        <Item iconLeft style={{paddingTop: 12, paddingBottom: 12}}>
                          <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
                          <View style={{ flex: 1 }}>
                           <TouchableOpacity onPress={this.showPicker}>
                              <Text style={{color: "#494949", fontSize: 20,textAlign:'left',  paddingLeft:10}}>
                                {this.state.start_time}
                              </Text>
                            </TouchableOpacity>

                            <DateTimePicker
                              isVisible={this.state.isVisible}
                              onConfirm={this.handlePicker}
                              onCancel={this.hidePicker}
                              mode={'datetime'}
                              is24Hour={false}
                              
                            />
                          </View>
                        </Item>
                      </View>
                    </View>

                    <View style={styles.layout_r_f1}>
                       <View  style={styles.flex1}>
                          <Item iconLeft style={{paddingTop: 12, paddingBottom: 12}}>
                            <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
                            <View style={{ flex: 1 }}>
                              <TouchableOpacity onPress={this.showPickerEnd}>
                                <Text style={{color: "#494949", fontSize: 20,textAlign:'left',  paddingLeft:10}}>
                                  {this.state.end_time}
                                </Text>
                              </TouchableOpacity>

                              <DateTimePicker
                                isVisible={this.state.isVisibleEnd}
                                onConfirm={this.handlePickerEnd}
                                onCancel={this.hidePickerEnd}
                                mode={'datetime'}
                                is24Hour={false}
                              />
                            </View>
                          </Item>
                      </View>
                    </View>

                    <Item iconLeft style={{}}>
                      <Icon style={{width:40}} name='account' type='MaterialCommunityIcons'/>
                      <View style={{height:'100%',width:'100%'}}>
                          <View>
                            <MultiSelect
                              items={itemsUsers}
                              uniqueKey="user_id"
                              ref={(component) => { this.multiSelect = component }}
                              onSelectedItemsChange={this.onSelectedItemsChangeSimple}
                              selectedItems={selectedItemsSimple}
                              selectText="Select attendee"
                              searchInputPlaceholderText="Search attendee..."
                              onChangeInput={ (text)=> console.log(text)}
                              tagRemoveIconColor="#494949"
                              tagBorderColor="#494949"
                              tagTextColor="#494949"
                              selectedItemTextColor="#CCC"
                              selectedItemIconColor="#CCC"
                              itemTextColor="#000"
                              displayKey="user_email"
                              searchInputStyle={{ color: '#494949' }}
                              submitButtonColor="#494949"
                            />
                            <View>
                              {this.multiSelect && this.multiSelect.getSelectedItemsExt(selectedItems)}
                            </View>
                          </View>
                          
                      </View>
                    </Item>

                    <View style={{paddingLeft: 15, marginTop: 10,}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center', 
                      borderBottomColor:'#d0cbd4', borderBottomWidth: 1,  paddingBottom: 15}}>
                        <View style={{flex: 1}}>
                          <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="ios-arrow-down-outline" />}
                            style={{ width: undefined }}
                            placeholder="Select your room"
                            placeholderStyle={{ color: "#a7a7a7", fontSize: 20 }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selectedDelegate}
                            onValueChange={this.onValueChange2.bind(this)}
                            enabled={this.state.check}
                          >
                            <Picker.Item label='Select user is delegated' value=""/> 
                            {listUserDelegate}
                          </Picker>
                        </View>
                         
                        <View style={styles.w50,styles.pdL5_100,styles.pdR5_100}>
                          <CheckBox 
                            style={{borderRadius: 5}}
                            onClick={()=>this.checkBoxDelegate()}
                            disabled={this.state.selectedItemsSimple.length == 0 ? true : false}
                            isChecked={this.state.check}
                          />
                        </View>
                      </View>
                    </View>

                    <View style={{paddingLeft: 15, marginTop: 10,}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center', 
                      borderBottomColor:'#d0cbd4', borderBottomWidth: 1,  paddingBottom: 15}}>
                        <View style={{flex: 1}}>
                          <Text style={{fontSize: 20,  color: '#494949'}}>Puclic event</Text>
                        </View>
                         
                         <View style={styles.w50,styles.pdL5_100,styles.pdR5_100}>
                          <Switch
                            onValueChange={(value) => this.changeStateEvent(value)}
                            style={{ transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }] }}
                            value={this.state.SwitchOnValueHolder} />
                         </View>
                      </View>
                    </View>

                    <View style={{paddingLeft: 15, paddingRight: 15, marginTop: 10}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center'}}>
                        <View style={{width: 40}}>
                          <Icon style={{width: 40}} name='clipboard-outline' type='MaterialCommunityIcons'/>
                        </View>
                         
                         <View style={{flex: 1}}>
                          <Textarea style={{flex: 1, fontSize: 20}} rowSpan={5} bordered placeholder="Note" onChangeText={(note) => this.setState({note})} value={this.state.note}/>
                         </View>
                      </View>
                    </View>
                  </Form>
                </View>
              </View>
            </ScrollView>
          </Content>
          {/*<Footer style={{backgroundColor:'#fff', height: 45}}>
            <View  style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center'}}>
              <Button info block full disabled={this.state.disableSubmit} onPress={() => this.validate()}>
                <Text style={{color:"#fff", fontSize: 16}}>SUBMIT</Text>
              </Button>
            </View>
          </Footer>*/}
        </Container>
      );
    }
  }
}

const mapStateToProps = (state) => ({
	reduxState:state
});
export default connect(mapStateToProps, {
  Auth,
  updateAccessToken,
})(AppointmentNew);