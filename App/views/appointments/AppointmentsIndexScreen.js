import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image, TouchableOpacity, ListView, Alert, Dimensions, Platform } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Card, CardItem, Thumbnail, Left, Body, Right, Form, 
  Item, Input, Label, Button, Icon ,DatePicker, List, ListItem, Fab, Picker, Textarea, Footer, Switch } from 'native-base';

import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import Axios from 'axios';
import { connect } from 'react-redux';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import _ from 'lodash';
import Auth from '../../redux/reducers/auth';
import { updateAccessToken, updateDetailEvent, updateQrCode } from '../../redux/actions/auth';
import { Url } from '../../../config/api/credentials';
import QRCodeScanner from 'react-native-qrcode-scanner';
import MultiSelect from 'react-native-multiple-select';
// import Toast from 'react-native-toast-native';
import Toast from 'react-native-root-toast';
import { TabNavigator, StackNavigator,createMaterialTopTabNavigator,createStackNavigator } from 'react-navigation';
import CheckBox from 'react-native-check-box';
import Ionicons from "react-native-vector-icons/Ionicons";
import * as Animatable from "react-native-animatable";
import Ripple from 'react-native-material-ripple';
import AppointmentCard from '../../components/appointments/Card';

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

console.disableYellowBox = true;

const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency
const rectDimensions = SCREEN_WIDTH * 0.85; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.0025; // this is equivalent to 2 from a 393 device width
const scanBarWidth = SCREEN_WIDTH * 0.6; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const rectBorderColor = "#fff";
const scanBarColor = "red";  
const iconScanColor = "#fff";


import { MenuContext, Menu, MenuOptions, MenuOption, MenuTrigger, MenuProvider, renderers } from 'react-native-popup-menu';
// import styles from '../../stylecss/style.css';
const styles = {
  
  wrapper: {
    flex: 1,
    marginTop: 150,
  },
  submitButton: {
    paddingHorizontal: 10,
    paddingTop: 20,
  },
  cardImage_h:{
    height: 80,flex: 1
  },

  footerbtn:{backgroundColor:'#fff', height: 45},
  outbtnfooter:{flex: 1, justifyContent: 'flex-start', alignItems: 'center'},
  btnfooter:{flex: 1, height: 45, justifyContent: 'center'},
  btnfooterText:{color:"#fff", fontSize: 16, textAlign:'center'},
  textForm:{
    textAlign: 'center', fontSize: 22,  fontWeight: 'bold', padding: 15,
  },

  color_blur:{color: '#2089dc'},

  layout_r_f1:{flexDirection: "row", flex: 1, justifyContent: 'space-between', },
  layout_c_f1:{flexDirection: "column", flex: 1, justifyContent: 'space-between', alignItems:'center'},

  w40:{width: 40}, w50:{width: 50}, w100:{width: 100}, w150:{width: 150}, w200:{width: 200}, w250:{width: 250}, w300:{width: 300},  w320:{width: 320},

  pdL5_100: {paddingLeft: '5%'}, 
  pdR5_100: {paddingRight: '5%'}, 

  mgB0:{marginBottom: 0}, mgB5:{marginBottom: 5}, mgB10:{marginBottom: 10}, mgB15:{marginBottom: 15}, mgB20:{marginBottom: 20}, 
  mgT50:{marginTop: 50},

  pdT0:{paddingTop: 0}, pdT5:{paddingTop: 5}, pdT10:{paddingTop: 10}, pdT15:{paddingTop: 15}, pdT20:{paddingTop: 20},
  pdB0:{paddingBottom: 0}, pdB5:{paddingBottom: 5}, pdB10:{paddingBottom: 10}, pdB15:{paddingBottom: 15}, pdB20:{paddingBottom: 20},
  pdL0:{paddingLeft: 0}, pdL5:{paddingLeft: 5}, pdL10:{paddingLeft: 10}, pdL15:{paddingLeft: 15}, pdL20:{paddingLeft: 20},
  pdR0:{paddingRight: 0}, pdR5:{paddingRight: 5}, pdR10:{paddingRight: 10}, pdR15:{paddingRight: 15}, pdR20:{paddingRight: 20},

  fSize16:{fontSize: 16}, fSize18:{fontSize: 18}, fSize20:{fontSize: 20}, fSize24:{fontSize: 24},

  textCenter:{textAlign:'center'}, textLeft:{textAlign:'left'}, textRight:{textAlign:'right'},

  flex1:{flex: 1}, jusbetween:{justifyContent:'space-between'}, 
  flex_column:{flexDirection:'column'}, flex_row:{flexDirection: 'row'}, aligI_center:{alignItems:'center'},
};

const stylesCamera = {
  rectangleContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    borderWidth: rectBorderWidth,
    borderColor: rectBorderColor,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: "center",
    alignItems: "center"
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor
  },

  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: scanBarColor
  }
};

const stylesToast={
  backgroundColor: "#2f4145",
  height: Platform.OS === ("ios") ? 70 : 125,
  color: "#ffffff",
  fontSize: 12,
  borderRadius: 15,
}

class ActiveBookingScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      ListEventArray: [],
    };
  }

  static navigationOptions = { header: null };

  componentDidMount = () => {
    this.loadPublicEvent();
  }

  refreshComponent() {
    this.loadPublicEvent();
  }

  async loadPublicEvent(){
    var self = this;
    var credentials = {'mtd': 4, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.setState({
          ListEventArray: response.data.data
        });
        self.loadPrivateEvent();
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  async loadPrivateEvent(){
    var self = this;
    var credentials = {'mtd': 5, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        var dataUnion = _.union(self.state.ListEventArray, response.data.data);
        self.setState({
          ListEventArray: dataUnion
        });
        self.loadCreatedEvent();
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  async loadCreatedEvent(){
    var self = this;
    var credentials = {'mtd': 6, token: this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        var dataUnion = _.union(self.state.ListEventArray, response.data.data);
        let arrayData = [];
        var today = moment(new Date()).format('YYYY-MM-DD');
        for (let eventObject of dataUnion) {
          var event_date = moment(eventObject.event_start,'YYYY-MM-DD');
          if(moment(event_date).isAfter(today) || moment(event_date).format('YYYY-MM-DD') == today){
            eventObject.start = moment(eventObject.event_start).format('h:mm a');
            eventObject.end = moment(eventObject.event_end).format('h:mm a');
            arrayData.push(eventObject);
          }
        }
        self.setState({
            dataSource: self.state.dataSource.cloneWithRows(_.sortBy(arrayData, ['event_start']))
        });
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  detailEvent(detail_event){
    this.props.screenProps.updateDetailEvent(detail_event);
    this.props.navigation.navigate("Details",{detail_event: detail_event, refreshComponent: () => this.refreshComponent()});
  }

  taoHang(data,navigate){
    return (
      <View style={{paddingLeft:10, paddingRight:10, paddingTop:10}} >
        <TouchableOpacity  onPress={() => this.detailEvent(data)}>
          <Card>
            <CardItem  >
              <Body>
                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Icon style={{width:40}} name='event' type='SimpleLineIcons'/>
                    <Text style={{fontSize:18, color: '#000'}}>
                      {data.event_name}
                    </Text>
                  </View>
                </View>

                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Icon style={{width:40}} name='location-city' type='MaterialIcons'/>
                    <Text style={{fontSize:18, color: '#000'}}>
                     {data.location_name}
                    </Text>
                  </View>
                </View>

                <View style={{flexDirection: "row", flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flex:1}}>
                    <View style={{flexDirection: "row", flex: 1, justifyContent: 'flex-start', alignItems: 'center',}}>
                      <Icon style={{width:40}} name='calendar' type='Octicons'/>
                      <Text style={{fontSize:18, color: '#000'}}>
                        {moment(data.event_start).format('MM/DD/YYYY')}
                      </Text>
                    </View>
                  </View>
                  
                  <View style={{flex:1}}>
                    <View style={{flexDirection: "row", flex: 1, justifyContent: 'flex-start', alignItems: 'center',}}>
                        <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
                        <Text style={{fontSize:18, color: '#000'}}>
                          {data.start}
                        </Text>
                    </View>
                  </View>
                </View>

                <View style={{flexDirection: "row", flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flex: 1}}>
                    <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                      <Icon style={{width:40}} name='calendar' type='Octicons'/>
                      <Text style={{fontSize:18, color: '#000'}}>
                        {moment(data.event_end).format('MM/DD/YYYY')}
                      </Text>
                    </View>
                  </View>
                  
                  <View style={{flex: 1}}>
                    <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                      <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
                      <Text style={{fontSize:18, color: '#000'}} >
                        {data.end}
                      </Text>
                    </View>
                  </View>
                </View>
              </Body>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </View>
    );
  }



  render(){
    const { navigate } = this.props.navigation;
    return(
      <Container>
        
        <View style={{flexDirection: "row", justifyContent: "space-between", backgroundColor: '#529ff3', height: 50, paddingTop: 10}}>
          <View style={{width: 50}}>
            <Button onPress={() => this.props.navigation.openDrawer()} style={{width: 100, height: 50, marginTop: -10, padding: 0}} transparent> 
              <Icon style={{color:'#fff', }} name='navicon' type='FontAwesome' />
            </Button>
          </View>
          <View style={{flex: 1,}}>
            <Text  style={{color: '#fff', fontSize: 20, fontWeight: 'bold', textAlign: 'center'}}>Active booking</Text>
          </View>
          <View style={{width: 50}}></View>
        </View>

        <Content style={{flex: 1}}>
          <View style={{paddingBottom:10, marginTop: 20,}}>
            <ListView  dataSource={this.state.dataSource}
              enableEmptySections
              renderRow={data => {
                return this.taoHang(data,navigate)
              }}
            />
          </View>
        </Content>
          
        <Fab 
          style={{ backgroundColor:'rgb(100,121,133)' }}
          position="bottomRight" >
            <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} rippleDuration = {400}
              style={{height: 56, width: 56, borderRadius: 100/2, justifyContent: 'center', alignItems: 'center'}}
              onPress={() => {this.props.navigation.navigate("New", { refreshComponent: () => this.refreshComponent() })}} >
              <Icon style={{color: '#fff'}} name="add" />
            </Ripple>
        </Fab>

      </Container>
     
      
    );
  }
}

class PastBookingScreen extends React.Component {
  static navigationOptions = { header: null };

  constructor(props){
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      ListEventArray: [],
    };
  }

  componentDidMount = () => {
    this.loadPublicEvent();
  }

  refreshComponent() {
    this.loadPublicEvent();
  }

  async loadPublicEvent(){
    var self = this;
    var credentials = {'mtd': 4, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.setState({
          ListEventArray: response.data.data
        });
        self.loadPrivateEvent();
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  async loadPrivateEvent(){
    var self = this;
    var credentials = {'mtd': 5, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        var dataUnion = _.union(self.state.ListEventArray, response.data.data);
        self.setState({
          ListEventArray: dataUnion
        });
        self.loadCreatedEvent();
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  async loadCreatedEvent(){
    var self = this;
    var credentials = {'mtd': 6, token: this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        let arrayData = [];
        var today = moment(new Date()).format('YYYY-MM-DD');
        for (let eventObject of response.data.data) {
          var event_date = moment(eventObject.event_start,'YYYY-MM-DD');
          if(moment(event_date).isBefore(today)){
            eventObject.start = moment(eventObject.event_start).format('h:mm a');
            eventObject.end = moment(eventObject.event_end).format('h:mm a');
            arrayData.push(eventObject);
          }
        }
        self.setState({
            dataSource: self.state.dataSource.cloneWithRows(_.sortBy(arrayData, ['-event_start']))
        });
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  detailEvent(detail_event){
    this.props.screenProps.updateDetailEvent(detail_event);
    this.props.navigation.navigate("Details2",{detail_event: detail_event, refreshComponent: () => this.refreshComponent()});
  }

  taoHang(data,navigate){
    return (
      <View style={{paddingLeft:10, paddingRight:10, paddingTop:10}} >
        <TouchableOpacity  onPress={() => this.detailEvent(data)}>
          <Card>

            <CardItem  >
              <Body>

                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Icon style={{width:40}} name='event' type='SimpleLineIcons'/>
                    <Text style={{fontSize:18, color: '#000'}}>
                      {data.event_name}
                    </Text>
                  </View>
                </View>

                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Icon style={{width:40}} name='location-city' type='MaterialIcons'/>
                    <Text style={{fontSize:18, color: '#000'}}>
                     {data.location_name}
                    </Text>
                  </View>
                </View>

                <View style={{flexDirection: "row", flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flex:1}}>
                    <View style={{flexDirection: "row", flex: 1, justifyContent: 'flex-start', alignItems: 'center',}}>
                      <Icon style={{width:40}} name='calendar' type='Octicons'/>
                      <Text style={{fontSize:18, color: '#000'}}>
                        {moment(data.event_start).format('MM/DD/YYYY')}
                      </Text>
                    </View>
                  </View>
                  
                  <View style={{flex:1}}>
                    <View style={{flexDirection: "row", flex: 1, justifyContent: 'flex-start', alignItems: 'center',}}>
                        <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
                        <Text style={{fontSize:18, color: '#000'}}>
                          {data.start}
                        </Text>
                    </View>
                  </View>
                </View>

                <View style={{flexDirection: "row", flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flex: 1}}>
                    <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                      <Icon style={{width:40}} name='calendar' type='Octicons'/>
                      <Text style={{fontSize:18, color: '#000'}}>
                        {moment(data.event_end).format('MM/DD/YYYY')}
                      </Text>
                    </View>
                  </View>
                  
                  <View style={{flex: 1}}>
                    <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                      <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
                      <Text style={{fontSize:18, color: '#000'}} >
                        {data.end}
                      </Text>
                    </View>
                  </View>
                </View>

              </Body>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </View>);
  }

  render(){
    const { navigate } = this.props.navigation;
    return(
      <Container>
        <View style={{flexDirection: "row", justifyContent: "space-between", backgroundColor: '#529ff3', height: 50, paddingTop: 10}}>
          <View style={{width: 50}}>
            <Button onPress={() => this.props.navigation.openDrawer()} style={{width: 100, height: 50, marginTop: -10, padding: 0}} transparent> 
              <Icon style={{color:'#fff', }} name='navicon' type='FontAwesome' />
            </Button>
          </View>
          <View style={{flex: 1,}}>
            <Text  style={{color: '#fff', fontSize: 20, fontWeight: 'bold', textAlign: 'center'}}>Past booking</Text>
          </View>
          <View style={{width: 50}}></View>
        </View>

        <Content>
          <View style={{paddingBottom:10, marginTop: 20, flex: 1}} >
            <ListView  dataSource={this.state.dataSource}
              enableEmptySections 
              renderRow={data => {
                return this.taoHang(data,navigate)
              }}
            />
          </View>
        </Content>
        
        <Fab 
          style={{ backgroundColor:'rgb(100,121,133)' }}
          position="bottomRight" >
            <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} rippleDuration = {400}
              style={{height: 56, width: 56, borderRadius: 100/2, justifyContent: 'center', alignItems: 'center'}}
              onPress={() => {this.props.navigation.navigate("New", { refreshComponent: () => this.refreshComponent() })}} >
              <Icon style={{color: '#fff'}} name="add" />
            </Ripple>
        </Fab>
      </Container>
    );
  }
}

const ActiveBookingStack = createStackNavigator({
  ActiveBooking: ActiveBookingScreen,
});

const PastBookingStack = createStackNavigator({
  PastBooking: PastBookingScreen,
});


const BookingTab = createMaterialTopTabNavigator(
  {
    ActiveBooking: { 
      screen: ActiveBookingStack,
      navigationOptions: {
        tabBarLabel: 'Active Booking',
      },  
    },
    PastBooking: { 
      screen: PastBookingStack,
      navigationOptions: {
        tabBarLabel: 'Past Booking',
      },

    }

  }, 
  {
    initialRouteName: 'ActiveBooking',
    tabBarPosition:'bottom',
  }
);

class CompletedOrdersScreen extends React.Component {
    static navigationOptions = {
        // title: "Completed Orders"
        header: null,
    }
    static router = BookingTab.router;
    render() {
        return(
         <BookingTab {...this.props} />
        );
    }
}

const mapStateToProps = (state) => ({
  reduxState:state
});

const mapDispatchToProps = (dispatch) => {
    return ({
        updateDetailEvent:(event_detail) => dispatch( updateDetailEvent(event_detail) ),
        updateQrCode:(location_list, qrcode_room) => dispatch( updateQrCode(location_list, qrcode_room) ),
    })
}

const mergeProps = (state, dispatch, ownProps) => {
    return ({
        ...ownProps,
     screenProps: {
          ...ownProps.screenProps,
          ...state,
          ...dispatch,
        }
  
    })
}
export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CompletedOrdersScreen);

// class AppointmentsIndexScreen extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       appointments: [],
//     }
//   }

//   static navigationOptions = {
//     title: 'Appointments',
//     headerLeft: (
//       <Button  transparent  onPress={() => alert('This is a button!')}><Icon name="menu"/></Button>),
//       headerStyle: {
//             backgroundColor: 'rgb(223, 63, 72)',
//         },    
//         headerTintColor: 'white',

//   };

  

//   render() {
    
//     return (
//       <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//         <Text>AppointmentsIndexScreen</Text>
//       </View>
      
//     );
//   }
// }

// const mapStateToProps = (state) => ({
// 	reduxState:state
// });
// export default connect(mapStateToProps, {
//   Auth,
//   updateAccessToken,
// })(AppointmentsIndexScreen);