import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image, 
  TouchableOpacity, TextInput, Switch, Dimensions, Platform, ListView, StatusBar } from 'react-native';
import { Container, Header, Content, Footer, Form, Item, Input, Label, 
  CardItem, Card, Button, Icon ,DatePicker, Picker, Left, Right, Body, Textarea } from 'native-base';
import GenerateForm from 'react-native-form-builder';
import Axios from 'axios';
import { connect } from 'react-redux';
import DateTimePicker from 'react-native-modal-datetime-picker';
import CheckBox from 'react-native-check-box';
import Auth from '../../redux/reducers/auth';
import { updateAccessToken, updateQrCodeRoom } from '../../redux/actions/auth';
import { Url } from '../../../config/api/credentials';
import QRCodeScanner from 'react-native-qrcode-scanner';
import MultiSelect from 'react-native-multiple-select';
import { updateDetailEvent} from '../../redux/actions/auth';

// import Toast from 'react-native-toast-native';
import Toast from 'react-native-root-toast';
import Ripple from 'react-native-material-ripple';
import moment from 'moment';
import _ from 'lodash';
import Ionicons from "react-native-vector-icons/Ionicons";
import * as Animatable from "react-native-animatable";

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[statusbar.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

const statusbar = {
  container: {
    height: 20,
    width: '100%'
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#000',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#000'
  },
}

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

console.disableYellowBox = true;

const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency
const rectDimensions = SCREEN_WIDTH * 0.85; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.0025; // this is equivalent to 2 from a 393 device width
const scanBarWidth = SCREEN_WIDTH * 0.6; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const rectBorderColor = "#fff";
const scanBarColor = "red";  
const iconScanColor = "#fff";

const styles = {
  
  wrapper: {
    flex: 1,
    marginTop: 150,
  },
  submitButton: {
    paddingHorizontal: 10,
    paddingTop: 20,
  },
  cardImage_h:{
    height: 80,flex: 1
  },

  footerbtn:{backgroundColor:'#fff', height: 45},
  outbtnfooter:{flex: 1, justifyContent: 'flex-start', alignItems: 'center'},
  btnfooter:{flex: 1, height: 45, justifyContent: 'center'},
  btnfooterText:{color:"#fff", fontSize: 16, textAlign:'center'},
  textForm:{
    textAlign: 'center', fontSize: 22,  fontWeight: 'bold', padding: 15,
  },

  color_blur:{color: '#2089dc'},

  layout_r_f1:{flexDirection: "row", flex: 1, justifyContent: 'space-between', },
  layout_c_f1:{flexDirection: "column", flex: 1, justifyContent: 'space-between', alignItems:'center'},

  w40:{width: 40}, w50:{width: 50}, w100:{width: 100}, w150:{width: 150}, w200:{width: 200}, w250:{width: 250}, w300:{width: 300},  w320:{width: 320},

  pdL5_100: {paddingLeft: '5%'}, 
  pdR5_100: {paddingRight: '5%'}, 

  mgB0:{marginBottom: 0}, mgB5:{marginBottom: 5}, mgB10:{marginBottom: 10}, mgB15:{marginBottom: 15}, mgB20:{marginBottom: 20}, 
  mgT50:{marginTop: 50},

  pdT0:{paddingTop: 0}, pdT5:{paddingTop: 5}, pdT10:{paddingTop: 10}, pdT15:{paddingTop: 15}, pdT20:{paddingTop: 20},
  pdB0:{paddingBottom: 0}, pdB5:{paddingBottom: 5}, pdB10:{paddingBottom: 10}, pdB15:{paddingBottom: 15}, pdB20:{paddingBottom: 20},
  pdL0:{paddingLeft: 0}, pdL5:{paddingLeft: 5}, pdL10:{paddingLeft: 10}, pdL15:{paddingLeft: 15}, pdL20:{paddingLeft: 20},
  pdR0:{paddingRight: 0}, pdR5:{paddingRight: 5}, pdR10:{paddingRight: 10}, pdR15:{paddingRight: 15}, pdR20:{paddingRight: 20},

  fSize16:{fontSize: 16}, fSize18:{fontSize: 18}, fSize20:{fontSize: 20}, fSize24:{fontSize: 24},

  textCenter:{textAlign:'center'}, textLeft:{textAlign:'left'}, textRight:{textAlign:'right'},

  flex1:{flex: 1}, jusbetween:{justifyContent:'space-between'}, 
  flex_column:{flexDirection:'column'}, flex_row:{flexDirection: 'row'}, aligI_center:{alignItems:'center'},
};

const stylesCamera = {
  rectangleContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    borderWidth: rectBorderWidth,
    borderColor: rectBorderColor,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: "center",
    alignItems: "center"
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor
  },

  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: scanBarColor
  }
};

const stylesToast={
  backgroundColor: "#2f4145",
  height: Platform.OS === ("ios") ? 70 : 125,
  color: "#ffffff",
  fontSize: 12,
  borderRadius: 15,
}


class MBookAttendee extends React.Component {
  static navigationOptions = {
    header: null,
    // title: 'List Attendee',
    // headerStyle: {
    //   backgroundColor: '#529ff3',
    // },    
    // headerTintColor: 'white',
    // headerTitleStyle: { fontWeight: 'bold', alignSelf: 'center', textAlign: 'center', justifyContent: 'center', flex: 1, marginLeft: -40 },
  };


  constructor(props){
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(this.props.reduxState.Auth.event_detail.attendee),
    };
  }

  taoHang(data,navigate){
    return (
      <View style={{paddingLeft:10, paddingRight:10, paddingTop:10}} >
        <Card>
          <CardItem  >
            <Body>
              <View>
                <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                  <Text style={{fontSize: 24, color: '#d9534f'}}>{data.user_profile_name}</Text>
                </View>
                <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                  <Text style={{fontSize:14, color:'#5bc0de', width: 60}}>
                    Email:
                  </Text>
                  <Text style={{fontSize:20, color: '#000'}}>
                    {data.user_email}
                  </Text>
                </View>
                <View style={{width: '100%', flexDirection: "row", justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                    {/*<Checkbox
                      disabled={true}
                      checked={data.event_attendee_status == '1' ? true : false}
                      style={{borderColor: 'green', color:'green', borderRadius: 5}}
                    />
                    <Text style={{ marginLeft:10 ,color: 'green'}}>
                      Yes
                    </Text>*/}
                    <CheckBox
                      style={{flex: 1, borderRadius: 5}}
                      onClick={()=> console.log()}
                      isChecked={data.event_attendee_status == '1' ? true : false}
                      rightText={"Yes"}
                      rightTextStyle={{color:'green'}}
                      checkBoxColor={'green'}
                      disabled={true}
                    />
                    <CheckBox
                      style={{flex: 1, borderRadius: 5}}
                      onClick={()=> console.log()}
                      isChecked={data.event_attendee_status == '2' ? true : false}
                      rightText={"No"}
                      rightTextStyle={{color:'red'}}
                      checkBoxColor={'red'}
                      disabled={true}
                    />
                    <CheckBox
                      style={{flex: 1, borderRadius: 5}}
                      onClick={()=> console.log()}
                      isChecked={data.event_attendee_status == '0' ? true : false}
                      rightText={"Maybe"}
                      rightTextStyle={{color:'#0066ff'}}
                      checkBoxColor={'#0066ff'}
                      disabled={true}
                    />
                  </View>
                </View>
              </View>
            </Body>
          </CardItem>
        </Card>
      </View>);
  }
  render(){
    const { navigate } = this.props.navigation;
    return(
      <Container>
        <View style={statusbar.container}>
          <MyStatusBar backgroundColor="#000" barStyle="light-content" />
          <View style={statusbar.appBar} />
          <View style={statusbar.content} />
        </View>
        <View style={{flexDirection: "row", justifyContent: "space-between", backgroundColor: '#529ff3', height: 58,
          shadowColor: '#494949', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 1, paddingLeft: 10, paddingRight: 10}}>
          <View style={{width: 40, justifyContent:'center', alignItems:'center'}}>
            <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
            onPress={() => this.props.navigation.goBack()} >
              <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                <Icon style={{color:'#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
              </Button>
            </Ripple>
          </View>
         
          <View style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
            <Text style={{fontSize: 20, color:'#fff', fontWeight: 'bold', textAlign: 'center'}}>
              List Attendee
            </Text>
          </View>

          <View style={{width: 40, justifyContent:'center', alignItems:'center'}}>
          
          </View>
        </View>
        <Content>
          <ListView  dataSource={this.state.dataSource}
            enableEmptySections
            renderRow={data => {
              return this.taoHang(data,navigate)
            }}
          />
          
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  reduxState:state
});

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

export default connect(mapStateToProps, {
  Auth,
  updateAccessToken,
})(MBookAttendee);