import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image, TouchableOpacity, ListView, Alert, Dimensions, Platform, StatusBar,NetInfo } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Card, CardItem, Thumbnail, Left, Body, Right, Form, 
  Item, Input, Label, Button, Icon ,DatePicker, List, ListItem, Fab, Picker, Textarea, Footer, Switch } from 'native-base';

import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import Axios from 'axios';
import { connect } from 'react-redux';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import _ from 'lodash';
import Auth from '../../redux/reducers/auth';
import { updateAccessToken, updateDetailEvent, updateQrCode } from '../../redux/actions/auth';
import { Url } from '../../../config/api/credentials';
import QRCodeScanner from 'react-native-qrcode-scanner';
import MultiSelect from 'react-native-multiple-select';
import Toast from 'react-native-root-toast';
import { TabNavigator, StackNavigator,createMaterialTopTabNavigator,createStackNavigator } from 'react-navigation';
import CheckBox from 'react-native-check-box';
import Ionicons from "react-native-vector-icons/Ionicons";
import * as Animatable from "react-native-animatable";
import Ripple from 'react-native-material-ripple';
import AppointmentCard from '../../components/appointments/Card';

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[statusbar.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

const statusbar = {
  container: {
    height: 20,
    width: '100%'
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#000',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#000'
  },
}

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

console.disableYellowBox = true;

const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency
const rectDimensions = SCREEN_WIDTH * 0.85; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.0025; // this is equivalent to 2 from a 393 device width
const scanBarWidth = SCREEN_WIDTH * 0.6; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const rectBorderColor = "#fff";
const scanBarColor = "red";  
const iconScanColor = "#fff";


import { MenuContext, Menu, MenuOptions, MenuOption, MenuTrigger, MenuProvider, renderers } from 'react-native-popup-menu';
// import styles from '../../stylecss/style.css';
const styles = {
  
  wrapper: {
    flex: 1,
    marginTop: 150,
  },
  submitButton: {
    paddingHorizontal: 10,
    paddingTop: 20,
  },
  cardImage_h:{
    height: 80,flex: 1
  },

  footerbtn:{backgroundColor:'#fff', height: 45},
  outbtnfooter:{flex: 1, justifyContent: 'flex-start', alignItems: 'center'},
  btnfooter:{flex: 1, height: 45, justifyContent: 'center'},
  btnfooterText:{color:"#fff", fontSize: 16, textAlign:'center'},
  textForm:{
    textAlign: 'center', fontSize: 22,  fontWeight: 'bold', padding: 15,
  },

  color_blur:{color: '#2089dc'},

  layout_r_f1:{flexDirection: "row", flex: 1, justifyContent: 'space-between', },
  layout_c_f1:{flexDirection: "column", flex: 1, justifyContent: 'space-between', alignItems:'center'},

  w40:{width: 40}, w50:{width: 50}, w100:{width: 100}, w150:{width: 150}, w200:{width: 200}, w250:{width: 250}, w300:{width: 300},  w320:{width: 320},

  pdL5_100: {paddingLeft: '5%'}, 
  pdR5_100: {paddingRight: '5%'}, 

  mgB0:{marginBottom: 0}, mgB5:{marginBottom: 5}, mgB10:{marginBottom: 10}, mgB15:{marginBottom: 15}, mgB20:{marginBottom: 20}, 
  mgT50:{marginTop: 50},

  pdT0:{paddingTop: 0}, pdT5:{paddingTop: 5}, pdT10:{paddingTop: 10}, pdT15:{paddingTop: 15}, pdT20:{paddingTop: 20},
  pdB0:{paddingBottom: 0}, pdB5:{paddingBottom: 5}, pdB10:{paddingBottom: 10}, pdB15:{paddingBottom: 15}, pdB20:{paddingBottom: 20},
  pdL0:{paddingLeft: 0}, pdL5:{paddingLeft: 5}, pdL10:{paddingLeft: 10}, pdL15:{paddingLeft: 15}, pdL20:{paddingLeft: 20},
  pdR0:{paddingRight: 0}, pdR5:{paddingRight: 5}, pdR10:{paddingRight: 10}, pdR15:{paddingRight: 15}, pdR20:{paddingRight: 20},

  fSize16:{fontSize: 16}, fSize18:{fontSize: 18}, fSize20:{fontSize: 20}, fSize24:{fontSize: 24},

  textCenter:{textAlign:'center'}, textLeft:{textAlign:'left'}, textRight:{textAlign:'right'},

  flex1:{flex: 1}, jusbetween:{justifyContent:'space-between'}, 
  flex_column:{flexDirection:'column'}, flex_row:{flexDirection: 'row'}, aligI_center:{alignItems:'center'},
};

const stylesCamera = {
  rectangleContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    borderWidth: rectBorderWidth,
    borderColor: rectBorderColor,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: "center",
    alignItems: "center"
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor
  },

  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: scanBarColor
  }
};

const stylesToast={
  backgroundColor: "#2f4145",
  height: Platform.OS === ("ios") ? 70 : 125,
  color: "#ffffff",
  fontSize: 12,
  borderRadius: 15,
}

class ActiveBookingScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      ListEventArray: [],
      isLoading: false,
      connection_Status : "",
      counter : 0,
    };
  }

  static navigationOptions = { header: null };

  componentDidMount = () => {
      NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true){ 
        this.setState({connection_Status : "Online"});
        this.loadPublicEvent();
      }
      else
      {
       
        this.setState({connection_Status : "Offline"})
      }

    });
   
  }

   componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );

  }

   /***
  Start Status connection
  ***/

    _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"});
        Toast.show('CONNECTION TO HOST RESUME.');
        this.loadPublicEvent();
      }
      else
      {
        Toast.show('Connection to host lost.');
        this.setState({connection_Status : "Offline"})
      }
    };

    connectionLost(self){
        self.setState({ isLoading: false })
        self.setState({counter: self.state.counter+1 });
       if(self.state.counter>=3){
            Toast.show('CONNECTION LOST');
       }
        
     }

     noInternet(self){
       if(self.state.connection_Status=="Online"){
         return true
       }else{
         Toast.show('Connection to host lost.');
         return false;
       }
     }


  /***
 End Status connection
  ***/
  refreshComponent() {
    this.loadPublicEvent();
  }

  async loadPublicEvent(){
    var self = this;
     /*if(!self.noInternet(self)){
       self.setState({
       disableSubmit: false,
       isLoading: false
       });
      return;
    }*/
    var credentials = {'mtd': 16, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      self.setState({counter: 0 });
      if(response.data.status == 3){
        Toast.show('Token expired.');
       self.props.navigation.navigate('Auth');
       return;   
      }
      if(response.data.status == 1){
        self.setState({
          ListEventArray: response.data.data
        });
        self.loadPrivateEvent();
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
      self.connectionLost(self);
      console.log(error);
    });
  }

  async loadPrivateEvent(){
    var self = this;
      if(!self.noInternet(self)){
       self.setState({
       disableSubmit: false,
       isLoading: false
       });
      return;
    }
    var credentials = {'mtd': 5, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      self.setState({counter: 0 });
      if(response.data.status == 3){
        Toast.show('Token expired.');
       self.props.navigation.navigate('Auth');
       return;   
      }
      if(response.data.status == 1){
        var dataUnion = _.union(self.state.ListEventArray, response.data.data);
        self.setState({
          ListEventArray: dataUnion
        });
        self.loadCreatedEvent();
      }else{
        console.log('Get Private Event Fail!');
      }
    })
    .catch(function (error) {
      self.connectionLost(self);
      console.log(error);
    });
  }

  async loadCreatedEvent(){
    var self = this;
    if(!self.noInternet(self)){
       self.setState({
       disableSubmit: false,
       isLoading: false
       });
      return;
    }
    var credentials = {'mtd': 6, token: this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      self.setState({counter: 0 });
      if(response.data.status == 3){
        Toast.show('Token expired.');
        self.props.navigation.navigate('Auth');
        return;   
      }
      if(response.data.status == 1){
        var dataUnion = _.union(self.state.ListEventArray, response.data.data);
        let arrayData = [];
        var today = moment(new Date()).format('YYYY-MM-DD');
        for (let eventObject of dataUnion) {
          var event_date = moment(eventObject.event_start,'YYYY-MM-DD');
          if(moment(event_date).isAfter(today) || moment(event_date).format('YYYY-MM-DD') == today){
            eventObject.start = moment(eventObject.event_start).format('h:mm a');
            eventObject.end = moment(eventObject.event_end).format('h:mm a');
            arrayData.push(eventObject);
          }
        }
        self.setState({
            dataSource: self.state.dataSource.cloneWithRows(_.sortBy(arrayData, [function(o) { return moment(o.event_start); }]))
        });
      }else{
        console.log('Get Created Event Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      self.connectionLost(self);
      console.log(error);
    });
  }

  detailEvent(detail_event){
    this.props.screenProps.updateDetailEvent(detail_event);
    this.props.navigation.navigate("MShow",{detail_event: detail_event, refreshComponent: () => this.refreshComponent()});
  }

  taoHang(data,navigate){
    return (
      <View style={{paddingLeft:10, paddingRight:10, paddingTop:10}} >
        <TouchableOpacity  onPress={() => this.detailEvent(data)}>
          <Card>
            <CardItem  >
              <Body>
                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Icon style={{width:40}} name='event' type='SimpleLineIcons'/>
                    <Text style={{fontSize:18, color: '#000'}}>
                      {data.event_name}
                    </Text>
                  </View>
                </View>

                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Icon style={{width:40}} name='location-city' type='MaterialIcons'/>
                    <Text style={{fontSize:18, color: '#000'}}>
                     {data.location_name}
                    </Text>
                  </View>
                </View>

                <View style={{flexDirection: "row", flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flex:1}}>
                    <View style={{flexDirection: "row", flex: 1, justifyContent: 'flex-start', alignItems: 'center',}}>
                      <Icon style={{width:40}} name='calendar' type='Octicons'/>
                      <Text style={{fontSize:18, color: '#000'}}>
                        {moment(data.event_start).format('MM/DD/YYYY')}
                      </Text>
                    </View>
                  </View>
                  
                  <View style={{flex:1}}>
                    <View style={{flexDirection: "row", flex: 1, justifyContent: 'flex-start', alignItems: 'center',}}>
                        <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
                        <Text style={{fontSize:18, color: '#000'}}>
                          {data.start}
                        </Text>
                    </View>
                  </View>
                </View>

                <View style={{flexDirection: "row", flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flex: 1}}>
                    <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                      <Icon style={{width:40}} name='calendar' type='Octicons'/>
                      <Text style={{fontSize:18, color: '#000'}}>
                        {moment(data.event_end).format('MM/DD/YYYY')}
                      </Text>
                    </View>
                  </View>
                  
                  <View style={{flex: 1}}>
                    <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                      <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
                      <Text style={{fontSize:18, color: '#000'}} >
                        {data.end}
                      </Text>
                    </View>
                  </View>
                </View>
              </Body>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </View>
    );
  }



  render(){
    const { navigate } = this.props.navigation;
    return(
      <Container>
        
        <Content>
          <View style={{paddingBottom:10, marginTop: 10,}}>
            <ListView  dataSource={this.state.dataSource}
              enableEmptySections
              renderRow={data => {
                return this.taoHang(data,navigate)
              }}
            />
          </View>

        </Content>
        <Fab 
          style={{ backgroundColor:'rgb(100,121,133)'}}
          position="bottomRight" >
            <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} rippleDuration = {400}
              style={{height: 56, width: 56, borderRadius: 100/2, justifyContent: 'center', alignItems: 'center'}}
              onPress={() => {this.props.navigation.navigate("MNew", { refreshComponent: () => this.refreshComponent() })}} >
              <Icon style={{color: '#fff'}} name="add" />
            </Ripple>
        </Fab>
      </Container>
    );
  }
}

class PastBookingScreen extends React.Component {
  static navigationOptions = { header: null };

  constructor(props){
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      ListEventArray: [],
      isLoading: false,
      connection_Status : "",
      counter : 0,
    };
  }

  componentDidMount = () => {
   NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {
      if(isConnected == true){
        this.setState({connection_Status : "Online"});
        this.loadPublicEvent();
      }
      else
      {
       
        this.setState({connection_Status : "Offline"})
      }

    });
   
  }

    componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );

  }
 /***
  Start Status connection
  ***/

    _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
        Toast.show('CONNECTION TO HOST RESUME.');
        this.loadPublicEvent();
        
      }
      else
      {
        Toast.show('Connection to host lost.');
        this.setState({connection_Status : "Offline"})
      }
    };

    connectionLost(self){
        self.setState({ isLoading: false })
        self.setState({counter: self.state.counter+1 });
       if(self.state.counter>=3){
            Toast.show('CONNECTION LOST');
       }
        
     }

     noInternet(self){
       if(self.state.connection_Status=="Online"){
         return true
       }else{
         Toast.show('Connection to host lost.');
         return false;
       }
     }


  /***
 End Status connection
  ***/


  refreshComponent() {
    this.loadPublicEvent();
  }

  async loadPublicEvent(){
    var self = this;
    var credentials = {'mtd': 16, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      self.setState({counter: 0 });
      if(response.data.status == 3){
        Toast.show('Token expired.');
        self.props.navigation.navigate('Auth');
        return;   
      }
      if(response.data.status == 1){
        self.setState({
          ListEventArray: response.data.data
        });
        self.loadPrivateEvent();
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
       self.connectionLost(self);
      console.log(error);
    });
  }

  async loadPrivateEvent(){
    var self = this;
     if(!self.noInternet(self)){
       self.setState({
       disableSubmit: false,
       isLoading: false
       });
      return;
    }
    var credentials = {'mtd': 5, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      self.setState({counter: 0 });
      if(response.data.status == 3){
        Toast.show('Token expired.');
        self.props.navigation.navigate('Auth');
        return;   
      }
      if(response.data.status == 1){
        var dataUnion = _.union(self.state.ListEventArray, response.data.data);
        self.setState({
          ListEventArray: dataUnion
        });
        self.loadCreatedEvent();
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
      self.connectionLost(self);
      console.log(error);
    });
  }

  async loadCreatedEvent(){
    var self = this;
    if(!self.noInternet(self)){
       self.setState({
       disableSubmit: false,
       isLoading: false
       });
      return;
    }
    var credentials = {'mtd': 6, token: this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
       self.setState({counter: 0 });
       if(response.data.status == 3){
        Toast.show('Token expired.');
        self.props.navigation.navigate('Auth');
        return;   
      }
      if(response.data.status == 1){
        let arrayData = [];
        var today = moment(new Date()).format('YYYY-MM-DD');
        for (let eventObject of response.data.data) {
          var event_date = moment(eventObject.event_start,'YYYY-MM-DD');
          if(moment(event_date).isBefore(today)){
            eventObject.start = moment(eventObject.event_start).format('h:mm a');
            eventObject.end = moment(eventObject.event_end).format('h:mm a');
            arrayData.push(eventObject);
          }
        }
        self.setState({
            dataSource: self.state.dataSource.cloneWithRows(_.sortBy(arrayData, [function(o) { return - moment(o.event_start); }]))
        });
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      self.connectionLost(self);
      console.log(error);
    });
  }

  detailEvent(detail_event){
    this.props.screenProps.updateDetailEvent(detail_event);
    this.props.navigation.navigate("MShow",{detail_event: detail_event, refreshComponent: () => this.refreshComponent()});
    // this.props.navigation.navigate("MShow",{detail_event: detail_event});
  }

  taoHang(data,navigate){
    return (
      <View style={{paddingLeft:10, paddingRight:10, paddingTop:10}} >
        <TouchableOpacity  onPress={() => this.detailEvent(data)}>
          <Card>

            <CardItem  >
              <Body>

                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Icon style={{width:40}} name='event' type='SimpleLineIcons'/>
                    <Text style={{fontSize:18, color: '#000'}}>
                      {data.event_name}
                    </Text>
                  </View>
                </View>

                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Icon style={{width:40}} name='location-city' type='MaterialIcons'/>
                    <Text style={{fontSize:18, color: '#000'}}>
                     {data.location_name}
                    </Text>
                  </View>
                </View>

                <View style={{flexDirection: "row", flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flex:1}}>
                    <View style={{flexDirection: "row", flex: 1, justifyContent: 'flex-start', alignItems: 'center',}}>
                      <Icon style={{width:40}} name='calendar' type='Octicons'/>
                      <Text style={{fontSize:18, color: '#000'}}>
                        {moment(data.event_start).format('MM/DD/YYYY')}
                      </Text>
                    </View>
                  </View>
                  
                  <View style={{flex:1}}>
                    <View style={{flexDirection: "row", flex: 1, justifyContent: 'flex-start', alignItems: 'center',}}>
                        <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
                        <Text style={{fontSize:18, color: '#000'}}>
                          {data.start}
                        </Text>
                    </View>
                  </View>
                </View>

                <View style={{flexDirection: "row", flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flex: 1}}>
                    <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                      <Icon style={{width:40}} name='calendar' type='Octicons'/>
                      <Text style={{fontSize:18, color: '#000'}}>
                        {moment(data.event_end).format('MM/DD/YYYY')}
                      </Text>
                    </View>
                  </View>
                  
                  <View style={{flex: 1}}>
                    <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                      <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
                      <Text style={{fontSize:18, color: '#000'}} >
                        {data.end}
                      </Text>
                    </View>
                  </View>
                </View>

              </Body>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </View>);
  }

  render(){
    const { navigate } = this.props.navigation;
    return(
      <Container>
        <Content>
          <View style={{paddingBottom:10, marginTop: 10, flex: 1}} >
            <ListView  dataSource={this.state.dataSource}
              enableEmptySections 
              renderRow={data => {
                return this.taoHang(data,navigate)
              }}
            />
          </View>
        </Content>

        <Fab 
          style={{ backgroundColor:'rgb(100,121,133)'}}
          position="bottomRight" >
            <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} rippleDuration = {400}
              style={{height: 56, width: 56, borderRadius: 100/2, justifyContent: 'center', alignItems: 'center'}}
              onPress={() => {this.props.navigation.navigate("MNew", { refreshComponent: () => this.refreshComponent() })}} >
              <Icon style={{color: '#fff'}} name="add" />
            </Ripple>
        </Fab>
        
      </Container>
    );
  }
}

const ActiveBookingStack = createStackNavigator({
  ActiveBooking: ActiveBookingScreen,
});

const PastBookingStack = createStackNavigator({
  PastBooking: PastBookingScreen,
});


const BookingTab = createMaterialTopTabNavigator(
  {
    ActiveBooking: { 
      screen: ActiveBookingStack,
      navigationOptions: {
        tabBarLabel: 'Active Booking',
      },
    },
    PastBooking: { 
      screen: PastBookingStack,
      navigationOptions: {
        tabBarLabel: 'Past Booking',
      },
    }

  }, 
  {
    initialRouteName: 'ActiveBooking',
    // tabBarPosition:'bottom',
    tabBarPosition:'top',
    
    tabBarOptions:{
      activeTintColor: 'red',
     activeBackgroundColor: 'blue',
     inactiveTintColor: '#666',
      style: {
         backgroundColor: '#fff',
        // top: 50,
       
      },

      tabBarSelectedItemStyle: {
        borderBottomWidth: 2,
        borderBottomColor: '#494949',
      },

      labelStyle:{
        color:'#494949',
      },
      
    },

    
  }
);

class CompletedOrdersScreen extends React.Component {
    static navigationOptions = {
        header: null,
    }
    static router = BookingTab.router;
    render() {
        return(
           <Container>
              <View style={statusbar.container}>
                <MyStatusBar backgroundColor="#000" barStyle="light-content" />
                <View style={statusbar.appBar} />
                <View style={statusbar.content} />
              </View>
              <View style={{flexDirection: "row", justifyContent: "space-between", backgroundColor: '#529ff3', height: 58,
                shadowColor: '#494949', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 1, paddingLeft: 10, paddingRight: 10}}>
                <View style={{width: 50, justifyContent:'center', alignItems:'center'}}>
                  <Button transparent onPress={() => this.props.navigation.openDrawer()}
                   style={{justifyContent:'center', alignItems:'center', width:60,paddingLeft: 0, paddingRight: 0, marginLeft: -5}}>
                    <Icon style={{color:'#fff', }} name='navicon' type='FontAwesome' />
                  </Button>
                </View>

                <View style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
                  <Text  style={{color: '#fff', fontSize: 20, fontWeight: 'bold', textAlign: 'center', width: '100%'}}>My booking</Text>
                </View>
                <View style={{width: 50, justifyContent:'center', alignItems:'center'}}>
                  
                </View>
              </View>
              
              <BookingTab {...this.props} />
           </Container>
          
        );
    }
}

const mapStateToProps = (state) => ({
  reduxState:state
});

const mapDispatchToProps = (dispatch) => {
    return ({
        updateDetailEvent:(event_detail) => dispatch( updateDetailEvent(event_detail) ),
        updateQrCode:(location_list, qrcode_room) => dispatch( updateQrCode(location_list, qrcode_room) ),
    })
}

const mergeProps = (state, dispatch, ownProps) => {
    return ({
        ...ownProps,
     screenProps: {
          ...ownProps.screenProps,
          ...state,
          ...dispatch,
        }
  
    })
}

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;
export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CompletedOrdersScreen);
