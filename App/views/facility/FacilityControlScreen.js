import React from 'react';
import moment from 'moment'
import { Platform, SafeAreaView, StyleSheet, View, Text, ScrollView, Dimensions, NetInfo } from 'react-native';
import { Container, Body, CardItem, Button, Icon } from 'native-base';
import { ListItem } from 'native-base';
import Axios from 'axios';
import { connect } from 'react-redux';
import Ripple from 'react-native-material-ripple';
import ActionSheet from 'react-native-actionsheet';
import Toast from 'react-native-root-toast';

import { Url } from '../../../config/api/credentials';
import { parseEquipmentJson, parseEquipmentIopValue } from '../../redux/actions/facility';
import { updateDetailEvent } from '../../redux/actions/auth';

import MyStatusBar from '../../components/navigation/MyStatusBar';
import LoadCover from './LoadCover';
import checkFacilities from './checkFacilities';
import CollapseComponent from './CollapseComponent';
import {
  PointAnalogRange, PointBinaryStatus, PointMultistateStatus, PointUnavailable, PointReadOnly
} from '../../components/facility';
import { RenderIf } from '../../components/logic';
import { facility } from '../../../config/share/Global';

console.disableYellowBox = true;

// IR = Interval Refs

class FacilityControlScreen extends React.Component {

  static navigationOptions = {
    header: null
  }

  constructor() {
    super();

    this.onOffElements = [];
    this.state = {
      loadingAll: true,
      loadingAllText: '',
      timeLeft: '00:00:00',
      iopWriteData: {},
      needWrite: false,
      readyToRender: false,
      facilities: [],
    };

  }

  componentDidMount() {

    // (*) Listen for orientation change
    this.getOrientation();
    Dimensions.addEventListener('change', this.getOrientation);

    // (*) Check for online/offline status
    // WTF is the line below for?
    // `this._handleConnectivityChange` is UNDEFINED!
    // NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectivityChange);
    NetInfo.isConnected.fetch().done((isConnected) => {
      if (isConnected === true) {
        this.setState({ connection_Status: "Online" });
      }
      else {
        this.setState({ connection_Status: "Offline" });
      }
    });

    // (*) Fetch facility info
    const { detail_event, id: locationId } = this.props.navigation.state.params;
    const credentials = {
      'mtd': 17,
      'token': this.props.reduxState.Auth.token["access-token"],
      'id': detail_event ? detail_event.location_id : locationId
      // Location ID can be obtained from props if this screen is pushed from LocationScreen
    };
    Axios.post(Url.event, credentials, { headers: Object.assign(Url.headers, credentials) })
      .then(function (response) { // ON SUCCESS
        if (response.data.status === 3) {
          Toast.show('Token expired.');
          this.props.navigation.navigate('Auth');
          return;
        }
        if (response.data.status === 1) {
          facility.jsonData = response.data.data;
          this.props.parseEquipmentJson(facility.jsonData);
          this.getEquipmentIopValue();
        } else {
          console.log('mtd:17 Fail!');
        }
      }.bind(this)).catch(function (error) {
        console.log(error);
      });

    // (*) Update equipment values & Determine time left
    this.IR_timeLeft = setInterval(() => {

      if (detail_event) { // Count down not needed if it's not from an event
        const start = moment(), end = moment(detail_event.event_end)
        const diff = end.diff(start)
        const hours = Math.floor(moment.duration(diff).asHours())
        let timeLeft = hours + ':' + moment.utc(moment.duration(diff).asMilliseconds()).format("mm:ss")
        this.setState({ timeLeft });
      }

      if (this.state.needWrite === true) { this.writeEquipmentValues(); }

    }, 1000);

    // (*) Listen for Iop value change (migrated from `componentWillMount()`)
    this.IR_getEquipmentIopValue = setInterval(() => {
      if (this.state.needWrite !== true) {
        this.getEquipmentIopValue();
      }
    }, 3000)

  }

  componentWillUnmount() {
    // NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectivityChange);

    // Making sure intervals are cleared to avoid memory leak
    clearInterval(this.IR_getEquipmentIopValue);
    clearInterval(this.IR_timeLeft);
  }

  ////ngang doc portrait landscape
  getOrientation = () => {
    if (this.refs.rootView) {
      const { height, width } = Dimensions.get('window');
      this.setState({ orientation: width < height ? 'portrait' : 'landscape' });
    }
  }

  getEquipmentIopValue() {
    var params = {};
    this.props.reduxState.Facility.deviceConfigs.map(device => {
      var deviceObjects = this.props.reduxState.Facility.facilities.filter((item) => item.device_config.device_id === device.device_id)
      var subParams = {};

      deviceObjects.map((item) => {
        var iops = item.iop_infor.map((info) => {
          return { iops: info.data_iop.pointname };
        });
        subParams[item.objects_id] = iops;
      });
      params[device.device_id] = subParams;
    });

    const credentials = { 'mtd': 18, 'token': this.props.reduxState.Auth.token["access-token"], 'data': params };
    Axios.post(Url.event, credentials, { headers: Object.assign(Url.headers, credentials) })
      .then(function (response) { // ON SUCCESS
        if (response.data.status === 3) {
          Toast.show('Token expired.');
          this.props.navigation.navigate('Auth');
          return;
        }
        if (response.data.status === 1) {
          this.props.parseEquipmentIopValue(response.data.data);

          const shouldUnmountLoadCover = checkFacilities({
            status: this.state.loadingAllText,
            data: this.state.facilities,
            facilities: this.props.reduxState.Facility.facilities,
            userlookups: this.props.reduxState.Facility.userlookups,
            onlyCheckDeviceType: ['LK.PointBinaryStatus'],
            // Currently only need to check if all of this type of equipment is on/off
          })
          if (shouldUnmountLoadCover) {
            this.setState({ loadingAll: false, loadingAllText: '' });
          }

          this.setState({
            readyToRender: true,
            facilities: this.props.reduxState.Facility.facilities
          });
        } else {
          console.log('mtd:18 Fail!');
        }
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
  }

  renderObjectListItem(data, index) {
    if (this.state.readyToRender === false) {
      return (<View key={index} />);
    }

    let Arr = data.iop_infor.map((iop_info, nIndex) => {
      const lookupId = iop_info.data_iop.pointproperty;
      const lookup = this.props.reduxState.Facility.userlookups[lookupId];
      return this.renderIopType(data, iop_info, lookup, nIndex);
    });

    return (
      <CollapseComponent key={index} objectsName={data.objects_name} arr={Arr} />
    );
  }

  renderIopType(data, iop_info, lookup, index) {

    let renderType = '';
    const template = data.template_info.fn.find(e => e.io_point_name === iop_info.data_iop.pointname);

    if (typeof iop_info.value !== 'number') {
      // This forbids controllers from rendering without a value
      // This prevents, for instance, a binary control with 'on' and 'off' unchecked
      renderType = 'unavailable';
    } else if (typeof template === 'undefined') {
      // Forbids controller from rendering if template is undefined
      // The device is probably offline
      renderType = 'read-only';
    }

    // In some cases, data returned from server may cause `template` to be undefined
    let functionName; if (template) { functionName = template.function_name; }

    if (renderType === 'unavailable') {
      return (
        <ListItem style={styles.listItem} noBorder key={index}>
          <PointUnavailable pointName={iop_info.pointname} />
        </ListItem>
      );
    } else if (renderType === 'read-only') {
      return (
        <ListItem style={styles.listItem} noBorder key={index}>
          <PointReadOnly
            lookupType={lookup.type}
            data={lookup.list}
            value={iop_info.value}
            pointName={iop_info.pointname}
            deviceId={data.device_config.device_id}
            objectId={data.objects_id}
            pointname={iop_info.pointname}
            fnName={functionName}
          />
        </ListItem>
      )
    } else if (lookup.type === 'LK.PointBinaryStatus') {
      return (
        <ListItem style={styles.listItem} noBorder key={index}>
          <PointBinaryStatus
            data={lookup.list}
            value={iop_info.value}
            pointName={iop_info.pointname}
            deviceId={data.device_config.device_id}
            objectId={data.objects_id}
            fnName={functionName}
            onValueChanged={this.onValueChanged}
            onRef={ref => (this.onOffElements.push(ref))} />
        </ListItem>
      );
    } else if (lookup.type === 'LK.PointAnalogRange') {
      return (
        <ListItem style={styles.listItem} noBorder key={index}>
          <PointAnalogRange
            data={lookup.list}
            value={iop_info.value}
            pointName={iop_info.pointname}
            deviceId={data.device_config.device_id}
            objectId={data.objects_id}
            fnName={functionName}
            onValueChanged={this.onValueChanged}
          />
        </ListItem>
      );
    } else if (lookup.type === 'LK.PointMultistateStatus') {
      return (
        <ListItem style={styles.listItem} noBorder key={index}>
          <PointMultistateStatus
            data={lookup.list}
            value={iop_info.value}
            pointName={iop_info.pointname}
            deviceId={data.device_config.device_id}
            objectId={data.objects_id}
            fnName={functionName}
            onValueChanged={this.onValueChanged}
          />
        </ListItem>
      );
    } else {
      return (
        <ListItem style={styles.listItem} noBorder key={index}>
          <PointUnavailable pointName={iop_info.pointname} />
        </ListItem>
      );
    }
  }

  onValueChanged = data => {
    const fnName = data.fnName;
    const fnValue = data.fnValue;
    const deviceId = data.device_id;
    const objectId = data.objectId;

    let writeData = this.state.iopWriteData;
    let objectData = {};
    let fnArray = [];

    if (writeData[deviceId] != null) {
      objectData = writeData[deviceId];
    }

    if (objectData[objectId] != null) {
      fnArray = objectData[objectId].fn;
    }

    fnArray = fnArray.filter(e => e[fnName] === null);
    let fn = {};
    fn[fnName] = fnValue;
    fnArray.push(fn);

    let obj = { fn: fnArray };
    objectData[objectId] = obj;

    writeData[deviceId] = objectData;

    this.setState({ iopWriteData: writeData, needWrite: true });
  };

  onSetAllOn = () => {

    setTimeout(() => {
      this.setState({
        loadingAll: true,
        loadingAllText: 'ON_ALL'
      });
    });

    const newFacilities = this.state.facilities.map(facility => {
      const deviceId = facility.device_config.device_id;
      const objectId = facility.objects_id;

      var iops = facility.iop_infor.map(iop => {
        const template = facility.template_info.fn.find(e => e.io_point_name === iop.data_iop.pointname);
        // In some cases, data returned from server may cause `template` to be undefined
        let fnName; if (template) { fnName = template.function_name; }
        var value = iop.value;
        const lookupId = iop.data_iop.pointproperty;
        const lookup = this.props.reduxState.Facility.userlookups[lookupId];

        if (lookup.type === 'LK.PointBinaryStatus') {
          value = 1;

          const writeData = { device_id: deviceId, objectId: objectId, fnName: fnName, fnValue: value };
          this.onValueChanged(writeData);
        }

        return { ...iop, value: value };
      });

      return { ...facility, iop_infor: iops };
    });

    this.setState({ facilities: newFacilities });
    this.onOffElements.forEach(item => {
      item.setOn();
    });

  }

  onSetAllOff = () => {
    setTimeout(() => {
      this.setState({
        loadingAll: true,
        loadingAllText: 'OFF_ALL'
      });
    });

    const newFacilities = this.state.facilities.map(facility => {
      const deviceId = facility.device_config.device_id;
      const objectId = facility.objects_id;

      var iops = facility.iop_infor.map(iop => {
        const template = facility.template_info.fn.find(e => e.io_point_name === iop.data_iop.pointname);
        // In some cases, data returned from server may cause `template` to be undefined
        let fnName; if (template) { fnName = template.function_name; }
        var value = iop.value;
        const lookupId = iop.data_iop.pointproperty;
        const lookup = this.props.reduxState.Facility.userlookups[lookupId];

        if (lookup.type === 'LK.PointBinaryStatus') {
          value = 0;

          const writeData = { device_id: deviceId, objectId: objectId, fnName: fnName, fnValue: value };
          this.onValueChanged(writeData);
        }

        return { ...iop, value: value };
      });

      return { ...facility, iop_infor: iops };
    });

    this.setState({ facilities: newFacilities });
    this.onOffElements.forEach(item => {
      item.setOff();
    });
  }

  onTimeExtension = () => {
    this.ActionSheet.show()
  }

  doTimeExtend = (index) => {
    const { detail_event } = this.props.navigation.state.params;

    // detail_event will be undefined if navigated from LocationScreen instead
    // And since it's not an event, time cannot be extended as well
    if (!detail_event) { return false; }

    var type = index + 1;

    /// Temp code
    var date = moment(detail_event.event_end);
    var extendMinutes = 0;
    var newEvent = detail_event;
    if (type === 1) {
      extendMinutes = 5;
    } else if (type === 2) {
      extendMinutes = 10;
    } else {
      extendMinutes = 15;
    }
    const newDate = date.add(extendMinutes, 'minutes');

    newEvent.event_end = moment(newDate).format('YYYY-MM-DD HH:mm:ss');
    newEvent.end = moment(newDate).format('h:mm a');

    this.props.updateDetailEvent(newEvent);
    ////

    const credentials = { 'mtd': 20, 'token': this.props.reduxState.Auth.token["access-token"], 'id': detail_event.event_id, 'typ': type };
    Axios.post(Url.event, credentials, { headers: Object.assign(Url.headers, credentials) })
      .then(function (response) { // ON SUCCESS
        if (response.data.status === 3) {
          Toast.show('Token expired.');
          this.props.navigation.navigate('Auth');
          return;
        }
        if (response.data.status === 1) {
          var date = moment(detail_event.event_end);
          var extendMinutes = 0;
          var newEvent = detail_event;
          if (type === 1) {
            extendMinutes = 5;
          } else if (type === 2) {
            extendMinutes = 10;
          } else {
            extendMinutes = 15;
          }
          const newDate = date.add(extendMinutes, 'minutes');
          newEvent.event_end = moment(newDate).format('YYYY-MM-DD HH:mm:ss');
          this.props.updateDetailEvent(newEvent);
        } else {
          console.log('mtd:20 Fail!');
        }
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
  }

  writeEquipmentValues() {
    this.setState({ needWrite: false });
    const credentials = {
      'mtd': 19,
      'token': this.props.reduxState.Auth.token["access-token"],
      'data': this.state.iopWriteData
    };
    Axios.post(Url.event, credentials, { headers: Object.assign(Url.headers, credentials) })
      .then(function (response) { // ON SUCCESS
        if (response.data.status === 3) {
          Toast.show('Token expired.');
          this.props.navigation.navigate('Auth');
          return;
        }
        if (response.data.status === 1) {
          this.setState({ iopWriteData: {} });
          this.getEquipmentIopValue();
        } else {
          console.log('mtd:19 Fail!');
        }
      }.bind(this))
      .catch(function (error) {
        console.log(error);
      });
  }

  goBack = () => {
    const { navigation } = this.props
    try {
      navigation.state.params.refreshComponent();
    } catch (error) {
      // Suppress error if this screen is navigated from LocationList
    }
    navigation.goBack()
  }

  render() {

    const { loadingAll, loadingAllText, timeLeft } = this.state;
    const { detail_event, name: locationName } = this.props.navigation.state.params;
    const facilities = this.state.facilities.map((element, index) => {
      return this.renderObjectListItem(element, index);
    });

    return (
      <Container style={{ flexDirection: 'column', flex: 1 }}>

        {/* <MyStatusBar backgroundColor="black" /> */}

        {/* Navbar */}
        <View style={styles.navBar}>
          <SafeAreaView>
            <View style={styles.navBarSub}>

              {/* Navbar button */}
              <View style={styles.navBarBackBtnView}>
                <Ripple
                  rippleContainerBorderRadius={100 / 2}
                  rippleCentered={true}
                  style={{ width: 40, height: 40, }}
                  onPress={() => this.goBack()}
                >
                  <Button style={styles.navBarBackBtnButton} transparent>
                    <Icon style={styles.navBarBackBtnIcon} name='arrow-left' type='MaterialCommunityIcons' />
                  </Button>
                </Ripple>
              </View>

              {/* Navbar Title */}
              <View style={styles.navBarTitleView}>
                <Text children='Facility Control For' style={styles.navBarTitleText} />
                <Text
                  children={detail_event ? detail_event.location_name : locationName}
                  style={styles.navBarTitleText}
                />
              </View>

            </View>
          </SafeAreaView>
        </View>

        <View ref="rootView" style={{ flexDirection: 'column', padding: 10, flex: 1 }}>

          {/*
            Bad practice: Should be rendered using FlatList
            Currently heavily suffering from performance issues
            Read: https://reactjs.org/docs/optimizing-performance.html#virtualize-long-lists
          */}
          <ScrollView style={{ flex: 1, marginBottom: 8, color: 'black' }}>
            <View children={facilities} style={{ flexDirection: 'column' }} />
          </ScrollView>

          <View style={styles.onOffAllContainer}>

            <Button disabled={loadingAll} success onPress={this.onSetAllOn} style={{
              ...styles.onOffAllButton,
              opacity: loadingAll ? 0.5 : 1,
            }}>
              <Text style={styles.onOffAllButtonText}>On All</Text>
            </Button>

            <Button disabled={loadingAll} danger onPress={this.onSetAllOff} style={{
              ...styles.onOffAllButton,
              opacity: loadingAll ? 0.5 : 1,
            }}>
              <Text style={styles.onOffAllButtonText}>Off All</Text>
            </Button>

            {/* Extends time button should not appear */}
            {/* when this screen is not pushed from an event screen  */}
            <RenderIf condition={detail_event !== undefined}>
              <View style={{ flex: 1, marginLeft: 8 }}>
                <CardItem style={{ flex: 1, height: '100%', margin: 0, padding: 0 }}>
                  <Body style={{ flex: 1, flexDirection: 'row' }}>
                    <Button disabled={loadingAll} onPress={this.onTimeExtension}
                      style={{
                        backgroundColor: 'grey',
                        flex: 1,
                        opacity: loadingAll ? 0.5 : 1
                      }}>
                      <View style={styles.timeleftButtonView}>
                        <Text children={`Time left:${timeLeft}`} style={styles.timeleftButtonText} />
                        <Text children='Click to extend' style={styles.timeleftButtonText} />
                      </View>
                    </Button>
                  </Body>
                </CardItem>
              </View>
            </RenderIf>

          </View>
        </View>

        <ActionSheet
          ref={o => this.ActionSheet = o}
          title=''
          options={['Extend 5 mins', 'Extend 10 mins', 'Extend 15 mins', 'Cancel']}
          cancelButtonIndex={3}
          onPress={(index) => { this.doTimeExtend(index) }}
        />

        <RenderIf condition={loadingAll}>
          <LoadCover text={loadingAllText} />
        </RenderIf>

      </Container>
    );
  }
}

const styles = StyleSheet.create({

  // Navbar
  navBar: {
    backgroundColor: '#529ff3',
    elevation: 1,
    paddingTop: Platform.OS === 'android' ? 20 : 0,
    shadowColor: '#494949',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.75,
  },

  navBarSub: {
    flexDirection: 'row',
    height: 58,
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 10,
  },

  // Navbar - Back Button
  navBarBackBtnView: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 40,
  },
  navBarBackBtnButton: {
    borderRadius: 100 / 2,
    height: 40,
    width: 40,
  },
  navBarBackBtnIcon: {
    color: '#FFFFFF',
    left: -9,
    position: 'absolute',
    top: 7,
  },

  // Navbar - Title
  navBarTitleView: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    margin: 2,
  },
  navBarTitleText: {
    color: '#FFFFFF',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
  },

  // On Off All
  onOffAllContainer: {
    height: 40,
    flexDirection: 'row',
    marginBottom: 16,
    paddingLeft: 16,
    paddingRight: 16,
  },
  onOffAllButton: {
    flex: 1,
    marginRight: 4,
  },
  onOffAllButtonText: {
    color: '#fff',
    textAlign: 'center',
    width: '100%',
  },

  // Timeleft button
  timeleftButtonText: {
    fontSize: 10,
    marginHorizontal: 10,
    textAlign: 'center',
  },
  timeleftButtonView: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },

  // Controllers
  listItem: {
    justifyContent: 'center',
    marginHorizontal: 2,
    marginVertical: 4,
    padding: 0,
  },

})

const mapStateToProps = (state) => ({
  reduxState: state
});

export default connect(mapStateToProps, {
  parseEquipmentJson,
  parseEquipmentIopValue,
  updateDetailEvent
})(FacilityControlScreen);
