import React from 'react';
import { ActivityIndicator, Dimensions, StyleSheet, Text, View } from 'react-native';
import PropTypes from 'prop-types';

/**
 * @description A component meant to cover the screen temporarily while iop values are waiting to be loaded/updated.
 */
function LoadCover({ text = '' }) {
	const { height: SCN_HEIGHT, width: SCN_WIDTH } = Dimensions.get('window');
	let textToUse = 'Please wait...'
	if (text === 'ON_ALL') {
		textToUse = 'Turning on everything...'
	} else if (text === 'OFF_ALL') {
		textToUse = 'Turning off everything...'
	}
	return (
		<View style={{ ...styles.container, height: SCN_HEIGHT, width: SCN_WIDTH }}>
			<View style={styles.subcontainer}>
				<ActivityIndicator size='small' color='#FFFFFF' />
				<View style={{ width: 10 }} />
				<Text children={textToUse} style={styles.loadingText} />
			</View>
		</View>
	);
}

LoadCover.propTypes = { text: PropTypes.string };
export default LoadCover;

const styles = StyleSheet.create({
	container: {
		alignItems: 'center',
		backgroundColor: '#00000066',
		flex: 1,
		justifyContent: 'center',
		position: 'absolute',
		zIndex: 1,
	},
	subcontainer: {
		backgroundColor: '#333333',
		borderRadius: 10,
		flexDirection: 'row',
		padding: 10,
		paddingHorizontal: 15,
	},
	loadingText: {
		color: '#FFFFFF',
		fontSize: 16,
		fontWeight: 'bold',
	}
});
