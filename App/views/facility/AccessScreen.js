import React from 'react';
import { StyleSheet, View, ActivityIndicator, ScrollView, Image, TouchableOpacity, ListView, Alert, Platform, StatusBar, Dimensions, NetInfo  } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Card, CardItem, Thumbnail, Left, Body, Right, Form,
  Item, Input, Label, Button, Icon ,DatePicker, List, ListItem, Fab, Picker, Text } from 'native-base';
import { connect } from 'react-redux';
import { updateDetailEvent} from '../../redux/actions/auth';
import moment from 'moment'
import { Url } from '../../../config/api/credentials';
import Axios from 'axios';
import Toast from 'react-native-root-toast';
import Ripple from 'react-native-material-ripple';
import QRCodeScanner from 'react-native-qrcode-scanner';
import Ionicons from "react-native-vector-icons/Ionicons";
import * as Animatable from "react-native-animatable";


const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[statusbar.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

const statusbar = {
  container: {
    height: 20,
    width: '100%'
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#000',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#000'
  },
}

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

console.disableYellowBox = true;

const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency
const rectDimensions = SCREEN_WIDTH * 0.85; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.0025; // this is equivalent to 2 from a 393 device width
const scanBarWidth = SCREEN_WIDTH * 0.6; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const rectBorderColor = "#fff";
const scanBarColor = "red";
const iconScanColor = "#fff";

class AccessScreen extends React.Component {
    static navigationOptions = {
        header: null
    }
    state={
        timeLeft: '00:00:00'
    }
  componentWillMount = () => {
    setInterval(()=>{
        const { detail_event } = this.props.navigation.state.params;
        const end = moment(detail_event.event_end)
        const start = moment()
        const diff = end.diff(start)
        const hours = Math.floor(moment.duration(diff).asHours())
        var timeLeft = hours+':'+moment.utc(moment.duration(diff).asMilliseconds()).format("mm:ss")
        this.setState({timeLeft})
     }, 1000)
    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );

    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {

        this.setState({connection_Status : "Online"})

      }
      else
      {

        this.setState({connection_Status : "Offline"})
      }

    });
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );

  }
 /***
  Start Status connection
  ***/

    _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"});
        Toast.show('CONNECTION TO HOST RESUME.');
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
    };

    connectionLost(self){
        self.setState({ isLoading: false })
        self.setState({counter: self.state.counter+1 });
       if(self.state.counter >= 3){
            Toast.show('CONNECTION LOST');
       }
     }

     noInternet(self){
       if(self.state.connection_Status=="Online"){

         return true
       }else{
         Toast.show('Connection to host lost.');
         return false;
       }
     }


  render() {
    const { detail_event } = this.props.navigation.state.params;
    const end = moment(detail_event.event_end)
    const start = moment(detail_event.event_start)
    const diff = end.diff(start)
    const hours = Math.floor(moment.duration(diff).asHours())
    var timeLeft = hours+':'+moment.utc(moment.duration(diff).asMilliseconds()).format("mm:ss")

      return (
        <Container>
          <View style={statusbar.container}>
            <MyStatusBar backgroundColor="#000" barStyle="light-content" />
            <View style={statusbar.appBar} />
            <View style={statusbar.content} />
          </View>

          <View style={{flexDirection: "row", justifyContent: "space-between", backgroundColor: '#529ff3', height: 58,
            shadowColor: '#494949', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 1, paddingLeft: 10, paddingRight: 10}}>
            <View style={{width: 40, justifyContent:'center', alignItems:'center'}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}
              onPress={() => this.props.navigation.goBack()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                </Button>
              </Ripple>
            </View>

            <View style={{flex: 1, justifyContent:'center', alignItems:'center', margin: 2}}>
              <Text style={{fontSize: 20, color:'#fff', fontWeight: 'bold', textAlign: 'left'}}>
                {'Facility Control For'}
              </Text>
              <Text style={{fontSize: 20, color:'#fff', fontWeight: 'bold', textAlign: 'left'}}>
                {detail_event.location_name}
              </Text>
            </View>
          </View>
          <Content>
            <Card style={styles.Card}>
                <CardItem>
                    <Body>
                        <View>
                            <Text style={{fontSize: 20, color: 'black' }}>
                                {detail_event.event_name}
                            </Text>
                            <Text style={{fontSize: 18, color: 'grey', marginLeft: 10 }}>
                                {'Start: '+detail_event.event_start}
                            </Text>
                            <Text style={{fontSize: 18, color: 'grey', marginLeft: 10 }}>
                                {'End: '+detail_event.event_end}
                            </Text>
                            <Text style={{fontSize: 20, color: 'black' }}>
                                {'Time Left: '+ this.state.timeLeft}
                            </Text>
                        </View>
                    </Body>
                </CardItem>
            </Card>
            <Card style={styles.Card}>
                <CardItem>
                    <Body>
                        <Text style={{fontSize: 22, color: 'black', marginTop: 10 }}>
                            {'Control Facility'}
                        </Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body style={{flexDirection: "row", justifyContent: "center"}}>
                        <Button center style={{backgroundColor: 'grey', alignItems: 'center'}} onPress={()=>{
                            this.props.navigation.navigate('Facility', {detail_event})
                        }}>
                            <Text>click to Control</Text>
                        </Button>
                    </Body>
                </CardItem>
            </Card>
            <Card style={styles.Card}>
                <CardItem>
                    <Body>
                        <Text style={{fontSize: 22, color: 'black', marginTop: 10 }}>
                            {'Time Extension'}
                        </Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body style={{flexDirection: "row", justifyContent: "center"}}>
                        <Button style={{backgroundColor: 'grey'}}>
                            <Text>{'Time left:'+this.state.timeLeft}</Text>
                        </Button>
                    </Body>
                </CardItem>
            </Card>
          </Content>
        </Container>
      );
    }
}

const styles = StyleSheet.create({
    Card: {
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
    }
});

const mapStateToProps = (state) => ({
  reduxState:state
});

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

export default connect(mapStateToProps, {updateDetailEvent})(AccessScreen);
