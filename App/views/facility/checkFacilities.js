import Memoizer from '../../modules/memoizer'

function checker(status) {
  if (status === 'ON_ALL') {
    return (value) => {
      if (typeof value === 'number') {
        return value > 0;
      } else {
        return true;
      }
    };
  } else if (status === 'OFF_ALL') {
    return (value) => {
      if (typeof value === 'number') {
        return value <= 0;
      } else {
        return true;
      }
    };
  } else {
    return value => typeof value !== 'undefined';
  }
}

const getAllIopNamesWithFnRef = new Memoizer();
function getAllIoPointNamesWithFn(data) {
  return getAllIopNamesWithFnRef.do(() => {
    let nameList = [];
    for (let i = 0; i < data.length; i++) {
      const { template_info: { fn = [] } = {} } = data[i];
      for (let j = 0; j < fn.length; j++) {
        nameList.push(fn[j]['io_point_name'])
      }
    }
    return nameList;
  }, [data])
}

const checkFacilitiesRef = new Memoizer()
/**
* @description Check if all facilities are on or off
* @param {object} props
* @param {data} props.data `this.state.facilities`
* @param {Array} props.facilities
* @param {object} props.userlookups Pass `this.props.reduxState.Facility.userlookups` in. Needed to determine the type of equipment
* @param {object} props.onlyCheckDeviceType Types of facilities to check
* @param {'ON_ALL'|'OFF_ALL'|any} props.checker
*/
function checkFacilities({
  data = [],
  facilities = [],
  userlookups = {},
  onlyCheckDeviceType = [],
  status = '',
}) {

  return checkFacilitiesRef.do(() => {
    const allIoPointNamesWithFn = getAllIoPointNamesWithFn(data);

    // So that loading does not get stuck forever
    if (facilities.length <= 0) { return true; }

    for (let i = 0; i < facilities.length; i++) {
      const { iop_infor } = facilities[i]; // template_info: { fn }
      for (let j = 0; j < iop_infor.length; j++) {

        // Only check for values if the control is binary type, skip if not
        const { value, data_iop: { pointproperty: lookupId = '' }, pointname } = iop_infor[j];
        const { type: iopType } = userlookups[lookupId]
        const shouldSkipChecking = (
          !onlyCheckDeviceType.includes(iopType) ||
          !allIoPointNamesWithFn.includes(pointname) // Skip read-only points
        )
        if (shouldSkipChecking) { continue; }

        // If even one of the values are false,
        // that means the values of facilities have already started to change
        const checkValue = checker(status)(value)
        console.log('checkValue: ' + checkValue)
        if (checkValue === false) {
          return false // ends the loop early to save computation power
        }
      }
    }

    // Each iopInfo must be looped through to confirm if they are really all on/off
    return true;
  }, [data, facilities, userlookups, onlyCheckDeviceType, status])

}

export default checkFacilities;
