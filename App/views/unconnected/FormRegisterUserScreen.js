import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image, Platform, StatusBar ,NetInfo} from 'react-native';
import { Container, Header, Content, Footer, Form, Item, Input, Label, CardItem, Button, Icon } from 'native-base';
import { StackNavigator } from 'react-navigation';
import GenerateForm from 'react-native-form-builder';
import Axios from 'axios';
import { connect } from 'react-redux';
import Ripple from 'react-native-material-ripple';
import { authSuccess } from '../../redux/actions/auth';
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';
// import Toast from 'react-native-toast-native';
import Toast from 'react-native-root-toast';
import Loader from '../../../App/views/unconnected/Loader';

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[statusbar.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

const statusbar = {
  container: {
    height: 20,
    width: '100%'
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#000',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#000'
  },
}

const stylesToast={
  backgroundColor: "#2f4145",
  height: Platform.OS === ("ios") ? 70 : 125,
  color: "#ffffff",
  fontSize: 12,
  borderRadius: 15,
}

const styles = {
  wrapper: {
    flex: 1,
    paddingTop: 50,
    backgroundColor: '#fff'
  },

  outcontent:{
    flexDirection: "column", 
    justifyContent:'space-between', 
    flex: 1, 
    backgroundColor: '#fff',
  },

  outForm:{
    paddingLeft: '5%', 
    paddingRight: '5%', 
    marginLeft: -5,
  },
  conbtnLogin: {paddingTop:10, paddingLeft: 20, paddingRight: 15},

  /////////////////scan///////////////////
  out_box: {
    height: 300, width: 300,  borderWidth: 0.5, borderColor: '#d6d7da', justifyContent:'center', alignItems:'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 40
  },

   cardImage_h:{
    height: 80,flex: 1
  },
//////////////////////////////////////////////////////////////////////////////////////////////////////////////  
  footerMain:{borderTopWidth: 2, borderTopColor: '#ffffff', backgroundColor:'#fff', height: 45, shadowOpacity: 0, elevation: 0, shadowOffset: {height: 0, width: 0},},   
  outbtnfooter:{flex: 1, justifyContent: 'flex-start', alignItems: 'center'},
  footerpd:{paddingRight: '5%', paddingLeft: '5%', height: 50, width: "100%", backgroundColor:'#fff'},
  footerpd1:{paddingRight:15, paddingLeft:15, width: '100%'},
  btnfooter:{flex: 1, height: 45, justifyContent: 'center'},
  btnfooterText:{color:"#fff", fontSize: 16, textAlign:'center'},
  textForm:{
    textAlign: 'center', fontSize: 22,  fontWeight: 'bold', padding: 15,
  },

/////////////////////////////////////////////////quickstyle/////////////////////////////////////////////////////////////  
  color_blur:{color: '#2089dc'},

  layout_r_f1:{flexDirection: "row", flex: 1, justifyContent: 'space-between', },
  layout_c_f1:{flexDirection: "column", flex: 1, justifyContent: 'space-between', alignItems:'center'},

  w40:{width: 40}, w50:{width: 50}, w100:{width: 100}, w150:{width: 150}, w200:{width: 200}, w250:{width: 250}, w300:{width: 300},  w320:{width: 320},

  pdL5_100: {paddingLeft: '5%'}, 
  pdR5_100: {paddingRight: '5%'}, 

  mgB0:{marginBottom: 0}, mgB5:{marginBottom: 5}, mgB10:{marginBottom: 10}, mgB15:{marginBottom: 15}, mgB20:{marginBottom: 20}, 
  mgT0:{marginTop: 0}, mgT5:{marginTop: 5},mgT10:{marginTop: 10}, mgT15:{marginTop: 15}, mgT20:{marginTop: 20}, mgT25:{marginTop: 25}, mgT30:{marginTop: 30}, mgT35:{marginTop: 35}, mgT40:{marginTop: 40}, mgT45:{marginTop: 45}, mgT50:{marginTop: 50},
  

  pdT0:{paddingTop: 0}, pdT5:{paddingTop: 5}, pdT10:{paddingTop: 10}, pdT15:{paddingTop: 15}, pdT20:{paddingTop: 20},
  pdB0:{paddingBottom: 0}, pdB5:{paddingBottom: 5}, pdB10:{paddingBottom: 10}, pdB15:{paddingBottom: 15}, pdB20:{paddingBottom: 20},
  pdL0:{paddingLeft: 0}, pdL5:{paddingLeft: 5}, pdL10:{paddingLeft: 10}, pdL15:{paddingLeft: 15}, pdL20:{paddingLeft: 20},
  pdR0:{paddingRight: 0}, pdR5:{paddingRight: 5}, pdR10:{paddingRight: 10}, pdR15:{paddingRight: 15}, pdR20:{paddingRight: 20},

  fSize16:{fontSize: 16}, fSize18:{fontSize: 18}, fSize20:{fontSize: 20}, fSize24:{fontSize: 24},

  textCenter:{textAlign:'center'}, textLeft:{textAlign:'left'}, textRight:{textAlign:'right'},


  flex1:{flex: 1}, jusbetween:{justifyContent:'space-between'}, 
  flex_column:{flexDirection:'column'}, flex_row:{flexDirection: 'row'}, aligI_center:{alignItems:'center'},
};

class FormRegisterUserScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      user_email: '',
      password: '',
      isLoading: false, 
      connection_Status : "",
      counter : 0,
    };
  }

  showLoader = () => {
    this.setState({ isLoading: true });
  };

  static navigationOptions = {
    header: null,
  };

    componentDidMount() {
    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {
       
        this.setState({connection_Status : "Online"})
      }
      else
      {
       
        this.setState({connection_Status : "Offline"})
      }

    });
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );

  }
   /***
  Start Status connection
  ***/

    _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"});
        Toast.show('CONNECTION TO HOST RESUME.');
      }
      else
      {
        this.setState({connection_Status : "Offline"})
      }
    };

    connectionLost(self){
        self.setState({ isLoading: false })
        self.setState({counter: self.state.counter+1 });
       if(self.state.counter>=3){
            Toast.show('CONNECTION LOST');
       }
        
     }

     noInternet(self){
       if(self.state.connection_Status=="Online"){
         return true
       }else{
         Toast.show('Connection to host lost.');
         return false;
       }
     }


  /***
 End Status connection
  ***/

  async linkbtnregister() {
    this.register();
  };

  register(){
    if(this.state.user_email == '' || this.state.password == ''){
      Toast.show('Please fill input!',Toast.SHORT, Toast.CENTER, stylesToast);
      return;
    }else{
      if(!this.validateEmail(this.state.user_email)){
        Toast.show('The email address is not in a valid format!',Toast.SHORT, Toast.CENTER, stylesToast);
        return;
      }else{
        this.registerUser(this.state.user_email, this.state.password);
        // this.props.navigation.navigate('Login');
      }
    }
  }
  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
  };

  registerUser(email,password){
    var self = this;
    var credentials = {
      "mtd": 1,
      "mail": email,
      "pass": password,
      "wid": this.props.reduxState.Auth.workspaceqr.qrcode
    }
    if(!self.noInternet(self)){
       self.setState({
       isLoading: false
       });
      return;
    }
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) {
        self.setState({counter: 0 });
      switch(response.data){
        case 1:
          self.showLoader();
          Toast.show('Register successful!',Toast.SHORT, Toast.CENTER, stylesToast);
          self.props.navigation.navigate("Login");
        break;
        case 2:
          Toast.show('Register fail!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
        case 3:
          Toast.show('Email not available!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
        default :
          Toast.show('Register Error!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
      }
    })
    .catch(function (error) {
       self.connectionLost(self);
      console.log(error);
    });
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <View style={statusbar.container}>
          <MyStatusBar backgroundColor="#000" barStyle="light-content" />
          <View style={statusbar.appBar} />
          <View style={statusbar.content} />
        </View>
        <View style={{backgroundColor:'#529ff3', flexDirection: 'row', justifyContent: "space-between",
          height: 58, shadowColor: '#494949', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 1, paddingLeft: 10, paddingRight: 10 }}>
          <View style={{width: 40, justifyContent: 'center', alignItems: 'center'}}>
            <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
            onPress={() => this.props.navigation.goBack()} >
              <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                <Icon style={{color:'#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
              </Button>
            </Ripple>
          </View>
         
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontSize: 20, color:'#fff', fontWeight: 'bold', textAlign: 'center'}}>
              Register user
            </Text>
          </View>

          <View style={{width: 40, justifyContent: 'center', alignItems: 'center'}}>
            
          </View>
        </View>
        <Content>
          <ScrollView style={styles.flex1}>
            <View style={styles.outcontent}>
              <View style={{paddingTop:50}}>
                <View style={styles.aligI_center}>
                  <CardItem style={{height: 200, width: '90%'}}>
                    <Image source ={require('../../image/logo-bms.png')} style={{ height:'100%', width: '100%', resizeMode:'contain'}} />
                  </CardItem>
                </View>

                <View full block style={styles.mgT20}>
                  <Text style={[styles.fSize20, styles.textCenter, styles.color_blur]}>{this.props.reduxState.Auth.workspaceqr.workspacename}</Text>
                </View>
                <View style={styles.outForm}>
                  <View style={{paddingLeft: 20,paddingRight: 15, marginBottom: 20}}>
                    <Item iconLeft style={{paddingTop: 20, paddingBottom: 10}}>
                      <Icon style={{width:40}} name='account' type='MaterialCommunityIcons'/>
                      <Input placeholder="User Email" placeholderTextColor="#a7a7a7" autoCapitalize="none" onChangeText={(user_email) => this.setState({user_email})} value={this.state.user_email}/>
                    </Item>
                    <Item iconLeft style={{paddingTop: 20, paddingBottom: 10}}>
                      <Icon style={{width:40}} name='ios-lock' type='Ionicons'/>
                      <Input placeholder="Password" secureTextEntry={true} placeholderTextColor="#a7a7a7" autoCapitalize="none" onChangeText={(password) => this.setState({password})} value={this.state.password}/>
                    </Item>
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
        </Content>
        <Footer style={{borderTopWidth: 2, borderTopColor: '#ffffff', backgroundColor:'#fff', height: 45, shadowOffset: {height: 0, width: 0}, shadowOpacity: 0, elevation: 0}}>
          <View  style={styles.outbtnfooter}>
            <View style={{width: '100%', backgroundColor:'#fff', height: 50, paddingRight: '5%', paddingLeft: '5%'}}>
              <View style={{paddingRight:15, paddingLeft:15, width: '100%'}}>
                <Button info block full>
                  <Ripple style={styles.btnfooter} onPress={() => this.linkbtnregister()}>
                    <Text style={{color: '#ffffff', fontSize: 16, textAlign: 'center'}}>REGISTER</Text>
                  </Ripple>
                </Button>
              </View>
            </View>
          </View>
        </Footer>
        <Loader opacity={1} loading={this.state.isLoading}/>
      </Container>
    );
  }
}
const mapStateToProps = (state) => ({
  reduxState:state
});

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

export default connect(mapStateToProps, {
  Auth,
})(FormRegisterUserScreen);
