import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, Platform, Dimensions, NetInfo } from 'react-native';
import { Container, Header, Content, Footer, ListItem, CheckBox, Body, CardItem, Button, Icon, Form, Item, Input } from 'native-base';
import { StackNavigator } from 'react-navigation';
import { AsyncStorage } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import { LoginButton, AccessToken, LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';

import NavbarWithHamburger from '../../components/navigation/NavbarWithHamburger';

import Axios from 'axios';
import { connect } from 'react-redux';
import { updateAccessToken, updateDetailEvent, updateQrCode, resetAuth, } from '../../redux/actions/auth';
import { authSuccess } from '../../redux/actions/auth';
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';
import QRCodeScanner from 'react-native-qrcode-scanner';
import Ripple from 'react-native-material-ripple';
// import Toast from 'react-native-toast-native';
import Toast from 'react-native-root-toast';
import Ionicons from "react-native-vector-icons/Ionicons";
import * as Animatable from "react-native-animatable";

import moment from 'moment';
import _ from 'lodash';

import Loader from '../../../App/views/unconnected/Loader';



const { SCREEN_HEIGHT, SCREEN_WIDTH } = Dimensions.get("window");

console.disableYellowBox = true;
const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency
const rectDimensions = SCREEN_WIDTH * 0.85; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.0025; // this is equivalent to 2 from a 393 device width
const scanBarWidth = SCREEN_WIDTH * 0.6; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const rectBorderColor = "#fff";
const scanBarColor = "red";
const iconScanColor = "#fff";

const stylesCamera = {
  rectangleContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    borderWidth: rectBorderWidth,
    borderColor: rectBorderColor,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: "center",
    alignItems: "center"
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor
  },

  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: scanBarColor
  }
};

const stylesToast = {
  backgroundColor: "#2f4145",
  height: Platform.OS === ("ios") ? 70 : 125,
  color: "#ffffff",
  fontSize: 12,
  borderRadius: 15,
}

class ProFileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: {},
      connection_Status: "",
      counter: 0,
    };
  }

  async backApp() {
    this.props.navigation.navigate('Calendars');
  };

  async btnEdit() {
    this.props.navigation.navigate('EditProfile', { refreshComponent: () => this.refreshComponent() });
  };

  componentWillMount = () => {
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this._handleConnectivityChange

    );

  }

  componentDidMount() {
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this._handleConnectivityChange

    );

    NetInfo.isConnected.fetch().done((isConnected) => {

      if (isConnected == true) {
        this.setState({ connection_Status: "Online" });
        this.getProfileUser(false);
      }
      else {

        this.setState({ connection_Status: "Offline" })
      }

    });

  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this._handleConnectivityChange

    );

  }

  refreshComponent() {
    this.getProfileUser(true);
  }

  /***
   Start Status connection
   ***/

  _handleConnectivityChange = (isConnected) => {

    if (isConnected == true) {
      this.setState({ connection_Status: "Online" });
      Toast.show('CONNECTION TO HOST RESUME.');
    }
    else {
      this.setState({ connection_Status: "Offline" })
    }
  };

  connectionLost(self) {
    self.setState({ isLoading: false })
    self.setState({ counter: self.state.counter + 1 });
    if (self.state.counter >= 3) {
      Toast.show('CONNECTION LOST');
    }

  }

  noInternet(self) {
    if (self.state.connection_Status == "Online") {
      return true
    } else {
      Toast.show('Connection to host lost.');
      return false;
    }
  }


  /***
 End Status connection
  ***/
  getProfileUser(setAuth) {

    var self = this;
    var accessToken = {
      "access-token": this.props.screenProps.reduxState.Auth.token["access-token"],
      "client": '',
      "uid": ''
    }

    var credentials = { 'mtd': 4, 'token': this.props.screenProps.reduxState.Auth.token["access-token"] };
    Axios.post(Url.user, credentials, { headers: Object.assign(Url.headers, accessToken) })
      .then(function (response) {
        self.setState({ counter: 0 });
        if (response.data.status == 3) {
          Toast.show('Token expired.');
          self.props.navigation.navigate('Auth');
          return;
        }
        if (response.data.status == 1) {
          if (setAuth) {

            self.props.screenProps.authSuccess(response, accessToken);
          }
          self.setState({
            userInfo: response.data.data
          });
        } else {
          console.log('Get profile fail!');
        }
      })
      .catch(function (error) {
        self.connectionLost(self);
        console.log("ERROR DURING TOKEN VALIDATION", error);
      });
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>

        <NavbarWithHamburger
          title='User Profile'
          openDrawerAction={this.props.navigation.openDrawer}
          rightItem={(
            <View style={this.styles.navbarRightItemContainer}>
              <Ripple rippleContainerBorderRadius={100 / 2} rippleCentered={true} style={{ width: 40, height: 40, }} onPress={() => this.btnEdit()}>
                <Button style={this.styles.navbarRightItemButton} transparent>
                  <Icon style={this.styles.navbarRightItemIcon} name='pencil' type='Octicons' />
                </Button>
              </Ripple>
            </View>
            // Old component (Goes back to Calendar Screen)
            // <Ripple rippleContainerBorderRadius={100 / 2} rippleCentered={true} style={{ width: 40, height: 40, }} onPress={() => this.backApp()}>
            //   <Button style={{ width: 40, height: 40, borderRadius: 100 / 2, }} transparent>
            //     <Icon style={{ color: '#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
            //   </Button>
            // </Ripple>
          )}
        />

        <Content>
          <ScrollView>
            <View>
              <View style={{ paddingRight: 15, paddingLeft: 15, }}>
                <View style={{ alignItems: 'center', borderBottomColor: '#ccc', borderBottomWidth: 1, paddingBottom: 25 }}>
                  <View style={{ marginTop: '5%', alignItems: 'center' }}>
                    <View style={{
                      backgroundColor: '#efefef',
                      height: 100,
                      width: 100,
                      borderRadius: 100 / 2,
                      justifyContent: 'center',
                      alignItems: 'center',
                      paddingBottom: 10,
                      overflow: 'hidden',
                    }}>

                      <CardItem style={{ height: 105, width: 105, paddingTop: 25, paddingBottom: 25, paddingLeft: 25, paddingRight: 25, backgroundColor: '#efefef' }}>
                        <Image source={require('../../image/user3.png')} style={{ height: '100%', width: '100%' }} />
                      </CardItem>
                    </View>
                  </View>
                </View>

                <View>

                  <View style={this.styles.infoItemContainer}>
                    <Icon style={this.styles.infoItemIcon} name='user' type='FontAwesome' />
                    <Text style={this.styles.infoItemText}>
                      {this.props.screenProps.reduxState.Auth.user.user_name}
                    </Text>
                  </View>

                  <View style={this.styles.infoItemContainer}>
                    <Icon style={this.styles.infoItemIcon} name='envelope' type='FontAwesome' />
                    <Text style={this.styles.infoItemText}>
                      {this.props.screenProps.reduxState.Auth.user.user_email}
                    </Text>
                  </View>

                  <View style={this.styles.infoItemContainer}>
                    <Icon style={this.styles.infoItemIcon} name='group' type='FontAwesome' />
                    <Text style={this.styles.infoItemText}>
                      {this.props.screenProps.reduxState.Auth.user.user_profile_name}
                    </Text>
                  </View>

                  <View style={this.styles.infoItemContainer}>
                    <Icon style={this.styles.infoItemIcon} name='phone' type='FontAwesome' />
                    <Text style={this.styles.infoItemText}>
                      {this.props.screenProps.reduxState.Auth.user.user_profile_phone}
                    </Text>
                  </View>

                  <View style={this.styles.infoItemContainer}>
                    <View style={this.styles.infoItemIcon}>
                      <CardItem style={{ height: 25, width: 25, paddingLeft: 0, paddingRight: 0, paddingTop: 0, paddingBottom: 0 }}>
                        <Image source={require('../../image/building.png')} style={{ height: '100%', width: '100%' }} />
                      </CardItem>
                    </View>
                    <Text style={this.styles.infoItemText}>
                      {this.props.screenProps.reduxState.Auth.workspaceqr.workspacename}
                    </Text>
                  </View>

                </View>
              </View>
            </View>
          </ScrollView>
        </Content>

      </Container>
    );
  }

  styles = StyleSheet.create({
    navbarRightItemContainer: {
      alignItems: 'center',
      justifyContent: 'center',
      width: 40,
    },
    navbarRightItemButton: {
      alignItems: 'center',
      borderRadius: 100 / 2,
      height: 40,
      justifyContent: 'center',
      width: 40,
    },
    navbarRightItemIcon: {
      color: '#FFFFFF',
      position: 'absolute',
      top: 7,
    },

    infoItemContainer: {
      alignItems: 'center',
      borderBottomColor: '#CCCCCC',
      borderBottomWidth: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      paddingTop: 15,
      paddingBottom: 15,
    },
    infoItemIcon: {
      width: 40
    },
    infoItemText: {
      color: '#000000',
      fontSize: 20,
      height: '100%',
      marginLeft: 10,
      paddingTop: 0,
      paddingBottom: 0,
    },
  })

}

class EditProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_name: this.props.screenProps.reduxState.Auth.user.user_name,
      user_profile_name: this.props.screenProps.reduxState.Auth.user.user_profile_name,
      user_email: this.props.screenProps.reduxState.Auth.user.user_email,
      phone: this.props.screenProps.reduxState.Auth.user.user_profile_phone,
      workspace_id: this.props.screenProps.reduxState.Auth.workspaceqr.qrcode,
      workspace_name: '',
      isLoading: false,
      connection_Status: "",
      counter: 0,
    };
  }

  async linkUpdateProfile() {
    if (this.state.workspace_id == this.props.screenProps.reduxState.Auth.workspaceqr.qrcode) {
      this.showLoader();
      this.updateProfile();
      this.updateProfileUserName();
      this.updateProfileMail();
      this.props.navigation.navigate('Profile');
    } else {
      this.showLoader();
      this.updateProfile();
      this.updateProfileUserName();
      this.updateProfileMail();
      this.verifyWorkspaceId(this.state.workspace_id);
      this.destroySession();
    }
  }

  showLoader = () => {
    this.setState({ isLoading: true });
  };

  componentWillMount() {
    this._configureGoogleSignIn();

    setTimeout(() => {
      this.setState({ loading: false });

    }, 2000);
  }

  componentDidMount() {

    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this._handleConnectivityChange

    );

    NetInfo.isConnected.fetch().done((isConnected) => {

      if (isConnected == true) {
        this.setState({ connection_Status: "Online" })
      }
      else {

        this.setState({ connection_Status: "Offline" })
      }

    });
  }

  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this._handleConnectivityChange

    );

  }
  /***
    Start Status connection
    ***/

  _handleConnectivityChange = (isConnected) => {

    if (isConnected == true) {
      this.setState({ connection_Status: "Online" })
      Toast.show('CONNECTION TO HOST RESUME.');
    }
    else {
      this.setState({ connection_Status: "Offline" })
    }
  };

  connectionLost(self) {
    self.setState({ isLoading: false })
    self.setState({ counter: self.state.counter + 1 });
    if (self.state.counter >= 3) {
      Toast.show('CONNECTION LOST');
    }

  }

  noInternet(self) {
    if (self.state.connection_Status == "Online") {
      return true
    } else {
      Toast.show('Connection to host lost.');
      return false;
    }
  }


  /***
 End Status connection
  ***/
  //bms:1095739769941-a84erfv04d3kvsgrvf267sg68mi2p751.apps.googleusercontent.com
  //bmsfamiie:787160340563-hmos18rocot8c6pmddvsdgsqkj4umm18.apps.googleusercontent.com
  _configureGoogleSignIn() {
    GoogleSignin.configure({
      webClientId: Url.googewebclientid,
      offlineAccess: false,
    });
  }

  async destroySession() {
    AccessToken.getCurrentAccessToken().then(
      (data) => {
        if (data) {
          console.log(data.accessToken);
          var accessToken = data.accessToken;
          let logout =
            new GraphRequest(
              "me/permissions/",
              {
                accessToken: accessToken,
                httpMethod: 'DELETE'
              },
              (error, result) => {
                if (error) {
                  console.log('Error fetching data: ' + error.toString());
                } else {
                  LoginManager.logOut();
                }
              });
          new GraphRequestManager().addRequest(logout).start();
        }
      }
    );
    const isSignedIn = await GoogleSignin.isSignedIn();
    if (isSignedIn) {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
    }
    const workspace = this.props.screenProps.reduxState.Auth.workspaceqr;
    await AsyncStorage.clear();
    this.props.screenProps.resetAuth();
    this.props.screenProps.updateQrCode(workspace.qrcode, workspace.opencamera, workspace.workspacename);
    Toast.show('Logout successful!', Toast.SHORT, Toast.CENTER, stylesToast);
    this.props.navigation.navigate('Auth');
  };

  updateProfile() {
    var self = this;
    var credentials = {
      "mtd": 5,
      "token": this.props.screenProps.reduxState.Auth.token["access-token"],
      "ufnm": this.state.user_profile_name,
      "unum": this.state.phone
    };
    if (!self.noInternet(self)) {
      self.setState({
        //  disableSubmit: false,
        isLoading: false
      });
      return;
    }
    Axios.post(Url.user, credentials, { headers: Object.assign(Url.headers, credentials) })
      .then(function (response) { // ON SUCCESS
        self.setState({ counter: 0 });
        if (response.data.status == 3) {
          Toast.show('Token expired.');
          self.props.navigation.navigate('Auth');
          return;
        }
        if (response.data.status == 1) {
          self.props.navigation.state.params.refreshComponent();
          Toast.show('Update profile successful!', Toast.SHORT, Toast.CENTER, stylesToast);
        } else {
          Toast.show('Update profile fail!', Toast.SHORT, Toast.CENTER, stylesToast);
        }
      })
      .catch(function (error) {
        // Return error if credentials is invalids
        self.connectionLost(self);
        console.log(error);
      });
  }

  updateProfileMail() {
    var self = this;
    var credentials = {
      "mtd": 22,
      "token": this.props.screenProps.reduxState.Auth.token["access-token"],
      "mail": this.state.user_email
    };
    Axios.post(Url.user, credentials, { headers: Object.assign(Url.headers, credentials) })
      .then(function (response) { // ON SUCCESS
        self.setState({ counter: 0 });
        if (response.data.status == 3) {
          Toast.show('Token expired.');
          self.props.navigation.navigate('Auth');
          return;
        }
        if (response.data.status == 1) {
          self.props.navigation.state.params.refreshComponent();
          // Toast.show('Update profile successful!',Toast.SHORT, Toast.CENTER, stylesToast);
        } else {
          // Toast.show('Update profile fail!',Toast.SHORT, Toast.CENTER, stylesToast);
        }
      })
      .catch(function (error) {
        // Return error if credentials is invalids
        console.log(error);
      });
  }

  updateProfileUserName() {
    var self = this;
    var credentials = {
      "mtd": 17,
      "token": this.props.screenProps.reduxState.Auth.token["access-token"],
      "unm": this.state.user_name
    };

    if (!self.noInternet(self)) {
      self.setState({
        disableSubmit: false,
        isLoading: false
      });
      return;
    }

    Axios.post(Url.user, credentials, { headers: Object.assign(Url.headers, credentials) })
      .then(function (response) { // ON SUCCESS
        self.setState({ counter: 0 });
        if (response.data.status == 3) {
          Toast.show('Token expired.');
          self.props.navigation.navigate('Auth');
          return;
        }
        if (response.data.status == 1) {
          self.props.navigation.state.params.refreshComponent();
          // Toast.show('Update profile successful!',Toast.SHORT, Toast.CENTER, stylesToast);
        } else {
          // Toast.show('Update profile fail!',Toast.SHORT, Toast.CENTER, stylesToast);
        }
      })
      .catch(function (error) {
        // Return error if credentials is invalids
        self.connectionLost(self);
        console.log(error);
      });
  }

  onSuccess(e) {
    this.verifyWorkspaceId(e.data);
    this.destroySession();
  }
  verifyWorkspaceId(workspace_id) {
    var self = this;
    var credentials = { mtd: 1, id: workspace_id };
    if (!self.noInternet(self)) {
      self.setState({
        disableSubmit: false,
        isLoading: false
      });
      return;
    }

    Axios.post(Url.workspace, credentials, { headers: Url.headers })
      .then(function (response) {
        if (response.data.status == 1) {
          self.setState({ counter: 0 });
          if (response.data.status == 3) {
            Toast.show('Token expired.');
            self.props.navigation.navigate('Auth');
            return;
          }
          self.setState({
            workspace_id: workspace_id,
            workspace_name: response.data.name
          });

          self.props.screenProps.updateQrCode(workspace_id, false, response.data.name);
          self.props.navigation.navigate('Auth');
        } else {
          if (self.scanner) {
            self.scanner.reactivate();
          }
        }
      })
      .catch(function (error) {
        self.connectionLost(self);
        console.log(error);
      });
  }

  onScanScreenCamera() {
    this.props.screenProps.updateQrCode(this.props.screenProps.reduxState.Auth.workspaceqr.qrcode, true, this.props.screenProps.reduxState.Auth.workspaceqr.workspacename);
  }

  async gobackScreen() {
    this.props.screenProps.updateQrCode(this.props.screenProps.reduxState.Auth.workspaceqr.qrcode, false, this.props.screenProps.reduxState.Auth.workspaceqr.workspacename);
  }


  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18
      },
      to: {
        [translationType]: fromValue
      }
    };
  }

  render() {
    // Get navigation props to be able to use navigate function
    const { navigate } = this.props.navigation;

    if (this.props.screenProps.reduxState.Auth.workspaceqr.opencamera) {
      return (
        <Container>

          <NavbarWithHamburger
            title='Scan QR Code'
            leftItem={(
              <Ripple rippleContainerBorderRadius={100 / 2} rippleCentered={true} style={{ width: 40, height: 40, }}
                onPress={() => this.gobackScreen()} >
                <Button style={{ width: 40, height: 40, borderRadius: 100 / 2, }} transparent>
                  <Icon style={{ color: '#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                </Button>
              </Ripple>
            )}
          />

          <Content>
            <QRCodeScanner ref={(node) => { this.scanner = node }} onRead={this.onSuccess.bind(this)}
              reactivate={true}
              showMarker
              cameraStyle={{ height: SCREEN_HEIGHT }}
              customMarker={
                <View style={stylesCamera.rectangleContainer}>
                  <View style={stylesCamera.topOverlay}>
                    <Text style={{ fontSize: 30, color: "white" }}>
                      REGISTER WORKSPACE
                    </Text>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <View style={stylesCamera.leftAndRightOverlay} />

                    <View style={stylesCamera.rectangle}>
                      <Ionicons
                        name="ios-qr-scanner"
                        size={SCREEN_WIDTH * 0.95}
                        color={iconScanColor}
                      />
                      <Animatable.View
                        style={stylesCamera.scanBar}
                        direction="alternate-reverse"
                        iterationCount="infinite"
                        duration={1700}
                        easing="linear"
                        animation={this.makeSlideOutTranslation(
                          "translateY",
                          SCREEN_WIDTH * -0.7
                        )}
                      />
                    </View>

                    <View style={stylesCamera.leftAndRightOverlay} />
                  </View>

                  <View style={stylesCamera.bottomOverlay} />
                </View>
              }
            />
          </Content>
        </Container>
      );
    } else {
      return (
        <Container>

          <NavbarWithHamburger
            title='Edit Profile'
            leftItem={(
              <Ripple rippleContainerBorderRadius={100 / 2} rippleCentered={true} style={{ width: 40, height: 40, justifyContent: 'center' }}
                onPress={() => this.props.navigation.goBack()} >
                <Button style={{ width: 40, height: 40, borderRadius: 100 / 2, }} transparent>
                  <Icon style={{ color: '#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                </Button>
              </Ripple>
            )}
            rightItem={(
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Ripple rippleContainerBorderRadius={100 / 2} rippleCentered={true} style={{ width: 40, height: 40 }}
                  onPress={() => this.linkUpdateProfile()} >
                  <Button style={{ width: 40, height: 40, borderRadius: 100 / 2, justifyContent: 'center', alignItems: 'center' }} transparent>
                    <Icon style={{ color: '#fff', position: "absolute", top: 7, }} name="check" type="Octicons" />
                  </Button>
                </Ripple>
              </View>
            )}
          />

          <Content>
            <ScrollView>
              <View style={{ paddingBottom: 20 }}>
                <View style={{ marginTop: 50 }}>
                  <View style={{ alignItems: 'center' }}>
                    <CardItem style={{ height: 200, width: '90%' }}>
                      <Image source={require('../../image/logo-bms.png')} style={{ height: '100%', width: '100%', resizeMode: 'contain' }} />
                    </CardItem>
                  </View>
                  <View style={{ marginTop: 10 }}>
                    <Text style={{ fontSize: 20, textAlign: 'center', color: '#2089dc' }}>{this.state.workspace_name == '' ? this.props.screenProps.reduxState.Auth.workspaceqr.workspacename : this.state.workspace_name}</Text>
                  </View>
                </View>
                <View style={{ paddingBottom: 20, paddingRight: 15 }}>
                  <Form >
                    <Item iconLeft style={this.styles.formIcon}>
                      <Text style={this.styles.formText} children='User Name:' />
                      <Input style={{ fontSize: 20 }} placeholderTextColor="#a7a7a7" placeholder="Username" onChangeText={(user_name) => this.setState({ user_name })} value={this.state.user_name} />
                    </Item>

                    <Item iconLeft style={this.styles.formIcon}>
                      <Text style={this.styles.formText} children='Email:' />
                      <Input style={{ fontSize: 20 }} placeholderTextColor="#a7a7a7" placeholder="Email" onChangeText={(user_email) => this.setState({ user_email })} value={this.state.user_email} />
                    </Item>

                    <Item iconLeft style={this.styles.formIcon}>
                      <Text style={this.styles.formText} children='Name:' />
                      <Input style={{ fontSize: 20 }} placeholderTextColor="#a7a7a7" placeholder="Name" onChangeText={(user_profile_name) => this.setState({ user_profile_name })} value={this.state.user_profile_name} />
                    </Item>

                    <Item iconLeft style={this.styles.formIcon}>
                      <Text style={this.styles.formText} children='Phone:' />
                      <Input style={{ fontSize: 20 }} keyboardType='numeric' placeholderTextColor="#a7a7a7" placeholder="Hand phone" onChangeText={(phone) => this.setState({ phone })} value={this.state.phone} />
                    </Item>

                    <Item iconLeft style={this.styles.formIcon}>
                      <Text style={this.styles.formText} children='Workspace ID:' />
                      <Input style={{ fontSize: 20 }} placeholderTextColor="#a7a7a7" placeholder="Workspace ID" onChangeText={(workspace_id) => this.setState({ workspace_id })} value={this.state.workspace_id} />
                    </Item>
                  </Form>
                </View>
                <View style={{ paddingBottom: 20, paddingLeft: 15, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                  <Button info block full style={{ borderRadius: 0, marginTop: 20, width: 300, marginLeft: 'auto', marginRight: 'auto' }}>
                    <Ripple style={{ flex: 1, height: 45, justifyContent: 'center' }} onPress={() => this.onScanScreenCamera()}>
                      <Text style={{ color: "white", fontSize: 16, textAlign: 'center' }}>{'Change workspace'.toUpperCase()}</Text>
                    </Ripple>
                  </Button>
                </View>
              </View>
            </ScrollView>
          </Content>
          <Loader opacity={1} loading={this.state.isLoading} />
        </Container>
      );
    }
  }

  styles = StyleSheet.create({
    formIcon: {
      paddingTop: 15
    },
    formText: {
      color: '#000000',
      fontSize: 20,
      fontWeight: 'bold',
      marginRight: 15,
    },
  })

}
const ProfileStack = createStackNavigator({
  Profile: {
    screen: ProFileScreen,
    navigationOptions: {
      header: null,
    },
  },
  EditProfile: {
    screen: EditProfileScreen,
    navigationOptions: {
      header: null,
    },
  },
});

const mapStateToProps = (state) => ({
  reduxState: state
});

const mapDispatchToProps = (dispatch) => {
  return ({
    updateQrCode: (workspace_id, opencamera, name) => dispatch(updateQrCode(workspace_id, opencamera, name)),
    authSuccess: (response, accessToken) => dispatch(authSuccess(response, accessToken)),
    resetAuth: () => dispatch(resetAuth()),
  })
}

const mergeProps = (state, dispatch, ownProps) => {
  return ({
    ...ownProps,
    screenProps: {
      ...ownProps.screenProps,
      ...state,
      ...dispatch,
    }

  })
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps, {
  Auth,
  resetAuth,
  authSuccess,
  updateAccessToken,
  updateQrCode,
})(ProfileStack);
