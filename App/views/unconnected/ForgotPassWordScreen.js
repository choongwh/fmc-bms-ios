import React from 'react';
import { SafeAreaView, StyleSheet, Text, View, ActivityIndicator, ScrollView, Image, TouchableOpacity, Platform, StatusBar, NetInfo } from 'react-native';
import { Container, Header, Content, Footer, Form, Item, Input, Label, CardItem, Button, Icon } from 'native-base';
import { StackNavigator } from 'react-navigation';
import styles from '../stylecss/style.css';
import Axios from 'axios';
import { connect } from 'react-redux';
import Ripple from 'react-native-material-ripple';
import { authSuccess } from '../../redux/actions/auth'
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';
// import Toast from 'react-native-toast-native';
import Toast from 'react-native-root-toast';
import Loader from '../../../App/views/unconnected/Loader';

const MyStatusBar = ({ backgroundColor, ...props }) => (
  <View style={[statusbar.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

const statusbar = {
  container: {
    height: 20,
    width: '100%'
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#000',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#000'
  },
}

const stylesToast = {
  backgroundColor: "#2f4145",
  height: Platform.OS === ("ios") ? 70 : 125,
  color: "#ffffff",
  fontSize: 12,
  borderRadius: 15,
}

class ForgotPassWordScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      isLoading: false,
      connection_Status: "",
      counter: 0,
    }
  }

  showLoader = () => {
    this.setState({ isLoading: true });
  };

  componentDidMount() {

    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this._handleConnectivityChange

    );

    NetInfo.isConnected.fetch().done((isConnected) => {

      if (isConnected == true) {

        this.setState({ connection_Status: "Online" })
      }
      else {

        this.setState({ connection_Status: "Offline" })
      }

    });

  }

  componentWillMount = () => {
    const headerConfig = {
      header: null,
    };
    this.props.navigation.setParams({ headerConfig });
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this._handleConnectivityChange

    );
  }

  /***
  Start Status connection
  ***/

  _handleConnectivityChange = (isConnected) => {

    if (isConnected == true) {
      this.setState({ connection_Status: "Online" });
      Toast.show('CONNECTION TO HOST RESUME.');
    }
    else {
      this.setState({ connection_Status: "Offline" })
    }
  };

  connectionLost(self) {
    self.setState({ isLoading: false })
    self.setState({ counter: self.state.counter + 1 });
    if (self.state.counter >= 3) {
      Toast.show('CONNECTION LOST');
    }

  }

  noInternet(self) {
    if (self.state.connection_Status == "Online") {
      return true
    } else {
      Toast.show('Connection to host lost.');
      return false;
    }
  }


  /***
 End Status connection
  ***/



  static navigationOptions = ({ navigation }) => {
    return navigation.state.params ? navigation.state.params.headerConfig : { title: '' };
  };

  async btnSM() {
    if (this.state.email == '' || !this.validateEmail(this.state.email)) {
      Toast.show('The email address is not in a valid format!', Toast.SHORT, Toast.CENTER, stylesToast);
      return;
    }
    var self = this;
    if (!self.noInternet(self)) {
      self.setState({

        isLoading: false
      });
      return;
    }
    var credentials = { 'mtd': 11, 'mail': this.state.email };
    Axios.post(Url.user, credentials, { headers: Object.assign(Url.headers, credentials) })
      .then(function (response) {
        self.setState({ counter: 0 });

        if (response.data.status == 1) {
          Toast.show('Reset passwork successful!', Toast.SHORT, Toast.CENTER, stylesToast);
          self.props.navigation.navigate('Login');
          self.showLoader();
        } else {
          Toast.show('Reset passwork fail!', Toast.SHORT, Toast.CENTER, stylesToast);
        }
      })
      .catch(function (error) {
        self.connectionLost(self);
      });
  };

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  render() {
    // Get navigation props to be able to use navigate function
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <StatusBar barStyle='light-content' />
        <View style={{
          backgroundColor: '#529ff3', shadowColor: '#494949', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 1 }}
        >
          <SafeAreaView>
            <View style={{ flexDirection: 'row', justifyContent: "space-between",
          height: 58, paddingLeft: 10, paddingRight: 10 }}>

              <View style={{ width: 40, justifyContent: 'center', alignItems: 'center' }}>
                <Ripple rippleContainerBorderRadius={100 / 2} rippleCentered={true} style={{ width: 40, height: 40, }}
                  onPress={() => this.props.navigation.goBack()} >
                  <Button style={{ width: 40, height: 40, borderRadius: 100 / 2, }} transparent>
                    <Icon style={{ color: '#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                  </Button>
                </Ripple>
              </View>

              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 20, color: '#fff', fontWeight: 'bold', textAlign: 'center' }}>
                  {this.props.reduxState.Auth.workspaceqr.workspacename}
                </Text>
              </View>

              <View style={{ width: 40, justifyContent: 'center', alignItems: 'center' }} />

            </View>
          </SafeAreaView>
        </View>
        <Content>
          <View style={styles.pdB20}>
            <View style={styles.mgT50}>
              <View style={styles.aligI_center}>
                <CardItem style={{ height: 200, width: '90%' }}>
                  <Image source={require('../../image/logo-bms.png')} style={{ height: '100%', width: '100%', resizeMode: 'contain' }} />
                </CardItem>
              </View>
            </View>
            <View style={[styles.pdB20, styles.pdL5_100, styles.pdR5_100]}>
              <Form style={[styles.pdR15]}>
                <View>
                  <View>
                    <Text style={styles.textForm}>
                      Please enter email to receive confirmation code
                    </Text>
                  </View>
                  <Item iconLeft style={{ paddingTop: 20, paddingBottom: 10 }}>
                    <Icon style={styles.w40} name='email' type='MaterialCommunityIcons' />
                    <Input style={styles.fSize20} placeholder="Email" placeholderTextColor="#a7a7a7" onChangeText={(email) => this.setState({ email })} value={this.state.email} />
                  </Item>
                </View>
              </Form>
            </View>
          </View>
        </Content>
        <Footer style={styles.footerMain}>
          <View style={styles.outbtnfooter}>
            <View style={styles.footerpd}>
              <View style={styles.footerpd1}>
                <Button info block full>
                  <Ripple style={styles.btnfooter} onPress={() => this.btnSM()}>
                    <Text style={styles.btnfooterText}>SUBMIT</Text>
                  </Ripple>
                </Button>
              </View>
            </View>
          </View>
        </Footer>
        <Loader opacity={1} loading={this.state.isLoading} />
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  reduxState: state
});

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

export default connect(mapStateToProps, {

})(ForgotPassWordScreen);