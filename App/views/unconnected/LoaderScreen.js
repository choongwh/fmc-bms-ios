import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image } from 'react-native';
import { ListItem, CheckBox, Body, CardItem, Button } from 'native-base';
import { StackNavigator } from 'react-navigation';

import Axios from 'axios';
import { connect } from 'react-redux';

import { authSuccess } from '../../redux/actions/auth'
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';

export default class LoaderScreen extends React.Component {
  static navigationOptions = {
    title: 'Loader',
  };

   refreshComponent(newAppointment) {
    this.fetchAppointments();
  }

  fetchAppointments() {
    var self = this;
    var accessToken = this.props.reduxState.Auth.token
    Axios.get(Url.appointments, {headers: Object.assign(Url.headers, accessToken)})
    .then(function (response) {
      if(response.headers["access-token"] != "") {
        self.props.updateAccessToken(response.headers["access-token"])
      }
      self.setState({
        appointments: response.data
      })
    })
    .catch(function (error) {
      console.log("ERROR DURING fecthAppointments", error);
    });
  }


  render() {
    // Get navigation props to be able to use navigate function
    const { navigate } = this.props.navigation;
    return (

        <View style={{ flex: 1, alignItems: 'center', justifyContent:'center', marginTop:50}}>
          <View style={{alignItems: 'center'}}>
              <CardItem style={{width:'90%'}}>
                <Image source = 
                {{uri: 'http://hoale.glinkcentos.ml/images/image/logo-bms.png'}}

                style={{height: 80,flex: 1, resizeMode:'contain', resizeMode:'contain'}}
                />
              </CardItem>
            </View>
          <View style={[styles.container, styles.horizontal]}>
            <ActivityIndicator size="large" color="#2089dc" />
          </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 40,
  }
})