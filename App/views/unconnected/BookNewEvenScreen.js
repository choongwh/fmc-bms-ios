import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Form, Item, Input, Label, CardItem, Button, Icon ,DatePicker} from 'native-base';
import { StackNavigator } from 'react-navigation';

import Axios from 'axios';
import { connect } from 'react-redux';
import DateTimePicker from 'react-native-modal-datetime-picker';

import moment from 'moment';

import { authSuccess } from '../../redux/actions/auth'
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';

export default class BookNewEvenScreen extends React.Component {
  static navigationOptions = {
    title: 'BookNewEven',
  };

  constructor(props) {
    super(props);
    this.state = { chosenDate: new Date() };
    this.setDate = this.setDate.bind(this);

    this.state={
      isVisible : false,
      chosenDate:''
    }

  }

  handlePicker = (time) => {
    this.setState({
      isVisible:false,
      chosenDate: moment(time).format('HH:mm'),
    })
  }

  showPicker = () => {
    this.setState({
      isVisible:true
    })
  }

  hidePicker = () => {
    this.setState({
      isVisible:false
    })
  }

  setDate(newDate) {
    this.setState({ chosenDate: newDate });
  }

   refreshComponent(newAppointment) {
    this.fetchAppointments();
  }

  fetchAppointments() {
    var self = this;
    var accessToken = this.props.reduxState.Auth.token
    Axios.get(Url.appointments, {headers: Object.assign(Url.headers, accessToken)})
    .then(function (response) {
      if(response.headers["access-token"] != "") {
        self.props.updateAccessToken(response.headers["access-token"])
      }
      self.setState({
        appointments: response.data
      })
    })
    .catch(function (error) {
      console.log("ERROR DURING fecthAppointments", error);
    });
  }


  render() {
    // Get navigation props to be able to use navigate function
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>

        <View style={{marginTop:50}}>
          <Text style={{fontSize:30, textAlign:'center', color:'#d9534f'}}>
            Book a new event
          </Text>
        </View>
        <View style={{paddingBottom:20}}>
          <Form >
            <Item iconLeft>
              <Icon style={{width:40}} name='calendar' type='Octicons'/>
              <DatePicker
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText="Date"
                textStyle={{ color: "#000000", fontSize: 18,textAlign:'left' }}
                placeHolderTextStyle={{ color: "#494949", fontSize: 18,textAlign:'left'  }} 
                onDateChange={this.setDate}
                
                />
             
            </Item>

            <Item iconLeft >
              <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
              <View style={{ flex: 1 }}>
               <TouchableOpacity onPress={this.showPicker}>
                  <Text style={{paddingBottom:15, paddingTop: 15, color: "#494949", fontSize: 17,textAlign:'left',  paddingLeft:10}}>
                    {this.state.chosenDate}
                  </Text>
                </TouchableOpacity>

                <DateTimePicker
                  isVisible={this.state.isVisible}
                  onConfirm={this.handlePicker}
                  onCancel={this.hidePicker}
                  mode={'time'}
                  is24Hour={false}
                  
                />
              </View>
            </Item>

            <Item iconLeft >
              <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
              <View style={{ flex: 1 }}>
               <TouchableOpacity onPress={this.showPicker}>
                  <Text style={{paddingBottom:15, paddingTop: 15, color: "#494949", fontSize: 17,textAlign:'left',  paddingLeft:10}}>
                    {this.state.chosenDate}
                  </Text>
                </TouchableOpacity>

                <DateTimePicker
                  isVisible={this.state.isVisible}
                  onConfirm={this.handlePicker}
                  onCancel={this.hidePicker}
                  mode={'time'}
                  is24Hour={false}
                  
                />
              </View>
            </Item>

            <Item iconLeft >
              <Icon style={{width:40}} name='account' type='MaterialCommunityIcons'/>
              <Input placeholder="Attendee" />
            </Item>

            <View style={{marginTop:40, paddingLeft:10, paddingRight:10}}>
              <Button info block >
                <Text style={{color:"#fff", }}>SUBMIT</Text>
              </Button>
            </View>
          </Form>
        </View>
      </ScrollView>
    );
  }
}

