import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image, TouchableOpacity, ListView } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Card, CardItem, Thumbnail, Left, Body, Right, Form, 
  Item, Input, Label, Button, Icon ,DatePicker, List, ListItem, Fab} from 'native-base';

import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import Axios from 'axios';
import { connect } from 'react-redux';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';

import { authSuccess } from '../../redux/actions/auth'
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';

import { createMaterialTopTabNavigator, createStackNavigator,withNavigation } from 'react-navigation';



const datas = [
  {view: 'Simon Mignolet1', date:'10/10/2018', startTime:'10:10', endTime:'10:30', active: 'Active Booking 1'},
  {view: 'Simon Mignolet2', date:'10/10/2018', startTime:'10:10', endTime:'10:30', active: 'Active Booking 2'},
  {view: 'Simon Mignolet3', date:'10/10/2018', startTime:'10:10', endTime:'10:30', active: 'Active Booking 3'},
  {view: 'Simon Mignolet4', date:'10/10/2018', startTime:'10:10', endTime:'10:30', active: 'Active Booking 4'},
  {view: 'Simon Mignolet5', date:'10/10/2018', startTime:'10:10', endTime:'10:30', active: 'Active Booking 5'},
  {view: 'Simon Mignolet6', date:'10/10/2018', startTime:'10:10', endTime:'10:30', active: 'Active Booking 6'},
  {view: 'Simon Mignolet7', date:'10/10/2018', startTime:'10:10', endTime:'10:30', active: 'Active Booking 7'},
  {view: 'Simon Mignolet8', date:'10/10/2018', startTime:'10:10', endTime:'10:30', active: 'Active Booking 8'},
  {view: 'Simon Mignolet9', date:'10/10/2018', startTime:'10:10', endTime:'10:30', active: 'Active Booking 9'},
  {view: 'Simon Mignolet10', date:'10/10/2018', startTime:'10:10', endTime:'10:30', active: 'Active Booking 10'},
  {view: 'Simon Mignolet11', date:'10/10/2018', startTime:'10:10', endTime:'10:30', active: 'Active Booking 11'},
  {view: 'Simon Mignolet12', date:'10/10/2018', startTime:'10:10', endTime:'10:30', active: 'Active Booking 12'},
  
];

class ActiveBookingScreen extends React.Component {
  static navigationOptions = { header: null };

  constructor(props){
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(datas),
    };
  }

  taoHang(data,navigate){
    return (
      <View style={{paddingLeft:10, paddingRight:10, paddingTop:10}} >
        <TouchableOpacity  onPress={() => navigate("Details")}>
          <Card>

            <CardItem  >
              <Body>
                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Icon style={{width:40}} name='location-city' type='MaterialIcons'/>
                    <Text >
                     {data.view}
                    </Text>
                  </View>
                </View>
                
                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                    <Icon style={{width:40}} name='calendar' type='Octicons'/>
                    <Text>
                      {data.date}
                    </Text>
                  </View>
                </View>

                <View style={{flexDirection: "row", flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flex:1}}>
                    <View style={{flexDirection: "row", flex: 1, justifyContent: 'flex-start', alignItems: 'center',}}>
                        <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
                        <Text>
                          {data.startTime}
                        </Text>
                    </View>
                  </View>
                  <View style={{flex:1}}>
                    <Text>
                      {data.active}
                    </Text>
                  </View>
                
                </View>

                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                    <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
                    <Text>
                      {data.endTime}
                    </Text>
                  </View>
                </View>

                
              </Body>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </View>);
  }



  render(){
    const { navigate } = this.props.navigation;
    return(
     
      <View style={{paddingBottom:10}} >
          
          <ListView  dataSource={this.state.dataSource}
            renderRow={data => {
              return this.taoHang(data,navigate)
            }}
          />
      </View>
    );
  }
}

class PastBookingScreen extends React.Component {
  static navigationOptions = { header: null };

  constructor(props){
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(datas),
    };
  }

  taoHang(data,navigate){
    return (
      <View style={{paddingLeft:10, paddingRight:10, paddingTop:10}} >
        <TouchableOpacity  onPress={() => navigate("Details")}>
          <Card>

            <CardItem  >
              <Body>
                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Icon style={{width:40}} name='location-city' type='MaterialIcons'/>
                    <Text >
                     {data.view}
                    </Text>
                  </View>
                </View>
                
                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                    <Icon style={{width:40}} name='calendar' type='Octicons'/>
                    <Text>
                      {data.date}
                    </Text>
                  </View>
                </View>

                <View style={{flexDirection: "row", flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flex:1}}>
                    <View style={{flexDirection: "row", flex: 1, justifyContent: 'flex-start', alignItems: 'center',}}>
                        <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
                        <Text>
                          {data.startTime}
                        </Text>
                    </View>
                  </View>
                  <View style={{flex:1}}>
                    <Text>
                      {data.active}
                    </Text>
                  </View>
                
                </View>

                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                    <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
                    <Text>
                      {data.endTime}
                    </Text>
                  </View>
                </View>

                
              </Body>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </View>);
  }



  render(){
    const { navigate } = this.props.navigation;
    return(
     
      <View style={{paddingBottom:10}} >
          
          <ListView  dataSource={this.state.dataSource}
            renderRow={data => {
              return this.taoHang(data,navigate)
            }}
          />
      </View>
    );
  }
}

class DetailScreen extends React.Component {
  static navigationOptions = { header: null };
  render() {
    return (
        <Text>Text</Text>
    );
  }
}

const ActiveBookingStack = createStackNavigator({
  ActiveBooking: ActiveBookingScreen,
  Details: DetailScreen,
});

const PastBookingStack = createStackNavigator({
  PastBooking: PastBookingScreen,
  Details2: DetailScreen,
});

export default createMaterialTopTabNavigator({
  ActiveBooking: { 
    screen: ActiveBookingStack,
    navigationOptions: {
      tabBarLabel: 'Active Booking',
    },  
  },
  PastBooking: { 
    screen: PastBookingStack,
    navigationOptions: {
      tabBarLabel: 'Past Booking',
    },

  }
},{
  tabBarOptions:{
    tabStyle: {
      height: 60,
      borderBottomColor:'#62b1f6',
    },

    style: {
      backgroundColor:'#ae0f0b',
      paddingTop:20,
    },
  }
});
