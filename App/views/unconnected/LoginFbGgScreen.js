import React,{ Component }  from 'react';
import { StackNavigator } from 'react-navigation';

import LoginForm from '../../models/forms/LoginForm';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image, AppRegistry,  Alert, TouchableOpacity, ListView } from 'react-native';
import {  Header, Content, Tab, Tabs, Card, CardItem, Thumbnail, Left, Body, Right, Form, Item, Input, Label, Button, Icon ,DatePicker, List, ListItem, Fab} from 'native-base';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import { LoginButton, AccessToken } from 'react-native-fbsdk';
import { Url } from '../../../config/api/credentials';

export default class LoginFbGgScreen  extends Component<{}, State> {  

    constructor(props) {
      super(props);
      this.state = {
        userInfo: null,
        error: null,
      };
    }

  async componentDidMount() {
    this._configureGoogleSignIn();
    await this._getCurrentUser();
  }
/***
GOOOGLE 
***/
//bms:1095739769941-a84erfv04d3kvsgrvf267sg68mi2p751.apps.googleusercontent.com
//bmsfamiie:787160340563-hmos18rocot8c6pmddvsdgsqkj4umm18.apps.googleusercontent.com
  _configureGoogleSignIn() {
    GoogleSignin.configure({
      webClientId: Url.googewebclientid,
      offlineAccess: false,
    });
  }

  async _getCurrentUser() {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      this.setState({ userInfo, error: null });
    } catch (error) {
      const errorMessage =
        error.code === statusCodes.SIGN_IN_REQUIRED ? 'Please sign in :)' : error.message;
      this.setState({
        error: new Error(errorMessage),
      });
    }
  }



_signIn = async () => {
  try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      this.setState({ userInfo, error: null });
  } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // sign in was cancelled
      alert('cancelled');
    } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation in progress already
      alert('in progress');
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      alert('play services not available or outdated');
    } else {
      console.log(error);
      alert('Something went wrong', error.toString());
        this.setState({
          error,
      });
    }
  }
};

_signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();

      this.setState({ userInfo: null, error: null });
  } catch (error) {
      this.setState({
        error,
    });
  }
};

renderIsSignedIn() {
    return (
      <Button
      onPress={async () => {
          const isSignedIn = await GoogleSignin.isSignedIn();
          Alert.alert(String(isSignedIn));
      }}
      title="is user signed in?"/>
      
      );
  }

  renderUserInfo(userInfo) {
    return (
    <View style={styles.container}>
    <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 20 }}>
    Welcome {userInfo.user.name}
    </Text>
    <Text>Your user info: {JSON.stringify(userInfo.user)}</Text>

    <TouchableOpacity onPress={this._signOut} >
        <Text>Log out</Text>
    </TouchableOpacity>
    </View>

    );
}
/***
END GOOOGLE 
***/
render() {
    const { userInfo } = this.state;
    const body = userInfo ? this.renderUserInfo(userInfo) : this.renderSignInButton();
    const { navigate } = this.props.navigation;
    return (

      <View style={[styles.container, { flex: 1 }]}>
        {this.renderIsSignedIn()}
        {body}
      </View>
   
    );
}

  renderError() {
    const { error } = this.state;
    if (!error) {
      return null;
    }
    const text = `${error.toString()} ${error.code ? error.code : ''}`;
    return <Text>{text}</Text>;
  }

 renderSignInButton() {
    return (
      <View style={styles.container}>
        <GoogleSigninButton
          style={{ width: 212, height: 48 }}
          size={GoogleSigninButton.Size.Standard}
          color={GoogleSigninButton.Color.Auto}
          onPress={this._signIn}
        />
        <LoginButton
          onLoginFinished={
            (error, result) => {
              if (error) {
                console.log("login has error: " + result.error);
              } else if (result.isCancelled) {
                console.log("login is cancelled.");
              } else {
                AccessToken.getCurrentAccessToken().then(
                  (data) => {
                    console.log(data.accessToken.toString())
                  }
                )
              }
            }
          }
          onLogoutFinished={() => console.log("logout.")}/>
        {this.renderError()}
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});