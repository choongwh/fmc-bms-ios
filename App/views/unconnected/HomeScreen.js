import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image, } from 'react-native';
import { ListItem, CheckBox, Body, CardItem, Button, DatePicker } from 'native-base';
import { StackNavigator } from 'react-navigation';

import Axios from 'axios';
import { connect } from 'react-redux';

import { authSuccess } from '../../redux/actions/auth'
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Home',
  };

  constructor(props) {
    super(props);
    this.state = { chosenDate: new Date() };
    this.setDate = this.setDate.bind(this);
  }
  setDate(newDate) {
    this.setState({ chosenDate: newDate });
  }

   refreshComponent(newAppointment) {
    this.fetchAppointments();
  }

  fetchAppointments() {
    var self = this;
    var accessToken = this.props.reduxState.Auth.token
    Axios.get(Url.appointments, {headers: Object.assign(Url.headers, accessToken)})
    .then(function (response) {
      if(response.headers["access-token"] != "") {
        self.props.updateAccessToken(response.headers["access-token"])
      }
      self.setState({
        appointments: response.data
      })
    })
    .catch(function (error) {
      console.log("ERROR DURING fecthAppointments", error);
    });
  }


  render() {
    // Get navigation props to be able to use navigate function
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <View style={{ flex: 1, alignItems: 'center', justifyContent:'center', marginTop:50}}>
          <View style={{alignItems: 'center'}}>
            <CardItem style={{width:'90%'}}>
              <Image source = 
              {{uri: 'http://hoale.glinkcentos.ml/images/image/logo-bms.png'}}

              style={{height: 80,flex: 1, resizeMode:'contain', resizeMode:'contain'}}
              />
            </CardItem>
          </View>
          <View>
            <DatePicker
              defaultDate={new Date(2018, 4, 4)}
              minimumDate={new Date(1992, 1, 1)}
              maximumDate={new Date(2050, 12, 31)}
              locale={"en"}
              timeZoneOffsetInMinutes={undefined}
              modalTransparent={false}
              animationType={"fade"}
              androidMode={"default"}
              placeHolderText="Select date"
              textStyle={{ color: "#2089dc", fontSize: 25,textAlign:'center' }}
              placeHolderTextStyle={{ color: "#d3d3d3", fontSize: 25,textAlign:'center'  }} 
              onDateChange={this.setDate}
              
              />
            <Text style={{textAlign:'center', fontSize: 25}}>
              Date: {this.state.chosenDate.toString().substr(4, 12)}
            </Text>
          </View>
      </View>
    </ScrollView>
    );
  }
}

