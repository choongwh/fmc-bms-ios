import React from 'react';
import { SafeAreaView, StyleSheet, Text, View, ActivityIndicator, ScrollView, TouchableOpacity, Dimensions, Platform, StatusBar, NetInfo } from 'react-native';
import { Container, Header, Content, Footer, ListItem, CheckBox, Body, CardItem, Button, Icon, Form, Item, Input } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { StackNavigator } from 'react-navigation';
import styles from '../stylecss/style.css';
import Axios from 'axios';
import { connect } from 'react-redux';
import { authSuccess, updateAccessToken, updateQrCode } from '../../redux/actions/auth'
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';
import Ripple from 'react-native-material-ripple';
import QRCodeScanner from 'react-native-qrcode-scanner';
import Ionicons from "react-native-vector-icons/Ionicons";
import * as Animatable from "react-native-animatable";
import Toast from 'react-native-root-toast';

// const MyStatusBar = ({ backgroundColor, ...props }) => (
//   <View style={[statusbar.statusBar, { backgroundColor }]}>
//     <StatusBar translucent backgroundColor={backgroundColor} {...props} />
//   </View>
// );

const statusbar = {
  container: {
    height: 20,
    width: '100%'
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#000',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#000'
  },
}

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

console.disableYellowBox = true;

const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency
const rectDimensions = SCREEN_WIDTH * 0.85; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.0025; // this is equivalent to 2 from a 393 device width
const scanBarWidth = SCREEN_WIDTH * 0.6; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const rectBorderColor = "#fff";
const scanBarColor = "red";
const iconScanColor = "#fff";

class ScanQRCodeScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      workspace_id: this.props.reduxState.Auth.workspaceqr.qrcode,
      workspace_name: '',
      connection_Status: "",
      counter: 0,
      errMsg: '',
      loading: false,
    };
  }

  async linkbtnSubmit() {
    this.props.navigation.navigate('Workspace');
  };

  componentDidMount() {

    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this._handleConnectivityChange

    );

    NetInfo.isConnected.fetch().done((isConnected) => {

      if (isConnected === true) {
        this.setState({ connection_Status: "Online" })
      }
      else {
        this.setState({ connection_Status: "Offline" })
      }

    });
  }

  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this._handleConnectivityChange

    );

  }
  componentWillMount = () => {
    const headerConfig = {
      header: null,
    };
    this.props.navigation.setParams({ headerConfig });
  }
  /***
   Start Status connection
   ***/

  _handleConnectivityChange = (isConnected) => {

    if (isConnected == true) {
      this.setState({ connection_Status: "Online" });
      Toast.show('CONNECTION TO HOST RESUME.');
    }
    else {
      this.setState({ connection_Status: "Offline" })
    }
  };

  connectionLost(self) {
    self.setState({ errMsg: 'Connection lost' });
    self.setState({ isLoading: false })
    self.setState({ counter: self.state.counter + 1 });
    if (self.state.counter >= 3) {
      Toast.show('CONNECTION LOST');
    }

  }

  noInternet(self) {
    if (self.state.connection_Status == "Online") {
      return true
    } else {
      Toast.show('Connection to host lost.');
      return false;
    }
  }


  /***
 End Status connection
  ***/

  static navigationOptions = ({ navigation }) => {
    return navigation.state.params ? navigation.state.params.headerConfig : { title: '' };
  };
  async linkUpdateRegister() {
    this.setState({ loading: true })
    this.verifyWorkspaceId(this.state.workspace_id);
  }

  onSuccess(e) {
    this.verifyWorkspaceId(e.data);
  }

  verifyWorkspaceId(workspace_id) {
    if (!this.noInternet(this)) {
      this.setState({
        disableSubmit: false,
        isLoading: false,
        errMsg: '',
      });
      return;
    }
    var credentials = { mtd: 1, id: workspace_id };
    Axios.post(Url.workspace, credentials, { headers: Url.headers })
      .then((response) => {
        this.setState({ counter: 0 });
        if (parseInt(response.data.status) == 1) {
          this.setState({
            workspace_id: workspace_id,
            workspace_name: response.data.name
          });
          this.props.updateQrCode(workspace_id, false, response.data.name);
          this.props.navigation.navigate('Login');
        } else {
          this.setState({ errMsg: 'Invalid workspace' });
          if (this.scanner) {
            this.scanner.reactivate();
          }
        }
      })
      .catch((error) => {
        this.connectionLost(this);
        console.log(error);
      });
  }


  onScanScreenCamera() {
    this.props.updateQrCode(this.props.reduxState.Auth.workspaceqr.qrcode, true, this.props.reduxState.Auth.workspaceqr.workspacename);
  }

  async gobackScreen() {
    this.props.updateQrCode(this.props.reduxState.Auth.workspaceqr.qrcode, false, this.props.reduxState.Auth.workspaceqr.workspacename);
  }

  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18
      },
      to: {
        [translationType]: fromValue
      }
    };
  }

  render() {
    const { navigate } = this.props.navigation;
    const { loading } = this.state
    if (this.props.reduxState.Auth.workspaceqr.opencamera) {
      return (
        <Container>
          <StatusBar barStyle='light-content' />
          <View style={{
            backgroundColor: '#529ff3', shadowColor: '#494949', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 1
          }}>
            <SafeAreaView>
              <View style={{
                flexDirection: 'row', justifyContent: "space-between",
                height: 58, paddingLeft: 10, paddingRight: 10
              }}>

                <View style={{ width: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Ripple rippleContainerBorderRadius={100 / 2} rippleCentered={true} style={{ width: 40, height: 40, }}
                    onPress={() => this.gobackScreen()} >
                    <Button style={{ width: 40, height: 40, borderRadius: 100 / 2, }} transparent>
                      <Icon style={{ color: '#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                    </Button>
                  </Ripple>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontSize: 20, color: '#fff', fontWeight: 'bold', textAlign: 'center' }}>
                    Scan qr code
                </Text>
                </View>

                <View style={{ width: 40, justifyContent: 'center', alignItems: 'center' }} />

              </View>
            </SafeAreaView>
          </View>
          <Content>
            <QRCodeScanner ref={(node) => { this.scanner = node }} onRead={this.onSuccess.bind(this)}
              showMarker
              cameraStyle={{ height: SCREEN_HEIGHT }}
              customMarker={
                <View style={stylesCamera.rectangleContainer}>
                  <View style={stylesCamera.topOverlay}>
                    <Text style={{ fontSize: 30, color: "white" }}>
                      REGISTER WORKSPACE
                    </Text>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <View style={stylesCamera.leftAndRightOverlay} />

                    <View style={stylesCamera.rectangle}>
                      <Ionicons
                        name="ios-qr-scanner"
                        size={SCREEN_WIDTH * 0.95}
                        color={iconScanColor}
                      />
                      <Animatable.View
                        style={stylesCamera.scanBar}
                        direction="alternate-reverse"
                        iterationCount="infinite"
                        duration={1700}
                        easing="linear"
                        animation={this.makeSlideOutTranslation(
                          "translateY",
                          SCREEN_WIDTH * -0.7
                        )}
                      />
                    </View>

                    <View style={stylesCamera.leftAndRightOverlay} />
                  </View>

                  <View style={stylesCamera.bottomOverlay} />
                </View>
              }
            />
          </Content>
        </Container>
      );
    } else {
      return (
        <Container>
          <StatusBar barStyle='light-content' />
          <View style={{
            backgroundColor: '#529ff3', shadowColor: '#494949', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 1
          }}>
            <SafeAreaView>
              <View style={{
                flexDirection: 'row', justifyContent: "space-between",
                height: 58, paddingLeft: 10, paddingRight: 10
              }}>
                <View style={{ width: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Ripple rippleContainerBorderRadius={100 / 2} rippleCentered={true} style={{ width: 40, height: 40, }}
                    onPress={() => this.props.navigation.goBack()} >
                    <Button style={{ width: 40, height: 40, borderRadius: 100 / 2, }} transparent>
                      <Icon style={{ color: '#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                    </Button>
                  </Ripple>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontSize: 20, color: '#fff', fontWeight: 'bold', textAlign: 'center' }}>
                    {this.props.reduxState.Auth.workspaceqr.workspacename}
                  </Text>
                </View>

                <View style={{ width: 40, justifyContent: 'center', alignItems: 'center' }}>

                </View>
              </View>
            </SafeAreaView>
          </View>
          <Content>
            <View style={[styles.layout_c_f1, styles.pdB20, styles.pdL5_100, styles.pdR5_100]}>
              <View style={[styles.pdL15, styles.pdR15, styles.mgT50]}>
                <View full block style={styles.mgB20}>
                  <Text style={[styles.fSize24, styles.textCenter, styles.color_blur]}>REGISTER TO WORKSPACE</Text>
                </View>
                <View block style={styles.out_box}>
                  <Text>Current workspace: </Text>
                  <Text> {this.props.reduxState.Auth.workspaceqr.workspacename} </Text>
                </View>
              </View>
              <View style={[styles.layout_c_f1, styles.pdB20, styles.pdL5_100, styles.pdR5_100]}>
                <Button info block full style={{ borderRadius: 0, marginTop: 20 }}>
                  <Ripple style={{ flex: 1, height: 45, justifyContent: 'center' }} onPress={() => this.onScanScreenCamera()}>
                    <Text style={{ color: "white", fontSize: 16, textAlign: 'center' }}>Change Workspace</Text>
                  </Ripple>
                </Button>
                <View style={{ marginTop: 15 }}>
                  <Text style={{ fontSize: 16, color: "#000", textAlign: 'center' }}>
                    Or input workspace id:
                  </Text>
                  <Form style={{ marginLeft: -8 }}>
                    <Item iconLeft style={{ paddingTop: 10, width: 300 }}>
                      <Icon style={{ width: 40, }} name='qrcode' type='MaterialCommunityIcons' />
                      <Input autoCapitalize={false} style={{ fontSize: 20 }} placeholderTextColor="#a7a7a7" placeholder="Workspace ID" onChangeText={(workspace_id) => this.setState({ workspace_id })} value={this.state.workspace_id} />
                    </Item>
                  </Form>
                  <Text
                    children={this.state.errMsg}
                    style={{ color: '#FF0000', fontSize: 16, height: 18, textAlign: 'center' }}
                  />
                  <Button info block full
                    style={{ borderRadius: 0, marginTop: 10, opacity: loading ? 0.5 : 1 }}
                    disabled={loading}
                  >
                    <Ripple style={{ flex: 1, height: 45, justifyContent: 'center' }} onPress={() => this.linkUpdateRegister()}>
                      <Text
                        children={loading ? 'Please wait...' : 'Confirm'}
                        style={{ color: "white", fontSize: 16, textAlign: 'center' }}
                      />
                    </Ripple>
                  </Button>
                </View>
              </View>
            </View>
          </Content>
        </Container>
      );
    }
  }
}

const stylesCamera = {
  rectangleContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    borderWidth: rectBorderWidth,
    borderColor: rectBorderColor,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: "center",
    alignItems: "center"
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor
  },

  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: scanBarColor
  }
};

const mapStateToProps = (state) => ({
  reduxState: state
});

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

export default connect(mapStateToProps, {
  Auth,
  updateAccessToken,
  updateQrCode
})(ScanQRCodeScreen);
