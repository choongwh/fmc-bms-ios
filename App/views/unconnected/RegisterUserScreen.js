import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image } from 'react-native';
import { ListItem, CheckBox, Body, CardItem, Button } from 'native-base';
import { StackNavigator } from 'react-navigation';

import Axios from 'axios';
import { connect } from 'react-redux';

import { authSuccess } from '../../redux/actions/auth'
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';

export default class RegisterUserScreen extends React.Component {
  static navigationOptions = {
    title: 'RegisterUser',
  };

  render() {
    // Get navigation props to be able to use navigate function
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <View style={{ flex: 1, alignItems: 'center', justifyContent:'center', marginTop:50}}>
          <View style={{alignItems: 'center'}}>
            <CardItem style={{height: 200, width: '90%'}}>
              <Image source ={{uri: '/image/logo-bms.png'}} style={{ height:'100%', width: '100%', resizeMode:'contain'}}/>
            </CardItem>
          </View>
          <View >
            <View style={{marginTop:40}}>
              <Text style={{fontSize: 25, textAlign: "center", color:"#2089dc" }}>
                Wellcome To
              </Text>
              <Text style={{fontSize: 30, textAlign: "center", color:"#d9534f" }}>
                Workspace name
              </Text>
            </View>

            <View style={{marginTop:40}}>
              <Button info block style={{width:300}}>
                <Text style={{color:"#fff", }}>CONTINUE REGISTER USER</Text>
              </Button>
            </View>
          </View>
      </View>
    </ScrollView>
    );
  }
}

