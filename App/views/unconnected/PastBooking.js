import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView } from 'react-native';
import { ListItem, CheckBox, Body, CardItem, Button } from 'native-base';
import { StackNavigator } from 'react-navigation';

import Axios from 'axios';
import { connect } from 'react-redux';

import { authSuccess } from '../../redux/actions/auth'
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';

export default class PastBooking extends React.Component {
  static navigationOptions = {
    title: 'PastBooking',
  };

   refreshComponent(newAppointment) {
    this.fetchAppointments();
  }

  fetchAppointments() {
    var self = this;
    var accessToken = this.props.reduxState.Auth.token
    Axios.get(Url.appointments, {headers: Object.assign(Url.headers, accessToken)})
    .then(function (response) {
      if(response.headers["access-token"] != "") {
        self.props.updateAccessToken(response.headers["access-token"])
      }
      self.setState({
        appointments: response.data
      })
    })
    .catch(function (error) {
      console.log("ERROR DURING fecthAppointments", error);
    });
  }


  render() {
    // Get navigation props to be able to use navigate function
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <View style={{ flex: 1, alignItems: 'center', justifyContent:'center'}}>
          <Text>
            tab 2
          </Text>
          
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 40
  }
})