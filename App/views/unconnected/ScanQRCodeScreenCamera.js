import React from 'react';
import { SafeAreaView, StyleSheet, Text, View, ActivityIndicator, ScrollView, TouchableOpacity, Dimensions, Platform, StatusBar } from 'react-native';
import { Container, Header, Content, Footer, ListItem, CheckBox, Body, CardItem, Button, Icon, Form, Item, Input } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { StackNavigator } from 'react-navigation';
import styles from '../stylecss/style.css';
import Axios from 'axios';
import { connect } from 'react-redux';
import { authSuccess, updateAccessToken, updateQrCode } from '../../redux/actions/auth'
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';
import Ripple from 'react-native-material-ripple';
import QRCodeScanner from 'react-native-qrcode-scanner';
import Ionicons from "react-native-vector-icons/Ionicons";
import * as Animatable from "react-native-animatable";

// const MyStatusBar = ({ backgroundColor, ...props }) => (
//   <View style={[statusbar.statusBar, { backgroundColor }]}>
//     <StatusBar translucent backgroundColor={backgroundColor} {...props} />
//   </View>
// );

const statusbar = {
  container: {
    height: 20,
    width: '100%'
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#000',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#000'
  },
}

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

console.disableYellowBox = true;

const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency
const rectDimensions = SCREEN_WIDTH * 0.85; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.0025; // this is equivalent to 2 from a 393 device width
const scanBarWidth = SCREEN_WIDTH * 0.6; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const rectBorderColor = "#fff";
const scanBarColor = "red";
const iconScanColor = "#fff";

class ScanQRCodeScreenCamera extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      workspace_id: this.props.reduxState.Auth.workspaceqr.qrcode,
      workspace_name: ''
    };
  }

  componentWillMount = () => {
    const headerConfig = {
      header: null,
    };
    this.props.navigation.setParams({ headerConfig });
    // if(this.props.reduxState.Auth.workspaceqr.qrcode != ''){
    //   this.props.navigation.navigate('Login');
    // }
  }

  async linkbtnshowLogin() {
    this.props.navigation.navigate('Login');
  };

  static navigationOptions = ({ navigation }) => {
    return navigation.state.params ? navigation.state.params.headerConfig : { title: 'SCAN QRCODE', headerTitleStyle: { fontWeight: 'bold', alignSelf: 'center', textAlign: 'center', justifyContent: 'center', flex: 1, marginLeft: -40 }, };
  };

  async linkUpdateRegister() {
    this.verifyWorkspaceId(this.state.workspace_id);
  }

  onSuccess(e) {
    this.verifyWorkspaceId(e.data);
  }
  verifyWorkspaceId(workspace_id) {
    var self = this;
    var credentials = { mtd: 1, id: workspace_id };
    Axios.post(Url.workspace, credentials, { headers: Url.headers })
      .then(function (response) {
        if (parseInt(response.data.status) == 1) {
          self.setState({
            workspace_id: workspace_id,
            workspace_name: response.data.name
          });

          self.props.updateQrCode(workspace_id, false, response.data.name);
          self.props.navigation.navigate('Login');
        } else {
          if (self.scanner) {
            self.scanner.reactivate();
          }
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  onScanScreenCamera() {

    this.props.updateQrCode(this.props.reduxState.Auth.workspaceqr.qrcode, true, this.props.reduxState.Auth.workspaceqr.workspacename);

  }

  async gobackScreen() {
    this.props.updateQrCode(this.props.reduxState.Auth.workspaceqr.qrcode, false, this.props.reduxState.Auth.workspaceqr.workspacename);
  }

  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18
      },
      to: {
        [translationType]: fromValue
      }
    };
  }

  render() {
    const { navigate } = this.props.navigation;
    if (this.props.reduxState.Auth.workspaceqr.opencamera) {
      return (
        <Container>
          <View style={{
            backgroundColor: '#529ff3', shadowColor: '#494949', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 1
          }}>
            <StatusBar barStyle='light-content' />
            <SafeAreaView>
              <View style={{
                flexDirection: 'row', justifyContent: "space-between",
                height: 58, paddingLeft: 10, paddingRight: 10
              }}>

                <View style={{ width: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Ripple rippleContainerBorderRadius={100 / 2} rippleCentered={true} style={{ width: 40, height: 40, }}
                    onPress={() => this.gobackScreen()} >
                    <Button style={{ width: 40, height: 40, borderRadius: 100 / 2, }} transparent>
                      <Icon style={{ color: '#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                    </Button>
                  </Ripple>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontSize: 20, color: '#fff', fontWeight: 'bold', textAlign: 'center' }}>
                    {/*{this.props.reduxState.Auth.workspaceqr.workspacename}*/}
                    Scan qr code
              </Text>
                </View>

                <View style={{ width: 40, justifyContent: 'center', alignItems: 'center' }} />

              </View>
            </SafeAreaView>
          </View>
          <Content>
            <QRCodeScanner ref={(node) => { this.scanner = node }} onRead={this.onSuccess.bind(this)}
              showMarker
              cameraStyle={{ height: SCREEN_HEIGHT }}
              customMarker={
                <View style={stylesCamera.rectangleContainer}>
                  <View style={stylesCamera.topOverlay}>
                    <Text style={{ fontSize: 30, color: "white" }}>
                      REGISTER WORKSPACE
                    </Text>
                  </View>

                  <View style={{ flexDirection: "row" }}>
                    <View style={stylesCamera.leftAndRightOverlay} />

                    <View style={stylesCamera.rectangle}>
                      <Ionicons
                        name="ios-qr-scanner"
                        size={SCREEN_WIDTH * 0.95}
                        color={iconScanColor}
                      />
                      <Animatable.View
                        style={stylesCamera.scanBar}
                        direction="alternate-reverse"
                        iterationCount="infinite"
                        duration={1700}
                        easing="linear"
                        animation={this.makeSlideOutTranslation(
                          "translateY",
                          SCREEN_WIDTH * -0.7
                        )}
                      />
                    </View>

                    <View style={stylesCamera.leftAndRightOverlay} />
                  </View>

                  <View style={stylesCamera.bottomOverlay} />
                </View>
              }
            />
          </Content>
        </Container>

      );
    } else {
      return (
        <Container>
          <View style={{ backgroundColor: '#529ff3', shadowColor: '#494949', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.75, elevation: 1 }}>
            <StatusBar barStyle='light-content' />
            <SafeAreaView>
              <View style={{
                flexDirection: 'row', justifyContent: "space-between",
                height: 58, paddingLeft: 10, paddingRight: 10
              }}>

                <View style={{ width: 40, justifyContent: 'center', alignItems: 'center' }}>
                  {/*<Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}
              onPress={() => this.linkbtnshowLogin()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                </Button>
              </Ripple>*/}
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontSize: 20, color: '#fff', fontWeight: 'bold', textAlign: 'center' }}>
                    {/*{this.props.reduxState.Auth.workspaceqr.workspacename}*/}
                    Scan QR code
              </Text>
                </View>

                <View style={{ width: 40, justifyContent: 'center', alignItems: 'center' }}>

                </View>

              </View>
            </SafeAreaView>
          </View>
          <Content>
            <View style={[styles.layout_c_f1, styles.pdB20, styles.pdL5_100, styles.pdR5_100]}>
              <View style={[styles.pdL15, styles.pdR15, styles.mgT50]}>
                <View full block style={styles.mgB20}>
                  <Text style={[styles.fSize24, styles.textCenter, styles.color_blur]}>REGISTER TO WORKSPACE</Text>
                </View>
                <View block style={styles.out_box}>
                  <Text>Current workspace: </Text>
                  <Text> {this.props.reduxState.Auth.workspaceqr.workspacename} </Text>
                </View>
              </View>
              <View style={[styles.layout_c_f1, styles.pdB20, styles.pdL5_100, styles.pdR5_100]}>

                <Button info block full style={{ borderRadius: 0, marginTop: 20 }}>
                  <Ripple style={{ flex: 1, height: 45, justifyContent: 'center' }} onPress={() => this.onScanScreenCamera()}>
                    <Text style={{ color: "white", fontSize: 16, textAlign: 'center' }}>Scan Workspace</Text>
                  </Ripple>
                </Button>

                <View style={{ marginTop: 15 }}>
                  <Text style={{ fontSize: 16, color: "#000", textAlign: 'center' }}>
                    Or input workspace id:
                  </Text>
                  <Form style={{ marginLeft: -8 }}>
                    <Item iconLeft style={{ paddingTop: 10, width: 300 }}>
                      <Icon style={{ width: 40, }} name='qrcode' type='MaterialCommunityIcons' />
                      <Input style={{ fontSize: 20 }} placeholderTextColor="#a7a7a7" placeholder="Workspace ID" onChangeText={(workspace_id) => this.setState({ workspace_id })} value={this.state.workspace_id} />
                    </Item>
                  </Form>
                  <Button info block full style={{ borderRadius: 0, marginTop: 10 }}>
                    <Ripple style={{ flex: 1, height: 45, justifyContent: 'center' }} onPress={() => this.linkUpdateRegister()}>
                      <Text style={{ color: "white", fontSize: 16, textAlign: 'center' }}>Confirm</Text>
                    </Ripple>
                  </Button>
                </View>
              </View>
            </View>
          </Content>
        </Container>
      );
    }
  }
}

const stylesCamera = {
  rectangleContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    borderWidth: rectBorderWidth,
    borderColor: rectBorderColor,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: "center",
    alignItems: "center"
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor
  },

  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: scanBarColor
  }
};

const mapStateToProps = (state) => ({
  reduxState: state
});

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;
export default connect(mapStateToProps, {
  Auth,
  updateAccessToken,
  updateQrCode
})(ScanQRCodeScreenCamera);