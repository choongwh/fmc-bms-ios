import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView, Image, TouchableOpacity, ListView, Alert, Dimensions, Platform } from 'react-native';
import { Container, Header, Content, Tab, Tabs, Card, CardItem, Thumbnail, Left, Body, Right, Form, 
  Item, Input, Label, Button, Icon ,DatePicker, List, ListItem, Fab, Picker, Textarea, Footer, Switch } from 'native-base';

import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import Axios from 'axios';
import { connect } from 'react-redux';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import _ from 'lodash';
import Auth from '../../redux/reducers/auth';
import { updateAccessToken, updateDetailEvent, updateQrCode } from '../../redux/actions/auth';
import { Url } from '../../../config/api/credentials';
import QRCodeScanner from 'react-native-qrcode-scanner';
import MultiSelect from 'react-native-multiple-select';
// import Toast from 'react-native-toast-native';
import Toast from 'react-native-root-toast';
import { TabNavigator, StackNavigator,createMaterialTopTabNavigator,createStackNavigator } from 'react-navigation';
import CheckBox from 'react-native-check-box';
import Ionicons from "react-native-vector-icons/Ionicons";
import * as Animatable from "react-native-animatable";
import Ripple from 'react-native-material-ripple';

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;

console.disableYellowBox = true;

const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency
const rectDimensions = SCREEN_WIDTH * 0.85; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = SCREEN_WIDTH * 0.0025; // this is equivalent to 2 from a 393 device width
const scanBarWidth = SCREEN_WIDTH * 0.6; // this is equivalent to 180 from a 393 device width
const scanBarHeight = SCREEN_WIDTH * 0.0025; //this is equivalent to 1 from a 393 device width
const rectBorderColor = "#fff";
const scanBarColor = "red";  
const iconScanColor = "#fff";


import { MenuContext, Menu, MenuOptions, MenuOption, MenuTrigger, MenuProvider, renderers } from 'react-native-popup-menu';

const styles = {
  
  wrapper: {
    flex: 1,
    marginTop: 150,
  },
  submitButton: {
    paddingHorizontal: 10,
    paddingTop: 20,
  },
  cardImage_h:{
    height: 80,flex: 1
  },

  footerbtn:{backgroundColor:'#fff', height: 45},
  outbtnfooter:{flex: 1, justifyContent: 'flex-start', alignItems: 'center'},
  btnfooter:{flex: 1, height: 45, justifyContent: 'center'},
  btnfooterText:{color:"#fff", fontSize: 16, textAlign:'center'},
  textForm:{
    textAlign: 'center', fontSize: 22,  fontWeight: 'bold', padding: 15,
  },

  color_blur:{color: '#2089dc'},

  layout_r_f1:{flexDirection: "row", flex: 1, justifyContent: 'space-between', },
  layout_c_f1:{flexDirection: "column", flex: 1, justifyContent: 'space-between', alignItems:'center'},

  w40:{width: 40}, w50:{width: 50}, w100:{width: 100}, w150:{width: 150}, w200:{width: 200}, w250:{width: 250}, w300:{width: 300},  w320:{width: 320},

  pdL5_100: {paddingLeft: '5%'}, 
  pdR5_100: {paddingRight: '5%'}, 

  mgB0:{marginBottom: 0}, mgB5:{marginBottom: 5}, mgB10:{marginBottom: 10}, mgB15:{marginBottom: 15}, mgB20:{marginBottom: 20}, 
  mgT50:{marginTop: 50},

  pdT0:{paddingTop: 0}, pdT5:{paddingTop: 5}, pdT10:{paddingTop: 10}, pdT15:{paddingTop: 15}, pdT20:{paddingTop: 20},
  pdB0:{paddingBottom: 0}, pdB5:{paddingBottom: 5}, pdB10:{paddingBottom: 10}, pdB15:{paddingBottom: 15}, pdB20:{paddingBottom: 20},
  pdL0:{paddingLeft: 0}, pdL5:{paddingLeft: 5}, pdL10:{paddingLeft: 10}, pdL15:{paddingLeft: 15}, pdL20:{paddingLeft: 20},
  pdR0:{paddingRight: 0}, pdR5:{paddingRight: 5}, pdR10:{paddingRight: 10}, pdR15:{paddingRight: 15}, pdR20:{paddingRight: 20},

  fSize16:{fontSize: 16}, fSize18:{fontSize: 18}, fSize20:{fontSize: 20}, fSize24:{fontSize: 24},

  textCenter:{textAlign:'center'}, textLeft:{textAlign:'left'}, textRight:{textAlign:'right'},

  flex1:{flex: 1}, jusbetween:{justifyContent:'space-between'}, 
  flex_column:{flexDirection:'column'}, flex_row:{flexDirection: 'row'}, aligI_center:{alignItems:'center'},
};

const stylesCamera = {
  rectangleContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  rectangle: {
    height: rectDimensions,
    width: rectDimensions,
    borderWidth: rectBorderWidth,
    borderColor: rectBorderColor,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  topOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    justifyContent: "center",
    alignItems: "center"
  },

  bottomOverlay: {
    flex: 1,
    height: SCREEN_WIDTH,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor,
    paddingBottom: SCREEN_WIDTH * 0.25
  },

  leftAndRightOverlay: {
    height: SCREEN_WIDTH * 0.65,
    width: SCREEN_WIDTH,
    backgroundColor: overlayColor
  },

  scanBar: {
    width: scanBarWidth,
    height: scanBarHeight,
    backgroundColor: scanBarColor
  }
};

const stylesToast={
  backgroundColor: "#2f4145",
  height: Platform.OS === ("ios") ? 70 : 125,
  color: "#ffffff",
  fontSize: 12,
  borderRadius: 15,
}

class ActiveBookingScreen extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      ListEventArray: [],
    };
  }

  static navigationOptions = { header: null };

  componentDidMount = () => {
    this.loadPublicEvent();
  }

  refreshComponent() {
    this.loadPrivateEvent();
  }

  async loadPrivateEvent(){
    var self = this;
    var credentials = {'mtd': 5, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        var dataUnion = _.union(self.state.ListEventArray, response.data.data);
        self.setState({
          ListEventArray: dataUnion
        });
        self.loadCreatedEvent();
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  async loadCreatedEvent(){
    var self = this;
    var credentials = {'mtd': 6, token: this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        var dataUnion = _.union(self.state.ListEventArray, response.data.data);
        let arrayData = [];
        var today = moment(new Date()).format('YYYY-MM-DD');
        for (let eventObject of dataUnion) {
          var event_date = moment(eventObject.event_start,'YYYY-MM-DD');
          if(moment(event_date).isAfter(today) || moment(event_date).format('YYYY-MM-DD') == today){
            eventObject.start = moment(eventObject.event_start).format('h:mm a');
            eventObject.end = moment(eventObject.event_end).format('h:mm a');
            arrayData.push(eventObject);
          }
        }
        self.setState({
            dataSource: self.state.dataSource.cloneWithRows(_.sortBy(arrayData, ['event_start']))
        });
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  detailEvent(detail_event){
    this.props.screenProps.updateDetailEvent(detail_event);
    this.props.navigation.navigate("Details",{detail_event: detail_event, refreshComponent: () => this.refreshComponent()});
  }

  taoHang(data,navigate){
    return (
      <View style={{paddingLeft:10, paddingRight:10, paddingTop:10}} >
        <TouchableOpacity  onPress={() => this.detailEvent(data)}>
          <Card>
            <CardItem  >
              <Body>
                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Icon style={{width:40}} name='event' type='SimpleLineIcons'/>
                    <Text style={{fontSize:18, color: '#000'}}>
                      {data.event_name}
                    </Text>
                  </View>
                </View>

                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Icon style={{width:40}} name='location-city' type='MaterialIcons'/>
                    <Text style={{fontSize:18, color: '#000'}}>
                     {data.location_name}
                    </Text>
                  </View>
                </View>

                <View style={{flexDirection: "row", flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flex:1}}>
                    <View style={{flexDirection: "row", flex: 1, justifyContent: 'flex-start', alignItems: 'center',}}>
                      <Icon style={{width:40}} name='calendar' type='Octicons'/>
                      <Text style={{fontSize:18, color: '#000'}}>
                        {moment(data.event_start).format('MM/DD/YYYY')}
                      </Text>
                    </View>
                  </View>
                  
                  <View style={{flex:1}}>
                    <View style={{flexDirection: "row", flex: 1, justifyContent: 'flex-start', alignItems: 'center',}}>
                        <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
                        <Text style={{fontSize:18, color: '#000'}}>
                          {data.start}
                        </Text>
                    </View>
                  </View>
                </View>

                <View style={{flexDirection: "row", flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flex: 1}}>
                    <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                      <Icon style={{width:40}} name='calendar' type='Octicons'/>
                      <Text style={{fontSize:18, color: '#000'}}>
                        {moment(data.event_end).format('MM/DD/YYYY')}
                      </Text>
                    </View>
                  </View>
                  
                  <View style={{flex: 1}}>
                    <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                      <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
                      <Text style={{fontSize:18, color: '#000'}} >
                        {data.end}
                      </Text>
                    </View>
                  </View>
                </View>
              </Body>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </View>
    );
  }



  render(){
    const { navigate } = this.props.navigation;
    return(
      <Container>
        
        <View style={{flexDirection: "row", justifyContent: "space-between", backgroundColor: '#529ff3', height: 50, paddingTop: 10}}>
          <View style={{width: 50}}>
            <Button onPress={() => this.props.navigation.openDrawer()} style={{width: 100, height: 50, marginTop: -10, padding: 0}} transparent> 
              <Icon style={{color:'#fff', }} name='navicon' type='FontAwesome' />
            </Button>
          </View>
          <View style={{flex: 1,}}>
            <Text  style={{color: '#fff', fontSize: 20, fontWeight: 'bold', textAlign: 'center'}}>Active booking</Text>
          </View>
          <View style={{width: 50}}></View>
        </View>

        <Content style={{flex: 1}}>
          <View style={{paddingBottom:10, marginTop: 20,}}>
            <ListView  dataSource={this.state.dataSource}
              enableEmptySections
              renderRow={data => {
                return this.taoHang(data,navigate)
              }}
            />
          </View>
        </Content>
          
        <Fab 
          style={{ backgroundColor:'rgb(100,121,133)' }}
          position="bottomRight" >
            <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} rippleDuration = {400}
              style={{height: 56, width: 56, borderRadius: 100/2, justifyContent: 'center', alignItems: 'center'}}
              onPress={() => {this.props.navigation.navigate("NewEvent", { refreshComponent: () => this.refreshComponent() })}} >
              <Icon style={{color: '#fff'}} name="add" />
            </Ripple>
        </Fab>

      </Container>
     
      
    );
  }
}

class PastBookingScreen extends React.Component {
  static navigationOptions = { header: null };

  constructor(props){
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      ListEventArray: [],
    };
  }

  componentDidMount = () => {
    this.loadPublicEvent();
  }

  refreshComponent() {
    this.loadPublicEvent();
  }

  async loadPublicEvent(){
    var self = this;
    var credentials = {'mtd': 4, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.setState({
          ListEventArray: response.data.data
        });
        self.loadPrivateEvent();
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  async loadPrivateEvent(){
    var self = this;
    var credentials = {'mtd': 5, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        var dataUnion = _.union(self.state.ListEventArray, response.data.data);
        self.setState({
          ListEventArray: dataUnion
        });
        self.loadCreatedEvent();
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  async loadCreatedEvent(){
    var self = this;
    var credentials = {'mtd': 6, token: this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        let arrayData = [];
        var today = moment(new Date()).format('YYYY-MM-DD');
        for (let eventObject of response.data.data) {
          var event_date = moment(eventObject.event_start,'YYYY-MM-DD');
          if(moment(event_date).isBefore(today)){
            eventObject.start = moment(eventObject.event_start).format('h:mm a');
            eventObject.end = moment(eventObject.event_end).format('h:mm a');
            arrayData.push(eventObject);
          }
        }
        self.setState({
            dataSource: self.state.dataSource.cloneWithRows(_.sortBy(arrayData, ['-event_start']))
        });
      }else{
        console.log('Get Public Event Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  detailEvent(detail_event){
    this.props.screenProps.updateDetailEvent(detail_event);
    this.props.navigation.navigate("Details2",{detail_event: detail_event, refreshComponent: () => this.refreshComponent()});
  }

  taoHang(data,navigate){
    return (
      <View style={{paddingLeft:10, paddingRight:10, paddingTop:10}} >
        <TouchableOpacity  onPress={() => this.detailEvent(data)}>
          <Card>

            <CardItem  >
              <Body>

                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Icon style={{width:40}} name='event' type='SimpleLineIcons'/>
                    <Text style={{fontSize:18, color: '#000'}}>
                      {data.event_name}
                    </Text>
                  </View>
                </View>

                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Icon style={{width:40}} name='location-city' type='MaterialIcons'/>
                    <Text style={{fontSize:18, color: '#000'}}>
                     {data.location_name}
                    </Text>
                  </View>
                </View>

                <View style={{flexDirection: "row", flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flex:1}}>
                    <View style={{flexDirection: "row", flex: 1, justifyContent: 'flex-start', alignItems: 'center',}}>
                      <Icon style={{width:40}} name='calendar' type='Octicons'/>
                      <Text style={{fontSize:18, color: '#000'}}>
                        {moment(data.event_start).format('MM/DD/YYYY')}
                      </Text>
                    </View>
                  </View>
                  
                  <View style={{flex:1}}>
                    <View style={{flexDirection: "row", flex: 1, justifyContent: 'flex-start', alignItems: 'center',}}>
                        <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
                        <Text style={{fontSize:18, color: '#000'}}>
                          {data.start}
                        </Text>
                    </View>
                  </View>
                </View>

                <View style={{flexDirection: "row", flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                  <View style={{flex: 1}}>
                    <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                      <Icon style={{width:40}} name='calendar' type='Octicons'/>
                      <Text style={{fontSize:18, color: '#000'}}>
                        {moment(data.event_end).format('MM/DD/YYYY')}
                      </Text>
                    </View>
                  </View>
                  
                  <View style={{flex: 1}}>
                    <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                      <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
                      <Text style={{fontSize:18, color: '#000'}} >
                        {data.end}
                      </Text>
                    </View>
                  </View>
                </View>

              </Body>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </View>);
  }



  render(){
    const { navigate } = this.props.navigation;
    return(
      <Container>
        <View style={{flexDirection: "row", justifyContent: "space-between", backgroundColor: '#529ff3', height: 50, paddingTop: 10}}>
          <View style={{width: 50}}>
            <Button onPress={() => this.props.navigation.openDrawer()} style={{width: 100, height: 50, marginTop: -10, padding: 0}} transparent> 
              <Icon style={{color:'#fff', }} name='navicon' type='FontAwesome' />
            </Button>
          </View>
          <View style={{flex: 1,}}>
            <Text  style={{color: '#fff', fontSize: 20, fontWeight: 'bold', textAlign: 'center'}}>Past booking</Text>
          </View>
          <View style={{width: 50}}></View>
        </View>

        <Content>
          <View style={{paddingBottom:10, marginTop: 20, flex: 1}} >
            <ListView  dataSource={this.state.dataSource}
              enableEmptySections 
              renderRow={data => {
                return this.taoHang(data,navigate)
              }}
            />
          </View>
        </Content>
        
        <Fab 
          style={{ backgroundColor:'rgb(100,121,133)' }}
          position="bottomRight" >
            <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} rippleDuration = {400}
              style={{height: 56, width: 56, borderRadius: 100/2, justifyContent: 'center', alignItems: 'center'}}
              onPress={() => {this.props.navigation.navigate("NewEvent2", { refreshComponent: () => this.refreshComponent() })}} >
              <Icon style={{color: '#fff'}} name="add" />
            </Ripple>
        </Fab>
      </Container>
    );
  }
}

class AttendesScreen extends React.Component {
  static navigationOptions = {
    title: 'List Attendee',
    headerStyle: {
      backgroundColor: '#529ff3',
    },    
    headerTintColor: 'white',
    headerTitleStyle: { fontWeight: 'bold', alignSelf: 'center', textAlign: 'center', justifyContent: 'center', flex: 1, marginLeft: -40 },
  };


  constructor(props){
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(this.props.screenProps.reduxState.Auth.event_detail.attendee),
    };
  }

  taoHang(data,navigate){
    return (
      <View style={{paddingLeft:10, paddingRight:10, paddingTop:10}} >
        <TouchableOpacity>
          <Card>
            <CardItem  >
              <Body>
                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Text style={{fontSize: 24, color: '#d9534f'}}>{data.user_profile_name}</Text>
                  </View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Text style={{fontSize:14, color:'#5bc0de', width: 60}}>
                      Email:
                    </Text>
                    <Text style={{fontSize:20, color: '#000'}}>
                      {data.user_email}
                    </Text>
                  </View>
                  <View style={{width: '100%', flexDirection: "row", justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                    <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                    
                      <CheckBox
                        style={{flex: 1, borderRadius: 5}}
                        onClick={()=> console.log()}
                        isChecked={data.event_attendee_status == '1' ? true : false}
                        rightText={"Yes"}
                        rightTextStyle={{color:'green'}}
                        checkBoxColor={'green'}
                        disabled={true}/>
                      <CheckBox
                        style={{flex: 1, borderRadius: 5}}
                        onClick={()=> console.log()}
                        isChecked={data.event_attendee_status == '2' ? true : false}
                        rightText={"No"}
                        rightTextStyle={{color:'red'}}
                        checkBoxColor={'red'}
                        disabled={true}
                      />
                      <CheckBox
                        style={{flex: 1, borderRadius: 5}}
                        onClick={()=> console.log()}
                        isChecked={data.event_attendee_status == '0' ? true : false}
                        rightText={"Maybe"}
                        rightTextStyle={{color:'#0066ff'}}
                        checkBoxColor={'#0066ff'}
                        disabled={true}
                      />
                    </View>
                  </View>
                </View>
              </Body>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </View>);
  }

  render(){
    const { navigate } = this.props.navigation;
    return(
     
      <View style={{paddingBottom:2, backgroundColor: '#fff', flex :1 }} >
          <ListView  dataSource={this.state.dataSource}
            enableEmptySections
            renderRow={data => {
              return this.taoHang(data,navigate)
            }}
          />
          
      </View>
    );
  }
}

class AttendesScreen2 extends React.Component {
  static navigationOptions = {
    title: 'List Attendee',
    headerStyle: {
      backgroundColor: '#529ff3',
    },    
    headerTintColor: 'white',
    headerTitleStyle: { fontWeight: 'bold', alignSelf: 'center', textAlign: 'center', justifyContent: 'center', flex: 1, marginLeft: -40 },
  };


  constructor(props){
    super(props);
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(this.props.screenProps.reduxState.Auth.event_detail.attendee),
    };
  }

  taoHang(data,navigate){
    return (
      <View style={{paddingLeft:10, paddingRight:10, paddingTop:10}} >
        <TouchableOpacity>
          <Card>
            <CardItem  >
              <Body>
                <View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Text style={{fontSize: 24, color: '#d9534f'}}>{data.user_profile_name}</Text>
                  </View>
                  <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5,}}>
                    <Text style={{fontSize:14, color:'#5bc0de', width: 60}}>
                      Email:
                    </Text>
                    <Text style={{fontSize:20, color: '#000'}}>
                      {data.user_email}
                    </Text>
                  </View>
                  <View style={{width: '100%', flexDirection: "row", justifyContent: 'space-between', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                    <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center', paddingTop:5, paddingBottom:5}}>
                      <CheckBox
                        style={{flex: 1, borderRadius: 5}}
                        onClick={()=> console.log()}
                        isChecked={data.event_attendee_status == '1' ? true : false}
                        rightText={"Yes"}
                        rightTextStyle={{color:'green'}}
                        checkBoxColor={'green'}
                        disabled={true}
                      />
                      <CheckBox
                        style={{flex: 1, borderRadius: 5}}
                        onClick={()=> console.log()}
                        isChecked={data.event_attendee_status == '2' ? true : false}
                        rightText={"No"}
                        rightTextStyle={{color:'red'}}
                        checkBoxColor={'red'}
                        disabled={true}
                      />
                      <CheckBox
                        style={{flex: 1, borderRadius: 5}}
                        onClick={()=> console.log()}
                        isChecked={data.event_attendee_status == '0' ? true : false}
                        rightText={"Maybe"}
                        rightTextStyle={{color:'#0066ff'}}
                        checkBoxColor={'#0066ff'}
                        disabled={true}
                      />
                    </View>
                  </View>
                </View>
              </Body>
            </CardItem>
          </Card>
        </TouchableOpacity>
      </View>);
  }

  render(){
    const { navigate } = this.props.navigation;
    return(
     
      <View style={{paddingBottom:2, backgroundColor: '#fff', flex: 1}} >
          <ListView  dataSource={this.state.dataSource}
            enableEmptySections
            renderRow={data => {
              return this.taoHang(data,navigate)
            }}
          />
          
      </View>
    );
  }
}

class DetailScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      attendee:[],
      hideEditDelete : this.props.screenProps.reduxState.Auth.event_detail.created_user != this.props.screenProps.reduxState.Auth.user.user_id ? true : false,
    };
  }
  componentWillMount = () => {
    this.getUserListByEventId(this.props.screenProps.reduxState.Auth.event_detail.event_id);
    const headerConfig = { 
      title : this.props.screenProps.reduxState.Auth.event_detail.event_name, 
      headerStyle: {
        backgroundColor: '#529ff3',
      },    
      headerTintColor: 'white',
      headerTitleStyle: { fontWeight: 'bold', alignSelf: 'center', textAlign: 'center', justifyContent: 'center', flex: 1, marginLeft: -40 },
  
    };
    this.props.navigation.setParams({headerConfig});
  }
  async getUserListByEventId(event_id){
    var self = this;
    var credentials = {'mtd': 9,'id': event_id , 'token':this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.props.screenProps.reduxState.Auth.event_detail.attendee = response.data.data;
        self.props.screenProps.updateDetailEvent(self.props.screenProps.reduxState.Auth.event_detail);
        self.setState({attendee: response.data.data});
      }else{
        console.log('Get User List In Event Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }
  deleteEvent = async () =>{
    var self = this;
    var credentials = {'mtd': 3,'id': this.props.screenProps.reduxState.Auth.event_detail.event_id , 'token':this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { 
      if(response.data.status == 1){
        Toast.show('Delete event successful!',Toast.SHORT, Toast.CENTER, stylesToast);
        self.props.screenProps.updateDetailEvent({});
        self.props.navigation.state.params.refreshComponent();
        self.props.navigation.navigate("ActiveBooking");
      }else{
        Toast.show('Delete event fail!',Toast.SHORT, Toast.CENTER, stylesToast);
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  confirmDeleteEvent = () =>{
    Alert.alert(
      'Delete Event',
      'Are you sure delete this event?',
      [
        {text: 'NO', onPress: () => console.log('NO Pressed'), style: 'cancel'},
        {text: 'YES', onPress: async () => this.deleteEvent()},
      ]
    );
  }

  static navigationOptions = ({ navigation }) => {
    return navigation.state.params.headerConfig;
  };

  renderBottomButton(detail_event){
    const { params } = this.props.navigation.state;
    if(this.state.hideEditDelete){
      return(
        <View style={{paddingTop:10, paddingBottom:10, paddingLeft:5, paddingRight: 5}}>
          <View style={{paddingBottom:10}}>
            <View style={{flex: 1, flexDirection: "row", justifyContent: 'space-between'}}>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
                <Button full info>
                  <Text style={{color:'#fff'}}>{'Access'.toUpperCase()}</Text>
                </Button>
              </View>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
                <Button full info>
                  <Text style={{color:'#fff'}}>{'Control'.toUpperCase()}</Text>
                </Button>
              </View>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
              </View>
            </View>
          </View>
        </View>
      );
    }else{
      return(
        <View style={{paddingTop:10, paddingBottom:10, paddingLeft:5, paddingRight: 5}}>
          <View style={{paddingBottom:10}}>
            <View style={{flex: 1, flexDirection: "row", justifyContent: 'space-between'}}>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
                <Button full info onPress={() => this.props.navigation.navigate("EventEdit", params) }>
                  <Text style={{color:'#fff'}}>{'Edit'.toUpperCase()}</Text>
                </Button>
              </View>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
                <Button full info onPress={this.confirmDeleteEvent}>
                  <Text  style={{color:'#fff'}}>{'Delete'.toUpperCase()}</Text>
                </Button>
              </View>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
                <Button full info>
                  <Text style={{color:'#fff'}}>{'Access'.toUpperCase()}</Text>
                </Button>
              </View>
            </View>
          </View>
          <View>
            <View style={{flex: 1, flexDirection: "row", justifyContent: 'space-between'}}>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
                <Button full info>
                  <Text style={{color:'#fff'}}>{'Control'.toUpperCase()}</Text>
                </Button>
              </View>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
              </View>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
              </View>
            </View>
          </View>
        </View>
      );
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    const { detail_event } = this.props.navigation.state.params;
    return (
      <View style={{flex: 1, backgroundColor:'#fff'}}>
        <ScrollView>
          <View style={{backgroundColor:'#ffffff'}}>
            <View  style={{padding: 10, backgroundColor:'#ffffff'}}>
              <View>
                <Card style={{paddingRight:20}}>
                  <CardItem>
                    <Body>
                      <View >
                        <View style={{borderBottomColor:'gray', borderBottomWidth:0.5, paddingTop:10, paddingBottom: 10}}>
                          <View style={{paddingBottom:5, paddingTop:5}}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("Attendee")}>
                              <View style={{ flexDirection: "row",justifyContent:'space-between'}}>
                                <View>
                                  <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                    ATTENDEE:
                                  </Text>
                                </View>
                                <Icon name='chevron-small-right' type='Entypo' style={{color:'gray'}}/>
                              </View>
                                
                              <View>
                                <Text style={{ fontSize: 20, color: '#000'}}>
                                  {this.state.attendee.length}
                                </Text>
                              </View>
                            </TouchableOpacity>
                          </View>

                          <View style={{paddingBottom:5, paddingTop:5}}>
                            <View style={{width:130}}>
                              <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                LOCATION:
                              </Text>
                            </View>
                            <View>
                              <Text style={{ fontSize: 20, color: '#000'}}>
                                {this.props.screenProps.reduxState.Auth.event_detail.location_name}
                              </Text>
                            </View> 
                          </View>
                          <View style={{flexDirection: 'row', justifyContent:'space-between'}}>
                            <View style={{paddingBottom:5, paddingTop:5, width: '50%'}}>
                              <View style={{width:130}}>
                                <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                  START DATE:
                                </Text>
                              </View>
                              <View>
                                <Text style={{ fontSize: 20, color: '#000'}}>
                                  {moment(this.props.screenProps.reduxState.Auth.event_detail.event_start).format('MM/DD/YYYY')}
                                </Text>
                              </View>
                            </View>
                            <View style={{paddingBottom:5, paddingTop:5, width: '50%'}}>
                              <View style={{width:130}}>
                                <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                  START TIME:
                                </Text>
                              </View>
                              <View>
                                <Text style={{ fontSize: 20, color: '#000'}}>
                                  {this.props.screenProps.reduxState.Auth.event_detail.start}
                                </Text>
                              </View>
                            </View> 
                          </View> 

                          <View style={{flexDirection: 'row', justifyContent:'space-between'}}> 
                            <View style={{paddingBottom:5, paddingTop:5, width: '50%'}}>
                              <View style={{width:130}}>
                                <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                  END DATE:
                                </Text>
                              </View>
                              <View>
                                <Text style={{ fontSize: 20, color: '#000'}}>
                                  {moment(this.props.screenProps.reduxState.Auth.event_detail.event_start).format('MM/DD/YYYY')}
                                </Text>
                              </View>
                            </View>

                            <View style={{paddingBottom:5, paddingTop:5, width: '50%'}}>
                              <View style={{width:130}}>
                                <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                  END TIME:
                                </Text>
                              </View>
                              <View>
                                <Text style={{ fontSize: 20, color: '#000'}}>
                                  {this.props.screenProps.reduxState.Auth.event_detail.end}
                                </Text>
                              </View>
                              
                            </View>
                          </View>

                        </View>
                      </View>  
                    </Body>
                  </CardItem>

                  <CardItem>
                    <Body>
                      <View >
                        <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center',}}>
                          <View>
                            <View>
                              <Icon style={{fontSize: 12}} name='asterisk' type='MaterialCommunityIcons'/>
                            </View>
                          </View>
                          <Text style={{fontSize: 14, color: '#000'}}>
                            NOTE:  
                          </Text>

                        </View>
                        <View>
                            <Text style={{fontStyle: 'italic', color:'gray', paddingLeft:10, textAlign:'justify', fontSize: 16}}>
                              {this.props.screenProps.reduxState.Auth.event_detail.event_note}
                            </Text>
                          </View>
                      </View>
                    </Body>
                  </CardItem>

                </Card>
              </View>

              <View>
                <Card>
                  {this.renderBottomButton(detail_event)}
                </Card>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

class DetailScreen2 extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      attendee:[],
      pastevent:true,
      hideEditDelete : this.props.screenProps.reduxState.Auth.event_detail.created_user != this.props.screenProps.reduxState.Auth.user.user_id ? true : false,
    };
  }
  componentWillMount = () => {
     
    this.getUserListByEventId(this.props.screenProps.reduxState.Auth.event_detail.event_id);
    const headerConfig = { 
      title : this.props.screenProps.reduxState.Auth.event_detail.event_name, 
      headerStyle: {
        backgroundColor: '#529ff3',
      },    
      headerTintColor: 'white',
      headerTitleStyle: { fontWeight: 'bold', alignSelf: 'center', textAlign: 'center', justifyContent: 'center', flex: 1, marginLeft: -40 },
  
    };
    this.props.navigation.setParams({headerConfig});
  }
  async getUserListByEventId(event_id){
    var self = this;
    var credentials = {'mtd': 9,'id': event_id , 'token':this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.props.screenProps.reduxState.Auth.event_detail.attendee = response.data.data;
        self.props.screenProps.updateDetailEvent(self.props.screenProps.reduxState.Auth.event_detail);
        self.setState({attendee: response.data.data});
      }else{
        console.log('Get User List In Event Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }
  deleteEvent = async () =>{
    var self = this;
    var credentials = {'mtd': 3,'id': this.props.screenProps.reduxState.Auth.event_detail.event_id , 'token':this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { 
      if(response.data.status == 1){
        Toast.show('Delete event successful!',Toast.SHORT, Toast.CENTER, stylesToast);
        self.props.screenProps.updateDetailEvent({});
        self.props.navigation.state.params.refreshComponent();
        self.props.navigation.navigate("PastBooking");
      }else{
        Toast.show('Delete event fail!',Toast.SHORT, Toast.CENTER, stylesToast);
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  confirmDeleteEvent = () =>{
    Alert.alert(
      'Delete Event',
      'Are you sure delete this event?',
      [
        {text: 'NO', onPress: () => console.log('NO Pressed'), style: 'cancel'},
        {text: 'YES', onPress: async () => this.deleteEvent()},
      ]
    );
  }

  static navigationOptions = ({ navigation }) => {
    return navigation.state.params.headerConfig;
  };

  renderBottomButton(detail_event){
    const { params } = this.props.navigation.state;
      if(this.state.pastevent ){
      return(
        <View style={{paddingTop:10, paddingBottom:10, paddingLeft:5, paddingRight: 5}}>
          <View style={{paddingBottom:10}}>
            <View style={{flex: 1, flexDirection: "row", justifyContent: 'space-between'}}>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
                <Button full info>
                  <Text style={{color:'#fff'}}>{'Access'.toUpperCase()}</Text>
                </Button>
              </View>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
                <Button full info>
                  <Text style={{color:'#fff'}}>{'Control'.toUpperCase()}</Text>
                </Button>
              </View>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
              </View>
            </View>
          </View>
        </View>
      );
    }else if(this.state.hideEditDelete ){
      return(
        <View style={{paddingTop:10, paddingBottom:10, paddingLeft:5, paddingRight: 5}}>
          <View style={{paddingBottom:10}}>
            <View style={{flex: 1, flexDirection: "row", justifyContent: 'space-between'}}>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
                <Button full info>
                  <Text style={{color:'#fff'}}>{'Access'.toUpperCase()}</Text>
                </Button>
              </View>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
                <Button full info>
                  <Text style={{color:'#fff'}}>{'Control'.toUpperCase()}</Text>
                </Button>
              </View>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
              </View>
            </View>
          </View>
        </View>
      );
    }else{
      return(
        <View style={{paddingTop:10, paddingBottom:10, paddingLeft:5, paddingRight: 5}}>
          <View style={{paddingBottom:10}}>
            <View style={{flex: 1, flexDirection: "row", justifyContent: 'space-between'}}>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
                <Button full info onPress={() => this.props.navigation.navigate("EventEdit2", params) }>
                  <Text style={{color:'#fff'}}>{'Edit'.toUpperCase()}</Text>
                </Button>
              </View>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
                <Button full info onPress={this.confirmDeleteEvent}>
                  <Text  style={{color:'#fff'}}>{'Delete'.toUpperCase()}</Text>
                </Button>
              </View>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
                <Button full info>
                  <Text style={{color:'#fff'}}>{'Access'.toUpperCase()}</Text>
                </Button>
              </View>
            </View>
          </View>
          <View>
            <View style={{flex: 1, flexDirection: "row", justifyContent: 'space-between'}}>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
                <Button full info>
                  <Text style={{color:'#fff'}}>{'Control'.toUpperCase()}</Text>
                </Button>
              </View>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
              </View>
              <View style={{flex: 1, paddingRight:5, paddingLeft: 5}}>
              </View>
            </View>
          </View>
        </View>
      );
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    const { detail_event } = this.props.navigation.state.params;
    return (
      <View style={{flex: 1, backgroundColor:'#fff'}}>
        <ScrollView>
          <View style={{backgroundColor:'#ffffff'}}>
            <View  style={{padding: 10, backgroundColor:'#ffffff'}}>
              <View>
                <Card style={{paddingRight:20}}>
                  <CardItem>
                    <Body>
                      <View >
                        <View style={{borderBottomColor:'gray', borderBottomWidth:0.5, paddingTop:10, paddingBottom: 10}}>
                          <View style={{paddingBottom:5, paddingTop:5}}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("Attendee2")}>
                              <View style={{ flexDirection: "row",justifyContent:'space-between'}}>
                                <View>
                                  <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                    ATTENDEE:
                                  </Text>
                                </View>
                                <Icon name='chevron-small-right' type='Entypo' style={{color:'gray'}}/>
                              </View>
                                
                              <View>
                                <Text style={{ fontSize: 20, color: '#000'}}>
                                  {this.state.attendee.length}
                                </Text>
                              </View>
                            </TouchableOpacity>
                          </View>

                          <View style={{paddingBottom:5, paddingTop:5}}>
                            <View style={{width:130}}>
                              <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                LOCATION:
                              </Text>
                            </View>
                            <View>
                              <Text style={{ fontSize: 20, color: '#000'}}>
                                {this.props.screenProps.reduxState.Auth.event_detail.location_name}
                              </Text>
                            </View> 
                          </View>
                          <View style={{flexDirection: 'row', justifyContent:'space-between'}}>
                            <View style={{paddingBottom:5, paddingTop:5, width: '50%'}}>
                              <View style={{width:130}}>
                                <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                  START DATE:
                                </Text>
                              </View>
                              <View>
                                <Text style={{ fontSize: 20, color: '#000'}}>
                                  {moment(this.props.screenProps.reduxState.Auth.event_detail.event_start).format('MM/DD/YYYY')}
                                </Text>
                              </View>
                            </View>
                            <View style={{paddingBottom:5, paddingTop:5, width: '50%'}}>
                              <View style={{width:130}}>
                                <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                  START TIME:
                                </Text>
                              </View>
                              <View>
                                <Text style={{ fontSize: 20, color: '#000'}}>
                                  {this.props.screenProps.reduxState.Auth.event_detail.start}
                                </Text>
                              </View>
                            </View> 
                          </View> 

                          <View style={{flexDirection: 'row', justifyContent:'space-between'}}> 
                            <View style={{paddingBottom:5, paddingTop:5, width: '50%'}}>
                              <View style={{width:130}}>
                                <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                  END DATE:
                                </Text>
                              </View>
                              <View>
                                <Text style={{ fontSize: 20, color: '#000'}}>
                                  {moment(this.props.screenProps.reduxState.Auth.event_detail.event_start).format('MM/DD/YYYY')}
                                </Text>
                              </View>
                            </View>

                            <View style={{paddingBottom:5, paddingTop:5, width: '50%'}}>
                              <View style={{width:130}}>
                                <Text style={{paddingRight:5, color:'#5bc0de', fontSize: 14 }}>
                                  END TIME:
                                </Text>
                              </View>
                              <View>
                                <Text style={{ fontSize: 20, color: '#000'}}>
                                  {this.props.screenProps.reduxState.Auth.event_detail.end}
                                </Text>
                              </View>
                              
                            </View>
                          </View>

                        </View>
                      </View>  
                    </Body>
                  </CardItem>

                  <CardItem>
                    <Body>
                      <View >
                        <View style={{flexDirection: "row", justifyContent: 'flex-start', alignItems: 'center',}}>
                          <View>
                            <View>
                              <Icon style={{fontSize: 12}} name='asterisk' type='MaterialCommunityIcons'/>
                            </View>
                          </View>
                          <Text style={{fontSize: 14, color: '#000'}}>
                            NOTE:  
                          </Text>

                        </View>
                        <View>
                            <Text style={{fontStyle: 'italic', color:'gray', paddingLeft:10, textAlign:'justify', fontSize: 16}}>
                              {this.props.screenProps.reduxState.Auth.event_detail.event_note}
                            </Text>
                          </View>
                      </View>
                    </Body>
                  </CardItem>

                </Card>
              </View>

              <View>
                <Card>
                  {this.renderBottomButton(detail_event)}
                </Card>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

class EventEditScreen extends React.Component { 

  constructor(props) {
    super(props);

    this.state={
      opencamera : false,
      isVisible : false,
      isVisibleEnd : false,
      event_name: this.props.screenProps.reduxState.Auth.event_detail.event_name,
      event_start: this.props.screenProps.reduxState.Auth.event_detail.event_start,
      event_end: this.props.screenProps.reduxState.Auth.event_detail.event_end,
      start_time: moment(this.props.screenProps.reduxState.Auth.event_detail.event_start).format('MM/DD/YYYY HH:mm:ss'),
      end_time: moment(this.props.screenProps.reduxState.Auth.event_detail.event_end).format('MM/DD/YYYY HH:mm:ss'),
      event_note: this.props.screenProps.reduxState.Auth.event_detail.event_note,
      selectedItems : [],
      selectedItemsSimple : [],
      items:[],
      itemsUsers:[],
      SwitchOnValueHolder : this.props.screenProps.reduxState.Auth.event_detail.event_type == '1' ? true : false,
      check: false,
      selectedDelegate: "",
      listUserDelegate : [],
    };
    this.onScanRoomScreenCamera = this.onScanRoomScreenCamera.bind(this);
  }

  componentWillMount = () => {
    this.getListUser();
    this.getLocation();
    this.getUserListByEventId(this.props.screenProps.reduxState.Auth.event_detail.event_id);
  }
  async getUserListByEventId(event_id){
    var self = this;
    var credentials = {'mtd': 9,'id': event_id , 'token':this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        var selectedItemsSimple = _.map(response.data.data, 'user_id');
        self.setState({
          selectedItemsSimple: selectedItemsSimple,
          listUserDelegate : _.filter(self.state.itemsUsers, function(o) { 
            return selectedItemsSimple.indexOf(o.user_id) > -1; 
          })
        });
      }else{
        console.log('Get User List In Event Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  getLocation(){
    var self = this;
    var credentials = {'mtd': 11, 'token':this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.setState({
          items:response.data.data,
        });
        self.setState({
          selectedItems:[self.props.screenProps.reduxState.Auth.event_detail.location_id],
        });
        self.props.updateQrCodeRoom(response.data.data,self.props.screenProps.reduxState.Auth.event_detail.location_id);
      }else{
        console.log('Get Location Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }
  getListUser(){
    var self = this;
    var credentials = {'mtd': 6, 'token':this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.setState({
          itemsUsers:response.data.data,
        });
        // self.getUserListByEventId(self.props.screenProps.reduxState.Auth.event_detail.event_id);
      }else{
        console.log('Get Users Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  onScanRoomScreenCamera(){
    if(this.scanner){
      this.scanner.reactivate();
    }
    this.setState({
      opencamera:true,
    })
  }

  onSuccess(e) {
    this.setState({
      opencamera:false,
    })
    this.props.updateQrCodeRoom(this.props.screenProps.reduxState.Auth.qrcode_room.location,e.data);
    var obj = _.find(this.props.screenProps.reduxState.Auth.qrcode_room.location, function(o) { return o.location_id == e.data; });
    if(obj){
      this.setState({
        selectedItems:[obj.location_id],
      })
    }
  }

  onSelectedItemsChangeSimple = selectedItemsSimple => {
    this.setState({ 
      selectedItemsSimple,
      listUserDelegate : _.filter(this.state.itemsUsers, function(o) { 
        return selectedItemsSimple.indexOf(o.user_id) > -1; 
      }) 
    });
  };

  onSelectedItemsChange = selectedItems => {
    this.setState({ selectedItems });
  };


  static navigationOptions = {
    header: null,
  };

  handlePicker = (time) => {
    this.setState({
      isVisible:false,
      event_start: moment(time).format('YYYY-MM-DD HH:mm:ss'),
      start_time: moment(time).format('MM/DD/YYYY HH:mm:ss'),
    })
  }

  handlePickerEnd = (time) => {
    if(!moment(this.state.event_start).isBefore(moment(time))){
      this.setState({
        isVisibleEnd:false,
      })
      Toast.show('Endtime must be greater than starttime!',Toast.SHORT, Toast.CENTER, stylesToast);
      return;
    }
    this.setState({
      isVisibleEnd:false,
      event_end: moment(time).format('YYYY-MM-DD HH:mm:ss'),
      end_time: moment(time).format('MM/DD/YYYY HH:mm:ss'),
    })
  }

  showPicker = () => {
    this.setState({
      isVisible:true
    })
  }

  showPickerEnd = () => {
    this.setState({
      isVisibleEnd:true
    })
  }

  hidePicker = () => {
    this.setState({
      isVisible:false
    })
  }

  hidePickerEnd = () => {
    this.setState({
      isVisibleEnd:false
    })
  }

  checkBoxDelegate(){
    if(this.state.check){
      this.setState({
        selectedDelegate: ''
      });
    }
    this.setState({
      check: !this.state.check
    })
  }

  onValueChange2(value: string) {
    this.setState({
      selectedDelegate: value
    });
  }

  validate(){
    if(this.state.event_name == '' || this.state.event_start == '' || this.state.event_end == '' || this.state.selectedItems.length == 0){
      Toast.show('Please fill input!');
      return;
    }else{
      this.updateEvent();
    }
  }

  updateEvent() {
    var self = this;
    var credentials = {
      "mtd": 2, 
      "token": this.props.screenProps.reduxState.Auth.token["access-token"],
      "id": this.props.screenProps.reduxState.Auth.event_detail.event_id,
      "nm": this.state.event_name,
      "lid": this.state.selectedItems[0],
      "srt": this.state.event_start,
      "end": this.state.event_end,
      "not": this.state.event_note,
      "typ": this.state.SwitchOnValueHolder ? 1 : 2,
    };
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.addAttendee(self.props.screenProps.reduxState.Auth.event_detail.event_id);
      }else{
        Toast.show('Update event fail!',Toast.SHORT, Toast.CENTER, stylesToast);
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  addAttendee(event_id){
    var self = this;
    var credentials = {
      "mtd": 7, 
      "token": this.props.screenProps.reduxState.Auth.token["access-token"],
      "id": event_id,
      "uid": this.state.selectedItemsSimple
    };
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) {
      switch(response.data.status){
        case 1:
          Toast.show('Update event successfull!',Toast.SHORT, Toast.CENTER, stylesToast);
          self.props.navigation.state.params.refreshComponent();
          self.props.navigation.navigate("ActiveBooking");
        break;
        case 2:
          Toast.show('Update attendee fail!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
        case 3:
          Toast.show('Token expired!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
        case 4:
          Toast.show('Nothing change attendee!',Toast.SHORT, Toast.CENTER, stylesToast);
          self.props.navigation.state.params.refreshComponent();
          self.props.navigation.navigate("ActiveBooking");
        break;
        default :
          Toast.show('Error!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  changeStateEvent = (value) =>{
    this.setState({
      SwitchOnValueHolder: value
    })
  }

  componentDidMount(){
    this.setState({
      _isMounted : true
    })
  }

  componentWillUnmount(){
    this.setState({
      _isMounted : false
    })
  }

  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18
      },
      to: {
        [translationType]: fromValue
      }
    };
  }

  render() {
    const { navigate } = this.props.navigation;
    const { detail_event } = this.props.navigation.state.params;
    const { selectedItems, selectedItemsSimple, items, itemsUsers, opencamera } = this.state;
    const listUserDelegate = this.state.listUserDelegate.map( (s, i) => {
        return <Picker.Item key={i} value={s.user_id} label={s.user_email} />
    });
    if(this.state.opencamera){
      return (
        <QRCodeScanner ref={(node) => { this.scanner = node }} onRead={this.onSuccess.bind(this)}
          showMarker
          cameraStyle={{ height: SCREEN_HEIGHT }}
          customMarker={
            <View style={stylesCamera.rectangleContainer}>
              <View style={stylesCamera.topOverlay}>
                <Text style={{ fontSize: 30, color: "white" }}>
                  SCAN ROOM
                </Text>
              </View>

              <View style={{ flexDirection: "row" }}>
                <View style={stylesCamera.leftAndRightOverlay} />

                <View style={stylesCamera.rectangle}>
                  <Ionicons
                    name="ios-qr-scanner"
                    size={SCREEN_WIDTH * 0.95}
                    color={iconScanColor}
                  />
                  <Animatable.View
                    style={stylesCamera.scanBar}
                    direction="alternate-reverse"
                    iterationCount="infinite"
                    duration={1700}
                    easing="linear"
                    animation={this.makeSlideOutTranslation(
                      "translateY",
                      SCREEN_WIDTH * -0.7
                    )}
                  />
                </View>

                <View style={stylesCamera.leftAndRightOverlay} />
              </View>

              <View style={stylesCamera.bottomOverlay} />
            </View>
          }
        />
      );
    }else{
      return (
        <Container>
          <Header style={{backgroundColor:'#529ff3', flexDirection: 'row', justifyContent: "space-between"}}>
            <View style={{width: 40, paddingTop: 8}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
              onPress={() => this.props.navigation.goBack()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                </Button>
              </Ripple>
            </View>
           
            <View style={{flex: 1, paddingTop: 15, paddingBottom: 12,}}>
              <Text style={{fontSize: 20, color:'#fff', fontWeight: 'bold', textAlign: 'center'}}>
                Edit event
              </Text>
            </View>

            <View style={{width: 40, paddingTop: 8,}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
              onPress={() => this.validate()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", top: 7, }} name="check" type="Octicons" />
                </Button>
              </Ripple>
            </View>
          </Header>
          <Content>
            <ScrollView>
              <View style={{paddingBottom: 20}}>
                <View style={{marginTop:30}}>
                  <View>
                    <Left style={{flex: 1}}></Left>
                    <Body style={{flex: 1}}>
                      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} >
                        <Button iconLeft transparent dark style={{paddingRight: 20}} onPress={this.onScanRoomScreenCamera}>
                          <Icon style={{fontSize: 30, marginRight: 10}} name='qrcode' type='MaterialCommunityIcons'/>
                          <Text style={{fontSize: 26, color:'#d9534f'}}>{'Scan QR Code'.toUpperCase()}</Text>
                        </Button>
                      </View>
                    </Body>
                    <Right style={{flex: 1}}>
                      
                    </Right>
                  </View>
                </View>
                <View style={{paddingBottom:20}}>
                  <Form style={{paddingRight:15}}>
                    <Item iconLeft style={{paddingTop: 5, paddingBottom: 5}}>
                      <Icon style={{width:40}} name='event' type='SimpleLineIcons'/>
                      <Input placeholder="Title event" placeholderTextColor="#a7a7a7" style={{fontSize: 20}} onChangeText={(event_name) => this.setState({event_name})} value={this.state.event_name}/>
                    </Item>

                    <Item iconLeft style={{}}>
                      <Icon style={{width:40}} name='location-city' type='MaterialIcons'/>
                      <View style={{height:'100%',width:'100%'}}>
                          <View>
                            <MultiSelect
                              single
                              items={items}
                              uniqueKey="location_id"
                              ref={(component) => { this.multiSelect = component }}
                              onSelectedItemsChange={this.onSelectedItemsChange}
                              selectedItems={selectedItems}
                              selectText="Select facility"
                              searchInputPlaceholderText="Search facility..."
                              onChangeInput={ (text)=> console.log(text)}
                              tagRemoveIconColor="#494949"
                              tagBorderColor="#494949"
                              tagTextColor="red"
                              selectedItemTextColor="#CCC"
                              selectedItemIconColor="#494949"
                              itemTextColor="#000"
                              displayKey="location_name"
                              searchInputStyle={{ color: '#494949' }}
                              submitButtonColor="#494949"
                            />
                          </View>
                          
                      </View>
                    </Item>

                    <View style={styles.layout_r_f1}>
                      <View style={styles.flex1}>
                        <Item iconLeft style={{paddingTop: 12, paddingBottom: 12}}>
                          <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
                          <View style={{ flex: 1 }}>
                           <TouchableOpacity onPress={this.showPicker}>
                              <Text style={{color: "#494949", fontSize: 20,textAlign:'left',  paddingLeft:10}}>
                                {this.state.start_time}
                              </Text>
                            </TouchableOpacity>

                            <DateTimePicker
                              isVisible={this.state.isVisible}
                              onConfirm={this.handlePicker}
                              onCancel={this.hidePicker}
                              mode={'datetime'}
                              is24Hour={false}
                              
                            />
                          </View>
                        </Item>
                      </View>
                    </View>

                    <View style={styles.layout_r_f1}>
                       <View  style={styles.flex1}>
                          <Item iconLeft style={{paddingTop: 12, paddingBottom: 12}}>
                            <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
                            <View style={{ flex: 1 }}>
                              <TouchableOpacity onPress={this.showPickerEnd}>
                                <Text style={{color: "#494949", fontSize: 20,textAlign:'left',  paddingLeft:10}}>
                                  {this.state.end_time}
                                </Text>
                              </TouchableOpacity>

                              <DateTimePicker
                                isVisible={this.state.isVisibleEnd}
                                onConfirm={this.handlePickerEnd}
                                onCancel={this.hidePickerEnd}
                                mode={'datetime'}
                                is24Hour={false}
                              />
                            </View>
                          </Item>
                      </View>
                    </View>

                    <Item iconLeft style={{}}>
                      <Icon style={{width:40}} name='account' type='MaterialCommunityIcons'/>
                      <View style={{height:'100%',width:'100%'}}>
                          <View>
                            <MultiSelect
                              items={itemsUsers}
                              uniqueKey="user_id"
                              ref={(component) => { this.multiSelect = component }}
                              onSelectedItemsChange={this.onSelectedItemsChangeSimple}
                              selectedItems={selectedItemsSimple}
                              selectText="Select attendee"
                              searchInputPlaceholderText="Search attendee..."
                              onChangeInput={ (text)=> console.log(text)}
                              tagRemoveIconColor="#494949"
                              tagBorderColor="#494949"
                              tagTextColor="#494949"
                              selectedItemTextColor="#CCC"
                              selectedItemIconColor="#CCC"
                              itemTextColor="#000"
                              displayKey="user_email"
                              searchInputStyle={{ color: '#494949' }}
                              submitButtonColor="#494949"
                            />
                            <View>
                              {this.multiSelect && this.multiSelect.getSelectedItemsExt(selectedItems)}
                            </View>
                          </View>
                          
                      </View>
                    </Item>

                    <View style={{paddingLeft: 15, marginTop: 10,}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center', 
                      borderBottomColor:'#d0cbd4', borderBottomWidth: 1,  paddingBottom: 15}}>
                        <View style={{flex: 1}}>
                          <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="ios-arrow-down-outline" />}
                            style={{ width: undefined }}
                            placeholder="Select your room"
                            placeholderStyle={{ color: "#a7a7a7", fontSize: 20 }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selectedDelegate}
                            onValueChange={this.onValueChange2.bind(this)}
                            enabled={this.state.check}
                          >
                            <Picker.Item label='Select user is delegated' value=""/> 
                            {listUserDelegate}
                          </Picker>
                        </View>
                         
                        <View style={styles.w50,styles.pdL5_100,styles.pdR5_100}>
                          <CheckBox 
                            style={{borderRadius: 5}}
                            onClick={()=>this.checkBoxDelegate()}
                            disabled={this.state.selectedItemsSimple.length == 0 ? true : false}
                            isChecked={this.state.check}
                          />
                        </View>
                      </View>
                    </View>

                    <View style={{paddingLeft: 15, marginTop: 10,}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center', 
                      borderBottomColor:'#d0cbd4', borderBottomWidth: 1,  paddingBottom: 15}}>
                        <View style={{flex: 1}}>
                          <Text style={{fontSize: 20,  color: '#494949'}}>Puclic event</Text>
                        </View>
                         
                         <View style={styles.w50,styles.pdL5_100,styles.pdR5_100}>
                          <Switch
                            onValueChange={(value) => this.changeStateEvent(value)}
                            style={{ transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }] }}
                            value={this.state.SwitchOnValueHolder} />
                         </View>
                      </View>
                    </View>

                    <View style={{paddingLeft: 15, paddingRight: 15, marginTop: 10}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center'}}>
                        <View style={{width: 40}}>
                          <Icon style={{width: 40}} name='clipboard-outline' type='MaterialCommunityIcons'/>
                        </View>
                         
                         <View style={{flex: 1}}>
                          <Textarea style={{flex: 1, fontSize: 20}} rowSpan={5} bordered placeholder="Note" onChangeText={(event_note) => this.setState({event_note})} value={this.state.event_note}/>
                         </View>
                      </View>
                    </View>

                  </Form>
                </View>
              </View>
              </ScrollView>
          </Content>
          {/*<Footer style={{backgroundColor:'#fff', height: 45}}>
            <View  style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center'}}>
              <Button info block full onPress={() => this.validate()}>
                <Text style={{color:"#fff", fontSize: 16}}>SUBMIT</Text>
              </Button>
            </View>
          </Footer>*/}
        </Container>
      );
    }
  }
}

class EventEditScreen2 extends React.Component { 

  constructor(props) {
    super(props);

    this.state={
      opencamera : false,
      isVisible : false,
      isVisibleEnd : false,
      event_name: this.props.screenProps.reduxState.Auth.event_detail.event_name,
      event_start: this.props.screenProps.reduxState.Auth.event_detail.event_start,
      event_end: this.props.screenProps.reduxState.Auth.event_detail.event_end,
      start_time: moment(this.props.screenProps.reduxState.Auth.event_detail.event_start).format('MM/DD/YYYY HH:mm:ss'),
      end_time: moment(this.props.screenProps.reduxState.Auth.event_detail.event_end).format('MM/DD/YYYY HH:mm:ss'),
      event_note: this.props.screenProps.reduxState.Auth.event_detail.event_note,
      selectedItems : [],
      selectedItemsSimple : [],
      items:[],
      itemsUsers:[],
      SwitchOnValueHolder : this.props.screenProps.reduxState.Auth.event_detail.event_type == '1' ? true : false,
      check: false,
      selectedDelegate: "",
      listUserDelegate : [],
    };
    this.onScanRoomScreenCamera = this.onScanRoomScreenCamera.bind(this);
  }

  componentWillMount = () => {
    this.getListUser();
    this.getLocation();
    this.getUserListByEventId(this.props.screenProps.reduxState.Auth.event_detail.event_id);
  }
  async getUserListByEventId(event_id){
    var self = this;
    var credentials = {'mtd': 9,'id': event_id , 'token':this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        var selectedItemsSimple = _.map(response.data.data, 'user_id');
        self.setState({
          selectedItemsSimple: selectedItemsSimple,
          listUserDelegate : _.filter(self.state.itemsUsers, function(o) { 
            return selectedItemsSimple.indexOf(o.user_id) > -1; 
          })
        });
      }else{
        console.log('Get User List In Event Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  getLocation(){
    var self = this;
    var credentials = {'mtd': 11, 'token':this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.setState({
          items:response.data.data,
        });
        self.setState({
          selectedItems:[self.props.screenProps.reduxState.Auth.event_detail.location_id],
        });
        self.props.updateQrCodeRoom(response.data.data,self.props.screenProps.reduxState.Auth.event_detail.location_id);
      }else{
        console.log('Get Location Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }
  getListUser(){
    var self = this;
    var credentials = {'mtd': 6, 'token':this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.setState({
          itemsUsers:response.data.data,
        });
        // self.getUserListByEventId(self.props.screenProps.reduxState.Auth.event_detail.event_id);
      }else{
        console.log('Get Users Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  onScanRoomScreenCamera(){
    if(this.scanner){
      this.scanner.reactivate();
    }
    this.setState({
      opencamera:true,
    })
  }

  onSuccess(e) {
    this.setState({
      opencamera:false,
    })
    this.props.updateQrCodeRoom(this.props.screenProps.reduxState.Auth.qrcode_room.location,e.data);
    var obj = _.find(this.props.screenProps.reduxState.Auth.qrcode_room.location, function(o) { return o.location_id == e.data; });
    if(obj){
      this.setState({
        selectedItems:[obj.location_id],
      })
    }
  }

  onSelectedItemsChangeSimple = selectedItemsSimple => {
    this.setState({ 
      selectedItemsSimple,
      listUserDelegate : _.filter(this.state.itemsUsers, function(o) { 
        return selectedItemsSimple.indexOf(o.user_id) > -1; 
      }) 
    });
  };

  onSelectedItemsChange = selectedItems => {
    this.setState({ selectedItems });
  };


  static navigationOptions = {
    header: null,
  };

  handlePicker = (time) => {
    this.setState({
      isVisible:false,
      event_start: moment(time).format('YYYY-MM-DD HH:mm:ss'),
      start_time: moment(time).format('MM/DD/YYYY HH:mm:ss'),
    })
  }

  handlePickerEnd = (time) => {
    if(!moment(this.state.event_start).isBefore(moment(time))){
      this.setState({
        isVisibleEnd:false,
      })
      Toast.show('Endtime must be greater than starttime!',Toast.SHORT, Toast.CENTER, stylesToast);
      return;
    }
    this.setState({
      isVisibleEnd:false,
      event_end: moment(time).format('YYYY-MM-DD HH:mm:ss'),
      end_time: moment(time).format('MM/DD/YYYY HH:mm:ss'),
    })
  }

  showPicker = () => {
    this.setState({
      isVisible:true
    })
  }

  showPickerEnd = () => {
    this.setState({
      isVisibleEnd:true
    })
  }

  hidePicker = () => {
    this.setState({
      isVisible:false
    })
  }

  hidePickerEnd = () => {
    this.setState({
      isVisibleEnd:false
    })
  }

  checkBoxDelegate(){
    if(this.state.check){
      this.setState({
        selectedDelegate: ''
      });
    }
    this.setState({
      check: !this.state.check
    })
  }

  onValueChange2(value: string) {
    this.setState({
      selectedDelegate: value
    });
  }

  validate(){
    if(this.state.event_name == '' || this.state.event_start == '' || this.state.event_end == '' || this.state.selectedItems.length == 0){
      Toast.show('Please fill input!',Toast.SHORT, Toast.CENTER, stylesToast);
      return;
    }else{
      this.updateEvent();
    }
  }

  updateEvent() {
    var self = this;
    var credentials = {
      "mtd": 2, 
      "token": this.props.screenProps.reduxState.Auth.token["access-token"],
      "id": this.props.screenProps.reduxState.Auth.event_detail.event_id,
      "nm": this.state.event_name,
      "lid": this.state.selectedItems[0],
      "srt": this.state.event_start,
      "end": this.state.event_end,
      "not": this.state.event_note,
      "typ": this.state.SwitchOnValueHolder ? 1 : 2,
    };
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.addAttendee(self.props.screenProps.reduxState.Auth.event_detail.event_id);
      }else{
        Toast.show('Update event fail!',Toast.SHORT, Toast.CENTER, stylesToast);
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  addAttendee(event_id){
    var self = this;
    var credentials = {
      "mtd": 7, 
      "token": this.props.screenProps.reduxState.Auth.token["access-token"],
      "id": event_id,
      "uid": this.state.selectedItemsSimple
    };
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) {
      switch(response.data.status){
        case 1:
          Toast.show('Update event successfull!',Toast.SHORT, Toast.CENTER, stylesToast);
          self.props.navigation.state.params.refreshComponent();
          self.props.navigation.navigate("PastBooking");
        break;
        case 2:
          Toast.show('Update attendee fail!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
        case 3:
          Toast.show('Token expired!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
        case 4:
          Toast.show('Nothing change attendee!',Toast.SHORT, Toast.CENTER, stylesToast);
          self.props.navigation.state.params.refreshComponent();
          self.props.navigation.navigate("PastBooking");
        break;
        default :
          Toast.show('Error!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  changeStateEvent = (value) =>{
    this.setState({
      SwitchOnValueHolder: value
    })
  }

  componentDidMount(){
    this.setState({
      _isMounted : true
    })
  }

  componentWillUnmount(){
    this.setState({
      _isMounted : false
    })
  }

  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18
      },
      to: {
        [translationType]: fromValue
      }
    };
  }

  render() {
    const { navigate } = this.props.navigation;
    const { detail_event } = this.props.navigation.state.params;
    const { selectedItems, selectedItemsSimple, items, itemsUsers, opencamera } = this.state;
    const listUserDelegate = this.state.listUserDelegate.map( (s, i) => {
        return <Picker.Item key={i} value={s.user_id} label={s.user_email} />
    });
    if(this.state.opencamera){
      return (
        <QRCodeScanner ref={(node) => { this.scanner = node }} onRead={this.onSuccess.bind(this)}
          showMarker
          cameraStyle={{ height: SCREEN_HEIGHT }}
          customMarker={
            <View style={stylesCamera.rectangleContainer}>
              <View style={stylesCamera.topOverlay}>
                <Text style={{ fontSize: 30, color: "white" }}>
                  SCAN ROOM
                </Text>
              </View>

              <View style={{ flexDirection: "row" }}>
                <View style={stylesCamera.leftAndRightOverlay} />

                <View style={stylesCamera.rectangle}>
                  <Ionicons
                    name="ios-qr-scanner"
                    size={SCREEN_WIDTH * 0.95}
                    color={iconScanColor}
                  />
                  <Animatable.View
                    style={stylesCamera.scanBar}
                    direction="alternate-reverse"
                    iterationCount="infinite"
                    duration={1700}
                    easing="linear"
                    animation={this.makeSlideOutTranslation(
                      "translateY",
                      SCREEN_WIDTH * -0.7
                    )}
                  />
                </View>

                <View style={stylesCamera.leftAndRightOverlay} />
              </View>

              <View style={stylesCamera.bottomOverlay} />
            </View>
          }
        />
      );
    }else{
      return (
        <Container>
          <Header style={{backgroundColor:'#529ff3', flexDirection: 'row', justifyContent: "space-between"}}>
            <View style={{width: 40, paddingTop: 8}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
              onPress={() => this.props.navigation.goBack()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                </Button>
              </Ripple>
            </View>
           
            <View style={{flex: 1, paddingTop: 15, paddingBottom: 12,}}>
              <Text style={{fontSize: 20, color:'#fff', fontWeight: 'bold', textAlign: 'center'}}>
                Edit event
              </Text>
            </View>

            <View style={{width: 40, paddingTop: 8,}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
              onPress={() => this.validate()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", top: 7, }} name="check" type="Octicons" />
                </Button>
              </Ripple>
            </View>
          </Header>
          <Content>
            <ScrollView>
              <View style={{paddingBottom: 20}}>
                <View style={{marginTop:30}}>
                  <View>
                    <Left style={{flex: 1}}></Left>
                    <Body style={{flex: 1}}>
                      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} >
                        <Button iconLeft transparent dark style={{paddingRight: 20}} onPress={this.onScanRoomScreenCamera}>
                          <Icon style={{fontSize: 30, marginRight: 10}} name='qrcode' type='MaterialCommunityIcons'/>
                          <Text style={{fontSize: 26, color:'#d9534f'}}>{'Scan QR Code'.toUpperCase()}</Text>
                        </Button>
                      </View>
                    </Body>
                    <Right style={{flex: 1}}>
                      
                    </Right>
                  </View>
                </View>
                <View style={{paddingBottom:20}}>
                  <Form style={{paddingRight:15}}>
                    <Item iconLeft style={{paddingTop: 5, paddingBottom: 5}}>
                      <Icon style={{width:40}} name='event' type='SimpleLineIcons'/>
                      <Input placeholder="Title event" placeholderTextColor="#a7a7a7" style={{fontSize: 20}} onChangeText={(event_name) => this.setState({event_name})} value={this.state.event_name}/>
                    </Item>

                    <Item iconLeft style={{}}>
                      <Icon style={{width:40}} name='location-city' type='MaterialIcons'/>
                      <View style={{height:'100%',width:'100%'}}>
                          <View>
                            <MultiSelect
                              single
                              items={items}
                              uniqueKey="location_id"
                              ref={(component) => { this.multiSelect = component }}
                              onSelectedItemsChange={this.onSelectedItemsChange}
                              selectedItems={selectedItems}
                              selectText="Select facility"
                              searchInputPlaceholderText="Search facility..."
                              onChangeInput={ (text)=> console.log(text)}
                              tagRemoveIconColor="#494949"
                              tagBorderColor="#494949"
                              tagTextColor="red"
                              selectedItemTextColor="#CCC"
                              selectedItemIconColor="#494949"
                              itemTextColor="#000"
                              displayKey="location_name"
                              searchInputStyle={{ color: '#494949' }}
                              submitButtonColor="#494949"
                            />
                          </View>
                          
                      </View>
                    </Item>

                    <View style={styles.layout_r_f1}>
                      <View style={styles.flex1}>
                        <Item iconLeft style={{paddingTop: 12, paddingBottom: 12}}>
                          <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
                          <View style={{ flex: 1 }}>
                           <TouchableOpacity onPress={this.showPicker}>
                              <Text style={{color: "#494949", fontSize: 20,textAlign:'left',  paddingLeft:10}}>
                                {this.state.start_time}
                              </Text>
                            </TouchableOpacity>

                            <DateTimePicker
                              isVisible={this.state.isVisible}
                              onConfirm={this.handlePicker}
                              onCancel={this.hidePicker}
                              mode={'datetime'}
                              is24Hour={false}
                              
                            />
                          </View>
                        </Item>
                      </View>
                    </View>

                    <View style={styles.layout_r_f1}>
                       <View  style={styles.flex1}>
                          <Item iconLeft style={{paddingTop: 12, paddingBottom: 12}}>
                            <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
                            <View style={{ flex: 1 }}>
                              <TouchableOpacity onPress={this.showPickerEnd}>
                                <Text style={{color: "#494949", fontSize: 20,textAlign:'left',  paddingLeft:10}}>
                                  {this.state.end_time}
                                </Text>
                              </TouchableOpacity>

                              <DateTimePicker
                                isVisible={this.state.isVisibleEnd}
                                onConfirm={this.handlePickerEnd}
                                onCancel={this.hidePickerEnd}
                                mode={'datetime'}
                                is24Hour={false}
                              />
                            </View>
                          </Item>
                      </View>
                    </View>

                    <Item iconLeft style={{}}>
                      <Icon style={{width:40}} name='account' type='MaterialCommunityIcons'/>
                      <View style={{height:'100%',width:'100%'}}>
                          <View>
                            <MultiSelect
                              items={itemsUsers}
                              uniqueKey="user_id"
                              ref={(component) => { this.multiSelect = component }}
                              onSelectedItemsChange={this.onSelectedItemsChangeSimple}
                              selectedItems={selectedItemsSimple}
                              selectText="Select attendee"
                              searchInputPlaceholderText="Search attendee..."
                              onChangeInput={ (text)=> console.log(text)}
                              tagRemoveIconColor="#494949"
                              tagBorderColor="#494949"
                              tagTextColor="#494949"
                              selectedItemTextColor="#CCC"
                              selectedItemIconColor="#CCC"
                              itemTextColor="#000"
                              displayKey="user_email"
                              searchInputStyle={{ color: '#494949' }}
                              submitButtonColor="#494949"
                            />
                            <View>
                              {this.multiSelect && this.multiSelect.getSelectedItemsExt(selectedItems)}
                            </View>
                          </View>
                          
                      </View>
                    </Item>

                    <View style={{paddingLeft: 15, marginTop: 10,}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center', 
                      borderBottomColor:'#d0cbd4', borderBottomWidth: 1,  paddingBottom: 15}}>
                        <View style={{flex: 1}}>
                          <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="ios-arrow-down-outline" />}
                            style={{ width: undefined }}
                            placeholder="Select your room"
                            placeholderStyle={{ color: "#a7a7a7", fontSize: 20 }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selectedDelegate}
                            onValueChange={this.onValueChange2.bind(this)}
                            enabled={this.state.check}
                          >
                            <Picker.Item label='Select user is delegated' value=""/> 
                            {listUserDelegate}
                          </Picker>
                        </View>
                         
                        <View style={styles.w50,styles.pdL5_100,styles.pdR5_100}>
                          <CheckBox 
                            style={{borderRadius: 5}}
                            onClick={()=>this.checkBoxDelegate()}
                            disabled={this.state.selectedItemsSimple.length == 0 ? true : false}
                            isChecked={this.state.check}
                          />
                        </View>
                      </View>
                    </View>

                    <View style={{paddingLeft: 15, marginTop: 10,}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center', 
                      borderBottomColor:'#d0cbd4', borderBottomWidth: 1,  paddingBottom: 15}}>
                        <View style={{flex: 1}}>
                          <Text style={{fontSize: 20,  color: '#494949'}}>Puclic event</Text>
                        </View>
                         
                         <View style={styles.w50,styles.pdL5_100,styles.pdR5_100}>
                          <Switch
                            onValueChange={(value) => this.changeStateEvent(value)}
                            style={{ transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }] }}
                            value={this.state.SwitchOnValueHolder} />
                         </View>
                      </View>
                    </View>

                    <View style={{paddingLeft: 15, paddingRight: 15, marginTop: 10}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center'}}>
                        <View style={{width: 40}}>
                          <Icon style={{width: 40}} name='clipboard-outline' type='MaterialCommunityIcons'/>
                        </View>
                         
                         <View style={{flex: 1}}>
                          <Textarea style={{flex: 1, fontSize: 20}} rowSpan={5} bordered placeholder="Note" onChangeText={(event_note) => this.setState({event_note})} value={this.state.event_note}/>
                         </View>
                      </View>
                    </View>

                  </Form>
                </View>
              </View>
              </ScrollView>
          </Content>
          {/*<Footer style={{backgroundColor:'#fff', height: 45}}>
            <View  style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center'}}>
              <Button info block full onPress={() => this.validate()}>
                <Text style={{color:"#fff", fontSize: 16}}>SUBMIT</Text>
              </Button>
            </View>
          </Footer>*/}
        </Container>
      );
    }
  }
}

class NewEventScreen extends React.Component { 

  constructor(props) {
    super(props);

    this.state={
      opencamera : false,
      disableSubmit : false,
      isVisible : false,
      isVisibleEnd : false,
      event_name:'',
      event_start:'',
      event_end:'',
      start_time:'Select start time',
      end_time:'Select end time',
      note:'',
      selectedItems : [],
      selectedItemsSimple : [],
      items:[],
      itemsUsers:[],
      SwitchOnValueHolder :  false,
      check: false,
      selectedDelegate: "",
      listUserDelegate: [],
    };
    this.onScanRoomScreenCamera = this.onScanRoomScreenCamera.bind(this);
  }

  componentWillMount = () => {
    this.getLocation();
    this.getListUser();
  }

  getLocation(){
    var self = this;
    var credentials = {'mtd': 11, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.setState({
          items:response.data.data,
        })
        self.props.updateQrCodeRoom(response.data.data,0);
      }else{
        console.log('Get Location Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }
  getListUser(){
    var self = this;
    var credentials = {'mtd': 6, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.setState({
          itemsUsers:response.data.data,
        })
      }else{
        console.log('Get Users Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  onScanRoomScreenCamera(){
    if(this.scanner){
      this.scanner.reactivate();
    }
    this.setState({
      opencamera:true,
    })
  }

  onSuccess(e) {
    this.setState({
      opencamera:false,
    })
    this.props.updateQrCodeRoom(this.props.screenProps.reduxState.Auth.qrcode_room.location,e.data);
    var obj = _.find(this.props.screenProps.reduxState.Auth.qrcode_room.location, function(o) { return o.location_id == e.data; });
    if(obj){
      this.setState({
        selectedItems:[obj.location_id],
      })
    }
  }

  onSelectedItemsChangeSimple = selectedItemsSimple => {
    this.setState({ 
      selectedItemsSimple,
      listUserDelegate : _.filter(this.state.itemsUsers, function(o) { 
        return selectedItemsSimple.indexOf(o.user_id) > -1; 
      }) 
    });
  };

  onSelectedItemsChange = selectedItems => {
    this.setState({ selectedItems });
  };


  static navigationOptions = {
    header: null,
  };

  handlePicker = (time) => {
    this.setState({
      isVisible:false,
      event_start: moment(time).format('YYYY-MM-DD HH:mm:ss'),
      start_time: moment(time).format('MM/DD/YYYY HH:mm:ss'),
    })
  }

  handlePickerEnd = (time) => {
    if(!moment(this.state.event_start).isBefore(moment(time))){
      this.setState({
        isVisibleEnd:false,
      })
      Toast.show('Endtime must be greater than starttime!',Toast.SHORT, Toast.CENTER, stylesToast);
      return;
    }
    this.setState({
      isVisibleEnd:false,
      event_end: moment(time).format('YYYY-MM-DD HH:mm:ss'),
      end_time: moment(time).format('MM/DD/YYYY HH:mm:ss'),
    })
  }

  showPicker = () => {
    this.setState({
      isVisible:true
    })
  }

  showPickerEnd = () => {
    this.setState({
      isVisibleEnd:true
    })
  }

  hidePicker = () => {
    this.setState({
      isVisible:false
    })
  }

  hidePickerEnd = () => {
    this.setState({
      isVisibleEnd:false
    })
  }


  validate(){
    if(this.state.event_name == '' || this.state.event_start == '' || this.state.event_end == '' || this.state.note == '' || this.state.selectedItems.length == 0){
      Toast.show('Please fill input!',Toast.SHORT, Toast.CENTER, stylesToast);
      return;
    }else{
      this.createEvent();
    }
  }

  createEvent() {
    this.setState({
      disableSubmit: true
    });
    var self = this;
    var credentials = {
      "mtd": 1, 
      "token": this.props.screenProps.reduxState.Auth.token["access-token"],
      "nm": this.state.event_name,
      "lid": parseInt(this.state.selectedItems[0]),
      "srt": this.state.event_start,
      "end": this.state.event_end,
      "not": this.state.note,
      "typ": this.state.SwitchOnValueHolder ? 1 : 2,
    };
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.addAttendee(response.data.data.event_id);
      }else{
        Toast.show('Add Event Fail!',Toast.SHORT, Toast.CENTER, stylesToast);
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  addAttendee(event_id){
    var self = this;
    var credentials = {
      "mtd": 7, 
      "token": this.props.screenProps.reduxState.Auth.token["access-token"],
      "id": event_id,
      "uid": this.state.selectedItemsSimple
    };
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      switch(response.data.status){
        case 1:
          self.setState({
            disableSubmit: false
          });
          Toast.show('Create event successfull!',Toast.SHORT, Toast.CENTER, stylesToast);
          self.props.navigation.state.params.refreshComponent();
          self.props.navigation.navigate("ActiveBooking");
        break;
        case 2:
          Toast.show('Add Attendee Fail!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
        case 3:
          Toast.show('Token expired!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
        default :
          Toast.show('Error!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  changeStateEvent = (value) =>{
    this.setState({
      SwitchOnValueHolder: value
    })
  }

  checkBoxDelegate(){
    if(this.state.check){
      this.setState({
        selectedDelegate: ''
      });
    }
    this.setState({
      check: !this.state.check
    })
  }

  onValueChange2(value: string) {
    this.setState({
      selectedDelegate: value
    });
  }
  
  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18
      },
      to: {
        [translationType]: fromValue
      }
    };
  }

  render() {
    const { navigate } = this.props.navigation;
    const { selectedItems, selectedItemsSimple, items, itemsUsers, opencamera } = this.state;
    const listUserDelegate = this.state.listUserDelegate.map( (s, i) => {
      return <Picker.Item key={i} value={s.user_id} label={s.user_email} />
    });
    if(this.state.opencamera){
      return (
        <QRCodeScanner ref={(node) => { this.scanner = node }} onRead={this.onSuccess.bind(this)}
          showMarker
          cameraStyle={{ height: SCREEN_HEIGHT }}
          customMarker={
            <View style={stylesCamera.rectangleContainer}>
              <View style={stylesCamera.topOverlay}>
                <Text style={{ fontSize: 30, color: "white" }}>
                  SCAN ROOM
                </Text>
              </View>

              <View style={{ flexDirection: "row" }}>
                <View style={stylesCamera.leftAndRightOverlay} />

                <View style={stylesCamera.rectangle}>
                  <Ionicons
                    name="ios-qr-scanner"
                    size={SCREEN_WIDTH * 0.95}
                    color={iconScanColor}
                  />
                  <Animatable.View
                    style={stylesCamera.scanBar}
                    direction="alternate-reverse"
                    iterationCount="infinite"
                    duration={1700}
                    easing="linear"
                    animation={this.makeSlideOutTranslation(
                      "translateY",
                      SCREEN_WIDTH * -0.7
                    )}
                  />
                </View>

                <View style={stylesCamera.leftAndRightOverlay} />
              </View>

              <View style={stylesCamera.bottomOverlay} />
            </View>
          }
        />
      );
    }else{
      return (
        <Container>
          <Header style={{backgroundColor:'#529ff3', flexDirection: 'row', justifyContent: "space-between"}}>
            <View style={{width: 40, paddingTop: 8}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
              onPress={() => this.props.navigation.goBack()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                </Button>
              </Ripple>
            </View>
           
            <View style={{flex: 1, paddingTop: 15, paddingBottom: 12,}}>
              <Text style={{fontSize: 20, color:'#fff', fontWeight: 'bold', textAlign: 'center'}}>
                New event
              </Text>
            </View>

            <View style={{width: 40, paddingTop: 8,}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
              onPress={() => this.validate()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", top: 7, }} name="check" type="Octicons" />
                </Button>
              </Ripple>
            </View>
          </Header>
          <Content>
            <ScrollView>
              <View style={{paddingBottom: 20}}>
                <View style={{marginTop:30}}>
                  <View>
                    <Left style={{flex: 1}}></Left>
                    <Body style={{flex: 1}}>
                      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} >
                        <Button iconLeft transparent dark style={{paddingRight: 20}} onPress={this.onScanRoomScreenCamera}>
                          <Icon style={{fontSize: 30, marginRight: 10}} name='qrcode' type='MaterialCommunityIcons'/>
                          <Text style={{fontSize: 26, color:'#d9534f'}}>{'Scan QR Code'.toUpperCase()}</Text>
                        </Button>
                      </View>
                    </Body>
                    <Right style={{flex: 1}}>
                      
                    </Right>
                  </View>
                </View>
                <View style={{paddingBottom:20}}>
                  <Form style={{paddingRight:15}}>
                    <Item iconLeft style={{paddingTop: 5, paddingBottom: 5}}>
                      <Icon style={{width:40}} name='event' type='SimpleLineIcons'/>
                      <Input placeholder="Title event" placeholderTextColor="#a7a7a7" style={{fontSize: 20}} onChangeText={(event_name) => this.setState({event_name})} value={this.state.event_name}/>
                    </Item>

                    <Item iconLeft style={{}}>
                      <Icon style={{width:40}} name='location-city' type='MaterialIcons'/>
                      <View style={{height:'100%',width:'100%'}}>
                          <View>
                            <MultiSelect
                              single
                              items={items}
                              uniqueKey="location_id"
                              onSelectedItemsChange={this.onSelectedItemsChange}
                              selectedItems={selectedItems}
                              selectText="Select facility"
                              searchInputPlaceholderText="Search facility..."
                              onChangeInput={ (text)=> console.log(text)}
                              tagRemoveIconColor="#494949"
                              tagBorderColor="#494949"
                              tagTextColor="red"
                              selectedItemTextColor="#CCC"
                              selectedItemIconColor="#494949"
                              itemTextColor="#000"
                              displayKey="location_name"
                              searchInputStyle={{ color: '#494949' }}
                              submitButtonColor="#494949"
                            />
                          </View>
                          
                      </View>
                    </Item>

                    <View style={styles.layout_r_f1}>
                      <View style={styles.flex1}>
                        <Item iconLeft style={{paddingTop: 12, paddingBottom: 12}}>
                          <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
                          <View style={{ flex: 1 }}>
                           <TouchableOpacity onPress={this.showPicker}>
                              <Text style={{color: "#494949", fontSize: 20,textAlign:'left',  paddingLeft:10}}>
                                {this.state.start_time}
                              </Text>
                            </TouchableOpacity>

                            <DateTimePicker
                              isVisible={this.state.isVisible}
                              onConfirm={this.handlePicker}
                              onCancel={this.hidePicker}
                              mode={'datetime'}
                              is24Hour={false}
                              
                            />
                          </View>
                        </Item>
                      </View>
                    </View>

                    <View style={styles.layout_r_f1}>
                       <View  style={styles.flex1}>
                          <Item iconLeft style={{paddingTop: 12, paddingBottom: 12}}>
                            <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
                            <View style={{ flex: 1 }}>
                              <TouchableOpacity onPress={this.showPickerEnd}>
                                <Text style={{color: "#494949", fontSize: 20,textAlign:'left',  paddingLeft:10}}>
                                  {this.state.end_time}
                                </Text>
                              </TouchableOpacity>

                              <DateTimePicker
                                isVisible={this.state.isVisibleEnd}
                                onConfirm={this.handlePickerEnd}
                                onCancel={this.hidePickerEnd}
                                mode={'datetime'}
                                is24Hour={false}
                              />
                            </View>
                          </Item>
                      </View>
                    </View>

                    <Item iconLeft style={{}}>
                      <Icon style={{width:40}} name='account' type='MaterialCommunityIcons'/>
                      <View style={{height:'100%',width:'100%'}}>
                          <View>
                            <MultiSelect
                              items={itemsUsers}
                              uniqueKey="user_id"
                              ref={(component) => { this.multiSelect = component }}
                              onSelectedItemsChange={this.onSelectedItemsChangeSimple}
                              selectedItems={selectedItemsSimple}
                              selectText="Select attendee"
                              searchInputPlaceholderText="Search attendee..."
                              onChangeInput={ (text)=> console.log(text)}
                              tagRemoveIconColor="#494949"
                              tagBorderColor="#494949"
                              tagTextColor="#494949"
                              selectedItemTextColor="#CCC"
                              selectedItemIconColor="#CCC"
                              itemTextColor="#000"
                              displayKey="user_email"
                              searchInputStyle={{ color: '#494949' }}
                              submitButtonColor="#494949"
                            />
                            <View>
                              {this.multiSelect && this.multiSelect.getSelectedItemsExt(selectedItems)}
                            </View>
                          </View>
                          
                      </View>
                    </Item>

                    <View style={{paddingLeft: 15, marginTop: 10,}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center', 
                      borderBottomColor:'#d0cbd4', borderBottomWidth: 1,  paddingBottom: 15}}>
                        <View style={{flex: 1}}>
                          <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="ios-arrow-down-outline" />}
                            style={{ width: undefined }}
                            placeholder="Select your room"
                            placeholderStyle={{ color: "#a7a7a7", fontSize: 20 }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selectedDelegate}
                            onValueChange={this.onValueChange2.bind(this)}
                            enabled={this.state.check}
                          >
                            <Picker.Item label='Select user is delegated' value=""/> 
                            {listUserDelegate}
                          </Picker>
                        </View>
                         
                        <View style={styles.w50,styles.pdL5_100,styles.pdR5_100}>
                          <CheckBox 
                            style={{borderRadius: 5}}
                            onClick={()=>this.checkBoxDelegate()}
                            disabled={this.state.selectedItemsSimple.length == 0 ? true : false}
                            isChecked={this.state.check}
                          />
                        </View>
                      </View>
                    </View>

                    <View style={{paddingLeft: 15, marginTop: 10,}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center', 
                      borderBottomColor:'#d0cbd4', borderBottomWidth: 1,  paddingBottom: 15}}>
                        <View style={{flex: 1}}>
                          <Text style={{fontSize: 20,  color: '#494949'}}>Puclic event</Text>
                        </View>
                         
                         <View style={styles.w50,styles.pdL5_100,styles.pdR5_100}>
                          <Switch
                            onValueChange={(value) => this.changeStateEvent(value)}
                            style={{ transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }] }}
                            value={this.state.SwitchOnValueHolder} />
                         </View>
                      </View>
                    </View>

                    <View style={{paddingLeft: 15, paddingRight: 15, marginTop: 10}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center'}}>
                        <View style={{width: 40}}>
                          <Icon style={{width: 40}} name='clipboard-outline' type='MaterialCommunityIcons'/>
                        </View>
                         
                         <View style={{flex: 1}}>
                          <Textarea style={{flex: 1, fontSize: 20}} rowSpan={5} bordered placeholder="Note" onChangeText={(note) => this.setState({note})} value={this.state.note}/>
                         </View>
                      </View>
                    </View>
                  </Form>
                </View>
              </View>
            </ScrollView>
          </Content>
          {/*<Footer style={{backgroundColor:'#fff', height: 45}}>
            <View  style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center'}}>
              <Button info block full disabled={this.state.disableSubmit} onPress={() => this.validate()}>
                <Text style={{color:"#fff", fontSize: 16}}>SUBMIT</Text>
              </Button>
            </View>
          </Footer>*/}
        </Container>
      );
    }
  }
}

class NewEventScreen2 extends React.Component { 

  constructor(props) {
    super(props);

    this.state={
      opencamera : false,
      disableSubmit : false,
      isVisible : false,
      isVisibleEnd : false,
      event_name:'',
      event_start:'',
      event_end:'',
      start_time:'Select start time',
      end_time:'Select end time',
      note:'',
      selectedItems : [],
      selectedItemsSimple : [],
      items:[],
      itemsUsers:[],
      SwitchOnValueHolder :  false,
      check: false,
      selectedDelegate: "",
      listUserDelegate: [],
    };
    this.onScanRoomScreenCamera = this.onScanRoomScreenCamera.bind(this);
  }

  componentWillMount = () => {
    this.getLocation();
    this.getListUser();
  }

  getLocation(){
    var self = this;
    var credentials = {'mtd': 11, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.setState({
          items:response.data.data,
        })
        self.props.updateQrCodeRoom(response.data.data,0);
      }else{
        console.log('Get Location Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }
  getListUser(){
    var self = this;
    var credentials = {'mtd': 6, 'token': this.props.screenProps.reduxState.Auth.token["access-token"]};
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.setState({
          itemsUsers:response.data.data,
        })
      }else{
        console.log('Get Users Fail!');
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  onScanRoomScreenCamera(){
    if(this.scanner){
      this.scanner.reactivate();
    }
    this.setState({
      opencamera:true,
    })
  }

  onSuccess(e) {
    this.setState({
      opencamera:false,
    })
    this.props.updateQrCodeRoom(this.props.screenProps.reduxState.Auth.qrcode_room.location,e.data);
    var obj = _.find(this.props.screenProps.reduxState.Auth.qrcode_room.location, function(o) { return o.location_id == e.data; });
    if(obj){
      this.setState({
        selectedItems:[obj.location_id],
      })
    }
  }

  onSelectedItemsChangeSimple = selectedItemsSimple => {
    this.setState({ 
      selectedItemsSimple,
      listUserDelegate : _.filter(this.state.itemsUsers, function(o) { 
        return selectedItemsSimple.indexOf(o.user_id) > -1; 
      }) 
    });
  };

  onSelectedItemsChange = selectedItems => {
    this.setState({ selectedItems });
  };


  static navigationOptions = {
    header: null,
  };

  handlePicker = (time) => {
    this.setState({
      isVisible:false,
      event_start: moment(time).format('YYYY-MM-DD HH:mm:ss'),
      start_time: moment(time).format('MM/DD/YYYY HH:mm:ss'),
    })
  }

  handlePickerEnd = (time) => {
    if(!moment(this.state.event_start).isBefore(moment(time))){
      this.setState({
        isVisibleEnd:false,
      })
      Toast.show('Endtime must be greater than starttime!',Toast.SHORT, Toast.CENTER, stylesToast);
      return;
    }
    this.setState({
      isVisibleEnd:false,
      event_end: moment(time).format('YYYY-MM-DD HH:mm:ss'),
      end_time: moment(time).format('MM/DD/YYYY HH:mm:ss'),
    })
  }

  showPicker = () => {
    this.setState({
      isVisible:true
    })
  }

  showPickerEnd = () => {
    this.setState({
      isVisibleEnd:true
    })
  }

  hidePicker = () => {
    this.setState({
      isVisible:false
    })
  }

  hidePickerEnd = () => {
    this.setState({
      isVisibleEnd:false
    })
  }


  validate(){
    if(this.state.event_name == '' || this.state.event_start == '' || this.state.event_end == '' || this.state.note == '' || this.state.selectedItems.length == 0){
      Toast.show('Please fill input!',Toast.SHORT, Toast.CENTER, stylesToast);
      return;
    }else{
      this.createEvent();
    }
  }

  createEvent() {
    this.setState({
      disableSubmit: true
    });
    var self = this;
    var credentials = {
      "mtd": 1, 
      "token": this.props.screenProps.reduxState.Auth.token["access-token"],
      "nm": this.state.event_name,
      "lid": parseInt(this.state.selectedItems[0]),
      "srt": this.state.event_start,
      "end": this.state.event_end,
      "not": this.state.note,
      "typ": this.state.SwitchOnValueHolder ? 1 : 2,
    };
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      if(response.data.status == 1){
        self.addAttendee(response.data.data.event_id);
      }else{
        Toast.show('Add Event Fail!',Toast.SHORT, Toast.CENTER, stylesToast);
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  addAttendee(event_id){
    var self = this;
    var credentials = {
      "mtd": 7, 
      "token": this.props.screenProps.reduxState.Auth.token["access-token"],
      "id": event_id,
      "uid": this.state.selectedItemsSimple
    };
    Axios.post(Url.event, credentials, {headers: Object.assign(Url.headers,credentials)})
    .then(function (response) { // ON SUCCESS
      switch(response.data.status){
        case 1:
          self.setState({
            disableSubmit: false
          });
          Toast.show('Create event successfull!',Toast.SHORT, Toast.CENTER, stylesToast);
          self.props.navigation.state.params.refreshComponent();
          self.props.navigation.navigate("PastBooking");
        break;
        case 2:
          Toast.show('Add Attendee Fail!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
        case 3:
          Toast.show('Token expired!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
        default :
          Toast.show('Error!',Toast.SHORT, Toast.CENTER, stylesToast);
        break;
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }

  changeStateEvent = (value) =>{
    this.setState({
      SwitchOnValueHolder: value
    })
  }

  checkBoxDelegate(){
    if(this.state.check){
      this.setState({
        selectedDelegate: ''
      });
    }
    this.setState({
      check: !this.state.check
    })
  }

  onValueChange2(value: string) {
    this.setState({
      selectedDelegate: value
    });
  }

  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18
      },
      to: {
        [translationType]: fromValue
      }
    };
  }

  render() {
    const { navigate } = this.props.navigation;
    const { selectedItems, selectedItemsSimple, items, itemsUsers, opencamera } = this.state;
    const listUserDelegate = this.state.listUserDelegate.map( (s, i) => {
      return <Picker.Item key={i} value={s.user_id} label={s.user_email} />
    });
    if(this.state.opencamera){
      return (
        <QRCodeScanner ref={(node) => { this.scanner = node }} onRead={this.onSuccess.bind(this)}
          showMarker
          cameraStyle={{ height: SCREEN_HEIGHT }}
          customMarker={
            <View style={stylesCamera.rectangleContainer}>
              <View style={stylesCamera.topOverlay}>
                <Text style={{ fontSize: 30, color: "white" }}>
                  SCAN ROOM
                </Text>
              </View>

              <View style={{ flexDirection: "row" }}>
                <View style={stylesCamera.leftAndRightOverlay} />

                <View style={stylesCamera.rectangle}>
                  <Ionicons
                    name="ios-qr-scanner"
                    size={SCREEN_WIDTH * 0.95}
                    color={iconScanColor}
                  />
                  <Animatable.View
                    style={stylesCamera.scanBar}
                    direction="alternate-reverse"
                    iterationCount="infinite"
                    duration={1700}
                    easing="linear"
                    animation={this.makeSlideOutTranslation(
                      "translateY",
                      SCREEN_WIDTH * -0.7
                    )}
                  />
                </View>

                <View style={stylesCamera.leftAndRightOverlay} />
              </View>

              <View style={stylesCamera.bottomOverlay} />
            </View>
          }
        />
      );
    }else{
      return (
        <Container>
          <Header style={{backgroundColor:'#529ff3', flexDirection: 'row', justifyContent: "space-between"}}>
            <View style={{width: 40, paddingTop: 8}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
              onPress={() => this.props.navigation.goBack()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", left: -9, top: 7, }} name='arrow-left' type='MaterialCommunityIcons' />
                </Button>
              </Ripple>
            </View>
           
            <View style={{flex: 1, paddingTop: 15, paddingBottom: 12,}}>
              <Text style={{fontSize: 20, color:'#fff', fontWeight: 'bold', textAlign: 'center'}}>
                New event
              </Text>
            </View>

            <View style={{width: 40, paddingTop: 8,}}>
              <Ripple rippleContainerBorderRadius= {100/2} rippleCentered = {true} style= {{ width: 40, height: 40,}}  
              onPress={() => this.validate()} >
                <Button  style={{ width: 40, height: 40, borderRadius: 100/2,}} transparent>
                  <Icon style={{color:'#fff', position: "absolute", top: 7, }} name="check" type="Octicons" />
                </Button>
              </Ripple>
            </View>
          </Header>
          <Content>
            <ScrollView>
              <View style={{paddingBottom: 20}}>
                <View style={{marginTop:30}}>
                  <View>
                    <Left style={{flex: 1}}></Left>
                    <Body style={{flex: 1}}>
                      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} >
                        <Button iconLeft transparent dark style={{paddingRight: 20}} onPress={this.onScanRoomScreenCamera}>
                          <Icon style={{fontSize: 30, marginRight: 10}} name='qrcode' type='MaterialCommunityIcons'/>
                          <Text style={{fontSize: 26, color:'#d9534f'}}>{'Scan QR Code'.toUpperCase()}</Text>
                        </Button>
                      </View>
                    </Body>
                    <Right style={{flex: 1}}>
                      
                    </Right>
                  </View>
                </View>
                <View style={{paddingBottom:20}}>
                  <Form style={{paddingRight:15}}>
                    <Item iconLeft style={{paddingTop: 5, paddingBottom: 5}}>
                      <Icon style={{width:40}} name='event' type='SimpleLineIcons'/>
                      <Input placeholder="Title event" placeholderTextColor="#a7a7a7" style={{fontSize: 20}} onChangeText={(event_name) => this.setState({event_name})} value={this.state.event_name}/>
                    </Item>

                    <Item iconLeft style={{}}>
                      <Icon style={{width:40}} name='location-city' type='MaterialIcons'/>
                      <View style={{height:'100%',width:'100%'}}>
                          <View>
                            <MultiSelect
                              single
                              items={items}
                              uniqueKey="location_id"
                              onSelectedItemsChange={this.onSelectedItemsChange}
                              selectedItems={selectedItems}
                              selectText="Select facility"
                              searchInputPlaceholderText="Search facility..."
                              onChangeInput={ (text)=> console.log(text)}
                              tagRemoveIconColor="#494949"
                              tagBorderColor="#494949"
                              tagTextColor="red"
                              selectedItemTextColor="#CCC"
                              selectedItemIconColor="#494949"
                              itemTextColor="#000"
                              displayKey="location_name"
                              searchInputStyle={{ color: '#494949' }}
                              submitButtonColor="#494949"
                            />
                          </View>
                          
                      </View>
                    </Item>

                    <View style={styles.layout_r_f1}>
                      <View style={styles.flex1}>
                        <Item iconLeft style={{paddingTop: 12, paddingBottom: 12}}>
                          <Icon style={{width:40}} name='timer' type='MaterialCommunityIcons'/>
                          <View style={{ flex: 1 }}>
                           <TouchableOpacity onPress={this.showPicker}>
                              <Text style={{color: "#494949", fontSize: 20,textAlign:'left',  paddingLeft:10}}>
                                {this.state.start_time}
                              </Text>
                            </TouchableOpacity>

                            <DateTimePicker
                              isVisible={this.state.isVisible}
                              onConfirm={this.handlePicker}
                              onCancel={this.hidePicker}
                              mode={'datetime'}
                              is24Hour={false}
                              
                            />
                          </View>
                        </Item>
                      </View>
                    </View>

                    <View style={styles.layout_r_f1}>
                       <View  style={styles.flex1}>
                          <Item iconLeft style={{paddingTop: 12, paddingBottom: 12}}>
                            <Icon style={{width:40}} name='timer-off' type='MaterialCommunityIcons'/>
                            <View style={{ flex: 1 }}>
                              <TouchableOpacity onPress={this.showPickerEnd}>
                                <Text style={{color: "#494949", fontSize: 20,textAlign:'left',  paddingLeft:10}}>
                                  {this.state.end_time}
                                </Text>
                              </TouchableOpacity>

                              <DateTimePicker
                                isVisible={this.state.isVisibleEnd}
                                onConfirm={this.handlePickerEnd}
                                onCancel={this.hidePickerEnd}
                                mode={'datetime'}
                                is24Hour={false}
                              />
                            </View>
                          </Item>
                      </View>
                    </View>

                    <Item iconLeft style={{}}>
                      <Icon style={{width:40}} name='account' type='MaterialCommunityIcons'/>
                      <View style={{height:'100%',width:'100%'}}>
                          <View>
                            <MultiSelect
                              items={itemsUsers}
                              uniqueKey="user_id"
                              ref={(component) => { this.multiSelect = component }}
                              onSelectedItemsChange={this.onSelectedItemsChangeSimple}
                              selectedItems={selectedItemsSimple}
                              selectText="Select attendee"
                              searchInputPlaceholderText="Search attendee..."
                              onChangeInput={ (text)=> console.log(text)}
                              tagRemoveIconColor="#494949"
                              tagBorderColor="#494949"
                              tagTextColor="#494949"
                              selectedItemTextColor="#CCC"
                              selectedItemIconColor="#CCC"
                              itemTextColor="#000"
                              displayKey="user_email"
                              searchInputStyle={{ color: '#494949' }}
                              submitButtonColor="#494949"
                            />
                            <View>
                              {this.multiSelect && this.multiSelect.getSelectedItemsExt(selectedItems)}
                            </View>
                          </View>
                          
                      </View>
                    </Item>

                    <View style={{paddingLeft: 15, marginTop: 10,}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center', 
                      borderBottomColor:'#d0cbd4', borderBottomWidth: 1,  paddingBottom: 15}}>
                        <View style={{flex: 1}}>
                          <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="ios-arrow-down-outline" />}
                            style={{ width: undefined }}
                            placeholder="Select your room"
                            placeholderStyle={{ color: "#a7a7a7", fontSize: 20 }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selectedDelegate}
                            onValueChange={this.onValueChange2.bind(this)}
                            enabled={this.state.check}
                          >
                            <Picker.Item label='Select user is delegated' value=""/> 
                            {listUserDelegate}
                          </Picker>
                        </View>
                         
                        <View style={styles.w50,styles.pdL5_100,styles.pdR5_100}>
                          <CheckBox 
                            style={{borderRadius: 5}}
                            onClick={()=>this.checkBoxDelegate()}
                            disabled={this.state.selectedItemsSimple.length == 0 ? true : false}
                            isChecked={this.state.check}
                          />
                        </View>
                      </View>
                    </View>

                    <View style={{paddingLeft: 15, marginTop: 10,}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center', 
                      borderBottomColor:'#d0cbd4', borderBottomWidth: 1,  paddingBottom: 15}}>
                        <View style={{flex: 1}}>
                          <Text style={{fontSize: 20,  color: '#494949'}}>Puclic event</Text>
                        </View>
                         
                         <View style={styles.w50,styles.pdL5_100,styles.pdR5_100}>
                          <Switch
                            onValueChange={(value) => this.changeStateEvent(value)}
                            style={{ transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }] }}
                            value={this.state.SwitchOnValueHolder} />
                         </View>
                      </View>
                    </View>

                    <View style={{paddingLeft: 15, paddingRight: 15, marginTop: 10}}>
                      <View style={{flexDirection: "row", flex: 1,justifyContent: 'center', alignItems: 'center'}}>
                        <View style={{width: 40}}>
                          <Icon style={{width: 40}} name='clipboard-outline' type='MaterialCommunityIcons'/>
                        </View>
                         
                         <View style={{flex: 1}}>
                          <Textarea style={{flex: 1, fontSize: 20}} rowSpan={5} bordered placeholder="Note" onChangeText={(note) => this.setState({note})} value={this.state.note}/>
                         </View>
                      </View>
                    </View>
                  </Form>
                </View>
              </View>
            </ScrollView>
          </Content>
          {/*<Footer style={{backgroundColor:'#fff', height: 45}}>
            <View  style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center'}}>
              <Button info block full disabled={this.state.disableSubmit} onPress={() => this.validate()}>
                <Text style={{color:"#fff", fontSize: 16}}>SUBMIT</Text>
              </Button>
            </View>
          </Footer>*/}
        </Container>
      );
    }
  }
}

const ActiveBookingStack = createStackNavigator({
  ActiveBooking: ActiveBookingScreen,
  Details: DetailScreen,
  Attendee: AttendesScreen,
  NewEvent: NewEventScreen,
  EventEdit: EventEditScreen,
});

const PastBookingStack = createStackNavigator({
  PastBooking: PastBookingScreen,
  Details2: DetailScreen2,
  Attendee2: AttendesScreen2,
  NewEvent2: NewEventScreen2,
  EventEdit2: EventEditScreen2,
});


const BookingTab = createMaterialTopTabNavigator(
  {
    ActiveBooking: { 
      screen: ActiveBookingStack,
      navigationOptions: {
        tabBarLabel: 'Active Booking',
      },  
    },
    PastBooking: { 
      screen: PastBookingStack,
      navigationOptions: {
        tabBarLabel: 'Past Booking',
      },

    }

  }, 
  {
    initialRouteName: 'ActiveBooking',
    tabBarPosition:'bottom',
  }
);

class CompletedOrdersScreen extends React.Component {
    static navigationOptions = {
        title: "Completed Orders"
    }
    static router = BookingTab.router;
    render() {
        return(
         <BookingTab {...this.props} />
        );
    }
}

const mapStateToProps = (state) => ({
  reduxState:state
});

const mapDispatchToProps = (dispatch) => {
    return ({
        updateDetailEvent:(event_detail) => dispatch( updateDetailEvent(event_detail) ),
        updateQrCode:(location_list, qrcode_room) => dispatch( updateQrCode(location_list, qrcode_room) ),
    })
}

const mergeProps = (state, dispatch, ownProps) => {
    return ({
        ...ownProps,
     screenProps: {
          ...ownProps.screenProps,
          ...state,
          ...dispatch,
        }
  
    })
}
export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(CompletedOrdersScreen);