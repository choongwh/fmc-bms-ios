import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, ScrollView } from 'react-native';
import { ListItem, CheckBox, Body, CardItem, Button } from 'native-base';
import { StackNavigator } from 'react-navigation';

import Axios from 'axios';
import { connect } from 'react-redux';

import { authSuccess } from '../../redux/actions/auth'
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';

export default class AboutScreen extends React.Component {
  static navigationOptions = {
    title: 'About',
  };

   refreshComponent(newAppointment) {
    this.fetchAppointments();
  }

  fetchAppointments() {
    var self = this;
    var accessToken = this.props.reduxState.Auth.token
    Axios.get(Url.appointments, {headers: Object.assign(Url.headers, accessToken)})
    .then(function (response) {
      if(response.headers["access-token"] != "") {
        self.props.updateAccessToken(response.headers["access-token"])
      }
      self.setState({
        appointments: response.data
      })
    })
    .catch(function (error) {
      console.log("ERROR DURING fecthAppointments", error);
    });
  }


  render() {
    // Get navigation props to be able to use navigate function
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <View style={{ flex: 1, alignItems: 'center', justifyContent:'center'}}>
          <View style={[styles.container, styles.horizontal]}>
            <ActivityIndicator size="large" color="#2089dc" />
          </View>
          <View style={{width:320, paddingLeft:10, paddingRight:10}}>
            <View full block style={{marginBottom:20}}>
              <Text style={{fontSize: 21, textAlign: "center", color:"#2089dc" }}>REGISTER WORKSPACE</Text>
            </View>
            <CardItem block style={{height: 300, width: 300,  borderWidth: 0.5, borderColor: '#d6d7da',}}>

            </CardItem>
            <CardItem block style={{marginBottom:10, marginTop:10,}}>
              <View style={{width:50}}>
                <CheckBox 
                  value={false}
                  color="#000"
                />
              </View>
              <View>
                <Text>
                  I`m a user
                </Text>
              </View>
              
            </CardItem>

            <Button full info block 
               onPress={() => {this.props.navigation.navigate("Workspace", { refreshComponent: () => this.refreshComponent() })} }
            >
              <Text style={{color: "white" }}>SUBMIT</Text>
            </Button>


          </View>
          
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 40
  }
})