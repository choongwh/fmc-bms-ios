import React from 'react';
import { ActivityIndicator, StatusBar, StyleSheet, View, Image } from 'react-native';
import { Text, Button, CardItem, Icon, Left, Right, } from 'native-base';
import { connect } from 'react-redux';
import { grantAccess } from '../../redux/actions/auth';
import Auth from '../../redux/reducers/auth';

class AuthLoadingScreen extends React.Component {

  constructor(props) {
    super(props);
    // Enter in this case if the user just logged in or if there was data stored in reduxPersist
    this.props.grantAccess(false);
    this.checkIfConnected();
  }

  checkIfConnected = () => {
    if (this.props.reduxState.Auth.workspaceqr.qrcode == '') {
      this.props.navigation.navigate('QrcodeScan');
    } else {
      if (this.props.reduxState.Auth.connected) {
        this.props.navigation.navigate('App');
      }
      else {
        this.props.navigation.navigate('Auth');
      }
    }
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.container}>

        <View style={{ alignItems: 'center', }}>
          <CardItem style={{ height: 200, width: '90%' }}>
            <Image source={{ uri: '/image/logo-bms.png' }} style={{ height: '100%', width: '100%', backgroundColor: '#fff', resizeMode: 'contain' }} />
          </CardItem>
        </View>
        <View style={{ height: '25%', position: "absolute", bottom: 0 }}>
          <ActivityIndicator />
          <StatusBar translucent barStyle='light-content' />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff'
  },
});

const mapStateToProps = (state) => ({
  reduxState: state
});
export default connect(mapStateToProps, {
  Auth,
  grantAccess
})(AuthLoadingScreen);