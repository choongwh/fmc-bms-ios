export default{
  //////////////////login////////////////////////////////////
    wrapper: {
    flex: 1,
    paddingTop: 50,
    backgroundColor: '#fff'
  },

  outcontent:{
    flexDirection: "column", 
    justifyContent:'space-between', 
    flex: 1, 
    backgroundColor: '#fff',
  },

  outForm:{
    paddingLeft: '5%', 
    paddingRight: '5%', 
    marginLeft: -5,
  },
  conbtnLogin: {paddingTop:10, paddingLeft: 20, paddingRight: 15},

  /////////////////scan///////////////////
   out_box: {
    height: 300, width: 300,  borderWidth: 0.5, borderColor: '#d6d7da', justifyContent:'center', alignItems:'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 40
  },

   cardImage_h:{
    height: 80,flex: 1
  },
//////////////////////////////////////////////////////////////////////////////////////////////////////////////  
  footerMain:{borderTopWidth: 2, borderTopColor: '#ffffff', backgroundColor:'#fff', height: 45, shadowOpacity: 0, elevation: 0, shadowOffset: {height: 0, width: 0},},   
  outbtnfooter:{flex: 1, justifyContent: 'flex-start', alignItems: 'center'},
  footerpd:{paddingRight: '5%', paddingLeft: '5%', height: 50, width: "100%", backgroundColor:'#fff'},
  footerpd1:{paddingRight:15, paddingLeft:15, width: '100%'},
  btnfooter:{flex: 1, height: 45, justifyContent: 'center'},
  btnfooterText:{color:"#fff", fontSize: 16, textAlign:'center'},
  textForm:{
    textAlign: 'center', fontSize: 22,  fontWeight: 'bold', padding: 15, color: '#000',
  },

/////////////////////////////////////////////////quickstyle/////////////////////////////////////////////////////////////  
  color_blur:{color: '#2089dc'}, color_black:{color: '#000'},

  layout_r_f1:{flexDirection: "row", flex: 1, justifyContent: 'space-between', },
  layout_c_f1:{flexDirection: "column", flex: 1, justifyContent: 'space-between', alignItems:'center'},

  w40:{width: 40}, w50:{width: 50}, w100:{width: 100}, w150:{width: 150}, w200:{width: 200}, w250:{width: 250}, w300:{width: 300},  w320:{width: 320},

  pdL5_100: {paddingLeft: '5%'}, 
  pdR5_100: {paddingRight: '5%'}, 

  mgB0:{marginBottom: 0}, mgB5:{marginBottom: 5}, mgB10:{marginBottom: 10}, mgB15:{marginBottom: 15}, mgB20:{marginBottom: 20}, 
  mgT0:{marginTop: 0}, mgT5:{marginTop: 5},mgT10:{marginTop: 10}, mgT15:{marginTop: 15}, mgT20:{marginTop: 20}, mgT25:{marginTop: 25}, mgT30:{marginTop: 30}, mgT35:{marginTop: 35}, mgT40:{marginTop: 40}, mgT45:{marginTop: 45}, mgT50:{marginTop: 50},
  

  pdT0:{paddingTop: 0}, pdT5:{paddingTop: 5}, pdT10:{paddingTop: 10}, pdT15:{paddingTop: 15}, pdT20:{paddingTop: 20},
  pdB0:{paddingBottom: 0}, pdB5:{paddingBottom: 5}, pdB10:{paddingBottom: 10}, pdB15:{paddingBottom: 15}, pdB20:{paddingBottom: 20},
  pdL0:{paddingLeft: 0}, pdL5:{paddingLeft: 5}, pdL10:{paddingLeft: 10}, pdL15:{paddingLeft: 15}, pdL20:{paddingLeft: 20},
  pdR0:{paddingRight: 0}, pdR5:{paddingRight: 5}, pdR10:{paddingRight: 10}, pdR15:{paddingRight: 15}, pdR20:{paddingRight: 20},

  fSize16:{fontSize: 16}, fSize18:{fontSize: 18}, fSize20:{fontSize: 20}, fSize24:{fontSize: 24},

  textCenter:{textAlign:'center'}, textLeft:{textAlign:'left'}, textRight:{textAlign:'right'},


  flex1:{flex: 1}, jusbetween:{justifyContent:'space-between'}, 
  flex_column:{flexDirection:'column'}, flex_row:{flexDirection: 'row'}, aligI_center:{alignItems:'center'},
}