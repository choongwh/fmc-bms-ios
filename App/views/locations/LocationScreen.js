import React from 'react';
import { Alert, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Input } from 'native-base';
import Axios from 'axios';
import Fuse from 'fuse.js';
import Toast from 'react-native-root-toast';
import { Url } from '../../../config/api/credentials';
import Auth from '../../redux/reducers/auth';
import { connect } from 'react-redux';
import { RenderIf, ConditionalRender, If, Else } from '../../components/logic';
import LocationList from '../../components/menus/LocationList';
import {
	compileSectionsFromCollection, convertLocationArrayToObject
} from '../../components/menus/LocationList/treeProcessor';
import NavbarWithHamburger from '../../components/navigation/NavbarWithHamburger';
import bridge from '../../modules/bridge'

class LocationScreen extends React.Component {

	static navigationOptions = {
		header: null,
	}

	constructor() {
		super();
		this.state = {
			loading: true,
			errorFetchingLocations: false,
			searchFilter: '',
			sections: [],
		};

		// In case of pull to refresh and the returned list of locations are the same
		// The already rendered list items don't need to be unmounted and re-rendered
		// The "Loading..." text only need to be shown upon initialization
		this.isFirstRender = true;

		this.searchDebounceTimer = null;
	}

	componentDidMount() {
		this.fetchLocations();
	}

	/**
	 * @description Fetch a collection of location then compiles it
	 * into a data structure that can be used by SectionList for rendering.
	 */
	fetchLocations = () => {
		this.setState({ loading: true });
		const { reduxState: { Auth: { token = {} } } } = this.props;
		const credentials = { 'mtd': 21, 'token': token["access-token"] };
		Axios.post(
			Url.event,
			credentials,
			{ headers: Object.assign(Url.headers, credentials) }
		).then((response) => {
			try {
				const { status, statusText, data = {} } = response.data;
				if (status === 1) {
					// This will allow both array and object structures to be rendered into the list
					if (Array.isArray(data)) {
						console.warn('Expected `data` to be an object, but received an array.')
					}
					this.compiledSections = compileSectionsFromCollection(
						Array.isArray(data) ? convertLocationArrayToObject(data) : data
					);
					this.setState({
						errorFetchingLocations: false,
						sections: this.compiledSections,
					});
				} else if (status === 2 || statusText === 'Invalid token') {
					console.log('Session expired!');
					setTimeout(() => {
						Toast.show('Session expired'.toUpperCase());
					}, 750);
					const destroySession = bridge.get('destroySession');
					if (typeof destroySession === 'function') { destroySession(); }
				} else {
					console.log(`Failed to fetch locations. Server responded with status code ${status}.`);
					this.setState({ errorFetchingLocations: true });
				}
				this.isFirstRender = false;
				this.setState({ loading: false });
			} catch (error) {
				this.isFirstRender = false;
				console.log('ERROR PARSING LIST:', error)
				this.setState({
					loading: false,
					errorFetchingLocations: true,
				})
			}
		}).catch((e) => {
			this.isFirstRender = false;
			this.setState({
				loading: false,
				errorFetchingLocations: true,
			})
			console.log('ERROR LOADING LIST:', e);
		})
	}

	/**
	 * @description Fuzzy search locations based on search value.
	 * @param {string} searchValue The searched location.
	 * @returns {Array} The filtered locations.
	 */
	getFilteredSections = (searchValue) => {
		let results = [];
		const OPTIONS = {
			keys: ['name', 'path', 'section'],
			shouldSort: true,
			threshold: 0.1,
		};
		for (let i = 0; i < this.compiledSections.length; i++) {
			results.push({
				title: this.compiledSections[i]['title'],
				data: new Fuse(this.compiledSections[i]['data'], OPTIONS).search(searchValue)
			});
		}
		return results.filter(({ data }) => data.length > 0);;
	}

	/**
	 * @description Setter function for the state.searchValue. It is used in the search box.
	 * @param {string} searchValue The searched location.
	 */
	setSearchFilter = (searchValue = '') => {
		this.setState({ searchFilter: searchValue });
		// Debounce prevents search from occuring everytime a character is added/removed/modified
		// However, if value is empty, this indicates a reset, so timeout becomes 0 in that case
		if (this.searchDebounceTimer) { clearTimeout(this.searchDebounceTimer); }
		if (searchValue.length > 0) {
			this.searchDebounceTimer = setTimeout(() => {
				this.setState({ sections: this.getFilteredSections(searchValue) });
			}, 250);
		} else {
			this.setState({ sections: this.compiledSections });
		}
	}

	render() {
		const { navigation } = this.props;
		const { loading, searchFilter, sections, errorFetchingLocations } = this.state;
		const listIsEmpty = sections.length <= 0 && searchFilter.length < 0;
		return (
			<>
				{/* Navbar */}
				<NavbarWithHamburger
					title='Locations'
					openDrawerAction={navigation.openDrawer}
				/>

				{/* Search box */}
				<View style={styles.searchBoxContainer}>
					<View style={{
						...styles.searchBoxSubcontainer,
						opacity: listIsEmpty ? 0.5 : 1
					}}>
						<Input
							disabled={listIsEmpty}
							placeholder={listIsEmpty ? null : 'Search for a location'}
							placeholderTextColor={listIsEmpty ? '#AAAAAA' : '#888888'}
							onChangeText={this.setSearchFilter}
							value={searchFilter}
							style={styles.searchBoxTextInput}
						/>
						<RenderIf condition={searchFilter.length > 0}>
							<TouchableOpacity onPress={() => { this.setSearchFilter(''); }}
								style={styles.searchBoxClearBtn}>
								<Text children="×" style={styles.searchBoxClearBtnLabel} />
							</TouchableOpacity>
						</RenderIf>
					</View>
				</View>

				{/* Location List */}
				<ConditionalRender>
					<If condition={loading && this.isFirstRender}>
						<View style={styles.loadingContainer}>
							<Text children='Loading...' style={styles.loadingText} />
						</View>
					</If>
					<Else>
						<LocationList sections={sections} hasError={errorFetchingLocations}
							onPress={(data) => {
								try {
									navigation.navigate('Facility', data);
								} catch (error) {
									console.log('Unable to navigate', error);
									const { name } = data;
									Alert.alert('Facility inaccessible', `There was an error loading the controls of ${name ? `'${name}'` : 'that facility'} for you.`)
								}
							}}
							searchFilter={searchFilter}
							refreshAction={this.fetchLocations}
						/>
					</Else>
				</ConditionalRender>

			</>
		);
	}

}

const mapStateToProps = (state) => ({ reduxState: state });
export default connect(mapStateToProps, { Auth })(LocationScreen);

const SEARCHBOX_CLRBTN_SIZE = 24;
const styles = StyleSheet.create({
	loadingContainer: {
		alignItems: 'center',
		flex: 1,
		justifyContent: 'center',
	},
	loadingText: {
		color: '#AAAAAA',
		fontSize: 28,
		fontWeight: 'bold',
	},
	searchBoxContainer: {
		backgroundColor: '#DDDDDD',
		padding: 7.5,
	},
	searchBoxSubcontainer: {
		alignItems: 'center',
		backgroundColor: '#FFFFFF',
		borderRadius: 5,
		flexDirection: 'row',
		overflow: 'hidden',
	},
	searchBoxTextInput: {
		flex: 1,
		fontSize: 18,
		padding: 7.5,
	},
	searchBoxDivider: {
		width: 5,
	},
	searchBoxClearBtn: {
		alignItems: 'center',
		backgroundColor: '#AAAAAA',
		borderRadius: SEARCHBOX_CLRBTN_SIZE / 2,
		height: SEARCHBOX_CLRBTN_SIZE,
		justifyContent: 'center',
		marginHorizontal: 5,
		width: SEARCHBOX_CLRBTN_SIZE,
	},
	searchBoxClearBtnLabel: {
		color: '#FFFFFF',
		fontSize: 16,
		fontWeight: 'bold',
	}
});
