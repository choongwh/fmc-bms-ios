import React, { Component } from 'react';
import { Image, ScrollView, TouchableOpacity, BackHandler, Platform, StatusBar, TextInput, KeyboardAvoidingView,NetInfo } from 'react-native';
import { Container, Header, Content, View, Text, Button, CardItem , Item, Icon, Input, Left, Right} from 'native-base';
import Axios from 'axios';
import { connect } from 'react-redux';
import { AsyncStorage } from 'react-native';
import Ripple from 'react-native-material-ripple';
import { authSuccess, updateFbGoogleInfo } from '../../redux/actions/auth'
import Auth from '../../redux/reducers/auth';
import { Url } from '../../../config/api/credentials';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import { LoginButton, AccessToken, LoginManager, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import Toast from 'react-native-root-toast';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';

import Loader from '../../../App/views/unconnected/Loader';

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[statusbar.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

const styles = {
   wrapper: {
    flex: 1,
    paddingTop: 50,
    backgroundColor: '#fff'
  },

  outcontent:{
    flexDirection: "column",
    justifyContent:'space-between',
    flex: 1,
    backgroundColor: '#fff',
  },

  outForm:{
    paddingLeft: '5%',
    paddingRight: '5%',
    marginLeft: -5,
  },
  conbtnLogin: {paddingTop:10, paddingLeft: 20, paddingRight: 15},

  /////////////////scan///////////////////
  out_box: {
    height: 300, width: 300,  borderWidth: 0.5, borderColor: '#d6d7da', justifyContent:'center', alignItems:'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 40
  },

   cardImage_h:{
    height: 80,flex: 1
  },
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  footerMain:{borderTopWidth: 2, borderTopColor: '#ffffff', backgroundColor:'#fff', height: 45, shadowOpacity: 0, elevation: 0, shadowOffset: {height: 0, width: 0},},
  outbtnfooter:{flex: 1, justifyContent: 'flex-start', alignItems: 'center'},
  footerpd:{paddingRight: '5%', paddingLeft: '5%', height: 50, width: "100%", backgroundColor:'#fff'},
  footerpd1:{paddingRight:15, paddingLeft:15, width: '100%'},
  btnfooter:{flex: 1, height: 45, justifyContent: 'center'},
  btnfooterText:{color:"#fff", fontSize: 16, textAlign:'center'},
  textForm:{
    textAlign: 'center', fontSize: 22,  fontWeight: 'bold', padding: 15,
  },

/////////////////////////////////////////////////quickstyle/////////////////////////////////////////////////////////////
  color_blur:{color: '#2089dc'},

  layout_r_f1:{flexDirection: "row", flex: 1, justifyContent: 'space-between', },
  layout_c_f1:{flexDirection: "column", flex: 1, justifyContent: 'space-between', alignItems:'center'},

  w40:{width: 40}, w50:{width: 50}, w100:{width: 100}, w150:{width: 150}, w200:{width: 200}, w250:{width: 250}, w300:{width: 300},  w320:{width: 320},

  pdL5_100: {paddingLeft: '5%'},
  pdR5_100: {paddingRight: '5%'},

  mgB0:{marginBottom: 0}, mgB5:{marginBottom: 5}, mgB10:{marginBottom: 10}, mgB15:{marginBottom: 15}, mgB20:{marginBottom: 20},
  mgT0:{marginTop: 0}, mgT5:{marginTop: 5},mgT10:{marginTop: 10}, mgT15:{marginTop: 15}, mgT20:{marginTop: 20}, mgT25:{marginTop: 25}, mgT30:{marginTop: 30}, mgT35:{marginTop: 35}, mgT40:{marginTop: 40}, mgT45:{marginTop: 45}, mgT50:{marginTop: 50},


  pdT0:{paddingTop: 0}, pdT5:{paddingTop: 5}, pdT10:{paddingTop: 10}, pdT15:{paddingTop: 15}, pdT20:{paddingTop: 20},
  pdB0:{paddingBottom: 0}, pdB5:{paddingBottom: 5}, pdB10:{paddingBottom: 10}, pdB15:{paddingBottom: 15}, pdB20:{paddingBottom: 20},
  pdL0:{paddingLeft: 0}, pdL5:{paddingLeft: 5}, pdL10:{paddingLeft: 10}, pdL15:{paddingLeft: 15}, pdL20:{paddingLeft: 20},
  pdR0:{paddingRight: 0}, pdR5:{paddingRight: 5}, pdR10:{paddingRight: 10}, pdR15:{paddingRight: 15}, pdR20:{paddingRight: 20},

  fSize16:{fontSize: 16}, fSize18:{fontSize: 18}, fSize20:{fontSize: 20}, fSize24:{fontSize: 24},

  textCenter:{textAlign:'center'}, textLeft:{textAlign:'left'}, textRight:{textAlign:'right'},


  flex1:{flex: 1}, jusbetween:{justifyContent:'space-between'},
  flex_column:{flexDirection:'column'}, flex_row:{flexDirection: 'row'}, aligI_center:{alignItems:'center'},
};

const statusbar = {
  container: {
    height: 20,
    width: '100%'
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#000',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#000'
  },
}

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: null,
      error: null,
      disableSubmit: false,
      user_email: '',
      password: '',
      isLoading: false,
      connection_Status : "",
      counter : 0,
    };
  }

  showLoader = () => {
    this.setState({ isLoading: true });
  };

  componentDidMount() {
    this._configureGoogleSignIn();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

    NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );

    NetInfo.isConnected.fetch().done((isConnected) => {

      if(isConnected == true)
      {

        this.setState({connection_Status : "Online"})
      }
      else
      {

        this.setState({connection_Status : "Offline"})
      }

    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange

    );

  }

  handleBackButton() {
    return true;
  }

  /***
  Start Status connection
  ***/

    _handleConnectivityChange = (isConnected) => {

    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"});
        Toast.show('CONNECTION TO HOST RESUME.');
      }
      else
      {
         Toast.show('Connection to host lost.');
        this.setState({connection_Status : "Offline"})
      }
    };

    connectionLost(self){
        self.setState({ isLoading: false })
        self.setState({counter: self.state.counter+1 });
       if(self.state.counter>=3){
            Toast.show('CONNECTION LOST');
       }

     }

     noInternet(self){
       if(self.state.connection_Status=="Online"){
         return true
       }else{
         Toast.show('Connection to host lost.');
         return false;
       }
     }


  /***
 End Status connection
  ***/

  _configureGoogleSignIn() {
    GoogleSignin.configure({
      webClientId: Url.googewebclientid,
      offlineAccess: false,
    });
  }

  login() {
    if(this.state.user_email == '' || this.state.password == ''){
      Toast.show('Please fill input!');
      return;
    }
    this.newSession(this.state.user_email, this.state.password)
  }

  async linkbtnQR() {
    this.props.navigation.navigate('Qrcode',{qrcode: this.props.reduxState.Auth.workspaceqr.workspacename});
  };

  async linkbtnForgotPassword(){
    this.props.navigation.navigate('ForgotPassWord',{qrcode: this.props.reduxState.Auth.workspaceqr.workspacename});
  };

  async linkbtnResUser(){
    this.props.navigation.navigate('Workspace',{qrcode: this.props.reduxState.Auth.workspaceqr.workspacename});
  };

  // Sign_in User
  newSession(email, password) {
    this.setState({
      disableSubmit: true,
      isLoading: true
    });
    var self = this;
    if(!self.noInternet(self)){
       self.setState({
       disableSubmit: false,
       isLoading: false
       });
      return;
    }
    var credentials = {'mtd': 2, 'id': email, 'pass': password,'wid': this.props.reduxState.Auth.workspaceqr.qrcode};
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers, credentials)})
    .then(function (response) {
      self.setState({counter: 0 });
      switch(parseInt(response.data.status)){
        case 1:
          setTimeout(() => {
            self.setState({ isLoading: false });
          }, 2000);
          self.validateToken(response);

        break;
        case 2:
          self.setState({
            isLoading: false
          });
          Toast.show('Login fail!');
        break;
        case 3:
          self.setState({
            isLoading: false
          });
          Toast.show('Username or password incorrect!');
        break;
        case 4:
          self.setState({
            isLoading: false
          });
          Toast.show('Account suspended!');
        break;
        case 5:
          self.setState({
            isLoading: false
          });
          Toast.show('Account terminated!');
        break;
        case 6:
          self.setState({
            isLoading: false
          });
          Toast.show('Incorrect workspace!');
        break;
        default :
          self.setState({
            isLoading: false
          });
          Toast.show('Error!');
        break;
      }
    })

    .catch(function (error) {
      // Return error if credentials is invalids
     self.connectionLost(self);
      console.log(error);
    });
  } // End of newSessions()

  validateToken(res){
    console.log(res)
    // save this in variable to dont forget
    var self = this;
    if(!self.noInternet(self)){
      self.setState({
        isLoading: false
      });
      return;
    }
    var accessToken = {
      "access-token": res.data.token,
      "client": res.headers.client,
      "uid": res.headers.uid
    }
    var credentials = {'mtd': 4, 'token' : res.data.token};
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers, accessToken)})
    .then(function (response) {
      console.log(response.data)
      self.setState({counter: 0 });
      Toast.show('Login successful');
      self.props.authSuccess(response, accessToken);
      // Go to the App router (logged users)
      self.props.navigation.navigate('App');

    })
    .catch(function (error) {
      // if an error occurs during validation
      self.connectionLost(self);
      console.log("ERROR DURING TOKEN VALIDATION", error);
    });
  }

  loginSocial(type,email,idSocial){
    var self = this;
    switch(type){
      case 'google':
        var typ = 1;
        var email_or_userId = email;
      break;
      case 'facebook':
        var typ = 2;
        var email_or_userId = idSocial;
      break;
    }
    var credentials = {'mtd': 13, 'id': email_or_userId, 'typ': typ, 'wid': this.props.reduxState.Auth.workspaceqr.qrcode};
    Axios.post(Url.user, credentials, {headers: Object.assign(Url.headers, credentials)})
    .then(function (response) {
      self.setState({counter: 0 });
      switch(parseInt(response.data.status)){
        case 1:
          self.validateToken(response);
          self.props.updateFbGoogleInfo({type: type, email: email, id: idSocial});
        break;
        case 2:
          Toast.show('Login fail!');
        break;
        case 3:
          Toast.show('Account suspended!');
        break;
        case 4:
          Toast.show('Account terminated!');
        break;
        default :
          Toast.show('Error!');
        break;
      }
    })
    .catch(function (error) {
      // Return error if credentials is invalids
      console.log(error);
    });
  }


  _signInGoogle = async () => {
    try {
        await GoogleSignin.hasPlayServices();
        const userInfo = await GoogleSignin.signIn();
        this.loginSocial('google',userInfo.user.email,userInfo.user.id);
    } catch (error) {
        if (error.code === statusCodes.SIGN_IN_CANCELLED) {
          // sign in was cancelled
          alert('cancelled');
      } else if (error.code === statusCodes.IN_PROGRESS) {
          // operation in progress already
          alert('in progress');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
          alert('play services not available or outdated');
      } else {
        console.log(error);
          alert('Something went wrong'+ error.toString());
          this.setState({
            error,
        });
      }
    }
  };

  handleFacebookLogin () {
    var self = this;
    LoginManager.logInWithReadPermissions(["public_profile", "email"]).then(
      function(result) {
        if (result.isCancelled) {
          console.log("Login cancelled");
        } else {
          console.log(
            "Login success with permissions: " +
              result.grantedPermissions.toString()
          );
          let accessToken = result.accessToken;
          const responseInfoCallback = (error, result) => {
            if (error) {
              console.log(error)
            } else {
              console.log(result)
              self.loginSocial('facebook',result.email,result.id);
            }
          }
          const infoRequest = new GraphRequest('/me',{accessToken: accessToken,
              parameters: {
                fields: {
                  string: 'email,name,first_name,middle_name,last_name'
                }
              }
            },
            responseInfoCallback
          );

          // Start the graph request.
          new GraphRequestManager().addRequest(infoRequest).start();
        }
      },
      function(error) {
        console.log("Login fail with error: " + error);
      }
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Content styles={{backgroundColor:"#fff"}}>
          <View style={[styles.wrapper,{width:'100%'}]}>
            <View style={styles.outcontent}>
              <View style={{marginBottom: 20}}>
                <View style={styles.aligI_center}>
                  <CardItem style={{height: 200, width: '90%'}}>
                    <Image source ={require('../../image/logo-bms.png')} style={{ height:'100%', width: '100%', resizeMode: 'contain'}} />
                  </CardItem>
                </View>

                <View full block style={styles.mgT10}>
                  <Text style={[styles.fSize20, styles.textCenter, styles.color_blur]}>{this.props.reduxState.Auth.workspaceqr.workspacename}</Text>
                </View>
                <View style={styles.outForm}>
                  <View style={{paddingLeft: 20,paddingRight: 15, marginBottom: 20}}>

                    <Item iconLeft style={{paddingTop: 10, paddingBottom: 10}}>
                        <Icon style={{width:40}} name='account' type='MaterialCommunityIcons'/>
                        <Input placeholder="Username/Email" placeholderTextColor="#a7a7a7" autoCapitalize="none" onChangeText={(user_email) => this.setState({user_email})} value={this.state.user_email}/>
                      </Item>
                      <Item iconLeft style={{paddingTop: 10, paddingBottom: 10}}>
                        <Icon style={{width:40}} name='ios-lock' type='Ionicons'/>
                        <Input placeholder="Password" secureTextEntry={true} placeholderTextColor="#a7a7a7" autoCapitalize="none" onChangeText={(password) => this.setState({password})} value={this.state.password}/>
                      </Item>
                  </View>

                  <View block style={styles.conbtnLogin}>
                    <Button block info onPress={() => this.login()} disbaled={this.state.disableSubmit} style={{borderRadius: 0}}>
                      <Text style={styles.fSize16}>Login</Text>
                    </Button>

                    <View style={[styles.layout_r_f1, styles.pdT20, styles.pdB10]}>
                      <View style={styles.flex1}>
                        <View style={[styles.pdT10, styles.pdR5]}>
                          <Text style={styles.fSize18}>Or Login with</Text>
                        </View>
                      </View>
                      <View style={styles.layout_r_f1}>

                        <View iconLeft  style={[styles.pdR5, styles.flex1]}>
                          <Button block full onPress={this.handleFacebookLogin.bind(this)}>
                            <Icon name="logo-facebook" />
                          </Button>
                        </View>
                        <View iconRight style={[styles.pdL5, styles.flex1]}>
                          <GoogleSigninButton
                            button
                            style={{ width: '100%', height: 53, marginTop: -4.5 }}
                            size={GoogleSigninButton.Size.Icon}
                            color={GoogleSigninButton.Color.Light}
                            onPress={this._signInGoogle}
                          />
                        </View>
                      </View>
                    </View>

                    <View>
                      <CardItem style={styles.pdL0}>
                        <TouchableOpacity  onPress={() => this.linkbtnForgotPassword()}>
                          <Text style={[styles.fSize16, styles.color_blur]}>{'Forgot password ?'.toUpperCase()}</Text>
                        </TouchableOpacity>
                      </CardItem>
                      <CardItem style={styles.pdL0}>
                        <TouchableOpacity onPress={() => this.linkbtnQR()}>
                          <Text style={[styles.fSize16, styles.color_blur]}>{'Register to workspace'.toUpperCase()}</Text>
                        </TouchableOpacity>
                      </CardItem>
                      {/* <CardItem style={styles.pdL0}>
                        <TouchableOpacity onPress={() => this.linkbtnResUser()}>
                          <Text style={[styles.fSize16, styles.color_blur]}>{'Register user'.toUpperCase()}</Text>
                        </TouchableOpacity>
                      </CardItem> */}
                    </View>

                  </View>
                </View>
              </View>
            </View>
          </View>
        </Content>
        <Loader opacity={1} loading={this.state.isLoading}/>
      </Container>
    );
  }
}
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;
const mapStateToProps = (state) => ({
	reduxState:state
});
// Export the react component included in redux connect
export default connect(mapStateToProps, {
	Auth,
  authSuccess,
  updateFbGoogleInfo,
})(LoginForm);
