import { PARSE_EQUIPMENT_JSON, PARSE_EQUIP_IOP_VALUE } from '../actions/types';

const INITIAL_STATE = {
  facility_data: {},
  facilities: [],
  userlookups: [],
  deviceConfigs: [],
  iopValues: {}
};
const Facility = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PARSE_EQUIPMENT_JSON:
      const facilityJson = action.payload.json

      var objects = [];
      var userlookups = [];
      var devices = [];

      let count = facilityJson.length;
      for(var i=0; i<count; i++) {
        userlookups = userlookups.concat(facilityJson[i].userlookups);
        devices.push(facilityJson[i].configs);

        let objs = facilityJson[i].objects.map (obj => {
          obj.device_config=facilityJson[i].configs;
          return obj
        });
        // console.log(objs);
        objects = objects.concat(objs);
      }

      /// Sort by lookup type
      // objects = objects.sort((a, b) => {
      //   if (a.object_name < b.object_name) {
      //     return -1;
      //   } else if (a.object_name > b.object_name) {
      //     return 1;
      //   } else {
      //     return 0;
      //   }
      // });

      var userlookupDict = {};

      userlookups.map ( lookup => {
          const id = lookup._id;
          userlookupDict[id] = lookup;
        }
      );

      return Object.assign({}, state, {
        facility_data: facilityJson,
        facilities: objects,
        userlookups: userlookupDict,
        deviceConfigs: devices
      });
    case PARSE_EQUIP_IOP_VALUE:
      var iopValues = {}
      var iopValueJson = action.payload.json
      state.deviceConfigs.map (device => {
        let iops = iopValueJson[device.device_id];
        let objectIds = Object.keys(iops);
        objectIds.map (objectId => {
          let objectValues = iops[objectId];
          var values = {};
          objectValues.map (value => {
            let key = value.iops;
            var val = value.value;
            if (val == "") {
              val = 0;
            }
            values[key] = val;
          });
          iopValues[objectId] = values;
        });
      });

      var newFacilites = state.facilities.map ( facility => {
        var iopInfos = facility.iop_infor.map ( iopInfo => {
          let pointname = iopInfo.data_iop.pointname;
          let objectId = facility.objects_id;
          let value = iopValues[objectId][pointname];
          var info = iopInfo;
          return {...iopInfo, value: value, pointname: pointname};
        });
        return {...facility, iop_infor: iopInfos};
      });

      return Object.assign({}, state, {
        facilities: newFacilites,
        iopValues: iopValues
      });
    default:
      return state;
  }
}


export default Facility;
