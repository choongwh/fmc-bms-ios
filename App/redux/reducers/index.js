import { combineReducers } from 'redux';
import Navigation from './navigation';
import Auth from './auth';
import Facility from './facility'

export default combineReducers({
  Navigation,
  Auth,
  Facility,
});