import { ON_SUCCESS_LOGIN, SET_BY_STORAGE, UPDATE_ACCESS_TOKEN, SET_QRCODE, DETAIL_EVENT, SET_QRCODE_ROOM, UPDATE_INFO_FB_GG, RESET, GRANT_ACCESS} from '../actions/types';
import { REHYDRATE } from 'redux-persist/constants'

const INITIAL_STATE = {
  connected: false,
  token: {},
  user: {},
  workspaceqr: {qrcode:'', opencamera:false, workspacename : ''},
  qrcode_room: {location: [], qrcode:''},
  event_detail: {},
  fb_google: {},
  access_granted_event_ids: [],
};
const Auth = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ON_SUCCESS_LOGIN:
      accessToken = action.payload.accessToken["access-token"];

      return Object.assign({}, state, {
        connected: true,
        token: Object.assign({}, state.token, {
          "access-token": accessToken,
          client: action.payload.success_datas.headers.client,
          uid: action.payload.success_datas.headers.uid,
        }),
        user: action.payload.success_datas.data.data,
        });

    case UPDATE_ACCESS_TOKEN:
      return Object.assign({}, state, {
        token: Object.assign({}, state.token, {
          "access-token": action.payload.new_token,
          }),
        });
    case SET_QRCODE:
      return Object.assign({}, state, {
        workspaceqr: Object.assign({}, state.workspaceqr,{qrcode: action.qrcode, opencamera: action.opencamera, workspacename: action.workspacename}),
        }); 
    case SET_QRCODE_ROOM:
      return Object.assign({}, state, {
        qrcode_room: Object.assign({}, state.qrcode_room,{location: action.location, qrcode: action.qrcode}),
        }); 
    case DETAIL_EVENT:
      return Object.assign({}, state, {
        event_detail: action.event_detail,
        });
    case UPDATE_INFO_FB_GG:
      return Object.assign({}, state, {
        fb_google: action.fb_google,
        });
    // When open app set the last state
    case REHYDRATE:
      return {...state, ...action.payload.Auth, rehydrated: true}

    case SET_BY_STORAGE:
      return action.payload.lastState

    case RESET:
      return INITIAL_STATE;

    case GRANT_ACCESS:
        var grantedEventIds = state.access_granted_event_ids;
        if (!grantedEventIds.includes(action.granted_event_id)) {
          grantedEventIds.push(action.granted_event_id);
        }
        return Object.assign({}, state, {
          access_granted_event_ids: grantedEventIds
        });

    default:
      return state;
  }
}

export default Auth;