import { ON_OPEN_CAMERA } from '../actions/type';
import { REHYDRATE } from 'redux-persist/constants';

const initialState = true;
const OnCamera = (state = initialState, action) => {
  switch (action.type) {
    case ON_OPEN_CAMERA:
    return initialState = state;
    default:
      return state;
  }
}

export default OnCamera;