import { PARSE_EQUIPMENT_JSON, PARSE_EQUIP_IOP_VALUE } from "./types";

function parseEquipmentJson(json) {
	return {
	  type: PARSE_EQUIPMENT_JSON,
	  payload: {json: json},
	}
}

function parseEquipmentIopValue(object) {
	return {
	  type: PARSE_EQUIP_IOP_VALUE,
	  payload: {json: object},
	}
}

export { parseEquipmentJson, parseEquipmentIopValue }
