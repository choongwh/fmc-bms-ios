import { ON_OPEN_CAMERA } from './type';

export const onOpenCamera = () => ({type:ON_OPEN_CAMERA});