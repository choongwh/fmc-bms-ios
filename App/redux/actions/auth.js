import { ON_SUCCESS_LOGIN, SET_BY_STORAGE, UPDATE_ACCESS_TOKEN, SET_QRCODE, SET_QRCODE_ROOM, DETAIL_EVENT, UPDATE_INFO_FB_GG, RESET, GRANT_ACCESS } from "./types";

function authSuccess(success_datas, accessToken) {
	return {
	  type: ON_SUCCESS_LOGIN,
	  payload: {success_datas: success_datas, accessToken: accessToken, status: 200},
	}
}

function updateAccessToken(new_token) {
	return {
	  type: UPDATE_ACCESS_TOKEN,
	  payload: {new_token},
	}
}

function updateQrCode(value, opencamera, workspacename) {
	return {
	  type: SET_QRCODE,
	  qrcode: value,
	  opencamera: opencamera,
	  workspacename: workspacename,
	}
}

function updateQrCodeRoom(location, value) {
	return {
	  type: SET_QRCODE_ROOM,
	  location: location,
	  qrcode: value,
	}
}

function updateDetailEvent(detail){
	return {
		type: DETAIL_EVENT,
		event_detail: detail,
	}
}

function updateFbGoogleInfo(fb_google){
	return {
		type: UPDATE_INFO_FB_GG,
		fb_google: fb_google,
	}
}

function resetAuth(){
	return {
		type: RESET,
	}
}

function grantAccess(event_id) {
	return {
		type: GRANT_ACCESS,
		granted_event_id: event_id
	}
}

export { authSuccess, updateAccessToken, updateQrCode, updateQrCodeRoom, updateDetailEvent, updateFbGoogleInfo, resetAuth, grantAccess }