export default class Memoizer {

  constructor(dependencies = []) {
    this.dependencies = JSON.stringify(dependencies);
    this.lastResult = null;
  }

  do(callback, dependencies = []) {
    if (JSON.stringify(dependencies) !== this.dependencies) {
      this.dependencies = JSON.stringify(dependencies);
      this.lastResult = callback();
    }
    return this.lastResult;
  }

}

// // Example
// const memo = new Memoizer([{}]);
// function foo(numArray = []) {
//   return memo.do(() => {
//     let sum = 0;
//     for (let i = 0; i < numArray.length; i++) {
//       sum += numArray[i];
//     }
//     return sum;
//   }, [numArray])
// }
