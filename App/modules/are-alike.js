/**
 * @description Compare and tell whether 2 or above strings are alike.
 * @param {string} stringArray The array of strings to compare.
 * @returns {boolean}
 */
function areAlike(stringArray = []) {
  if (stringArray.length < 1) {
    console.error(`Cannot perform comparison. Expected 2 (or above) strings in the array for comparison but got ${stringArray.length}.`);
    return false;
  }
  let previousCleanedString = '', currentCleanedString = '', allAreAlike = true;
  try { previousCleanedString = getCleanedString(stringArray[0]) } catch (e) { }
  for (let i = 1; i < stringArray.length && allAreAlike === true; i++) {
    currentCleanedString = getCleanedString(stringArray[i]);
    allAreAlike = (
      previousCleanedString.includes(currentCleanedString)
      ||
      currentCleanedString.includes(previousCleanedString)
    );
  }
  return allAreAlike;
}

const getCleanedString = str => str.replace(/\s/g, '').toLowerCase();

export default areAlike;
