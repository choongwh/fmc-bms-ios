let store = {}
export default {
  set: (data) => { store = { ...store, ...data } },
  get: (key) => store[key]
}
