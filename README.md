# FMC BMS

## Running on simulator
* React Native version is too old, however, updating RN version without considering the packages used might cause unwanted side-effects because some of them do not support newer versions of RN yet.
* Because of the old version of RN used, you will probably get an error mentioning that no simulators are found.
* To fix this, run `node scripts/patchSimulatorMatcher.js`;

## Extra
* Use `npm run reset` as an alternative to `react-native start -- -reset-cache`
* Use `npm run ios` as an alternative to `react-native run-ios`
* These commands are not available in other RN projects unless you specify them, refer to the `scripts` section package.json.
