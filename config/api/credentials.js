// const prefix = value => `http://192.168.1.10:3000/api/bms/${value}`;
// const prefix = value => `https://famiie.io:8082/api/${value}`;
const prefix = value => `https://fmcc.io:8082/apis/${value}`;

export const Url = {
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'X-Gravitee-Api-Key': 'fc349c77-d7f7-4d9e-8d76-ab79b6e595ed',
  },

  user: prefix('user'),
  event: prefix('event'),
  workspace: prefix('workspace'),
  locations: prefix('thing'),

  //bms:1095739769941-a84erfv04d3kvsgrvf267sg68mi2p751.apps.googleusercontent.com
  //bmsfamiie:787160340563-hmos18rocot8c6pmddvsdgsqkj4umm18.apps.googleusercontent.com
  googewebclientid: '787160340563-hmos18rocot8c6pmddvsdgsqkj4umm18.apps.googleusercontent.com',
  // status:1
}
