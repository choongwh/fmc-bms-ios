import { createDrawerNavigator, DrawerNavigator } from 'react-navigation';
import SideMenu from '../../App/components/menus/SideMenu';
import { EventStack } from './EventStack';
import { MBookStack } from './MBookStack';
import { LocationStack } from './LocationStack';
// import MyBookingScreen from '../../App/views/unconnected/MyBookingScreen';
import ProFileScreen from '../../App/views/unconnected/ProFileScreen';

// Router
export const AppStack = createDrawerNavigator(
  {
    Locations: {
      screen: LocationStack,
    },
    Calendars: {
      screen: EventStack,
    },
    Mybooking: {
      screen: MBookStack,
    },
    // Mybooking: {
    //   screen: MyBookingScreen,
    // },
    Profile: {
      screen: ProFileScreen,
    },
  },
  {
    contentComponent: SideMenu,
    drawerWidth: 300,
  }
);