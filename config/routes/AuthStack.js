import { createStackNavigator } from 'react-navigation';
// Vues
import LoginScreen from '../../App/views/unconnected/LoginScreen';
import ScanQRCodeScreen from '../../App/views/unconnected/ScanQRCodeScreen';
import ScanQRCodeScreenCamera from '../../App/views/unconnected/ScanQRCodeScreenCamera';
import LoaderScreen from '../../App/views/unconnected/LoaderScreen';
// import RegisterUserScreen from '../../App/views/unconnected/RegisterUserScreen';
// import FormRegisterUserScreen from '../../App/views/unconnected/FormRegisterUserScreen';
import HomeScreen from '../../App/views/unconnected/HomeScreen';
import BookNewEvenScreen from '../../App/views/unconnected/BookNewEvenScreen';
import MyBookingScreen from '../../App/views/unconnected/MyBookingScreen';
import ForgotPassWordScreen from '../../App/views/unconnected/ForgotPassWordScreen';
import Loader from '../../App/views/unconnected/Loader';
//import demopustnotifile from '../../App/views/unconnected/demopustnotifile';

export const AuthStack = createStackNavigator(
{
    Login: {
      screen: LoginScreen,
    },
    Qrcode: {
      screen: ScanQRCodeScreen,
    },

    QrcodeScan: {
      screen: ScanQRCodeScreenCamera,
    },

    Loader: {
      screen: LoaderScreen,
    },

    // Register: {
    //   screen: RegisterUserScreen,
    // },

    // Workspace: {
    //   screen: FormRegisterUserScreen,
    // },

    Home: {
      screen: HomeScreen,
    },

    BookNewEven: {
      screen: BookNewEvenScreen,
    },

    MyBooking: {
      screen: MyBookingScreen,
    },
    ForgotPassWord: {
      screen: ForgotPassWordScreen,
    },
    Loader: {
      screen: Loader,
    }
    // ,
    // demopust: {
    //   screen: demopustnotifile,
    // }
  },
  // Default route
  {
    initialRouteName: 'Login',
    navigationOptions: {
      swipeEnabled: false
    }
  }
);

AuthStack.navigationOptions = ({navigation}) => ({
  swipeEnabled: false,
  drawerLockMode: 'locked-closed'
});