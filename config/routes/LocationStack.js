import { createStackNavigator } from 'react-navigation';
import LocationScreen from '../../App/views/locations/LocationScreen';
import FacilityControlScreen from '../../App/views/facility/FacilityControlScreen';

export const LocationStack = createStackNavigator(
    {
        Index: {
            screen: LocationScreen,
        },
        Facility: {
            screen: FacilityControlScreen,
        }
    }, {
        initialRouteName: 'Index',
    }
);
