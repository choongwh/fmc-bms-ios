import { createStackNavigator } from 'react-navigation';
// Vues

import MbookingIndexScreen from '../../App/views/mybooking/MbookingIndexScreen';
// import BookShow from '../../App/views/appointments/Show';
import MBookNew from '../../App/views/mybooking/New';
import MBookShow from '../../App/views/mybooking/Show';
import MBookEdit from '../../App/views/mybooking/Edit';
import MBookAttendee from '../../App/views/mybooking/BookAttendee';
// import BookEdit from '../../App/views/appointments/Edit';

// Router
export const MBookStack = createStackNavigator(
  {
    Index: {
      screen: MbookingIndexScreen,
    },
    MShow: {
      screen: MBookShow,
    },
    MNew: {
      screen: MBookNew,
    },
    MEdit: {
      screen: MBookEdit,
    },
    MAttendee: {
      screen: MBookAttendee,
    },
  },
  // Default route
  {
    initialRouteName: 'Index',
  }
);