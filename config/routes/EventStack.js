import { createStackNavigator } from 'react-navigation';
// Vues
import BooksIndexScreen from '../../App/views/books/BooksIndexScreen';
import BookShow from '../../App/views/books/Show';
import BookAttendee from '../../App/views/books/BookAttendee';
import BookNew from '../../App/views/books/New';
import BookEdit from '../../App/views/books/Edit';
import FacilityControlScreen from '../../App/views/facility/FacilityControlScreen'
import AccessScreen from '../../App/views/facility/AccessScreen'
// Router
export const EventStack = createStackNavigator(
  {
    Index: {
      screen: BooksIndexScreen,
    },
    Show: {
      screen: BookShow,
    },
    Attendee: {
      screen: BookAttendee,
    },
    New: {
      screen: BookNew,
    },
    Edit: {
      screen: BookEdit,
    },
    Access: {
      screen: AccessScreen,
    },
    Facility: {
      screen: FacilityControlScreen,
    }
  },
  // Default route
  {
    initialRouteName: 'Index',
  }
);