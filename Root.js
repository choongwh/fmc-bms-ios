import React from 'react';
import { StyleSheet, Text, View, Button, StatusBar, ActivityIndicator, Image } from 'react-native';
import { CardItem , Icon, Left, Right } from 'native-base';
import Router from './config/routes/Router';

export default class Root extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }
  // formbulder requires some font, there may be forms anywhere in the app so as well place the call here
  async componentDidMount() {
     setTimeout(() => {
      this.setState({ rehydrated: true });

    }, 2000);
    setTimeout(() => {
      this.setState({ loading: false });

    }, 2000);

  }
  render() {
    if (this.state.loading) {
      return (
        <View style={styles.containerloader}>
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',flexDirection: 'column',}}>
            <View >
              <CardItem style={{height: 200, width: '90%'}}>
                <Image source ={require('./App/image/logo-bms.png')} style={{ height:'100%', width: '100%', resizeMode:'contain'}} />
              </CardItem>
            </View>
            <View style={{}}>
             <ActivityIndicator/>
              <StatusBar barStyle="default" />
            </View>
          </View>

        </View>
      );
    }
    return (
      <View style={styles.container}>
        <StatusBar hidden={false} />
        <Router />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',


  },
  containerloader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',

  },
});
