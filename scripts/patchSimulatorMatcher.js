const fs = require('fs');

console.log('\nSearching for patch target...');
const path = './node_modules/react-native/local-cli/runIOS/findMatchingSimulator.js';
const rawFileContent = fs.readFileSync(path, 'utf-8');
const replace = '!version.startsWith(\'com.apple.CoreSimulator.SimRuntime.iOS\')';

if (rawFileContent.includes(replace)) {
  console.log('Patch has been applied previously. No further steps required.\n');
} else {
  console.log('Attempting to applying patch contents...');
  const search = '!version.startsWith(\'iOS\')';
  const newFileContent = rawFileContent.replace(search, replace);
  fs.writeFileSync(path, newFileContent, { encoding: 'utf-8' });
  console.log('Patch complete. You may now debug the app from the terminal by running `react-native run-ios`.\n')
}
